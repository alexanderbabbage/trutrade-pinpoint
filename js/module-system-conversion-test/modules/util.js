/**
 * Tests if a given number is a number or if it can be type cast
 * as a valid number (eg. '300').
 *
 * @method isNum
 * @public
 *
 * @param {*} val - A value being tested to determine if it is a number.
 * @return {boolean} - True if n is a number, false if it is not.
 */
const isNum = val => !isNaN(parseFloat(val)) && !isNaN(val * 1);

/**
 * Adds commas to a number to separate values.
 *
 * @method addCommas
 * @public
 *
 * @param {number|string} num - A number value.
 * @return {string} - The given number with commas added to separate values.
 */
const addCommas = num => (num + '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');

/**
 * Takes a given number or string representation of a number and returns
 * that number fixed to a single decimal place. This is a more performant,
 * but also more limited implementation of the native toFixed(1) method.
 *
 * @method fixToOne
 * @public
 *
 * @param {number|string} num - A number value.
 * @return {string} The given value, num, fixed to a single decimal place.
 */
const fixToOne = num =>
  (num = Math.round(num * 10) / 10) + (num === (num|0) ? '.0' : '');

export { isNum, addCommas, fixToOne };
