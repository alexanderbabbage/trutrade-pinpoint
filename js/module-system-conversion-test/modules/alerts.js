/**
 * Prints a default error message which can be replaced with a custom
 * message by passing it as an argument.
 *
 * @method errMsg
 * @public
 *
 * @param {string} [msg] - If a message other than the default error message
 * is needed, the custom message can be passed as an argument.
 */
const errMsg = (msg) => {
  msg = msg ||
    'Your request has ended in an error. Please try again. If this ' +
    'problem persists, contact your Alexander Babbage representative.';

  swal({
    title: 'Error Encountered!',
    text: msg,
    icon: 'error',
    button: 'OK'
  });
};

/**
 * This function calls the Sweet Alert API to initiate an Access
 * Restriction popup modal.
 *
 * @method accessCheckRefresh
 * @public
 */
const accessCheckRefresh = () => {
  swal({
    title: 'Access Restriction',
    text: 'You do not have access to this module. Please contact an ' +
          'Alexander Babbage representative to receive access.',
    icon: 'error',
    button: 'OK'
  });
};

export { errMsg, accessCheckRefresh };
