export default function () {
  let _compVisible = true,
      _benchVisible = false;

  return {
    /**
     * Returns the current state of benchmark visibility. If the a
     * competitor is being shown returns true. If 'No Benchmark' is selected
     * returns false.
     *
     * @method getBenchState
     * @public
     *
     * @return {boolean} The current benchmark visibility state. True if
     * a benchmark is selected, false if it is not.
     */
    getBenchState() {
      return _benchVisible;
    },

    /**
     * Returns the current state of competitor visibility. If the a
     * competitor is being shown, returns true. If no competitor is being
     * shown, like with the 'Focus on One Competitor' option, state()
     * returns false.
     *
     * @method getCompState
     * @public
     *
     * @return {boolean} The current competitor visibility state. True if
     * a competitor is currently visible, false if it is not.
     */
    getCompState() {
      return _compVisible;
    },

    /**
     * Gets the current layout type as a string, generated based on the
     * current state of benchmark and competitor visibility.
     *
     * @type {string}
     * @memberof AB.modules.state
     * @public
     *
     * @return {string} The current UI layout's name.
     */
    getlayout() {
      return (
        _compVisible
          ? (_benchVisible ? 'BenchAndComp' : 'NoBench')
          : (_benchVisible ? 'NoComp' : 'NoBenchNoComp')
      );
    },

    /**
     * Gets the number of table columns that are currently hidden in the
     * tables with Competitor and Benchmark values on the Overview page.
     *
     * @type {number}
     * @memberof AB.modules.state
     * @public
     *
     * @return {number} The current number of hidden columns in the tables.
     */
    getNumColHidden() {
      return (
        _compVisible
          ? (_benchVisible ? 0 : 1)
          : (_benchVisible ? 1 : 2)
      );
    },

    /**
     * Set benchmark visibility to a hidden (false) state.
     *
     * @method hideBench
     * @public
     */
    hideBench() {
      _benchVisible = false;
    },

    /**
     * Set competitor visibility to a hidden (false) state.
     *
     * @method hideComp
     * @public
     */
    hideComp() {
      _compVisible = false;
    },

    /**
     * Set benchmark visibility to a shown (true) state.
     *
     * @method showBench
     * @public
     */
    showBench() {
      _benchVisible = true;
    },

    /**
     * Set competitor visibility to a shown (true) state.
     *
     * @method showComp
     * @public
     */
    showComp() {
      _compVisible = true;
    },

    /**
     * Toggles the benchmark visibility state. If it is currently in a
     * shown (true) state, toggle() will change it to hidden (false). If
     * benchmark visibility is in a hidden (false) state, toggle() will
     * change it to a shown (true) state. The new state is then returned.
     *
     * @method toggleBench
     * @public
     *
     * @return {boolean} The new, post-toggle benchmark visibility state.
     */
    toggleBench() {
      return (_benchVisible = !_benchVisible);
    },

    /**
     * Toggles the competitor visibility state. If it is currently in a
     * shown (true) state, toggle() will change it to hidden (false). If
     * competitor visibility is in a hidden (false) state, toggle() will
     * change it to a shown (true) state. The new state is then returned.
     *
     * @method toggleComp
     * @public
     *
     * @return {boolean} The new, post-toggle competitor visibility state.
     */
    toggleComp() {
      return (_compVisible = !_compVisible);
    }
  };
}
