import { state } from './state';
import { util } from './util';

    /**
     * Replaces the values of a column with corresponding data animations.
     *
     * @function animCol
     * @private
     *
     * @param {number} [colIndex=0] - The index number of the column whose
     * values are to be replaced with chart elements. The index starts at 0 for
     * the Actual vs Potential Visitor tile's table, because there is only that
     * one column being animated, but all other table's columns start at 1.
     * @param {number} [tileType=0] - A number that relates to a given tile. If
     * no value is passed then 0 is the default. Values are as follows: tileType
     * 0 = Key Performance Indicator tile; tileType 1 = Visitor Origin Tile;
     * tileType 2 = Visitor Penetration Tile; tileType 3 = Actual vs Potential
     * Visitors Tile.
     * @param {boolean} [forceNoAnim=false] - A truthy value passed here will
     * skip the tile visibility check before chart animation and force the
     * charts to appear in their final state. Use this on the initial page load
     * or any other time that forcing charts to appear without animating is
     * desired.
     */
    const animCol = (colIndex, tileType, forceNoAnim) => {
      /**
       * This function will create individual radial chart data visualizations.
       *
       * @function createRad
       * @private
       *
       * @param {HTMLElement} wrapPointer - Pointer associated with the div
       * wrapper where the radial will go.
       * @param {string} cellVal - Value in the the cell that is being replaced
       * with the radial chart.
       * @param {number} [radSize=0] - Size of the radial created. Passable
       * values are: 0 = The default radial size of 85px. 1 = The larger size of
       * 120px. The sizes for these radials only effect the font size and text
       * indent in the function, the radial's actual dimensions are set by their
       * css wrapper.
       * @param {boolean} [tileOnScreen] - True if the tile containing the
       * radial chart is on screen at the time the function is called. If this
       * is true the chart will animate, if it is false the chart will simply be
       * set to its finished position without animating.
       */
      const createRad = (wrapPointer, cellVal, radSize, tileOnScreen) => {
        radSize = radSize || 0;

        // Set the point the chart will animate to. Must be between 0 and 1.
        const progressPortion = cellVal / 100,
              colorBrand = '#2a3c68', // The platform's primary brand color
              radChart = new ProgressBar.Circle(wrapPointer, {
                color: colorBrand, // Set the font color for the radial's value
                strokeWidth: 12, // To prevent clipping, match with 'to' width
                trailWidth: 12, // Trail the radial animates along
                trailColor: 'rgba(79,162,217,.14)',
                easing: 'easeInOut',
                duration: 1250, // Time to reach 100% chart fullness in ms
                text: { autoStyleContainer: false },
                // Changes loading bar color & width as it progresses. The radial
                // will only reach the stated 'to' color and width at 100% mark
                from: { // Starting color and width
                  color: '#39518c',
                  width: 8
                },
                to: { // Ending color and width
                  color: colorBrand,
                  width: 12 // To prevent clipping, match with strokeWidth
                },
                // Now animate the path of the radial and the text inside it
                step: (state, circle) => {
                  const path = circle.path;

                  path.setAttribute('stroke', state.color);
                  path.setAttribute('stroke-width', state.width);

                  circle.setText(Math.round(circle.value() * 100) + '%');
                }
              }),
              radTxtStyle = radChart.text.style;

        if (radSize) {
          radTxtStyle.fontSize = '30px';
          radTxtStyle.textIndent = '.8ch';
        } else {
          radTxtStyle.fontSize = '20px';
          radTxtStyle.textIndent = '.4ch';
        }

        radTxtStyle.fontWeight = '700';

        (tileOnScreen
          ? radChart.animate(progressPortion)
          : radChart.set(progressPortion)
        );
      };

      /**
       * This function will create individual bar graph data visualizations.
       *
       * @function createBar
       * @private
       *
       * @param {HTMLElement} wrapPointer - Pointer associated with the div
       * wrapper where the bar will go.
       * @param {string} cellVal - Value in the the cell being that is being
       * replaced with the bar chart.
       * @param {number} barType - Type of bar being created. Passable values
       * are: 0 = horizontal bar based on an index that changes colors based on
       * the value. 1 = horizontal bar based on a percentage. 2 = vertical bar
       * based on minutes. 3 = vertical bar based on population with the text
       * outside of the bar.
       * @param {boolean} [tileOnScreen] - True if the tile containing the
       * bar chart is on screen at the time the function is called. If this
       * is true the chart will animate, if it is false the chart will simply be
       * set to its finished position without animating.
       */
      const createBar = (wrapPointer, cellVal, barType, tileOnScreen) => {
        // Color variables are for convenience - removed by minification
        const colorBrand = '#2a3c68', // The platform's primary brand color
              colorGreen = '#6dc05d', // Type 0 bars exceeding 100 are green
              colorGray = '#524b48', // Type 0 bars that equal 100 are gray
              colorRed = '#e61e25', // Type 0 bars less than 100 are red
              colorGreenLight = '#81c973', // Type 0 green bars begin this shade
              colorGrayLight = '#625a56', // Type 0 gray bars begin this shade
              colorRedLight = '#e93a40', // Type 0 red bars begin this shade
              // Avoid implicit type conversions, save cellVal as a number
              cellValNum = +cellVal;

        let endText    = '',         // The label text for the value
            posType    = 'absolute', // The position type of the bar text
            leftPos    = '-30%',     // Left position value of the bar text
            topPos     = '41%',      // Top position value of the bar text
            textSize   = '18px',     // Size of the bar's value text
            textWeight = '700',
            textWidth  = '100%',     // Width of box containing the value text
            textColor  = '#414042',  // Color of the bar's value text
            endColor   = colorBrand, // Color the bar ends as
            startColor = '#39518c',  // Color the bar begins as
            progressMax = 100; // The max value for the charts

        if (!barType || barType === 1) {
          posType = 'relative';
          textSize = '1em';
          textWeight = '400';
          textWidth = '60px';

          (!barType
            ? (
              ((progressMax = 300),
              (leftPos = '125px'),
              (topPos = '-20px')) &&
              (cellValNum > 100
                ? (
                  (endColor = colorGreen),
                  (startColor = colorGreenLight)
                )
                : (cellValNum < 100
                  ? (
                    (endColor = colorRed),
                    (startColor = colorRedLight)
                  )
                  : (
                    (endColor = colorGray),
                    (startColor = colorGrayLight)
                  )
                )
              )
            )
            : (
              (endText = '%'),
              (leftPos = '5em'),
              (topPos = '-25px')
            )
          );
        } else if (barType === 2) {
          progressMax = 120;
          endText = ' mins';

          (cellValNum > 30
            ? (
              (textColor = '#fff'),
              (textWeight = '400')
            )
            : (textColor = (cellValNum > 16 ? '#d4d2e4' : colorBrand))
          );
        } else {
          progressMax = 4000000;
          leftPos = '-150px'; // Really the bottom value due to chart rotation
          topPos = '25%'; // Really the left value due to chart rotation
          textSize = '22px';
          textWidth = '8rem';
        }

        const fixToOne = util.fixToOne,
              addCommas = util.addCommas,
              // Set the point the chart will animate to. Must be between 0 and 1
              progressPortion = (cellValNum >= progressMax
                ? 1
                : cellValNum / progressMax
              ),
              exceedsLimit = cellValNum > progressMax,
              barChart = new ProgressBar.Line(wrapPointer, {
                color: endColor, // Sets the color that the bar will be
                trailColor: 'rgba(79,162,217,.14)',
                easing: 'easeInOut',
                duration: 1250, // Time to reach 100% chart fullness in ms
                svgStyle: {
                  width: '100%',
                  height: '100%'
                },
                text: {
                  style: {
                    color: textColor,
                    fontSize: textSize,
                    fontWeight: textWeight,
                    left: leftPos,
                    position: posType,
                    textAlign: 'center',
                    top: topPos,
                    width: textWidth
                  },
                  autoStyleContainer: false
                },
                from: { color: startColor },
                to: { color: endColor }, // Sets the color of the bar at 100%

                // Now to animate the path of the bar and the text inside it
                step: (state, chart) => {
                  const path = chart.path;

                  // Value of the bar as it animates towards cellVal
                  let curBarVal;

                  path.setAttribute('stroke', state.color);

                  if (!barType || barType === 1) {
                    curBarVal = Math.round(chart.value() * progressMax);

                    !barType && exceedsLimit && curBarVal === progressMax &&
                      (curBarVal = addCommas(cellValNum|0));
                  } else if (barType === 2) {
                    curBarVal = fixToOne(chart.value() * progressMax);

                    exceedsLimit && +curBarVal === progressMax &&
                      (curBarVal = fixToOne(cellValNum));
                  } else {
                    const curBarValNoCommas = (chart.value() * progressMax)|0;

                    curBarVal = addCommas(
                      (exceedsLimit && curBarValNoCommas === progressMax
                        ? cellVal
                        : curBarValNoCommas
                      )
                    );
                  }
                  chart.setText(curBarVal + endText);
                }
              }),
              barTxtStyle = barChart.text.style;

        if (barType > 1) {
          barTxtStyle.transform = (barType === 3
            ? 'translate(50%, 50%) rotate(90deg)'
            : 'rotate(90deg)'
          );

          barType === 2 && cellValNum <= 31 && cellValNum > 16 &&
            (barTxtStyle.textShadow = '#000 1px 1px');
        }

        (tileOnScreen
          ? barChart.animate(progressPortion)
          : barChart.set(progressPortion)
        );

        // Apply on-hover tooltips to the horizontal bar charts
        if (!barType) {
          if (cellValNum > 100) {
            barChart._container.title =
              'Green colored bars indicate that the value is greater than 100';

            exceedsLimit &&
              setTimeout(() => {
                barTxtStyle.fontWeight = '700';
                barChart.text.title =
                  'Bold values are a visual indication that the 300 point ' +
                  'scale of the chart has been been exceeded.';
              }, 1000);
          } else if (cellValNum === 100) {
            barChart._container.title =
              'Gray colored bars indicate that the value is equal to 100';
          } else {
            barChart._container.title =
              'Red colored bars indicate that the value is below 100';
          }
        } else if (barType === 1) {
          barChart._container.title = (wrapPointer.id.slice(-1) === '4'
            ? 'Millennials - '
            : (wrapPointer.id.slice(-1) === '5'
              ? 'Generation X - '
              : 'Baby Boomers - '
            )
          ) + cellVal + '%';
        }
      };

      /**
       * Creates a 'Data currently unavailable' message and appends it to a
       * given table cell.
       *
       * @function insertUnavailableMsg
       * @private
       *
       * @param {HTMLElement} cell - Pointer to the cell where the message goes.
       * @param {?string} [msgWidth] - Width of the message's wrapper.
       */
      const insertUnavailableMsg = (cell, msgWidth) => {
        let msgClass = 'unavail-msg',
            style = '';

        (msgWidth
          ? style = ' style="width:' + msgWidth + '"'
          : msgClass += ' unavail-msg--sm'
        );

        cell.innerHTML =
          '<div class="' + msgClass + '"' + style +
          '>Data currently unavailable</div>';
      };

      // #animCol logic starts here

      colIndex = colIndex || 0;
      tileType = tileType || 0;
      forceNoAnim = forceNoAnim || false;

      const d = doc,
            numVals = (!tileType ? 7 : (tileType === 3 ? 23 : 2)),
            get = id => d.getElementById(id),
            isOnScreen = (element) => {
              const w = win,
                    elBounds = element.getBoundingClientRect(),
                    scrollTop = w.pageYOffset,
                    elTop = elBounds.y + scrollTop;

              return (
                elTop < (w.innerHeight + scrollTop) &&
                elTop > (scrollTop - elBounds.height)
              );
            },
            isVisible = (forceNoAnim
              ? false
              : (isOnScreen(get(
                !tileType
                  ? 'kpi_tile'
                  : (tileType === 3
                    ? 'avpv_tile'
                    : (tileType === 2
                      ? 'vp_tile'
                      : 'vo_tile'
                    )
                  )
              )))
            );

      // curCell is the current cell number in the column at that moment in the
      // loop, it starts with the bottom of the table and counts down towards 1
      // at the top
      for (
        let curCell = numVals + 1,
            chartWrapId,
            chartWrapPointer,
            cellId,
            cell,
            cellVal;
        --curCell;
      ) {
        // Skip Avg Dwell time in the MSA colummn of KPI (it is always n/a)
        if (!tileType && curCell === 1 && colIndex === 1) continue;

        cellId = 'col' + colIndex + '_val' + curCell;
        chartWrapId = 'chartwrap_col' + colIndex + '_val' + curCell;

        if (tileType === 1) {
          cellId = 'vo_' + cellId,
          chartWrapId = 'vo_' + chartWrapId;
        } else if (tileType === 2) {
          cellId = 'vp_' + cellId;
          chartWrapId = 'vp_' + chartWrapId;
        }

        cell = get(cellId);

        if (!cell) break;

        cellVal = cell.innerHTML.trim();

        // Input scrubbing
        // Break if cell already has a chart (the resize event can trigger this)
        if (cellVal[0] === '<') break;
        if (cellVal.substring(0, 3) === 'n/a') continue;
        if (!util.isNum(cellVal)) {
          insertUnavailableMsg(
            cell,
            (!tileType
              ? (curCell > 3 && curCell < 7 ? null : '50%') // KPI tile
              : (numVals === 2 ? '120px' : null)
            )
          );
          continue;
        }

        // End of input scrubbing - At this point, it is ensured the cell
        // contains no child nodes except for text that can be parsed into a
        // number, with no spaces at the beginning or end

        // Handle logic for the various tile types and call createBar/createRad
        if (!tileType) { // This targets the KPI tile
          if (curCell > 3 && curCell !== 7) {
            // This targets the horizontal % bar charts
            cell.innerHTML =
              '<div class="h-bar-container">' +
              '<div id="' + chartWrapId + '" class="h-bar-wrap"></div></div>';

            (chartWrapPointer = get(chartWrapId)) &&
              setTimeout(createBar, 99, chartWrapPointer, cellVal, 1, isVisible);
            continue;
          }
          if (curCell !== 1) {
            // This targets the radials
            cell.innerHTML =
              '<div class="chart-container">' +
              '<div id="' + chartWrapId + '" class="rad-wrap"></div></div>';

            (chartWrapPointer = get(chartWrapId)) &&
              setTimeout(createRad, 99, chartWrapPointer, cellVal, 0, isVisible);
          } else {
            // Targets the Avg Dwell Time vertical bar charts
            cell.innerHTML =
              '<div class="chart-container">' +
              '<div id="' + chartWrapId + '" class="bar-wrap"></div></div>';

            (chartWrapPointer = get(chartWrapId)) &&
              setTimeout(createBar, 99, chartWrapPointer, cellVal, 2, isVisible);
          }
        } else if (tileType === 3) { // Targets the AvPV tile
          cell.innerHTML =
            '<div class="h-bar-mc-container">' +
            '<div id="' + chartWrapId + '" class="h-bar-mc-wrap"></div></div>';

          (chartWrapPointer = get(chartWrapId)) &&
            setTimeout(createBar, 99, chartWrapPointer, cellVal, 0, isVisible);
        } else { // This targets the VO and VP tiles (tileType 1 and 2)
          if (curCell === 1) {
            cell.innerHTML =
              '<div class="chart-container">' +
              '<div id="' + chartWrapId + '" class="bar-wrap"></div></div>';

            (chartWrapPointer = get(chartWrapId)) &&
              setTimeout(createBar, 99, chartWrapPointer, cellVal, 3, isVisible);
          } else {
            cell.innerHTML =
              '<div class="chart-container">' +
              '<div id="' + chartWrapId + '" class="rad-wrap-lg"></div></div>';

            (chartWrapPointer = get(chartWrapId)) &&
              setTimeout(createRad, 99, chartWrapPointer, cellVal, 1, isVisible);
          }
        }
      } // End of the for loop going through all the column's values
    };

    /**
     * Animates the data in the Key Performance Indicator table, cycling through
     * the columns from the last to the first and swapping out values for data
     * visualizations. Skips column 3 as that is the Index column that has no
     * charts.
     *
     * @function animKpi
     * @private
     *
     * @param {boolean} [isCompShown=true] - True if the Competitor column is
     * visible, false if it is hidden.
     * @param {boolean} [forceNoAnim=false] - A truthy value passed here will
     * skip the tile visibility check before chart animation and force the
     * charts to appear in their final state. Use this on the initial page load
     * or any other time that forcing charts to appear without animating is
     * desired.
     */
    const animKpi = (isCompShown, forceNoAnim) => {
      forceNoAnim = forceNoAnim || false;

      for (
        let colIndex = (
          typeof isCompShown === 'undefined' || isCompShown
            ? 6
            : 5
        );
        --colIndex;
      ) {
        colIndex !== 3 && setTimeout(animCol, 99, colIndex, 0, forceNoAnim);
      }
    };

    /**
     * Animates the data in either the Visitor Origin or the Visitor Penetration
     * table, cycling through the columns from the last to the first and
     * swapping out values for data visualizations.
     *
     * @method animVoVp
     * @public
     *
     * @param {number} tileType - Pass 1 to animate the Visitor Origin tile's
     * charts, pass 2 to animate the Visitor Penetration tile's charts.
     * @param {boolean} [isCompShown=true] - True if the Competitor column is
     * visible, false if it is hidden.
     */
    const animVoVp = (tileType, isCompShown) => {
      isCompShown = (typeof isCompShown === 'undefined'
        ? state.getCompState()
        : isCompShown
      );

      // If there is a competitor shown, there are no VO charts visible
      if (tileType === 2 || !isCompShown) {
        for (
          let colIndex = (tileType === 2 && isCompShown ? 4 : 3);
          --colIndex;
        ) {
          animCol(colIndex, tileType);
        }
      }
    };

    /**
     * Modifies the values for the Population and Avg Household Income row on
     * the Key Performance Indicators table so that they have commas separating
     * the groupings of digits.
     *
     * @method modifyValues
     * @public
     */
    const modifyValues = () => {
      const addCommas = util.addCommas,
            kpiTable = doc.getElementById('kpi_table'),
            csvEls = kpiTable.querySelectorAll('.js-csv'),
            usdEls = kpiTable.querySelectorAll('.js-usd');

      for (const el of usdEls) {
        const curTxt = el.textContent;

        if (curTxt[0] === '$') break;

        el.textContent = '$' + addCommas(el.textContent);
        el.classList.remove('transparent');
      }

      for (const el of csvEls) {
        el.textContent = addCommas(el.textContent);
        el.classList.remove('transparent');
      }
    };

    /**
     * Activates sorting functionality on all table elements of a given class
     * name. The user can click on a table's header cell once to sort the table
     * according to the ascending order of that cell's column. Clicking the same
     * header cell again will reverse the sort into descending order. Clicking a
     * third time returns the table to its original order. The function sorts
     * a variety of data formats, including integers, floats, strings, currency,
     * percentages, and dates in the standardized formats accepted by the
     * Date.parse() API. The implementations works in in conjunction with the
     * following CSS classes which are applied by the function:
     * '.table-sort-header', 'table-sort-asc', and '.table-sort-desc'. They are
     * applied to the `th` elements that are children of the `thead` element and
     * provide a visual indication for the sorting feature itself and the
     * current sorting order (ascending, descending, or unsorted).
     *
     * @method activateTableSorting
     * @public
     *
     * @param {string} tables - The class name for the table(s) having the
     * sorting feature applied.
     * @param {string} [docRoot=document] - The ID of a root element that
     * contains the tables being sorted.
     */
    const activateTableSorting = (tables, docRoot) => {
      const l = parseFloat,
            m = u(/^(?:\s*)([\-+]?(?:\d+)(?:,\d{3})*)(\.\d*)?$/g, /,/g),
            g = u(/^(?:\s*)([\-+]?(?:\d+)(?:\.\d{3})*)(,\d*)?$/g, /\./g),

            getLen = x => x.length,

            containsTrue = (arr, cBack) => arr.map(cBack).indexOf(true) !== -1,

            forEach = (arr, cBack) => {
              for (let index = 0, len = getLen(arr); index < len; ++index) {
                cBack(arr[index], index);
              }
            },

            v = (n, r) => {
              r(n),
              forEach(n.childNodes, n => void v(n, r));
            },

            s = (n) => {
              const i = (n) => {
                const t = l(n);

                return !isNaN(t) && getLen(t + '') + 1 >= getLen(n) ? t : NaN;
              };

              let e = [];

              return forEach([i, m, g], (t) => {
                getLen(e) || containsTrue((t = n.map(t)), isNaN) || (e = t);
              }), e;
            },

            activateSorting = (table) => {
              if (table.nodeName === 'TABLE') {
                const e = d(table),
                      a = e[0],
                      o = e[1],
                      u = getLen(a),
                      i = u > 1 && (getLen(a[0]) < getLen(a[1]) ? 1 : 0),
                      s = i + 1,
                      v = a[i],
                      p = getLen(v),
                      l = [],
                      m = [],
                      g = [];

                for (let h = s; h < u; ++h) {
                  for (let index = 0; index < p; ++index) {
                    getLen(m) < p && m.push([]);
                    m[index].push((a[h][index].textContent || '').trim());
                  }
                  g.push(h - s);
                }

                const classNameSortAsc = 'table-sort-asc',
                      classNameSortDesc = 'table-sort-desc',
                      b = () => {
                        for (let n = p; n--;) {
                          const headerClasses = v[n].classList;

                          headerClasses.remove(classNameSortAsc),
                          headerClasses.remove(classNameSortDesc),
                          (l[n] = 0);
                        }
                      };

                forEach(v, (headerCell, t) => {
                  const headerClasses = headerCell.classList;

                  l[t] = 0;

                  headerClasses.add('table-sort-header'),
                  headerCell.title = 'Click to sort',
                  headerCell.addEventListener('click', () => {
                    const i = m[t],
                          p = g.slice(),
                          n = (k, r) => { // Comparison function for sorting
                            const t = d[k],
                                  e = d[r];

                            return (t > e
                              ? a
                              : (e > t
                                ? -a
                                : a * (k - r)
                              )
                            );
                          },
                          f = (n) => {
                            const r = n.map(Date.parse);

                            return (containsTrue(r, isNaN) ? [] : r);
                          };

                    let parentEl = null,
                        a = l[t],
                        d = c(i),
                        // Comparison function for sorting
                        v = (n, r) =>
                          a * i[n].localeCompare(i[r]) || a * (n - r);

                    b(),
                    (a = (a === 1 ? -1 : +!a)) && headerClasses.add(
                      a > 0
                        ? classNameSortAsc
                        : classNameSortDesc
                    ),
                    (l[t] = a);

                    (getLen(d) || getLen((d = f(i)))) && (v = n);

                    p.sort(v);

                    for (let nodeIndex = s; nodeIndex < u; ++nodeIndex) {
                      (parentEl = o[nodeIndex].parentNode)
                        .removeChild(o[nodeIndex]);
                    }

                    for (let nodeIndex = s; nodeIndex < u; ++nodeIndex) {
                      parentEl.appendChild(o[s + p[nodeIndex - s]]);
                    }
                  });
                });
              }
            },

            c = (n) => {
              let z = s(n);

              if (!getLen(z)) {
                const a = (n) => {
                        let e = n[0];

                        return forEach(n, (n) => {
                          while (!n.startsWith(e)) {
                            e = e.substring(0, getLen(e) - 1);
                          }
                        }), getLen(e);
                      },
                      o = a(n),
                      u = a(n.map(str => str.split('').reverse().join('')));
                z = s(n.map(n => n.substring(o, getLen(n) - u)));
              }

              return z;
            },

            d = (n) => {
              const t = [],
                    e = [];

              let r;

              return v(n, (node) => {
                const curNodeName = node.nodeName;

                if (node.parentNode.nodeName !== 'TFOOT') {
                  (curNodeName === 'TR'
                    ? ((r = []), t.push(r), e.push(node))
                    : ((curNodeName === 'TD' || curNodeName === 'TH') &&
                      r.push(node)
                    )
                  );
                }
              }), [t, e];
            };

      function u(n, r) {
        return (t) => {
          let e = '';

          return (
            t.replace(n, (n, t, a) =>
              (e = t.replace(r, '') + '.' + (a || '').substring(1))), l(e)
          );
        };
      }

      docRoot = (typeof docRoot !== 'undefined'
        ? doc.getElementById(docRoot)
        : doc) || doc;

      tables = docRoot.getElementsByClassName(tables);

      for (let tableNum = getLen(tables); tableNum--;) {
        try {
          activateSorting(tables[tableNum]);
        } catch (e) {}
      }
    };

    /**
     * Initiates the window interface's open() method if the window if the
     * window does not already exist or has been closed. If it does exist and is
     * open then the window is focused.
     *
     * @method requestNewView
     * @public
     *
     * @param {string} url A DOMString indicating the URL of the resource to be
     * loaded. This can be a path or URL to an HTML page, image file, or any
     * other browser supported resource. If an empty string ('') is specified as
     * the url, a blank page is opened into the targeted browsing context.
     * @param {string} windowName A DOMString specifying the name of the
     * browsing context (window, iframe, or tab) into which to load the
     * specified resource. If the name doesn't indicate an existing context, a
     * new window is created and is given the name specified by windowName. The
     * name should not contain any whitespace. Keep in mind that this will NOT
     * be used as the window's title.
     * @param {string} [windowFeatures] - A DOMString containing a
     * comma-separated list of window features given with their corresponding
     * values in the form of "name=value". The features list string must not
     * contain whitespace.
     */
    const requestNewView = (url, windowName, windowFeatures) => {
      (W_H === null || W_H.closed
        ? W_H = window.open(url, windowName, windowFeatures)
        : W_H.focus()
      );
    };

    /**
     * Provides a function for a child window to call that will return an array
     * of HTMLElements containing the elements matching selector which have a
     * display value that is not 'none'. This function is called in the
     * insert-reports.js file and the array of elements is unpacked in the
     * overview-report.html file.
     *
     * @method sendPageElements
     * @public
     *
     * @param {string} selector - The selector used in a querySelectorAll
     * method.
     * @return {Element[]} The array of Elements found by selector and then
     * filtered to not include any elements with a display value of 'none'.
     *
     * @see insert-reports.js
     */
    const sendPageElements = (selector) => {
      const allElements = doc.querySelectorAll(selector),
            numElements = allElements.length,
            locName =
              doc.getElementById('OVERVIEW_BUILD.V.R1.TBLOCATION').value ||
              doc.querySelector('.chart-label').textContent,
            hyphIndex = locName.lastIndexOf('-'),
            shortLocName = (hyphIndex !== -1
              ? locName.substring(0, hyphIndex)
              : locName
            ),
            visibleElArray = [];

      for (
        let index = 0,
            clonedEl,
            curEl,
            curClass,
            curTileMaps,
            curTileCloneMaps,
            screenMapUrlStr;
        index < numElements;
        ++index
      ) {
        curClass = (curEl = allElements[index]).classList[1];

        // Filter out the tiles that are not currently visible and those that
        // are not being used for the report
        if (
          (curClass !== 'def-tile' && curClass !== 'vo-tile-sub') ||
          (curClass === 'vo-tile-sub' && state.getCompState())
        ) {
          // Clone the node to prevent taking the actual element from the DOM
          clonedEl = curEl.cloneNode(true);

          // Find the map-based tiles
          if (
            curClass === 'vo-tile-top' || curClass === 'bav-tile' ||
            curClass === 'vp-tile' || curClass === 'vo-tile-sub'
          ) {
            // Grab all the maps in the tiles (there will usually just be one)
            curTileMaps = curEl.querySelectorAll('.js-map');
            curTileCloneMaps = clonedEl.querySelectorAll('.js-map');

            // Loop all the maps, grabbing all the POST'd href values
            // Cannot use cloned version for this part
            for (let curMap = curTileMaps.length; curMap--;) {
              try {
                screenMapUrlStr =
                  curTileMaps[curMap].contentWindow.location.href;

                // Modify the URL to create the print version
                screenMapUrlStr =
                  ((str, index, subStr) =>
                    str.slice(0, index) + subStr + str.slice(index)
                  )(
                    screenMapUrlStr,
                    screenMapUrlStr.lastIndexOf('/') + 1,
                    'print'
                  );

                // Replace the cloned maps' URLs with the new print version
                curTileCloneMaps[curMap].setAttribute('src', screenMapUrlStr);
              } catch (e) {}
            }
          }

          // Package the cloned node into an array to send to the print window
          visibleElArray[index] = clonedEl;
        }
      }
      // Attach the name of the selected location to the end of the array
      visibleElArray.push(shortLocName);

      return visibleElArray;
    };

    /**
     * Handles all the function calls necessary after the tiles (viewboxes) on
     * the Overview page have been refreshed from a location or benchmark
     * changing.
     *
     * @method locOnChangeNonMap
     * @public
     */
    const locOnChangeNonMap = () => {
      animCol(0, 3); // Anim AvPV table
      animKpi(state.getCompState());
      setTimeout(activateTableSorting, 3000, 'ttzc-table', 'ttzc_tile');
    };

    /**
     * Determines the name of the correct viewbox layout from an assortment of
     * different input data types.
     * @method determineLayout
     * @public
     *
     * @param {number|boolean|string} [layout] - Pass 0, false, or 'Default' for
     * the Default layout, pass 1, true, or 'NoCompetitor' for the No Competitor
     * layout. If no value is passed then the competitor visibility is checked
     * using compVisibility.state() and layout is based on the returning value.
     * If a string is passed, that string is used as the name of the layout.
     * @return {string} - The name of the proper layout to switch to, given what
     * was passed in by layout.
     */
    const determineLayout = layout =>
      (typeof layout !== 'undefined'
        ? (!layout
          ? 'Default'
          : (typeof layout === 'string'
            ? layout
            : 'NoCompetitor'
          )
        )
        : (state.getCompState() ? 'Default' : 'NoCompetitor')
      );

    /**
     * Set and store the current Overview page's layout state after a change
     * event on the Overview page's Competitor search field.
     *
     * @method setLayoutState
     * @public
     *
     * @param {string} val - The value of the Overview page's Competitor search
     * field's associated hidden field.
     */
    const setLayoutState = (val) => {
      const cbx = doc.getElementById('no_comp_cbx');

      if (val !== 0) {
        cbx.removeAttribute('disabled');
        cbx.checked = false;
        state.showComp();
      } else {
        state.hideComp();
      }
    };

    // Page initialization event handling

    doc.addEventListener('DOMContentLoaded', () => {
      activateTableSorting('ttzc-table', 'ttzc_tile');
    });

    win.addEventListener('resize', () => {
      setTimeout(() => {
        const compVisible = state.getCompState(),
              numColumns = 4 - state.getNumColHidden();

        // Only re-chart the VO table if a competitor is not visible
        if (!compVisible) {
          for (let colIndex = numColumns; --colIndex;) {
            animCol(colIndex, 1, true);
          }
        }

        animKpi(compVisible, true);

        // Re-chart VP table
        for (let colIndex = numColumns; --colIndex;) {
          animCol(colIndex, 2, true);
        }

        animCol(0, 3, true); // Animate Actual Vs. Potential Visitors table

        activateTableSorting('ttzc-table', 'ttzc_tile');
      }, 2000); // Wait for user to finish resizing the window

      setTimeout(modifyValues, 1000);
    });

    win.addEventListener('load', () => {
      modifyValues();

      animKpi(true, true); // Set charts in the KPI Table
      animCol(0, 3, true); // Set charts in the AvPV table
      // Set charts in VP table (VO table has no charts showing on page load)
      for (let colIndex = 4; --colIndex;) {
        animCol(colIndex, 2, true);
      }

      setTimeout(() => {
        const loadScreen = $('#loading_screen');

        loadScreen.find('.loading-screen__spinner').addClass('stop-anim');
        setTimeout(loadScreen.fadeOut, 15, 2000);
      }, 5000);
    });

    // Reveal all public methods
    return {
      activateTableSorting,
      animVoVp,
      determineLayout,
      locOnChangeNonMap,
      modifyValues,
      requestNewView,
      sendPageElements,
      setLayoutState
    };
  })();
