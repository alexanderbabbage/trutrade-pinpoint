/**
 * @fileoverview This file handles the majority of the front-end JavaScript
 * functionality for the TruTrade application, copyright © 2019, Alexander
 * Babbage, Inc. The functions contained here allow the values from a table to
 * be replaced with several different animated data visualizations that fit
 * within the table cell. It provides table sorting functionality as well as a
 * number of other DOM and data manipulation utilities.
 *
 * @author Stephen M Irving
 * @version 3.1.0, 10/30/19
 * @requires progressbar.js
 * @link https://kimmobrunfeldt.github.io/progressbar.js/
 * @requires sweetalert.js
 * @link https://sweetalert.js.org/
**/

/**
 * @const {object}
 * @global
 * @see requestNewView
 */
WIN_HANDLER = null;

/**
 * An immediately invoked function expression that uses a closure to track the
 * state of competitor visibility on the Overview page.
 *
 * @example compVisibility.state()  // Returns either true or false
 * @example compVisibility.show()   // Privately sets visibility to true
 * @example compVisibility.hide()   // Privately sets visibility to false
 * @example compVisibility.toggle() // Privately toggles and returns visibility
 */
const compVisibility = (function () {
  'use strict';

  /** @type {boolean} */
  let _visible = true;

  return {
    /**
     * Sets competitor visibility into a hidden (false) state.
     * @memberof compVisibility
     */
    hide: function () {
      _visible = false;
    },

    /**
     * Set competitor visibility into a shown (true) state.
     * @memberof compVisibility
     */
    show: function () {
      _visible = true;
    },

    /**
     * Toggles the competitor visibility state. If it is currently in a
     * shown (true) state, toggle() will change it to hidden (false). If
     * competitor visibility is in a hidden (false) state, toggle() will
     * change it to a shown (true) state. The new state is then returned.
     * @memberof compVisibility
     *
     * @return {boolean} The new, post-toggle competitor visibility state.
     */
    toggle: function () {
      return _visible = !_visible;
    },

    /**
     * Returns the current state of competitor visibility. If the a
     * competitor is being shown, returns true. If no competitor is being
     * shown, like with the 'Focus on One Competitor' option, state()
     * returns false.
     * @memberof compVisibility
     *
     * @return {boolean} The current competitor visibility state. True if
     * a competitor is currently visible, false if it is not.
     */
    state: function () {
      return _visible;
    }
  };
}());

/**
 * Tests if a given number is a number or if it can be type cast
 * as a valid number (eg. '300').
 *
 * @param {*} n - A value being tested to determine if it is a number.
 * @return {boolean} - True if n is a number, false if it is not.
 */
function isNumber(n) {
  return !isNaN(parseFloat(n)) && !isNaN(n * 1);
}

/**
 * Modifies the values for the Population and Avg Household Income row on the
 * KPI table so that they have commas separating the groupings of digits.
 */
function modifyValues() {
  'use strict';

  const addCommas = function (num) {
          return (num + '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        },
        isNum = isNumber,
        kpiTable = document.getElementById('kpi_table'),
        csvEls = kpiTable.querySelectorAll('.js-csv'),
        usdEls = kpiTable.querySelectorAll('.js-usd');

  let i = csvEls.length,
      curEl,
      curVal;

  while (i--) {
    curEl = csvEls[i];
    curVal = curEl.textContent;
    if (isNum(curVal)) curEl.textContent = addCommas(curVal);
  }

  for (i = usdEls.length; i--;) {
    curEl = usdEls[i];
    curVal = curEl.textContent;
    if (isNum(curVal)) curEl.textContent = '$' + addCommas(curVal);
  }
}

/**
 * Replaces the values of a column with corresponding data animations.
 *
 * @param {number} [colIndex=0] - The index number of the column whose
 * values are to be replaced with chart elements. The index starts at 0 for the
 * Actual vs Potential Visitor tile's table, because there is only that one
 * column being animated, but all other table's columns start at 1.
 * @param {number} [tileType=0] - A number that relates to a given tile. If no
 * value is passed then 0 is the default. Values are as follows:
 * tileType 0 = Key Performance Indicator tile;
 * tileType 1 = Visitor Origin Tile;
 * tileType 2 = Visitor Penetration Tile;
 * tileType 3 = Actual vs Potential Visitors Tile.
 * @param {boolean|string|number} [forceNoAnim=false] - Pass anything except for
 * false, 0, null, or undefined when being called on the initial page load or
 * any other time that forcing charts to appear without animating is desired.
 *
 */
function animCol(colIndex, tileType, forceNoAnim) {
  'use strict';

  /**
   * This function will create individual radial chart data visualizations.
   *
   * @param {Element} wrapPointer - Pointer associated with the div
   * wrapper where the radial will go. (#wrap_colX_valX)
   * @param {string|number} cellVal - Value in the the cell that is being
   * replaced with the radial chart.
   * @param {number} [radSize=0] - Size of the radial created.
   * Passable values are:
   * 0 = The default radial size of 85px.
   * 1 = The larger size of 120px.
   * The sizes for these radials only effect the font size and text indent in
   * the function, the radial's actual dimensions are set by their css wrapper.
   * @param {boolean} [tileOnScreen] - True if the tile containing the radial
   * chart is on screen at the time the function is called. If this is true the
   * chart will animate, if it is false the chart will simply be set to its
   * finished position without animating.
   */
  function createRad(wrapPointer, cellVal, radSize, tileOnScreen) {
    radSize = radSize || 0;

    // Set the point the chart will animate to. Must be between 0 and 1
    const progressPortion = cellVal / 100,
          colorBrand = '#2a3c68', // The platform's primary brand color
          radChart = new ProgressBar.Circle(wrapPointer, {
            color: colorBrand, // Set the font color for the radial's value
            strokeWidth: 12, // To prevent clipping, match with the to width
            trailWidth: 12, // The width of the trail the radial animates along
            trailColor: 'rgba(79,162,217,.14)',
            easing: 'easeInOut',
            duration: 1250, // Time to reach 100% chart fullness in ms
            text: { autoStyleContainer: false },
            // Changes loading bar color & width as it progresses. The radial
            // will only reach the stated 'to' color and width at 100% mark
            from: { // Starting color and width
              color: '#39518c',
              width: 8
            },
            to: { // Ending color and width
              color: colorBrand,
              width: 12 // To prevent clipping, match with strokeWidth
            },
            // How to animate the path of the radial and the text inside it
            step: function step(state, circle) {
              const path = circle.path;
              path.setAttribute('stroke', state.color);
              path.setAttribute('stroke-width', state.width);
              circle.setText(Math.round(circle.value() * 100) + '%');
            }
          }),
          radTxtStyle = radChart.text.style;

    radTxtStyle.fontSize = (!radSize ? '20px' : '30px');
    radTxtStyle.fontWeight = '700';
    radTxtStyle.textIndent = (!radSize ? '.4ch' : '.8ch');

    if (tileOnScreen) radChart.animate(progressPortion);
    else radChart.set(progressPortion);
  }

  /**
   * This function will create individual bar graph data visualizations.
   *
   * @param {Element} wrapPointer - Pointer associated with the div
   * wrapper where the bar will go. (wrap_colX_valX)
   * @param {string} cellVal - Value in the the cell being replaced
   * @param {number} barType - Type of bar being created.
   * Passable values are:
   * 0 = horizontal bar based on an index that changes colors based on the value.
   * 1 = horizontal bar based on a percentage.
   * 2 = vertical bar based on minutes.
   * 3 = vertical bar based on population with the text outside of the bar.
   * @param {boolean} [tileOnScreen] - True if the tile containing the radial
   * chart is on screen at the time the function is called. If this is true the
   * chart will animate, if it is false the chart will simply be set to its
   * finished position without animating.
   */
  function createBar(wrapPointer, cellVal, barType, tileOnScreen) {
    const colorBrand = '#2a3c68', // The platform's primary brand color
          colorGreen = '#6dc05d', // Color of type 5 bars that exceed 100
          colorGray = '#524b48', // Color of type 5 bars that equal 100
          colorRed = '#e61e25', // Color of type 5 bars that are less than 100
          colorGreenLight = '#81c973', // Lighter color the green bars begin as
          colorGrayLight = '#625a56', // Lighter color the gray bars begin as
          colorRedLight = '#e93a40'; // Lighter color the red bars begin as

    let endText    = '',         // The label text for the value
        posType    = 'absolute', // The position type of the bar text
        leftPos    = '-30%',     // Left position value of the bar text
        topPos     = '41%',      // Top position value of the bar text
        textSize   = '18px',     // Size of the bar's value text
        textWeight = '700',
        textWidth  = '100%',     // Width of the box containing the value text
        textColor  = '#414042',  // Color of the bar's value text
        endColor   = colorBrand, // Color the bar ends as
        startColor = '#39518c',  // Color the bar begins as
        progressMax; // The max value for the chart's

    cellVal *= 1;

    if (!barType || barType === 1) {
      posType = 'relative',
      textSize = '1em',
      textWeight = '400',
      textWidth = '60px';

      (!barType
        ? (
          (progressMax = 300,
          leftPos = '125px',
          topPos = '-20px') &&
          (cellVal > 100
            ? (
              endColor = colorGreen,
              startColor = colorGreenLight
            )
            : (cellVal < 100
              ? (
                endColor = colorRed,
                startColor = colorRedLight
              )
              : (
                endColor = colorGray,
                startColor = colorGrayLight
              )
            )
          )
        )
        : (
          progressMax = 100,
          endText = '%',
          leftPos = '5em',
          topPos = '-25px'
        )
      );
    } else if (barType === 2) {
      progressMax = 120,
      endText = ' mins';

      if (cellVal > 30) {
        textColor = '#fff',
        textWeight = '400';
      } else {
        textColor = (cellVal > 16 ? '#d4d2e4' : colorBrand);
      }
    } else {
      progressMax = 4000000,
      leftPos = '-150px', // Really the bottom value due to chart rotation
      topPos = '25%',     // Really the left value due to chart rotation
      textSize = '22px',
      textWidth = '8rem';
    }

    // Set the point the chart will animate to. Must be between 0 and 1
    const progressPortion = (cellVal >= progressMax
            ? 1
            : cellVal / progressMax
          ),
          exceedsLimit = cellVal > progressMax,
          barChart = new ProgressBar.Line(wrapPointer, {
            color: endColor, // Sets the color that the bar will be
            trailColor: 'rgba(79,162,217,.14)',
            easing: 'easeInOut',
            duration: 1250, // Time to reach 100% chart fullness in ms
            svgStyle: {
              width: '100%',
              height: '100%'
            },
            text: {
              style: {
                color: textColor,
                fontSize: textSize,
                fontWeight: textWeight,
                left: leftPos,
                position: posType,
                textAlign: 'center',
                top: topPos,
                width: textWidth
              },
              autoStyleContainer: false
            },
            from: { color: startColor }, // Set the starting color of the bar
            to: { color: endColor }, // Sets the color of the bar at 100%

            // How to animate the path of the bar and the text inside it
            step: function step(state, chart) {
              const path = chart.path,
                    addCommas = function (num) {
                      return (num + '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                    },
                    round = Math.round;

              let barValTxt; // Value of the bar as it animates towards cellVal

              path.setAttribute('stroke', state.color);

              if (!barType || barType === 1) {
                barValTxt = round(chart.value() * progressMax);

                if (!barType) {
                  (barValTxt === cellVal) &&
                    (path.setAttribute('stroke', endColor));
                  (exceedsLimit && barValTxt === progressMax) &&
                    (barValTxt = addCommas(cellVal|0));
                }
              } else if (barType === 2) {
                const fixedToOne = function (num) {
                  return (num = round(num * 10) / 10) +
                    (num === (num|0) ? '.0' : '');
                };
                barValTxt = fixedToOne(chart.value() * progressMax);

                if (exceedsLimit && barValTxt == progressMax) {
                  barValTxt = fixedToOne(cellVal);
                }
              } else {
                const noCommaVal = (chart.value() * progressMax)|0;

                barValTxt = (exceedsLimit && noCommaVal === progressMax
                  ? addCommas(cellVal)
                  : addCommas(noCommaVal)
                );
              }
              chart.setText(barValTxt + endText);
            }
          }),
          barTxtStyle = barChart.text.style;

    if (barType > 1) {
      barTxtStyle.transform = (barType === 3
        ? 'translate(50%, 50%) rotate(90deg)'
        : 'rotate(90deg)'
      );

      if (barType === 2 && cellVal <= 31 && cellVal > 16) {
        barTxtStyle.textShadow = '#000 1px 1px';
      }
    }

    if (tileOnScreen) barChart.animate(progressPortion);
    else barChart.set(progressPortion);

    // Apply on-hover tooltips to the horizontal bar charts
    if (!barType) {
      if (cellVal > 100) {
        barChart._container.title =
          'Green colored bars indicate that the value is greater than 100';

        if (cellVal > progressMax) {
          setTimeout(function () {
            barTxtStyle.fontWeight = '700';
            barChart.text.title =
              'Bold values are a visual indication that the 300 point ' +
              'scale of the chart has been been exceeded.';
          }, 1e3);
        }
      } else if (cellVal < 100) {
        barChart._container.title =
          'Red colored bars indicate that the value is below 100';
      } else {
        barChart._container.title =
          'Gray colored bars indicate that the value is equal to 100';
      }
    } else if (barType === 1) {
      barChart._container.title = (wrapPointer.id.slice(-1) === '4'
        ? 'Millennials - ' + cellVal + '%'
        : wrapPointer.id.slice(-1) === '5'
          ? 'Generation X - ' + cellVal + '%'
          : 'Baby Boomers - ' + cellVal + '%'
      );
    }
  }

  /**
   * Creates a 'Data currently unavailable' message and appends it
   * to a given table cell.
   *
   * @param {Element} cell - Pointer associated with the cell where
   * the msg goes.
   * @param {string|boolean} [msgWidth] - Optional argument that sets the width
   * of the unavailable message's div wrapper.
   */
  function unavailableMsg(cell, msgWidth) {
    const doc = document,
          nodeNoData = doc.createElement('div'),
          nodeTxt = doc.createTextNode('Data currently unavailable');

    // Clear out node of content
    while (cell.lastChild) {
      cell.removeChild(cell.lastChild);
    }

    nodeNoData.className = 'unavail-msg';

    if (msgWidth) {
      if (msgWidth === '90%') {
        // Defines the msg style for the KPI Population & Avg Income rows
        nodeNoData.classList.add('unavail-msg--sm');
      }
      nodeNoData.style.width = msgWidth;
    } else {
      // Defines the msg style for the horizontal-mc bars
      nodeNoData.classList.add('unavail-msg--mc-bars');
    }
    nodeNoData.appendChild(nodeTxt);
    cell.appendChild(nodeNoData);
  }

  // #animCol logic starts here

  colIndex = colIndex || 0;
  tileType = tileType || 0;
  forceNoAnim = forceNoAnim || false;

  const doc = document,
        numVals = (!tileType ? 7 : (tileType === 3 ? 23 : 2)),
        chartContainer = doc.createElement('div'),
        get = function (id) {
          return doc.getElementById(id);
        },
        isNum = isNumber,
        isOnScreen = function (element) {
          const win = window,
                elBounds = element.getBoundingClientRect(),
                scrollTop = win.pageYOffset,
                elTop = elBounds.y + scrollTop;

          return (
            elTop < (win.innerHeight + scrollTop) &&
            elTop > (scrollTop - elBounds.height)
          );
        },
        isVisible = (forceNoAnim
          ? false
          : (isOnScreen(get(
            (!tileType
              ? 'kpi_tile'
              : (tileType === 3
                ? 'avpv_tile'
                : (tileType === 2
                  ? 'vp_tile'
                  : 'vo_tile'
                )
              )
            )
          )))
        );

  chartContainer.className = 'chart-container';

  // curCell is the current cell number in the column at that moment in the loop,
  // it starts with the bottom of the table and counts down towards 1 at the top
  for (
    let curCell = numVals + 1,
        wrapperId,
        cellId,
        cell,
        cellVal,
        containerNode,
        wrapperNode;
    --curCell;
  ) {
    // Skip Avg Dwell time in the MSA colummn of KPI table's (it is always n/a)
    if (!tileType && curCell === 1 && colIndex === 1) continue;

    if (tileType !== 3) {
      wrapperId = 'wrap_col' + colIndex + '_val' + curCell;
      cellId = 'col' + colIndex + '_val' + curCell;

      if (tileType) {
        (tileType === 2
          ? (
            wrapperId = 'vp_' + wrapperId,
            cellId = 'vp_' + cellId
          )
          : (
            wrapperId = 'vo_' + wrapperId,
            cellId = 'vo_' + cellId
          )
        );
      }
    } else {
      wrapperId = 'wrap_val' + curCell;
      cellId = 'av_val' + curCell;
    }

    cell = get(cellId);

    if (!cell) break;

    cellVal = cell.innerHTML.trim();

    // Input scrubbing
    // Break if cell already has a chart, (the resize event can trigger this)
    if (cellVal[0] === '<') break;
    if (cellVal.substring(0, 3) === 'n/a') continue;
    if (!isNum(cellVal)) {
      unavailableMsg(
        cell,
        (!tileType
          ? ((curCell > 3 && curCell < 7) ? '90%' : '50%')
          : (numVals === 2)
            ? '120px'
            : !1
        )
      );
      continue;
    }
    // End of input scrubbing - At this point, it is ensured the cell contains
    // no child nodes except for text that can be parsed into a number, with no
    // spaces at the beginning or end

    // Clear out cell before inserting chart
    cell.firstChild.remove();

    // Set up the node settings that exist for every chart
    wrapperNode = doc.createElement('div');
    wrapperNode.id = wrapperId;

    // Handle logic for the various tile types and call createBar or createRad
    if (!tileType) { // This targets the KPI tile
      if (curCell > 3 && curCell < 7) {
        // This targets the horizontal % bar charts
        containerNode = doc.createElement('div');
        containerNode.className = 'h-bar-container';
        wrapperNode.className = 'h-bar-wrap';
        containerNode.appendChild(wrapperNode);
        cell.appendChild(containerNode);
        setTimeout(function () {
          createBar(get(wrapperId), cellVal, 1, isVisible);
        }, 75);
        continue;
      }
      containerNode = chartContainer.cloneNode(false);
      containerNode.appendChild(wrapperNode);
      if (curCell !== 1) {
        // This targets the radials
        wrapperNode.className = 'rad-wrap';
        cell.appendChild(containerNode);
        setTimeout(function () {
          createRad(get(wrapperId), cellVal, 0, isVisible);
        }, 80);
      } else {
        // Targets the Avg Dwell Time vertical bar charts
        wrapperNode.className = 'bar-wrap';
        cell.appendChild(containerNode);
        setTimeout(function () {
          createBar(get(wrapperId), cellVal, 2, isVisible);
        }, 75);
      }
    } else if (tileType === 3) { // Targets the Actual v Potential Visitor tile
      containerNode = doc.createElement('div');
      containerNode.className = 'h-bar-mc-container';
      wrapperNode.className = 'h-bar-mc-wrap';
      containerNode.appendChild(wrapperNode);
      cell.appendChild(containerNode);
      setTimeout(function () {
        createBar(get(wrapperId), cellVal, 0, isVisible);
      }, 60);
    } else { // This targets the VO and VP tiles (tileType 1 and 2)
      containerNode = chartContainer.cloneNode(false);
      containerNode.appendChild(wrapperNode);
      if (curCell === 1) {
        wrapperNode.className = 'bar-wrap';
        cell.appendChild(containerNode);
        // Create the chart in the new wrapper
        setTimeout(function () {
          createBar(get(wrapperId), cellVal, 3, isVisible);
        }, 60);
      } else {
        wrapperNode.className = 'rad-wrap-lg';
        cell.appendChild(containerNode);
        setTimeout(function () {
          createRad(get(wrapperId), cellVal, 1, isVisible);
        }, 60);
      }
    }
  } // End of the for loop going through all the column's values
}

/**
 * Animates the data in the Key Performance Indicator table, cycling through the
 * columns from the last to the first and swapping out values for data
 * visualizations. Skips column 3 as that is the Index column that has no
 * charts.
 *
 * @param {boolean} [isCompShown=true] - True if the Competitor column is
 * visible, false if it is hidden.
 * @param {boolean|string|number} [forceNoAnim=0] - Pass anything except false,
 * 0, null, or undefined when being called on the initial page load or any other
 * time that forcing the charts to appear without animating is desired.
 */
function animKpi(isCompShown, forceNoAnim) {
  forceNoAnim = forceNoAnim || 0;

  for (
    let colIndex = (typeof isCompShown === 'undefined' || isCompShown ? 6 : 5),
        first = colIndex - 1;
    --colIndex;
  ) {
    if (colIndex !== 3) {
      setTimeout(function () {
        animCol(colIndex, 0, forceNoAnim);
      }, (first === colIndex ? 0 : 210));
    }
  }
}

/**
 * Animates the data in either the Visitor Origin or the Visitor Penetration
 * table, cycling through the columns from the last to the first and swapping
 * out values for data visualizations.
 *
 * @param {number} tileNum - Pass 1 to animate the Visitor Origin tile's
 * charts, pass 2 to animate the Visitor Penetration tile's charts.
 * @param {boolean} [isCompShown=true] - True if the Competitor column is
 * visible, false if it is hidden.
 */
function animVoVp(tileNum, isCompShown) {
  isCompShown = typeof isCompShown === 'undefined'
    ? compVisibility.state()
    : isCompShown;

  // If there is a competitor shown, there are no VO charts visible
  if (tileNum === 2 || !isCompShown) {
    for (let colIndex = (tileNum === 2 && isCompShown ? 4 : 3); --colIndex;) {
      setTimeout(function () {
        animCol(colIndex, tileNum);
      }, 60);
    }
  }
}

/**
 * Activates sorting functionality on all table elements of a given class name.
 * The user can click on a table's header cell once to sort the table according
 * to the ascending order or that cell's column. Clicking the same header cell
 * again will reverse the sort into descending order. Clicking a third time
 * returns the table to its original order. The function sorting a variety of
 * data types, including integers, floats, strings, currency, percentages, and
 * dates in the standardized formats accepted by the Date.parse() API. The
 * implementations works in in conjunction with the following CSS classes which
 * are applied by the function: '.table-sort-header', 'table-sort-asc', and
 * '.table-sort-desc', which are applied to the th elements that are children of
 * the thead element and provide a visual indication for the sorting feature
 * itself and the current sorting order (ascending, descending, or unsorted).
 *
 * @param {string} tables - The class name for the table(s) having the
 * sorting feature applied.
 * @param {string} [docRoot=document] - The optional document root ID that
 * contains the tables being sorted.
 */
function activateTableSorting(tables, docRoot) {
  'use strict';

  const l = parseFloat,
        m = u(/^(?:\s*)([\-+]?(?:\d+)(?:,\d{3})*)(\.\d*)?$/g, /,/g),
        g = u(/^(?:\s*)([\-+]?(?:\d+)(?:\.\d{3})*)(,\d*)?$/g, /\./g),
        getLen = function (x) {
          return x.length;
        },
        foundTrue = function (n, r) {
          return n.map(r).indexOf(!0) != -1;
        };

  function t(n, t) {
    if (n) {
      for (let i = 0, a = getLen(n); i < a; ++i) {
        t(n[i], i);
      }
    }
  }

  function u(n, r) {
    return function (t) {
      let e = '';

      return (
        t.replace(n, function (n, t, a) {
          return (e = t.replace(r, '') + '.' + (a || '').substring(1));
        }), l(e)
      );
    };
  }

  function s(n) {
    const i = function (n) {
      const t = l(n);

      return !isNaN(t) && getLen(t + '') + 1 >= getLen(n) ? t : NaN;
    };

    let e = [];

    return t([i, m, g], function (t) {
      let a;

      getLen(e) || foundTrue((a = n.map(t)), isNaN) || (e = a);
    }), e;
  }

  function c(n) {
    let z = s(n);

    if (!getLen(z)) {
      const a = function (n) {
              let e = n[0];

              return t(n, function (n) {
                while (!n.startsWith(e)) {
                  e = e.substring(0, getLen(e) - 1);
                }
              }), getLen(e);
            },
            o = a(n),
            u = a(n.map(function (str) {
              return str.split('').reverse().join('');
            })),
            i = n.map(function (n) {
              return n.substring(o, getLen(n) - u);
            });
      z = s(i);
    }
    return z;
  }

  function v(n, r) {
    r(n),
    t(n.childNodes, function (n) {
      v(n, r);
    });
  }

  function d(n) {
    const t = [],
          e = [];

    let r;

    return v(n, function (node) {
      const curNodeName = node.nodeName,
            parentNodeName = node.parentNode.nodeName;

      if (parentNodeName !== 'TFOOT') {
        (curNodeName === 'TR'
          ? ((r = []), t.push(r), e.push(node))
          : ((curNodeName === 'TD' || curNodeName === 'TH') && r.push(node))
        );
      }
    }), [t, e];
  }

  function activateSorting(table) {
    if (table.nodeName === 'TABLE') {
      for (
        var e = d(table),
            a = e[0],
            o = e[1],
            u = getLen(a),
            i = u > 1 && (getLen(a[0]) < getLen(a[1]) ? 1 : 0),
            s = i + 1,
            v = a[i],
            p = getLen(v),
            l = [],
            m = [],
            g = [],
            h = s;
        h < u;
        ++h
      ) {
        for (let index = 0; index < p; ++index) {
          const T = a[h][index],
                C = T.textContent || '';

          getLen(m) < p && m.push([]);
          m[index].push(C.trim());
        }
        g.push(h - s);
      }

      const classNameSortAsc = 'table-sort-asc',
            classNameSortDesc = 'table-sort-desc',
            b = function () {
              let n = p;
              while (n--) {
                const headerClasses = v[n].classList;

                headerClasses.remove(classNameSortAsc),
                headerClasses.remove(classNameSortDesc),
                (l[n] = 0);
              }
            };

      t(v, function (headerCell, t) {
        const headerClasses = headerCell.classList;

        l[t] = 0;

        headerClasses.add('table-sort-header'),
        headerCell.title = 'Click to sort',
        headerCell.addEventListener('click', function () {
          const i = m[t],
                p = g.slice(),
                n = function (k, r) { // A sort-compare function
                  const t = d[k],
                        e = d[r];

                  return (t > e ? a : e > t ? -a : a * (k - r));
                },
                f = function (n) {
                  const r = n.map(Date.parse);

                  return foundTrue(r, isNaN) ? [] : r;
                };

          let a = l[t],
              d = c(i),
              v = function (n, r) { // Other possible sort-compare function
                return a * i[n].localeCompare(i[r]) || a * (n - r);
              },
              parentEl = null;

          b(),
          a = (a == 1 ? -1 : +!a),
          a && headerClasses.add(a > 0 ? classNameSortAsc : classNameSortDesc),
          (l[t] = a);

          (getLen(d) || getLen((d = f(i)))) && (v = n);

          p.sort(v);

          for (let nodeIndex = s; nodeIndex < u; ++nodeIndex) {
            (parentEl = o[nodeIndex].parentNode),
            parentEl.removeChild(o[nodeIndex]);
          }
          for (let nodeIndex = s; nodeIndex < u; ++nodeIndex) {
            parentEl.appendChild(o[s + p[nodeIndex - s]]);
          }
        });
      });
    }
  }

  docRoot = (typeof docRoot !== 'undefined'
    ? document.getElementById(docRoot)
    : document) || document;

  tables = docRoot.getElementsByClassName(tables);

  for (let tableNum = getLen(tables); tableNum--;) {
    try {
      activateSorting(tables[tableNum]);
    } catch (e) {}
  }
}

/**
 * Initiates the window interface's open() method if the window if the window
 * does not already exist or has been closed. If it does exist and is open then
 * the window is focused.
 *
 * @param {string} url A DOMString indicating the URL of the resource to be
 * loaded. This can be a path or URL to an HTML page, image file, or any other
 * browser supported resource. If an empty string ('') is specified as the url,
 * a blank page is opened into the targeted browsing context.
 * @param {string} windowName A DOMString specifying the name of the browsing
 * context (window, iframe, or tab) into which to load the specified resource.
 * If the name doesn't indicate an existing context, a new window is created and
 * is given the name specified by windowName. The name should not contain any
 * whitespace. Keep in mind that this will NOT be used as the window's title.
 * @param {string} [windowFeatures] - A DOMString containing a comma-separated
 * list of window features given with their corresponding values in the form of
 * "name=value". The features list string must not contain whitespace.
 */
function requestNewView(url, windowName, windowFeatures) {
  if (WIN_HANDLER === null || WIN_HANDLER.closed) {
    WIN_HANDLER = window.open(url, windowName, windowFeatures);
  } else {
    WIN_HANDLER.focus();
  }
}

/**
 * Provides a function for a child window to call that will return an array of
 * HTMLElements containing the elements matching selector which have a display
 * value that is not 'none'. This function is called in the insert-reports.js
 * file and the array of elements is unpacked in the overview-report.html file.
 *
 * @param {string} selector - The selector used in a querySelectorAll method.
 * @return {Element[]} The array of Elements found by selector
 * and then filtered to not include any elements with a display value of 'none'.
 *
 * @see insert-reports.js
 */
function sendPageElements(selector) {
  'use strict';

  const doc = document,
        allElements = doc.querySelectorAll(selector),
        numElements = allElements.length,
        locName =
          doc.getElementById('OVERVIEW_BUILD.V.R1.TBLOCATION').value ||
          doc.querySelector('.chart-label').textContent,
        hyphIndex = locName.lastIndexOf('-'),
        shortLocName = (hyphIndex !== -1
          ? locName.substring(0, hyphIndex)
          : locName
        ),
        visibleElArray = [];

  for (
    let i = 0,
        clonedEl,
        curEl,
        curClass,
        curTileMaps,
        curTileCloneMaps,
        curMap,
        screenMapUrlStr;
    i < numElements;
    ++i
  ) {
    curEl = allElements[i],
    curClass = curEl.classList[1];

    // Filter out the tiles that are not currently visible and those that
    // are not being used for the report
    if (
      (curClass !== 'def-tile' && curClass !== 'vo-tile-sub') ||
      (curClass === 'vo-tile-sub' && compVisibility.state())
    ) {
      // Clone the node to prevent taking the actual element from the DOM
      clonedEl = curEl.cloneNode(true);

      // Find the map-based tiles
      if (
        curClass === 'vo-tile-top' || curClass === 'vo-tile-sub' ||
        curClass === 'bav-tile' || curClass === 'vp-tile'
      ) {
        // Grab all the maps in the tiles (there will usuall just be one)
        curTileMaps = curEl.querySelectorAll('.js-map');
        curTileCloneMaps = clonedEl.querySelectorAll('.js-map');

        // Loop all the maps, grabbing all the POST'd href values
        // Cannot use cloned version for this part
        for (let k = curTileMaps.length; k--;) {
          curMap = curTileMaps[k];

          try {
            screenMapUrlStr = curMap.contentWindow.location.href;
            // Change URL to the print version
            screenMapUrlStr = (function (str, index, subStr) {
              return str.slice(0, index) + subStr + str.slice(index);
            }(screenMapUrlStr, screenMapUrlStr.lastIndexOf('/') + 1, 'print'));
            // Replace the cloned maps' URLs with the print version
            curTileCloneMaps[k].setAttribute('src', screenMapUrlStr);
          } catch (e) {}
        }
      }

      // Package the cloned node into an array to send to the print window
      visibleElArray[i] = clonedEl;
    }
  }
  // Attach the name of the selected location to the end of the array
  visibleElArray.push(shortLocName);

  return visibleElArray;
}

/**
 * This function calls the Sweet Alert API to initiate an Access Restriction
 * popup modal.
 */
function accessCheckRefresh() {
  swal({
    title: 'Access Restriction',
    text: 'You do not have access to this module. Please contact an ' +
          'Alexander Babbage representative to receive access.',
    icon: 'error',
    button: 'Alright'
  });
}

/**
 * Handles all the function calls necessary after the tiles (viewboxes) on
 * the Overview page have been refreshed from a location or benchmark changing.
 */
function locOnChangeNonMap() {
  'use strict';

  setTimeout(function () {
    setTimeout(function () {
      animCol(0, 3); // Anim AvPV table
    }, 200);
    animKpi(compVisibility.state()); // Anim KPI table
  }, 1e3);
  activateTableSorting('ttzc-table', 'ttzc_tile');
}

/**
 * Determines the name of the correct viewbox layout from an assortment of
 * different input data types.
 *
 * @param {number|boolean|string} [layout] - Pass 0, false, or 'Default' for
 * the Default layout, pass 1, true, or 'NoCompetitor' for the No Competitor
 * layout. If no value is passed then the competitor visibility is checked using
 * compVisibility.state() and layout is based on the returning value. If a
 * string is passed, that string is used as the name of the layout.
 * @return {string} - The name of the proper layout to switch to, given what was
 * passed in by layout.
 */
function determineLayout(layout) {
  return (
    typeof layout === 'undefined'
      ? (compVisibility.state() ? 'Default' : 'NoCompetitor')
      : !layout
        ? 'Default'
        : typeof layout === 'string'
          ? layout
          : 'NoCompetitor'
  );
}

/**
 * Set and store the current Overview page's layout state after a change event
 * on the Overview page's Competitor search field.
 *
 * @param {string} val - The value of the Overview page's Competitor search
 * field's associated hidden field.
 */
function setLayoutState(val) {
  'use strict';

  const cbx = document.getElementById('no_comp_cbx');

  if (val === '0') {
    compVisibility.hide();
  } else {
    cbx.removeAttribute('disabled');
    cbx.checked = false;
    if (!compVisibility.state()) compVisibility.show();
  }
}

/**
 * Reset the layout for all the Overview tiles (viewboxes) that do not contain
 * maps by checking the current state and then swapping out the layout HTML for
 * the tile.
 */
function resetNonMapLayoutState() {
  if (!compVisibility.state()) swapNonMapLayouts(1);
  else (swapNonMapLayouts(0));
}

/**
 * Reset the layout for the Visitor Origin tile (viewbox) by checking the
 * current state and then swapping out the layout HTML for the tile.
 */
function resetVoLayoutState() {
  if (!compVisibility.state()) swapTileLayout('viewbox_visorig', 1);
  else swapTileLayout('viewbox_visorig', 0);
}

/**
 * Reset the layout for the Visitor Penetration tile (viewbox) by checking the
 * current state and then swapping out the layout HTML for the tile.
 */
function resetVpLayoutState() {
  if (!compVisibility.state()) swapTileLayout('viewbox_vispen', 1);
  else swapTileLayout('viewbox_vispen', 0);
}

/**
 * Page setup/initialization with event handlers
 */
!function (win, doc, jQ) {
  'use strict';

  win.addEventListener('resize', function () {
    setTimeout(function () {
      const compVisible = compVisibility.state();

      // Only re-chart the VO table if a competitor is not visible
      if (!compVisible) {
        for (let colIndex = 3; --colIndex;) {
          animCol(colIndex, 1, true);
        }
      }

      animKpi(compVisible, true);

      // Re-chart VP table
      for (let colIndex = (compVisible ? 4 : 3); --colIndex;) {
        animCol(colIndex, 2, true);
      }

      animCol(0, 3, true); // Animate Actual Vs. Potential Visitors table

      activateTableSorting('ttzc-table', 'ttzc_tile');
    }, 2000); // Wait for user to finish resizing window
    setTimeout(modifyValues, 1000);
  });

  win.addEventListener('load', function () {
    modifyValues();
    animKpi(true, true); // Set charts in the KPI Table
    animCol(0, 3, true); // Set charts in the AvPV table
    // Set charts in VP table (VO table has no charts showing on page load)
    for (let colIndex = 4; --colIndex;) animCol(colIndex, 2, true);

    setTimeout(function () {
      const loadScreen = jQ('#loading_screen');

      loadScreen.find('.loading-screen__spinner').addClass('stop-anim');
      setTimeout(function () {
        loadScreen.fadeOut(2000);
      }, 99);
    }, 6000);
  }, false);
}(this, document, jQuery);
