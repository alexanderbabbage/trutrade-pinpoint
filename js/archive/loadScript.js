(function (doc) {
  var uAgent = navigator.userAgent;

  (function loadScript(url, callback) {
    var script = doc.createElement('script');

    if (callback) {
      if (script.readyState) { // IE
        script.onreadystatechange = function () {
          if (script.readyState == 'loaded' || script.readyState == 'complete') {
            script.onreadystatechange = null;
            callback();
          }
        };
      } else { // Others
        script.onload = function () {
          callback();
        };
      };
    }

    script.src = url;
    doc.getElementsByTagName('head')[0].appendChild(script);
  })(
    (
      /Edge/.test(uAgent) || /Trident/.test(uAgent)
        ? 'js/main.ie.min.js'
        : 'js/main.min.js'
    ),
    (function () {
      AB.modules.dom.activateTableSorting('ttzc-table', 'ttzc_tile');
    })
  );
}(document));
