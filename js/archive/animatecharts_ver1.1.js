/**
* @name animatecharts.js
*
* @file This js file contains functions designed to take the the values from a
*       table and replace them with small charts (either a radial or a bar)
*       that animate from 0 to the given number.
*
* @author    Stephen M Irving
* @version   1.1.0, 01/25/19
* @requires  progressbar.min.js
*/

/**
* Modularizes the creation of individual radials.
*
* @param {object} wrapperName  - Pointer associated with the div wrapper where
*                                the radial will go. (chart_wrapperX)
* @param {number} progressNum  - Number that is in the cell being replaced with
*                                the radial
* @param {string} startColor   - Hex color value the created radial start as
* @param {string} endColor     - Hex color value the created radial will become
*                                as well as the color of the radial's text
* @param {number} animTime     - Time it takes to reach 100% completion (in ms)
*/
function createRad(wrapperName, progressNum, startColor, endColor, animTime) {
    // Set value the chart will animate to. Must be between 0 and 1 (2 decimals)
    const progress = progressNum / 100.0;
    // Create a radial
    const rad = new ProgressBar.Circle(wrapperName, {
        color: endColor, // Set the font color of the num in the center of the radial
        strokeWidth: 8, // To prevent clipping, make the same as the max width it will animate to
        trailWidth: 8, // The width of the trail the radial will animate along
        trailColor: '#e2e2e2', // The color of the trail
        easing: 'easeInOut', // Animation easing function
        duration: animTime, // Sets the total time in ms for the rad to reach 100%
        text: { autoStyleContainer: false },
        // Changes loading bar color & width as it progresses. Keep in mind: The
        // radial bar will only reach the stated 'to' color and width at 100% mark
        from: { // Starting color and width
            color: startColor,
            width: 4
        },
        to: { // Ending color and width
            color: endColor,
            width: 8 // To prevent clipping, make width here the same as strokeWidth
        },
        // Animate the path of the radial and the text inside it
        step: function step(state, circle) {
            circle.path.setAttribute('stroke', state.color);
            circle.path.setAttribute('stroke-width', state.width);
            const value = Math.round(circle.value() * 100);
            circle.setText(value + '%');
        }
    });
    // Set the text style of above radial
    rad.text.style.fontFamily = '"Roboto","Helvetica Neue", HelveticaNeue, Helvetica-Neue, Helvetica, -apple-system, BlinkMacSystemFont, "BBAlpha Sans", "Segoe UI", Arial, "Noto Sans", sans-serif';
    rad.text.style.fontSize = '20px';
    // Initialize the animation of above radial
    rad.animate(progress);
};

/**
* This function will modularize the creation of individual bar graphs.
*
* @param {object} wrapperName - Name associated with the div wrapper where the
*                               bar will go. (bar_wrapperX)
* @param {number} progressNum - Number that is in the cell being replaced with
*                                the bar
* @param {string} startColor  - Hex color value the created bar start as
* @param {string} endColor    - Hex color value the created bar will become
* @param {number} animTime    - Time it takes to reach 100% completion (in ms)
* @param {string} endText     - Label for the chart (either 'miles' or 'mins')
*/
function createBar(wrapperName, progressNum, startColor, endColor, animTime, endText) {
    'use strict';
    // Set value the chart will animate to. Must be between 0 and 1 (2 decimals)
    const progress = progressNum / 50.0; // Divide by the maximum num of the chart
    const bar = new ProgressBar.Line(wrapperName, {
        color: endColor, // Sets the color that the bar will be
        trailColor: '#e2e2e2', // The color of the trail
        easing: 'easeInOut', // Animation easing function
        duration: animTime, // Sets the total time in ms for the bar to reach 100%
        svgStyle: { width: '100%', height: '100%' },
        text: { // Set the attributes of the text inside the bar
            style: {
                width: '100%',
                color: '#fff', // Set text color
                fontfamily: '"Roboto","Helvetica Neue", HelveticaNeue, Helvetica-Neue, Helvetica, -apple-system, BlinkMacSystemFont, "BBAlpha Sans", "Segoe UI", Arial, "Noto Sans", sans-serif',
                fontSize: '18px', // Text font size
                transform: 'rotate(90deg)', // Rotate the text back so that it is not sideways.
                position: 'absolute',
                textShadow: '1px 1px #a7a9ac',
                left: '-30%', // Due to rotation, 'left' attribute is really 'bottom' attribute
                top: '41%' // Due to rotation, 'top' attribute is really 'left' attribute
            },
            autoStyleContainer: false
        },
        from: { color: startColor }, // Set the starting color of the bar
        to: { color: endColor }, // Sets the color of the bar if it reaches 100%

        // Animate the path of the bar and the text inside it
        step: function step(state, bar) {
            bar.path.setAttribute('stroke', state.color);
            bar.setText(progressNum + ' ' + endText);
        }
    });
    bar.animate(progress);
};

/**
* On window load, this function will pull a number from a table cell, and swap
* that number with a matching radial or bar chart that will represent it.
*/
window.onload = function() {
    'use strict';
    const totalRads = 9; // The total number of radials on the page
    const totalBars = 6; // The total number of bars on the page
    const totalCharts = totalRads + totalBars; // Total bars and radials
    const animTime = 1400; // Time it takes radial to reach 100% completion, in ms
    const startColor = '#e2b9bd'; // Set color the charts begin as at 0%
    const endColor = '#58b86b'; // Set color of radial text & charts at 100%
    let cellVal; // The cell value to be converted
    let wrapperName; // The name of all the radial wrapper's cell
    let cellId; // placeholder for the IDs of the cells

    // Iterate through the number of radials there will be, creating all the
    // possible ids for the table cells and the radial wrappers. Then record the
    // values of those cells and swap the numbers in the cells
    // for the radial container divs
    for (let i = 0; i < totalCharts; i++) {
        wrapperName = 'chart_wrapper' + i;
        cellId = 'num_' + i;
        // Get the contents of table cells, this is the progress number
        cellVal = document.getElementById(cellId).innerHTML;
        if (i < totalRads) { // Checks if we are still applying the radial wrappers
            // Replace the nums in the cells with the corresponding radial wrappers
            document.getElementById(cellId).innerHTML =
            '<div class="container"><div id="' + wrapperName
            + '" class="rad-wrapper"></div></div>';
            createRad(
                document.getElementById(wrapperName),
                cellVal,
                startColor,
                endColor,
                animTime);
            } else { // radials finished, now do the bar chart wrapppers
                // Replace the nums in the cells with the corresponding bar wrappers
                document.getElementById(cellId).innerHTML =
                '<div class="container"><div id="' + wrapperName
                + '" class="bar-wrapper"></div></div>';
                createBar(
                    document.getElementById(wrapperName),
                    cellVal,
                    startColor,
                    endColor,
                    animTime,
                    'miles'
                    );
                }
            }
        };
