/**
 * @name animate-charts.js
 *
 * @file This js file is designed to take the the values from a table and
 * replace them with small charts (either a radial or a bar) that animate from 0
 * to the number pulled from the table cell.
 *
 * @author    Stephen M Irving
 * @version   2.0.0, 4/11/19
 * @requires  progressbar.min.js
 */

/**
 * @name createRad
 * @description This function will create individual radial charts
 *
 * @param {object} wrapperName Pointer associated with the div wrapper where
 * the radial will go. (wrap_colX_valX)
 * @param {number} progressNum Value in the the cell being replaced with a radial
 * @param {string} startColor Hex color value the created radial starts as
 * @param {string} endColor Hex color value the created radial will become
 * as well as the color of the radial's text
 * @param {number} animTime    Time it takes to reach 100% completion (in ms)
 */
function createRad(wrapperName, progressNum, startColor, endColor, animTime) {
  'use strict';
  // Set the point the chart will animate to. Must be between 0 and 1
  const PROGRESS = progressNum / 100.0;
  // Create a radial
  let rad = new ProgressBar.Circle(wrapperName, {
    color: endColor, // Set the font color of the num in the center of the radial
    strokeWidth: 12, // To prevent clipping, same as the max width it will animate to
    trailWidth: 12, // The width of the trail the radial will animate along
    trailColor: '#E2E2E2', // The color of the trail
    easing: 'easeInOut', // Animation easing function
    duration: animTime, // Sets the total time in ms for the rad to reach 100%
    text: { autoStyleContainer: false },
    // Changes loading bar color & width as it progresses. Keep in mind: The
    // radial bar will only reach the stated 'to' color and width at 100% mark
    from: { // Starting color and width
      color: startColor,
      width: 8
    },
    to: { // Ending color and width
      color: endColor,
      width: 12 // To prevent clipping, make width the same as strokeWidth
    },
    // Animate the path of the radial and the text inside it
    step: function step(state, circle) {
      circle.path.setAttribute('stroke', state.color);
      circle.path.setAttribute('stroke-width', state.width);
      circle.setText(Math.round(circle.value() * 100) + '%');
    }
  });
  // Set the text style of above radial
  rad.text.style.fontSize = '20px';
  rad.text.style.fontWeight = '900';
  rad.text.style.textIndent = '.4ch';
  rad.animate(PROGRESS); // Initializes the animation of above radial
}

/**
* @name createBar
* @description This function will create vertical bar graphs.
*
* @param {object} wrapperName Pointer associated with the div wrapper where
* the bar will go. (wrap_colX_valX)
* @param {number} progressNum Value in the the cell being replaced with a bar
* @param {string} startColor Hex color value the created bar starts as
* @param {string} endColor Hex color value the created bar will become
* @param {number} animTime Time it takes to reach 100% completion (in ms)
* @param {string} endText Label for the chart (either 'miles' or 'mins')
* @param {boolean} textOnBar Determins whether the text for the bar's value will
* appear on the bar itself, or just below it. Defaults to true;
*/
function createBar(
wrapperName,
progressNum,
startColor,
endColor,
animTime,
endText,
textOnBar) {
  'use strict';

  if (typeof textOnBar === 'undefined') textOnBar = true;
  // Set the max value for the chart's animation, max 30 for miles, 120 for mins
  const PROGRESS_MAX = (endText === 'miles') ? 30 : 120;
  // Set the point the chart will animate to. Must be between 0 and 1
  const PROGRESS = (progressNum >= PROGRESS_MAX) ? 1 : progressNum / PROGRESS_MAX;

  let barValText; // Holds the value of the bar as it animates towards progressNum
  let bar = new ProgressBar.Line(wrapperName, {
    color: endColor, // Sets the color that the bar will be
    trailColor: '#F1F2F2', // The color of the trail
    easing: 'easeInOut', // Animation easing function
    duration: animTime, // Sets the total time in ms for the bar to reach 100%
    svgStyle: { width: '100%', height: '100%' },
    text: { // Set the attributes of the text inside the bar
      style: {
        width: '100%',
        color: '#FFF',
        fontSize: '18px',
        transform: 'rotate(90deg)', // Rotate the text back so it isn't sideways
        position: 'absolute',
        textShadow: '1px 1px #A7A9AC',
        left: '-30%', // 'left' attribute is 'bottom' attribute due to rotation
        top: '41%' // 'top' attribute is 'left' attribute
      },
      autoStyleContainer: false
    },
    from: { color: startColor }, // Set the starting color of the bar
    to: { color: endColor }, // Sets the color of the bar if it reaches 100%

    // Animate the path of the bar and the text inside it
    step: function step(state, chart) {
      chart.path.setAttribute('stroke', state.color);
      barValText = (chart.value() * PROGRESS_MAX).toFixed(1);
      if (progressNum > PROGRESS_MAX && barValText == PROGRESS_MAX)
        barValText = (progressNum * 1).toFixed(1);
      chart.setText(barValText + ' ' + endText);
    }
  });
  bar.animate(PROGRESS); // Initializes the animation of above bar
}

/**
 * @name createHBarPercent
 * @description This function will create horizontal bar graphs.
 *
 * @param {object} wrapperName Pointer associated with the div wrapper where
 * the bar will go. (wrap_colX_valX)
 * @param {number} progressNum Value in the the cell being replaced with a bar
 * @param {string} startColor Hex color value the created bar starts as
 * @param {string} endColor Hex color value the created bar will become
 * @param {number} animTime Time it takes to reach 100% completion (in ms)
 */
function createHBarPercent(wrapperName, progressNum, startColor, endColor, animTime) {
  'use strict';
  // Set the point the chart will animate to. Must be between 0 and 1
  const PROGRESS = (progressNum >= 100) ? 1 : progressNum / 100;
  let bar = new ProgressBar.Line(wrapperName, {
    color: endColor, // Sets the color that the bar will be
    trailColor: '#F1F2F2', // The color of the trail
    easing: 'easeInOut', // Animation easing function
    duration: animTime, // Sets the total time in ms for the bar to reach 100%
    svgStyle: { width: '100%', height: '100%' },
    text: { // Set the attributes of the text inside the bar
      style: {
        width: '60px',
        color: '#414042',
        fontSize: '16px',
        position: 'relative',
        left: '75px',
        top: '-27px'
      },
      autoStyleContainer: false
    },
    from: { color: startColor }, // Set the starting color of the bar
    to: { color: endColor }, // Sets the color of the bar if it reaches 100%

    // Animate the path of the bar and the text inside it
    step: function step(state, chart) {
      chart.path.setAttribute('stroke', state.color);
      chart.setText(Math.round(chart.value() * 100) + '%');
    }
  });
  bar.animate(PROGRESS); // Initializes the animation of above bar
}

/**
 * @name createHBarMC
 * @description Swaps the value of a table cell for a horizontal bar graph that
 * presents as one of three colors according to the cell's value. Red for values
 * under 100, gray for values equal to 100, green for values over 100.
 * @param {object} wrapperName A pointer to the HTML object where the bar will go
 * @param {string} progressNum Value in the the cell being replaced with a bar
 */
function createHBarMC(wrapperName, progressNum) {
  'use strict';
  const PROGRESS_MAX = 300;  // Max value for the chart's animation
  const GREEN = '#6DC05D';   // Color of the bars that exceed 100
  const GRAY  = '#524B48';   // Color of the bars that equal 100
  const RED   = '#E61E25';   // Color of the bars that are less than 100
  const GREEN_LT = '#81C973'; // 6% lighter color green bars will begin as
  const GRAY_LT  = '#625A56'; // 6% lighter color gray bars will begin as
  const RED_LT   = '#E93A40'; // 6% lighter color red bars will begin as
  // Set the point the chart will animate to. Must be between 0 and 1
  const PROGRESS = (progressNum >= PROGRESS_MAX) ? 1 : progressNum / PROGRESS_MAX;

  let endColor = RED, // Color of the bar, according to the cell's value (progressNum)
      startColor = RED_LT, // Color the bar will begin as, according to the cell's value
      barValText; // Holds the value of the bar as it animates towards progressNum

  if (progressNum > 100) {
    endColor = GREEN;
    startColor = GREEN_LT;
  } else if (progressNum === '100') {
    endColor = GRAY;
    startColor = GRAY_LT;
  }

  let bar = new ProgressBar.Line(wrapperName, {
    color: endColor, // Sets the color that the bar will be
    trailColor: '#F1F2F2', // The color of the trail
    easing: 'easeInOut', // Animation easing function
    duration: 1300, // Sets the total time in ms for the bar to reach 100%
    svgStyle: { width: '100%', height: '100%' },
    text: { // Set the attributes of the text inside the bar
      style: {
        width: '60px',
        color: '#414042',
        fontSize: '16px',
        position: 'relative',
        left: '120px',
        top: '-25px'
      },
      autoStyleContainer: false
    },
    from: { color: startColor }, // Set the starting color of the bar
    to: { color: endColor }, // Sets the final color of the bar

    // Animate the path of the bar and the text inside it
    step: function step(state, chart) {
      chart.path.setAttribute('stroke', state.color);
      barValText = Math.round(chart.value() * PROGRESS_MAX);
      if (barValText == progressNum)
        chart.path.setAttribute('stroke', endColor);
      if (progressNum > PROGRESS_MAX && barValText == PROGRESS_MAX)
        barValText = parseInt(progressNum);
      chart.setText(barValText);
    }
  });
  bar.animate(PROGRESS); // Initializes the animation of above bar
}

/**
 * @name addCommas
 * @description Takes a number or a string representing a number and adds commas
 * to it. Displays numbers with more than 2 decimal places incorrectly.
 *
 * @param {string|number} x A number or a string representing a number
 * @returns {string} The number displayed with commas
 */
function addCommas(x) {
  return (x + '').replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

/**
 * @name stripCommas
 * @description Takes a string of a number with commas in it and strips the commas
 *
 * @param {string} x A string representing a number that is displayed with commas
 * @returns {number} The number value of the string with the commas stripped
 */
function stripCommas(x) {
  return (x + '').replace(/\,/g, '') * 1;
}

/**
 * @name countUp
 * @description Takes a value and visually counts up to the number on the page
 *
 * @param {string} elementId The ID of the element containing the number being counted
 * @param {number} startVal The starting value, defaults to 0
 */
function countUp(elementId, startVal) {
  "use strict";
  startVal = (typeof startVal !== 'undefined' ? startVal : 0);
  try {
    let curVal = startVal,
        finalVal = document.getElementById(elementId).innerHTML,
        increment = Math.ceil(finalVal / 77),
        counter;
    // 77 is an arbitrary number chosen to make the timing close to other animations
    document.getElementById(elementId).innerHTML = curVal;
    counter = setInterval(function() {
      if (curVal >= finalVal) {
        clearInterval(counter);
        document.getElementById(elementId).innerHTML = addCommas(finalVal);
      } else {
        curVal = parseInt(stripCommas(document.getElementById(elementId).innerHTML));
        curVal += increment;
        document.getElementById(elementId).innerHTML = addCommas(curVal);
      }
    }, 1);
  } catch(err) {
    console.error(err);
  }
}

/**
* @name dataUnavailable
* @description Data validation function. Takes the trimmed innerHTML value of a
* table cell or othe HTML element and returns 1 if the data is unavailable,
* determined by the value not being a number AND not having HTML tags, OR being
* equal to 0 OR an empty string. If those conditions aren't meant, returns 0.
*
* @param {string} cellVal The inner contents of a table cell or HTML element
* @returns {number} notAvailable: 1 for true, 0 for false
*/
function dataUnavailable(cellVal) {
  'use strict';

  // Regular expression is true if string can be converted to a float or integer
  const IS_NUMBER = new RegExp('^[0-9]*\.?[0-9]+$');
  // Regular expression is true if string has < or > symbols
  const HAS_TAGS = new RegExp('[<>]+');
  let notAvailable = 0;

  if ((!IS_NUMBER.test(cellVal) && !HAS_TAGS.test(cellVal)) || cellVal == 0 || cellVal === '') {
    notAvailable = 1;
  }

  return notAvailable;
}

/**
 * @name unavailableMsg
 * @description Creates a 'data currently unavailable' message and appends it to
 * a given table cell
 *
 * @param {object} cell HTML pointer associated with the cell where the msg goes
 */
function unavailableMsg(cell) {
  const TEXT_MSG = document.createTextNode('data currently unavailable');
  let node = document.createElement('div');
  cell.innerHTML = '';
  node.style.fontSize = '.9em';
  node.style.fontVariant = 'small-caps';
  node.style.margin = '0 auto';
  node.style.width = '92px';
  node.appendChild(TEXT_MSG);
  cell.appendChild(node);
}

/**
* @name hideCol
* @description Hides a table column, removing it from the table
*
* @param {string} elementClass The class name for the table cells being hidden
* @param {string} innerClass The class name for the content wrapper in those cells
*/
function hideCol(elementClass, innerClass) {
  'use strict';
  const els = document.getElementsByClassName(elementClass);
  const innerEls = document.getElementsByClassName(innerClass);
  const hbarwrap = document.getElementsByClassName('h-bar-container');

  Array.prototype.forEach.call(innerEls, function(el) {
    el.style.display = 'none';
  });

  Array.prototype.forEach.call(els, function(el) {
    el.className += ' comp-bgfade';
    el.style.opacity = .2;
    el.style.width = '1px';
  });

  setTimeout(function() {
    Array.prototype.forEach.call(els, function(el) {
      el.style.display = 'none';
    });
    if (hbarwrap.length > 0) {
      Array.prototype.forEach.call(hbarwrap, function(el) {
        el.style.marginLeft = '22.5%';
      });
    }
  }, 650);
}

/**
* @name showCol
* @description Displays a previously hidden table column
*
* @param {string} elementClass The class name for the table cells being unhidden
* @param {string} innerClass The class name for the content wrapper in those cells
*/
function showCol(elementClass, innerClass) {
  'use strict';
  const els = document.getElementsByClassName(elementClass);
  const innerEls = document.getElementsByClassName(innerClass);
  const hbarwrap = document.getElementsByClassName('h-bar-container');

  if (hbarwrap.length > 0) {
    Array.prototype.forEach.call(hbarwrap, function(el) {
      el.style.marginLeft = '18%';
    });
  }

  Array.prototype.forEach.call(els, function(el) {
    el.style.width = '120px';
    el.style.opacity = 1;
    el.classList.remove('comp-bgfade');
    el.style.display = 'table-cell';
  });

  Array.prototype.forEach.call(innerEls, function(el) {
    el.style.display = 'block';
  });
}

/**
 * @name animCol
 * @description Replaces the values of a column with corresponding data animations
 *
 * @param {number} colIndex The index number of the column whose values are to
 * be replaced with chart elements (index starts at 0)
 */
function animCol(colIndex) {
  'use strict';
  const TOTAL_VALS = 9; // Total values to be animated
  const ANIM_TIME = 1300; // Time in ms it takes chart to reach 100% completion
  const START_COLOR_BAR = '#39518C'; // Set color the bar charts begin as
  const START_COLOR_RAD = '#39518C'; // Set color the rad charts begin as
  const END_COLOR = '#2A3C68'; // Set color of radial text & charts at 100%

  let wrapperName, // The name of all the chart's wrapper's cell
      cellId,      // Placeholder for the IDs of the cells
      cell,        // The cell itself
      cellVal,     // The cell's value to be converted
      barText;     // Either 'miles' or 'mins'

  /// In this loop, n represents the number of the value being animated,
  /// starting with 1 at the top of the table
  for (let n = 1; n < (TOTAL_VALS + 1); n++) {
    wrapperName = 'wrap_col' + colIndex + '_val' + n;
    cellId = 'col' + colIndex + '_val' + n;
    cell = document.getElementById(cellId);
    cellVal = cell.innerHTML.trim();

    // Input scrubbing
    if (cellVal.startsWith('n/a')) continue;
    if (dataUnavailable(cellVal)) {
      unavailableMsg(cell);
      continue;
    }
    // Fixes possibility of charts sitting on top of one another when swapping
    if (isNaN(cellVal)) break;
    // End of input scrubbing

    // Animate the Population and Avg Income by counting up to their value
    if (n === 1 || n === 5) {
      countUp(cellId);
      continue;
    } else if (colIndex === 1 && n === 2) {
      // Skips the MSA col Avg Dwell Time because it will always be 'n/a'
      continue;
    } else if (n === 2) {
      // Animate the Avg Dwell Time for the other columns
      // Replace the nums in the cells with the corresponding bar wrappers
      cell.innerHTML = '<div class="chart-container"><div id="' + wrapperName +
                      '" class="bar-wrap"></div></div>';
      barText = 'mins'
      createBar(
        document.getElementById(wrapperName),
        cellVal,
        START_COLOR_BAR,
        END_COLOR,
        ANIM_TIME,
        barText
      );
      continue;
    } else if ((n > 2 && n < 5) || n === 9) { // Animate the percentage radials
      // Replace the cell's content with the corresponding radial wrappers
      cell.innerHTML = '<div class="chart-container"><div id="' + wrapperName +
                       '" class="rad-wrap"></div></div>';
      createRad(
        document.getElementById(wrapperName),
        cellVal,
        START_COLOR_RAD,
        END_COLOR,
        ANIM_TIME
      );
      continue;
    } else { // The only n values left are for the
      cell.innerHTML = '<div class="h-bar-container"><div id="' + wrapperName +
                       '" class="h-bar-wrap"></div></div>';
      createHBarPercent(
        document.getElementById(wrapperName),
        cellVal,
        START_COLOR_BAR,
        END_COLOR,
        ANIM_TIME
      );
    }
  } // End the for loop of all the column's charts
}

/**
* @name animTable
* @description Animates the data in a table, cycling through the columns and
* swapping out values for data visualizations.
*/
function animKpi() {
  'use strict';
  for(let i = 1; i < 6; i++) {
    if (i === 3) continue;
    animCol(i);
  }
}

/**
 * @name animIndex
 * @description Animates the Index vs 3 Miles column of the Actual vs. Potential
 * Visitors table.
 */
function animIndex() {
  'use strict';
  const TOTAL_VALS  = 11; // Total number of values to be animated

  let wrapperName, // The name of all the chart's wrapper's cell
      cellId,      // Placeholder for the IDs of the cells
      cell,        // The cell itself
      cellVal;     // The cell's value to be converted

  /// In this loop, n represents the number of the value being animated,
  /// starting with 1 at the top of the table
  for (let n = 1; n < (TOTAL_VALS + 1); n++) {
    wrapperName = 'wrap_val' + n;
    cellId = 'av_val' + n;
    cell = document.getElementById(cellId);
    cellVal = cell.innerHTML.trim();

    // Input scrubbing
    if (cellVal.startsWith('n/a')) continue;
    if (dataUnavailable(cellVal)) {
      unavailableMsg(cell);
      continue;
    }
    // Fixes possibility of charts sitting on top of one another when swapping
    if (isNaN(cellVal)) break;
    // End of input scrubbing

    // Replace the nums in the cells with the corresponding bar wrappers
    cell.innerHTML = '<div class="h-bar-mc-container"><div id="' + wrapperName +
                     '" class="h-bar-mc-wrap"></div></div>';

    createHBarMC(document.getElementById(wrapperName), cellVal);
  }
}
