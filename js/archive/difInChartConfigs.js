// DIFFERENCES IN CHART CONFIGS / CHART OBJECT

// config object
config = {
	pattern: '#.#' // 	'$' is the only one where the key is prefix instead of pattern
	width: '85%',
	left: 50 // ALL are 50 except for the '$' pattern which is 70 and  '$#,###' pattern which is 100
	vAxis: {
		format: '$#,###',
		gridlines: { color: 'none' }
	},
	isBar: false,
	
}

configPossibilities = {
	digits: 1||2 // CAN GET DIGITS FROM PATTERN
	pattern: '$'||'$#,###'||'#%'||'#.#'
	width: '85%'||'100%'
	left: 50||70||100
	vAxis: {
		format: '$#,###',||'###.#%'||currency||'###%'||'###.#'
	},
	isBar: true||false
	
	
	
	
	
	legend: { position: 'bottom', maxLines: 4 },
	curveType: 'function'
	|| OR ||
	bar: { groupWidth: '75%' },
    isStacked: 'true'||'percent'
}

/*
24 different chart configurations

pattern:
	'$'      - 1
	'#.#'    - 9
	'$#,###' - 1
	'#%'     - 13
	
width:
	'85%'    - 13
	'100%'   - 11

	
format:
	'$#,###'  - 1
	'###.#%'  - 8
	currency  - 1
	'###%'    - 6
	'###.#'   - 8
	
isLine:
	true  - 18
	false - 6
	
stacked:
	'true'         - 2
	'percent'      - 4
	null/undefined - 18

*/


