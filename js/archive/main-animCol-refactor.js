/**
* @fileoverview This file handles the majority of the front-end JavaScript
* functionality for the TruTrade application, copyright © 2019, Alexander
* Babbage, Inc. The functions contained here allow the values from a table to
* be replaced with several different animated data visualizations that fit
* within the table cell. It provides table sorting functionality as well as a
* number of other DOM and data manipulation utilities.
*
* @author Stephen M Irving
* @version 3.1.0, 10/01/19
* @requires progressbar.js
* @link https://kimmobrunfeldt.github.io/progressbar.js/
* @requires sweetalert.js
* @link https://sweetalert.js.org/
**/

/**
* @const {object}
* @global
* @see requestNewView
*/
WIN_HANDLER = null;

/**
* An immediately invoked function expression that uses a closure to track the
* state of competitor visibility on the Overview page.
*
* @example compVisibility.state()  // Returns either true or false
* @example compVisibility.show()   // Privately sets visibility to true
* @example compVisibility.hide()   // Privately sets visibility to false
* @example compVisibility.toggle() // Privately toggles visibility and returns the new state
*/
const compVisibility = (function () {
  /** @type {boolean} */
  let _visible = true;

  return {
    /**
    * Sets competitor visibility into a hidden (false) state.
    * @memberof compVisibility
    */
    hide: function () {
      _visible = false;
    },

    /**
    * Set competitor visibility into a shown (true) state.
    * @memberof compVisibility
    */
    show: function () {
      _visible = true;
    },

    /**
    * Toggles the competitor visibility state. If it is currently in a
    * shown (true) state, toggle() will change it to hidden (false). If
    * competitor visibility is in a hidden (false) state, toggle() will
    * change it to a shown (true) state. The new state is then returned.
    * @memberof compVisibility
    *
    * @return {boolean} The new, post-toggle competitor visibility state.
    */
    toggle: function () {
      return _visible = !_visible;
    },

    /**
    * Returns the current state of competitor visibility. If the a
    * competitor is being shown, returns true. If no competitor is being
    * shown, like with the 'Focus on One Competitor' option, state()
    * returns false.
    * @memberof compVisibility
    *
    * @return {boolean} The current competitor visibility state. True if
    * a competitor is currently visible, false if it is not.
    */
    state: function () {
      return _visible;
    }
  };
}());

/**
* Tests if a given number is a number or if it can be type cast
* as a valid number (eg. '300').
*
* @param {*} n - A value being tested to determine if it is a number.
* @return {boolean} - True if n is a number, false if it is not.
*/
function isNumber(n) {
  return !isNaN(parseFloat(n)) && !isNaN(n * 1);
}

/**
* Takes a number or a string that is parseable as a number and adds commas to
* separate values. Displays numbers with more than 2 decimal places
* incorrectly, so do not use on floats without using toFixed() first (or
* refactoring the function's regular expression).
*
* @param {string|number} num - Either a number or a string that is parseable
* as a number.
* @return {string} The number displayed with commas separating values.
*/
function addCommas(num) {
  return (num + '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

/**
* Modifies the values for the Population and Avg Household Income row on the
* KPI table so that they have commas separating the groupings of digits.
*/
function modifyValues() {
  for (let i = (compVisibility.state() ? 5 : 4), curEl, curVal; --i;) {
    curEl = document.getElementById('pop_val' + i);
    if (curEl) {
      curVal = curEl.textContent;
      if (isNumber(curVal)) curEl.textContent = addCommas(curVal);
    }
    curEl = document.getElementById('inc_val' + i);
    if (curEl) {
      curVal = curEl.textContent;
      if (isNumber(curVal)) curEl.textContent = '$' + addCommas(curVal);
    }
  }
}

/**
* Replaces the values of a column with corresponding data animations.
*
* @param {number} [colIndex=0] - The index number of the column whose
* values are to be replaced with chart elements. The index starts at 0 for the
* Actual vs Potential Visitor tile's table, but all other table's columns start
* at 1.
* @param {number} [tileType=0] - A number that relates to a given tile. If no
* value is passed then 0 is the default. Values are as follows:
* tileType 0 = Key Performance Indicator tile;
* tileType 1 = Visitor Origin Tile;
* tileType 2 = Visitor Penetration Tile;
* tileType 3 = Actual vs Potential Visitors Tile.
* @param {boolean|string|number} [forceNoAnim=false] - Pass anything except,
* false, 0, null, or undefined when being called on the initial page load or
* any other time that forcing the charts to appear without animating is desired.
*
*/
function animCol(colIndex, tileType, forceNoAnim) {
  colIndex = colIndex || 0;
  tileType = tileType || 0;
  forceNoAnim = forceNoAnim || false;

  const doc = document,
        numVals = (!tileType ? 7 : (tileType === 3 ? 23 : 2)),
        chartContainer = doc.createElement('div'),
        isVisible = (forceNoAnim
          ? false
          : (isOnScreen(
            (!tileType
              ? 'kpi_tile'
              : (tileType === 3
                ? 'avpv_tile'
                : (tileType === 2
                  ? 'vp_tile'
                  : 'vo_tile'
                )
              )
            )
          ))
        );

  let wrapperName,    // The name of all the chart's wrapper's cells
      cellId,         // Placeholder for the IDs of the cells
      cell,           // The cell itself
      cellVal,        // The cell's value to be converted
      loopControl,    // Holds return value for chartSetup()
      tempNodeParent, // Temporary node holder
      tempNodeChild;  // Temporary node holder

  chartContainer.className = 'chart-container';

  // Returns 0 for break, 1 for continue, undefined for neither
  function chartSetup() {
    cell = get(cellId);

    if (!cell) return 0;

    cellVal = cell.innerHTML.trim();

    // Input scrubbing
    // Break if cell already has a chart, (the resize event can trigger this)
    if (cellVal[0] === '<') return 0;
    if (cellVal.substring(0, 3) === 'n/a') return 1;
    if (!cellVal || !isNumber(cellVal)) {
      unavailableMsg(
        cell,
        (!tileType
          ? ((curVal > 3 && curVal < 7) ? '90%' : '50%')
          : (numVals === 2)
            ? '120px'
            : !1
        )
      );
      return 1;
    }

    // Clear out cell before inserting chart
    cell.firstChild.remove();

    tempNodeChild = doc.createElement('div');
    tempNodeChild.id = wrapperName;
  }

  if (!tileType) {
    for (let curVal = numVals + 1; --curVal;) {
      if (curVal === 1 && colIndex === 1) continue;

      wrapperName = 'wrap_col' + colIndex + '_val' + curVal;
      cellId = 'col' + colIndex + '_val' + curVal;

      loopControl = chartSetup();
      if (loopControl) continue;
      else if (loopControl === 0) break;

      if (curVal > 3 && curVal < 7) {
        tempNodeParent = doc.createElement('div');
        tempNodeParent.className = 'h-bar-container';
        tempNodeChild.className = 'h-bar-wrap';
        tempNodeParent.appendChild(tempNodeChild);
        cell.appendChild(tempNodeParent);
        createBar(get(wrapperName), cellVal, 4, isVisible);
        continue;
      }
      tempNodeParent = chartContainer.cloneNode(false);
      tempNodeParent.appendChild(tempNodeChild);
      if (curVal !== 1) {
        tempNodeChild.className = 'rad-wrap';
        cell.appendChild(tempNodeParent);
        createRad(get(wrapperName), cellVal, 0, isVisible);
      } else {
        tempNodeChild.className = 'bar-wrap';
        cell.appendChild(tempNodeParent);
        createBar(get(wrapperName), cellVal, 2, isVisible);
      }
    }
  } else if (tileType === 3) {
    for (let curVal = numVals + 1; --curVal;) {
      wrapperName = 'wrap_val' + curVal;
      cellId = 'av_val' + curVal;

      loopControl = chartSetup();
      if (loopControl) continue;
      else if (loopControl === 0) break;

      tempNodeParent = doc.createElement('div');
      tempNodeParent.className = 'h-bar-mc-container';
      tempNodeChild.className = 'h-bar-mc-wrap';
      tempNodeParent.appendChild(tempNodeChild);
      cell.appendChild(tempNodeParent);
      createBar(get(wrapperName), cellVal, 5, isVisible);
    }
  } else {
    for (let curVal = numVals + 1; --curVal;) {
      (tileType === 2
        ? (
          wrapperName = 'vp_wrap_col' + colIndex + '_val' + curVal,
          cellId = 'vp_col' + colIndex + '_val' + curVal
        )
        : (
          wrapperName = 'vo_wrap_col' + colIndex + '_val' + curVal,
          cellId = 'vo_col' + colIndex + '_val' + curVal
        )
      );

      loopControl = chartSetup();
      if (loopControl) continue;
      else if (loopControl === 0) break;

      tempNodeParent = chartContainer.cloneNode(false);
      tempNodeParent.appendChild(tempNodeChild);
      if (curVal === 1) {
        tempNodeChild.className = 'bar-wrap';
        cell.appendChild(tempNodeParent);
        // Create the chart in the new wrapper
        createBar(get(wrapperName), cellVal, 3, isVisible);
        continue;
      }
      tempNodeChild.className = 'rad-wrap-lg';
      cell.appendChild(tempNodeParent);
      createRad(get(wrapperName), cellVal, 1, isVisible);
    }
  }

  /**
  * This function will create individual radial chart data visualizations.
  *
  * @param {Element} wrapPointer - Pointer associated with the div
  * wrapper where the radial will go. (#wrap_colX_valX)
  * @param {string|number} cellVal - Value in the the cell that is being
  * replaced with the radial chart.
  * @param {number} [radSize=0] - Size of the radial created.
  * Passable values are:
  * 0 = The default radial size of 85px;
  * 1 = The larger size of 120px;
  * The sizes for these radials only effect the font size and text indent in
  * the function, the radial's actual dimensions are set by their css wrapper.
  * @param {boolean} [tileOnScreen] - True if the tile containing the radial
  * chart is on screen at the time the function is called. If this is true the
  * chart will animate, if it is false the chart will simply be set to its
  * finished position without animating.
  */
  function createRad(wrapPointer, cellVal, radSize, tileOnScreen) {
    radSize = radSize || 0;

    // Set the point the chart will animate to. Must be between 0 and 1
    const progressPortion = cellVal / 100,
          colorBrand = '#2a3c68', // The platform's primary brand color
          radChart = new ProgressBar.Circle(wrapPointer, {
            color: colorBrand, // Set the font color for the radial's value
            strokeWidth: 12, // To prevent clipping, match with the to width
            trailWidth: 12, // The width of the trail the radial animates along
            trailColor: 'rgba(79,162,217,.13)',
            easing: 'easeInOut',
            duration: 1250, // Time to reach 100% chart fullness in ms
            text: { autoStyleContainer: false },
            // Changes loading bar color & width as it progresses. The radial
            // will only reach the stated 'to' color and width at 100% mark
            from: { // Starting color and width
              color: '#39518c',
              width: 8
            },
            to: { // Ending color and width
              color: colorBrand,
              width: 12 // To prevent clipping, match with strokeWidth
            },
            // How to animate the path of the radial and the text inside it
            step: function step(state, circle) {
              const path = circle.path;
              path.setAttribute('stroke', state.color);
              path.setAttribute('stroke-width', state.width);
              circle.setText(Math.round(circle.value() * 100) + '%');
            }
          }),
          radTxtStyle = radChart.text.style;

    // Set the text style of above radial
    radTxtStyle.fontSize = (!radSize ? '20px' : '30px');
    radTxtStyle.fontWeight = '700';
    radTxtStyle.textIndent = (!radSize ? '.4ch' : '.8ch');
    // Initializes the animation of above radial if the tile is on screen
    if (tileOnScreen) radChart.animate(progressPortion);
    else radChart.set(progressPortion); // Otherwise set the radial w/o animating
  }

  /**
  * This function will create individual bar graph data visualizations.
  *
  * @param {Element} wrapPointer - Pointer associated with the div
  * wrapper where the bar will go. (wrap_colX_valX)
  * @param {string} cellVal - Value in the the cell being replaced
  * @param {number} barType - Type of bar being created. Values are:
  * 1 = vertical bar based on miles;
  * 2 = vertical bar based on minutes;
  * 3 = vertical bar based on population with the text outside of the bar;
  * 4 = horizontal bar based on a percentage;
  * 5 = horizontal bar based on an index that changes colors based on the value.
  * @param {boolean} [tileOnScreen] - True if the tile containing the radial
  * chart is on screen at the time the function is called. If this is true the
  * chart will animate, if it is false the chart will simply be set to its
  * finished position without animating.
  */
  function createBar(wrapPointer, cellVal, barType, tileOnScreen) {
    const colorBrand = '#2a3c68', // The platform's primary brand color
          colorGreen = '#6dc05d', // Color of the type 5 bars that exceed 100
          colorGray = '#524b48', // Color of the type 5 bars that equal 100
          colorRed = '#e61e25', // Color of the type 5 bars that are less than 100
          colorGreenLight = '#81c973', // Lighter color the green bars begin as
          colorGrayLight = '#625a56', // Lighter color the gray bars begin as
          colorRedLight = '#e93a40'; // Lighter color the red bars begin as

    let endText    = '',         // The label text for the value
        posType    = 'absolute', // The position type of the bar text
        leftPos    = '-30%',     // Left position value of the bar text
        topPos     = '41%',      // Top position value of the bar text
        textSize   = '18px',     // Size of the bar's value text
        textWeight = '400',
        textWidth  = '100%',     // Width of the box containing the value text
        textColor  = '#414042',  // Color of the bar's value text
        endColor   = colorBrand, // Color the bar ends as
        startColor = '#39518c', // Color the bar begins as
        progressMax; // The max value for the chart's

    cellVal *= 1;

    if (barType === 5 || barType === 4) {
      posType = 'relative',
      textSize = '1em',
      textWidth = '60px';

      (barType === 5
        ? (
          (progressMax = 300,
          leftPos = '125px',
          topPos = '-20px') &&
          (cellVal > 100
            ? (
              endColor = colorGreen,
              startColor = colorGreenLight
            )
            : (cellVal < 100
              ? (
                endColor = colorRed,
                startColor = colorRedLight
              )
              : (
                endColor = colorGray,
                startColor = colorGrayLight
              )
            )
          )
        )
        : (
          progressMax = 100,
          endText = '%',
          leftPos = '5em',
          topPos = '-25px',
          textWeight = '700'
        )
      );
    } else if (barType === 3) {
      progressMax = 2500000;
      posType = 'relative',
      leftPos = '-65px', // Bottom value in barType 1-3 due to rotation
      topPos = '-75px',  // Actually the left value due to chart rotation
      textSize = '22px',
      textWeight = '700';
    } else {
      if (cellVal > 15) {
        textColor = '#fff';
      } else {
        textColor = colorBrand,
        textWeight = '700';
      }
      if (barType === 2) {
        endText = ' mins',
        progressMax = 120;
      } else {
        endText = ' miles',
        progressMax = 30;
      };
    }

    // Set the point the chart will animate to. Must be between 0 and 1
    const progressPortion = (cellVal >= progressMax
            ? 1
            : cellVal / progressMax
          ),
          exceedsLimit = cellVal > progressMax,
          barChart = new ProgressBar.Line(wrapPointer, {
            color: endColor, // Sets the color that the bar will be
            trailColor: 'rgba(79,162,217,.13)',
            easing: 'easeInOut',
            duration: 1250, // Time to reach 100% chart fullness in ms
            svgStyle: {
              width: '100%',
              height: '100%'
            },
            text: {
              style: {
                color: textColor,
                fontSize: textSize,
                fontWeight: textWeight,
                left: leftPos,
                position: posType,
                top: topPos,
                width: textWidth
              },
              autoStyleContainer: false
            },
            from: { color: startColor }, // Set the starting color of the bar
            to: { color: endColor }, // Sets the color of the bar at 100%

            // How to animate the path of the bar and the text inside it
            step: function step(state, chart) {
              const path = chart.path;
              let barValTxt; // Value of the bar as it animates towards cellVal

              path.setAttribute('stroke', state.color);

              if (barType === 5 || barType === 4) {
                barValTxt = Math.round(chart.value() * progressMax);

                if (barType === 5) {
                  (barValTxt === cellVal) &&
                    (path.setAttribute('stroke', endColor));
                  (exceedsLimit && barValTxt === progressMax) &&
                    (barValTxt = cellVal|0);
                }
              } else if (barType === 3) {
                const noCommaVal = (chart.value() * progressMax)|0;

                barValTxt = (exceedsLimit && noCommaVal === progressMax
                  ? addCommas(cellVal)
                  : addCommas(noCommaVal)
                );
              } else {
                barValTxt = (chart.value() * progressMax).toFixed(1);

                if (exceedsLimit && barValTxt == progressMax) {
                  barValTxt = cellVal.toFixed(1);
                }
              }
              chart.setText(barValTxt + endText);
            }
          }),
          barTxtStyle = barChart.text.style;

    if (barType < 4) {
      barTxtStyle.transform = 'rotate(90deg)';

      if (cellVal < 38 && cellVal > 14 && barType !== 3) {
        barTxtStyle.textShadow = '#000 1px 1px';
      }
    }

    // Initialize the animation of above bar if the tile is visible on screen
    if (tileOnScreen) barChart.animate(progressPortion);
    else barChart.set(progressPortion); // Otherwise merely set it w/o animating

    // Apply tooltips on hover to the bars
    if (barType === 5) {
      if (cellVal > 100) {
        barChart._container.title =
          'Green colored bars indicate that the value is greater than 100';

        if (cellVal > progressMax) {
          setTimeout(function () {
            barTxtStyle.fontWeight = '700';
            barChart.text.title =
              'Bold values are a visual indication that the 300 point ' +
              'scale of the chart has been been exceeded.';
          }, 1245);
        }
      } else if (cellVal < 100) {
        barChart._container.title =
          'Red colored bars indicate that the value is below 100';
      } else {
        barChart._container.title =
          'Gray colored bars indicate that the value is equal to 100';
      }
    } else if (barType === 4) {
      barChart._container.title = (wrapPointer.id.slice(-1) === '6'
        ? 'Millennials - ' + cellVal + '%'
        : wrapPointer.id.slice(-1) === '7'
          ? 'Generation X - ' + cellVal + '%'
          : 'Baby Boomers - ' + cellVal + '%'
      );
    }
  }

  /**
  * Creates a 'Data currently unavailable' message and appends it
  * to a given table cell.
  *
  * @param {Element} cell - Pointer associated with the cell where
  * the msg goes.
  * @param {string|boolean} [msgWidth] - Optional argument that sets the width
  * of the unavailable message's div wrapper.
  */
  function unavailableMsg(cell, msgWidth) {
    const nodeNoData = doc.createElement('div'),
          nodeTxt = doc.createTextNode('Data currently unavailable');

    // Clear out node of content
    while (cell.lastChild) {
      cell.removeChild(cell.lastChild);
    }

    nodeNoData.className = 'unavail-msg';

    if (msgWidth) {
      if (msgWidth === '90%') {
        // Defines the msg style for the KPI Population & Avg Income rows
        nodeNoData.classList.add('unavail-msg--sm');
      }
      nodeNoData.style.width = msgWidth;
    } else {
      // Defines the msg style for the horizontal-mc bars
      nodeNoData.classList.add('unavail-msg--mc-bars');
    }
    nodeNoData.appendChild(nodeTxt);
    cell.appendChild(nodeNoData);
  }

  /**
  * Returns true if the given element is visible on screen, even if it is only
  * partially visible.
  *
  * @param {string|node} element - The ID selector for the element.
  * @return {boolean} True if the element is visible on screen, false if not.
  */
  function isOnScreen(element) {
    element = get(element);

    if (element) {
      const win = this,
            elBounds = element.getBoundingClientRect(),
            scrollTop = win.pageYOffset,
            elTop = elBounds.y + scrollTop;

      return (
        elTop < (win.innerHeight + scrollTop) &&
        elTop > (scrollTop - elBounds.height)
      );
    }
  }

  /**
  * Shorthand function that simply take an ID as input and returns the element
  * using document.getElementById().
  *
  * @param {string} id - The ID attribute of any element.
  * @return {Element} - An element with a matching id from the document.
  */
  function get(id) {
    return doc.getElementById(id);
  }
}

/**
* Animates the data in the Key Performance Indicator table, cycling through the
* columns from the last to the first and swapping out values for data
* visualizations. Skips column 3 as that is the Index column that has no
* charts.
*
* @param {boolean} [isCompShown=true] - True if the Competitor column is
* visible, false if it is hidden.
* @param {boolean|string|number} [forceNoAnim=0] - Pass anything except false,
* 0, null, or undefined when being called on the initial page load or any other
* time that forcing the charts to appear without animating is desired.
*/
function animKpi(isCompShown, forceNoAnim) {
  forceNoAnim = forceNoAnim || 0;

  for (
    let colIndex = (typeof isCompShown === 'undefined' || isCompShown ? 6 : 5),
        first = colIndex - 1;
    --colIndex;
  ) {
    if (colIndex !== 3) {
      setTimeout(function () {
        animCol(colIndex, 0, forceNoAnim);
      }, (first === colIndex ? 0 : 105));
    }
  }
}

/**
* Animates the data in either the Visitor Origin or the Visitor Penetration
* table, cycling through the columns from the last to the first and swapping
* out values for data visualizations.
*
* @param {number} tileNum - Pass 1 to animate the Visitor Origin tile's
* charts, pass 2 to animate the Visitor Penetration tile's charts.
* @param {boolean} [isCompShown=true] - True if the Competitor column is
* visible, false if it is hidden.
*/
function animVoVp(tileNum, isCompShown) {
  isCompShown = typeof isCompShown === 'undefined'
    ? compVisibility.state()
    : isCompShown;

  // If there is a competitor shown, there are no VO charts visible
  if (tileNum === 2 || !isCompShown) {
    for (let colIndex = (tileNum === 2 && isCompShown ? 4 : 3); --colIndex;) {
      setTimeout(function () {
        animCol(colIndex, tileNum);
      }, 20);
    }
  }
}

/**
* Activates sorting functionality on all table elements of a given class name.
* The user can click on a table's header cell once to sort the table according
* to the ascending order or that cell's column. Clicking the same header cell
* again will reverse the sort into descending order. Clicking a third time
* returns the table to its original order. The function sorting a variety of
* data types, including integers, floats, strings, currency, percentages, and
* dates in the standardized formats accepted by the Date.parse() API. The
* implementations works in in conjunction with the following CSS classes which
* are applied by the function: '.table-sort-header', 'table-sort-asc', and
* '.table-sort-desc', which are applied to the th elements that are children of
* the thead element and provide a visual indication for the sorting feature
* itself and the current sorting order (ascending, descending, or unsorted).
*
* @param {string} tables - The class name for the table(s) having the sorting
* feature applied.
*/
function activateTableSorting(tables) {
  const l = parseFloat,
        m = u(/^(?:\s*)([+-]?(?:\d+)(?:,\d{3})*)(\.\d*)?$/g, /,/g),
        g = u(/^(?:\s*)([+-]?(?:\d+)(?:\.\d{3})*)(,\d*)?$/g, /\./g);

  tables = document.getElementsByClassName(tables);

  for (let i = getLen(tables); i--;) {
    try {
      activateSorting(tables[i]);
    } catch (err) {}
  }

  function getLen(x) {
    return x.length;
  }

  function t(n, t) {
    if (n) {
      for (let i = 0, a = getLen(n); i < a; ++i) {
        t(n[i], i);
      }
    }
  }

  function revStr(str) {
    return str.split('').reverse().join('');
  }

  function a(n) {
    let e = n[0];

    return t(n, function (n) {
      while (!n.startsWith(e)) {
        e = e.substring(0, getLen(e) - 1);
      }
    }), getLen(e);
  }

  function o(n, r) {
    return -1 != n.map(r).indexOf(!0);
  }

  function u(n, r) {
    return function (t) {
      let e = '';

      return (
        t.replace(n, function (n, t, a) {
          return (e = t.replace(r, '') + '.' + (a || '').substring(1));
        }), l(e)
      );
    };
  }

  function i(n) {
    const t = l(n);

    return !isNaN(t) && getLen('' + t) + 1 >= getLen(n) ? t : NaN;
  }

  function s(n) {
    let e = [];

    return t([i, m, g], function (t) {
      let a;

      getLen(e) || o((a = n.map(t)), isNaN) || (e = a);
    }), e;
  }

  function c(n) {
    let t = s(n);

    if (!getLen(t)) {
      const o = a(n),
            u = a(n.map(revStr)),
            i = n.map(function (n) {
              return n.substring(o, getLen(n) - u);
            });

      t = s(i);
    }
    return t;
  }

  function f(n) {
    const r = n.map(Date.parse);

    return o(r, isNaN) ? [] : r;
  }

  function v(n, r) {
    r(n),
    t(n.childNodes, function (n) {
      v(n, r);
    });
  }

  function d(n) {
    const t = [],
          e = [];
    let r;

    return v(n, function (node) {
      const curNodeName = node.nodeName,
            parentNodeName = node.parentNode.nodeName;

      if (parentNodeName !== 'TFOOT') {
        (curNodeName === 'TR'
          ? ((r = []), t.push(r), e.push(node))
          : ((curNodeName === 'TD' || curNodeName === 'TH') && r.push(node))
        );
      }
    }), [t, e];
  }

  function activateSorting(table) {
    if (table.nodeName === 'TABLE') {
      for (
        var e = d(table),
            a = e[0],
            o = e[1],
            u = getLen(a),
            i = u > 1 && (getLen(a[0]) < getLen(a[1]) ? 1 : 0),
            s = i + 1,
            v = a[i],
            p = getLen(v),
            l = [],
            m = [],
            g = [],
            h = s;
        h < u;
        ++h
      ) {
        for (let index = 0; index < p; ++index) {
          const T = a[h][index],
                C = T.textContent || T.innerText || '';

          getLen(m) < p && m.push([]);
          m[index].push(C.trim());
        }
        g.push(h - s);
      }

      const classNameSortAsc = 'table-sort-asc',
            classNameSortDesc = 'table-sort-desc',
            b = function () {
              let n = p;
              while (n--) {
                const headerClasses = v[n].classList;

                headerClasses.remove(classNameSortAsc),
                headerClasses.remove(classNameSortDesc),
                (l[n] = 0);
              }
            };

      t(v, function (headerCell, t) {
        l[t] = 0;
        const headerClasses = headerCell.classList;

        headerClasses.add('table-sort-header'),
        headerCell.addEventListener('click', function () {
          // A sort-compare function
          function n(k, r) {
            const t = d[k],
                  e = d[r];

            return (t > e ? a : e > t ? -a : a * (k - r));
          }

          let a = l[t];

          b(),
          a = (a == 1 ? -1 : +!a),
          a && headerClasses.add(a > 0 ? classNameSortAsc : classNameSortDesc),
          (l[t] = a);

          const i = m[t];
          let v = function (n, r) { // Other possible sort-compare function
                return a * i[n].localeCompare(i[r]) || a * (n - r);
              },
              d = c(i),
              parentEl = null;

          (getLen(d) || getLen((d = f(i)))) && (v = n);

          const p = g.slice();
          p.sort(v);

          for (let nodeIndex = s; nodeIndex < u; ++nodeIndex) {
            (parentEl = o[nodeIndex].parentNode),
            parentEl.removeChild(o[nodeIndex]);
          }
          for (let q = s; q < u; ++q) {
            parentEl.appendChild(o[s + p[q - s]]);
          }
        });
      });
    }
  }
}

/**
* Initiates the window interface's open() method if the window if the window
* does not already exist or has been closed. If it does exist and is open then
* the window is focused.
*
* @param {string} url A DOMString indicating the URL of rthe resource to be
* loaded. This can be a path or URL to an HTML page, image file, or any other
* browser supported resource. If an empty string ('') is specified as the url,
* a blank page is opened into the targeted browsing context.
* @param {string} windowName A DOMString specifying the name of the browsing
* context (window, iframe, or tab) into which to load the specified resource.
* If the name doesn't indicate an existing context, a new window is created and
* is given the name specified by windowName. The name should not contain any
* whitespace. Keep in mind that this will NOT be used as the window's title.
* @param {string} [windowFeatures] - A DOMString containing a comma-separated
* list of window features given with their corresponding values in the form of
* "name=value". The features list string must not contain whitespace.
*/
function requestNewView(url, windowName, windowFeatures) {
  if (WIN_HANDLER === null || WIN_HANDLER.closed) {
    WIN_HANDLER = window.open(url, windowName, windowFeatures);
  } else {
    WIN_HANDLER.focus();
  }
}

/**
* Provides a function for a child window to call that will return an array of
* HTMLElements containing the elements matching selector which have a display
* value that is not 'none'. This function is called in the insert-reports.js
* file and the array of elements is unpacked in the overview-report.html file.
*
* @param {string} selector - The selector used in a querySelectorAll method.
* @return {Element[]} The array of Elements found by selector
* and then filtered to not include any elements with a display value of 'none'.
*
* @see insert-reports.js
*/
function sendPageElements(selector) {
  const doc = document,
        allElements = doc.querySelectorAll(selector),
        numElements = allElements.length,
        locName = doc.getElementById('OVERVIEW_BUILD.V.R1.TBLOCATION').value,
        visibleElArray = [],
        compVisible = compVisibility.state();

  let cloneEl,
      curEl,
      curClass,
      curTileMaps,
      curMap,
      screenMapUrlStr;

  for (let i = 0; i < numElements; ++i) {
    curEl = allElements[i],
    curClass = curEl.classList[1];

    // Filter out the tiles that are not currently visible and those that
    // are not being used for the report
    if (
      (curClass !== 'def-tile' && curClass !== 'vo-tile-sub') ||
      (curClass === 'vo-tile-sub' && compVisible)
    ) {
      // Clone the node to prevent taking the actual element from the DOM
      cloneEl = allElements[i].cloneNode(true);

      // The map-based tile's maps gets replaced with the print version
      if (
        curClass === 'vo-tile-top' || curClass === 'vo-tile-sub' ||
        curClass === 'bav-tile' || curClass === 'vp-tile'
      ) {
        // Get the current tile's map's iframes
        curTileMaps = cloneEl.querySelectorAll('.js-map');
        // Loop through each map (There will usually just be one)
        for (let k = curTileMaps.length; k--;) {
          curMap = curTileMaps[k];
          screenMapUrlStr = curMap.getAttribute('src');
          // Replace the map with print version
          curMap.setAttribute(
            'src',
            strSplice(
              screenMapUrlStr, screenMapUrlStr.lastIndexOf('/') + 1, 0, 'print'
            )
          );
        }
      }
      // Package the cloned node into an array to send to the print window
      visibleElArray.push(cloneEl);
    }
  }

  // Attach the name of the selected location to the end of the array
  visibleElArray.push(locName.substring(0, locName.lastIndexOf('-')));

  return visibleElArray;

  /**
  * The strSplice function changes the content of a string by removing a
  * range of characters and/or adding new characters and then returning the
  * new string.
  *
  * @param {string} str - The original string into which another string is
  * being spliced.
  * @param {number} index - Index at which to start changing the string.
  * @param {number} delCount - An integer indicating the number of old chars
  * to remove.
  * @param {string} subStr - The new substring that is spliced in.
  * @return {string} A new string with the spliced substring.
  */
  function strSplice(str, index, delCount, subStr) {
    return str.slice(0, index) + subStr + str.slice(index + delCount);
  }
}

/**
* This function calls the Sweet Alert API to initiate an Access Restriction
* popup modal.
*/
function accessCheckRefresh() {
  swal({
    title: 'Access Restriction',
    text: 'You do not have access to this module. Please contact us to receive access.',
    icon: 'error',
    button: 'OK'
  });
}

/**
* Handles all the function calls necessary after the tiles (viewboxes) on
* the Overview page have been refreshed from a location or benchmark changing.
*/
function locOnChangeNonMap() {
  modifyValues(); // Add commmas to KPI table pop and avg income rows
  setTimeout(function () {
    setTimeout(function () {
      animCol(0, 3); // Anim AvPV table
    }, 200);
    animKpi(compVisibility.state()); // Anim KPI table
  }, 1e3);
  activateTableSorting('ttzc-table');
}

/**
* Determines the name of the correct viewbox layout from an assortment of
* different input data types.
*
* @param {number|boolean|string} [layout] - Pass 0, false, or 'Default' for
* the Default layout, pass 1, true, or 'NoCompetitor' for the No Competitor
* layout. If no value is passed then the competitor visibility is checked using
* compVisibility.state() and layout is based on the returning value. If a
* string is passed, that string is used as the name of the layout.
* @return {string} - The name of the proper layout to switch to, given what was
* passed in by layout.
*/
function determineLayout(layout) {
  return (
    typeof layout === 'undefined'
      ? (compVisibility.state() ? 'Default' : 'NoCompetitor')
      : !layout
        ? 'Default'
        : typeof layout === 'string'
          ? layout
          : 'NoCompetitor'
  );
}

/**
* Set and store the current Overview page's layout state after a change event
* on the Overview page's Competitor search field.
*
* @param {string} val - The value of the Overview page's Competitor search
* field's associated hidden field.
*/
function setLayoutState(val) {
  const cbx = document.getElementById('no_comp_cbx');

  if (val === '0') {
    compVisibility.hide();
  } else {
    cbx.removeAttribute('disabled');
    cbx.checked = false;
    if (!compVisibility.state()) compVisibility.show();
  }
}

/**
* Reset the layout for all the Overview tiles (viewboxes) that do not contain
* maps.
*/
function resetNonMapLayoutState() {
  if (!compVisibility.state()) swapNonMapLayouts(1);
  else (swapNonMapLayouts(0));
}

/**
* Reset the layout for the Visitor Origin tile (viewbox).
*/
function resetVoLayoutState() {
  if (!compVisibility.state()) swapTileLayout('viewbox_visorig', 1);
  else swapTileLayout('viewbox_visorig', 0);
}

/**
* Reset the layout for the Visitor Penetration tile.
*/
function resetVpLayoutState() {
  if (!compVisibility.state()) swapTileLayout('viewbox_vispen', 1);
  else swapTileLayout('viewbox_vispen', 0);
}

/**
* Page setup/initialization with event handlers
*/
!function (win, doc) {
  function breakLoad(e) {
    e = e || window.event;

    if (e.isComposing || e.keyCode === 229) {
      return;
    }

    if (e.ctrlKey && e.keyCode === 66) {
      e.preventDefault();
      doc.getElementById('loading_screen').style.display = 'none';
      doc.removeEventListener('keydown', breakLoad);
    }

    e.stopPropagation();
  }

  doc.addEventListener('keydown', breakLoad);

  doc.addEventListener('DOMContentLoaded', function (e) {
    activateTableSorting('ttzc-table');
    e.stopPropagation();
  });

  win.addEventListener('resize', function () {
    setTimeout(modifyValues, 2000);
    setTimeout(function () {
      const compVisible = compVisibility.state();

      // Only re-chart the VO table if a competitor is not visible
      if (!compVisible) {
        for (let colIndex = 3; --colIndex;) {
          animCol(colIndex, 1, true);
        }
      }

      animKpi(compVisible, true);

      // Animate VP table
      setTimeout(function () {
        for (let colIndex = (compVisible ? 4 : 3); --colIndex;) {
          animCol(colIndex, 2, true);
        }
      }, 0);

      animCol(0, 3, true); // Animate Actual Vs. Potential Visitors table

      activateTableSorting('ttzc-table');
    }, 3000); // Wait for viewboxes to finish refreshing
  });

  win.addEventListener('load', function () {
    doc.removeEventListener('keydown', breakLoad);

    modifyValues();
    setTimeout(function () {
      jQuery('#loading_screen').fadeOut(3e3);
      animKpi(true, true); // Animate KPI Table
      animCol(0, 3, true); // Animate AvPV table
      // Animate VP table (VO table has no charts showing on page load)
      for (let i = 1; i < 4; i++) animCol(i, 2, true);
    }, 3000);
  }, false);
}(this, document);
