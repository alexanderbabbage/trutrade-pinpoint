/**
 * @name animate-charts.js
 *
 * @file This JavaScript file is designed to provide multiple elements of
 * visual functionality to application data. It can take the the values from a
 * table and replace them with several different animated data visualizations
 * that fit within the table cell. It provides sort, fade in, fade out, and
 * element removing and adding to tables and also provides functionality for
 * adding an 'open' state to dropdowns so that their appearance can be altered.
 *
 * This is the 3.0 Version of the file.
 *
 * @author    Stephen M Irving
 * @version   2.3.0, 05/20/19
 * @requires  progressbar.min.js https://github.com/kimmobrunfeldt/progressbar.js
**/

// eslint-disable-next-line no-unused-vars
const _animate = {
  /**
   * @name stripCommas
   * @description Takes a string of a number with commas in it and strips the
   * commas out of the string, returning the number value.
   * @public
   *
   * @param {string} x - A string representing a number that is displayed with
   * commas
   *
   * @return {number} The number value of the string with the commas
   * stripped
  **/
  stripCommas: function(x) {
    return (x + '').replace(/\,/g, '') * 1;
  },

  /**
   * @name hideCol
   * @description Hides a table column, removing it from the table
   * @public
   *
   * @param {string} elementClass - The class name for the table cells being hidden
   * @param {string} innerClass - The class name for the content wrapper in those
   * cells
  **/
  hideCol: function(elementClass, innerClass) {
    'use strict';
    const COL_ELEMENTS = document.getElementsByClassName(elementClass),
          H_BAR_WRAP = document.getElementsByClassName('h-bar-container');

    Array.prototype.forEach.call(document.getElementsByClassName(innerClass),
      function(el) {
        el.style.display = 'none';
      }
    );

    Array.prototype.forEach.call(COL_ELEMENTS, function(el) {
      el.style.width = '1px';
      el.style.minWidth = '1px';
    });

    setTimeout(function() {
      Array.prototype.forEach.call(COL_ELEMENTS, function(el) {
        el.style.display = 'none';
      });
      if (H_BAR_WRAP.length > 0) {
        Array.prototype.forEach.call(H_BAR_WRAP, function(el) {
          el.style.marginLeft = '32%';
        });
      }
    }, 550);
  },

  /**
   * @name showCol
   * @description Displays a previously hidden table column
   * @public
   *
   * @param {string} elementClass - The class name for the table cells being
   * unhidden
   * @param {string} innerClass - The class name for the content wrapper in those
   * cells
  **/
  showCol: function(elementClass, innerClass) {
    'use strict';
    const H_BAR_WRAP = document.getElementsByClassName('h-bar-container');

    if (H_BAR_WRAP.length > 0) {
      Array.prototype.forEach.call(H_BAR_WRAP, function(el) {
        el.style.marginLeft = '26.5%';
      });
    }

    Array.prototype.forEach.call(document.getElementsByClassName(elementClass),
      function(el) {
        el.style.width = '120px';
        el.style.display = 'table-cell';
      }
    );

    Array.prototype.forEach.call(document.getElementsByClassName(innerClass),
      function(el) {
        el.style.display = 'block';
      }
    );
  },

  /**
   * @name animCol
   * @description Replaces the values of a column with corresponding data
   * animations
   * @public
   *
   * @param {number} [colIndex=0] - The index number of the column whose
   * values are to be replaced with chart elements (index starts at 0)
  **/
  animCol: function(colIndex) {
    'use strict';
    // If no argument is passed for colIndex set it to 0
    colIndex = colIndex || 0;

    const NUM_VALS = 9;

    let wrapperName, // The name of all the chart's wrapper's cells
        cellId,      // Placeholder for the IDs of the cells
        cell,        // The cell itself
        cellVal;     // The cell's value to be converted

    // curVal is the current value in the column at that moment in the loop, it
    // starts with 1 at the top of the table
    for (let curVal = 1; curVal < (NUM_VALS + 1); curVal++) {
      wrapperName = 'wrap_col' + colIndex + '_val' + curVal,
      cellId = 'col' + colIndex + '_val' + curVal,
      cell = document.getElementById(cellId);

      try {
        cellVal = cell.innerHTML.trim();
      }
      catch (e) {
        if (e instanceof TypeError) {
          console.warn('There is no cell with the ID of "' + cellId + '".');
          break;
        }
        else {
          logMyErrors(e);
          break;
        }
      }

      // Input scrubbing
      if (cellVal.substring(0, 3) === 'n/a') continue;
      if (dataUnavailable(cellVal)) {
        (curVal == 1 || curVal == 5)
          ? unavailableMsg(cell, '90%')
          : unavailableMsg(cell, '92px');
        continue;
      }
      // Fixes possibility of charts sitting on top of one another when swapping
      if (isNaN(cellVal)) break;
      // End of input scrubbing

      // Animate the Population and Avg Income by counting up to their value
      if (curVal == 1 || curVal == 5) {
        countUp(cellId);
        continue;
      }
      else if (colIndex === 1 && curVal == 2) {
        // Skips the MSA col Avg Dwell Time because it will always be 'n/a'
        continue;
      }
      else if (curVal == 2) {
        // Animate the Avg Dwell Time for the other columns
        // Replace the nums in the cells with the corresponding bar wrappers
        cell.innerHTML = '<div class="chart-container"><div id="' +
            wrapperName + '" class="bar-wrap"></div></div>';
        createBar(document.getElementById(wrapperName), cellVal, 2);
        continue;
      } // Animate the percentage radials
      else if ((curVal > 2 && curVal < 5) || curVal == 9) {
        // Replace the cell's content with the corresponding radial wrappers
        cell.innerHTML = '<div class="chart-container"><div id="' +
            wrapperName + '" class="rad-wrap"></div></div>';
        createRad(document.getElementById(wrapperName), cellVal);
        continue;
      }
      else { // The only curVal values left are for the horizontal % bar charts
        cell.innerHTML = '<div class="h-bar-container"><div id="' +
            wrapperName + '" class="h-bar-wrap"></div></div>';
        createBar(document.getElementById(wrapperName), cellVal, 4);
      }
    } // End the for loop going through all the column's values


    /**
     * @name createRad
     * @description This function will create individual radial charts.
     * @private
     *
     * @param {HTMLElement} wrapperName - Pointer associated with the div
     * wrapper where the radial will go. (#wrap_colX_valX)
     * @param {number} progressNum - Value in the the cell being replaced with a
     * radial
     * @param {number} [radSize=0] - Size of the radial created.
     * Passable values are:
     * 0 = The default radial size of 85px;
     * 1 = The larger size of 120px;
     * The sizes for these radials only effect the font size and text indent in
     * the function, the radial's actual dimensions are set by their css wrapper
    **/
    function createRad(wrapperName, progressNum) {
      // Set the default radSize to 1 if no value is passed
      radSize = radSize || 0;

      // Set the point the chart will animate to. Must be between 0 and 1
      const PROGRESS    = progressNum / 100.0;

      // eslint-disable-next-line one-var
      const RAD = new ProgressBar.Circle(wrapperName, {
        color: '#2A3C68', // Set the font color for the radial's center value
        strokeWidth: 12,  // To Prevent clipping set to the width it will animate to
        trailWidth: 12,   // The width of the trail the radial will animate along
        trailColor: '#E2E2E2', // The color of the trail
        easing: 'easeInOut',   // Animation easing function
        duration: 1300,   // Sets the total time in ms for the rad to reach 100%
        text: { autoStyleContainer: false },
        // Changes loading bar color & width as it progresses. Keep in mind: The
        // radial bar will only reach the stated 'to' color and width at 100% mark
        from: { // Starting color and width
          color: '#39518C',
          width: 8
        },
        to: { // Ending color and width
          color: '#2A3C68',
          width: 12 // To prevent clipping, make width the same as strokeWidth
        },
        // Animate the path of the radial and the text inside it
        step: function step(state, circle) {
          circle.path.setAttribute('stroke', state.color);
          circle.path.setAttribute('stroke-width', state.width);
          circle.setText(Math.round(circle.value() * 100) + '%');
        }
      });
      // Set the text style of above radial
      RAD.text.style.fontSize = '20px',
      RAD.text.style.fontWeight = '700',
      RAD.text.style.textIndent = '.4ch';
      RAD.animate(PROGRESS); // Initializes the animation of above radial
    }

    /**
     * @name createBar
     * @description This function will create vertical bar graphs.
     * @private
     *
     * @param {HTMLElement} wrapperName - Pointer associated with the div
     * wrapper where the bar will go. (wrap_colX_valX)
     * @param {number} progressNum - Value in the the cell being replaced
     * @param {number} barType - Type of bar being created. Values are:
     * 1 = vertical bar based on miles;
     * 2 = vertical bar based on minutes;
     * 3 = horizontal bar based on a percentage;
    **/
    function createBar(wrapperName, progressNum, barType) {
      'use strict';

      let endText    = '',         // The label text after the value
          posType    = 'absolute', // The position type of the bar text
          leftPos    = '-30%',     // Left position value of the bar text
          topPos     = '41%',      // Top position value of the bar text
          textColor  = '#414042',  // Color of the bar's value text
          textSize   = '18px',     // Size of the bar's value text
          textWidth  = '100%',     // Width of the box containing the value text
          barValText,  // Value of the bar as it animates towards progressNum
          progressMax; // The max value for the chart's animation

      if (isBarType(1)) {
        progressMax = 30,
        endText = ' miles',
        textColor = '#FFF';
      }
      else if (isBarType(2)) {
        progressMax = 120,
        endText = ' mins',
        textColor = '#FFF';
      }
      else {
        posType = 'relative',
        textSize = '16px',
        textWidth = '60px',
        progressMax = 100,
        endText = ' %',
        leftPos = '100px',
        topPos = '-27px';
      }

      // Set the point the chart will animate to. Must be between 0 and 1
      const PROGRESS = (
        progressNum >= progressMax
          ? 1
          : progressNum / progressMax
      );

      // eslint-disable-next-line one-var
      const BAR = new ProgressBar.Line(wrapperName, {
        color: endColor, // Sets the color that the bar will be
        trailColor: '#F1F2F2', // The color of the trail
        easing: 'easeInOut', // Animation easing function
        duration: 1300, // Sets the total time in ms for the bar to reach 100%
        svgStyle: { width: '100%', height: '100%' },
        text: { // Set the attributes of the text inside the bar
          style: {
            color: textColor,
            fontSize: textSize,
            fontWeight: '400',
            left: leftPos,
            position: posType,
            top: topPos,
            width: textWidth
          },
          autoStyleContainer: false
        },
        from: { color: startColor }, // Set the starting color of the bar
        to: { color: endColor }, // Sets the color of the bar if it reaches 100%

        // Animate the path of the bar and the text inside it
        step: function step(state, chart) {
          chart.path.setAttribute('stroke', state.color);
          (isBarType(1) || isBarType(2)
            ? (barValText = (chart.value() * progressMax).toFixed(1),
              (progressNum > progressMax && barValText == progressMax)
                && (barValText = (progressNum * 1).toFixed(1)))
            : (isBarType(3)
                && (barValText = Math.round(chart.value() * progressMax)))
          );
          chart.setText(barValText + endText);
        }
      });
      (isBarType(1) || isBarType(2))
          && (BAR.text.style.transform = 'rotate(90deg)',
              BAR.text.style.textShadow = '1px 1px #A7A9AC');

      BAR.animate(PROGRESS); // Initializes the animation of above bar

      // Apply tooltips on hover to the bars
      (isBarType(3)) && (BAR._container.title = (
        wrapperName.id.slice(-1) == 6
          ? 'Millennials - ' + progressNum + '%'
          : wrapperName.id.slice(-1) == 7
            ? 'Generation X - ' + progressNum + '%'
            : 'Baby Boomers - ' + progressNum + '%'
      ));

      /**
       * @name isBarType
       * @description Checks if the passed bar type is the current bar type
       * @private
       *
       * @param {number} type - The number value that represents the bar type. See
       * the comments at the head of the createBar function for the type key.
       *
       * @return {boolean} Returns true if the current bar type is equal to the
       * passed type
      **/
      function isBarType(type) {
        return barType === type;
      }
    }

    /**
     * @name countUp
     * @description Takes a value and visually counts up to the number on the page.
     * @private
     *
     * @param {string} elementId - The ID of the element containing the number
     * being counted
     * @param {number} [startVal=0] - The starting value, defaults to 0
    **/
    function countUp(elementId, startVal) {
      'use strict';

      startVal = startVal || 0;
      const finalVal = document.getElementById(elementId).textContent,
            increment = (finalVal / 77)|0;
      // increment is the number added to the displayed value on every subsequent
      // increase. 77 is an arbitrary number chosen to make the timing close to other
      // animations. Bitwise OR method (|0) converts the float into an int by
      // truncating everything after the decimal place.

      let curVal = startVal;

      document.getElementById(elementId).textContent = curVal;
      const counter = setInterval(function() {
        if (curVal >= finalVal) {
          clearInterval(counter);
          document.getElementById(elementId).textContent = (
            elementId.slice(-1) === '5'
              ? '$' + addCommas(finalVal)
              : addCommas(finalVal)
          );
        }
        else {
          curVal = stripCommas(document.getElementById(elementId).textContent)|0;
          curVal += increment;
          document.getElementById(elementId).textContent = addCommas(curVal);
        }
      }, 1);
    }

    /**
     * @name dataUnavailable
     * @description Data validation function. Takes the trimmed innerHTML value of
     * a table cell or the HTML element and returns true if the data is
     * unavailable, determined by the value not being a number AND not having HTML
     * tags, OR being equal to either 0 OR an empty string. If those conditions
     * aren't meant, returns false.
     * @private
     *
     * @param {string} cellVal - The inner contents of a table cell or HTML element
     *
     * @return {boolean} True for data not available, false when data is available
    **/
    function dataUnavailable(cellVal) {
      // Regular expression tests true if string can be converted to a float or
      // integer (tests false for negative numbers)
      const IS_NUM = /^[0-9]*\.?[0-9]+$/,
            HAS_TAGS = /[<>]+/; // Tests true if string has < or >

      let notAvailable = false;

      ((!IS_NUM.test(cellVal) && !HAS_TAGS.test(cellVal)) || cellVal === 0
          || cellVal === '') && (notAvailable = true);

      /** @type {boolean} */
      return notAvailable;
    }


    /**
     * @name unavailableMsg
     * @description Creates a 'data currently unavailable' message and appends it
     * to a given table cell
     * @private
     *
     * @param {HTMLElement} cell - Pointer associated with the cell where
     * the msg goes
     * @param {string} [msgWidth] - Optional argument that sets the width of the
     * unavailable message's div wrapper
    **/
    function unavailableMsg(cell, msgWidth) {
      const TEXT_MSG = document.createTextNode('data currently unavailable'),
            NODE = document.createElement('div');
      cell.innerHTML = '',
      NODE.style.fontSize = '.9em',
      NODE.style.fontVariant = 'small-caps',
      NODE.style.margin = '0 auto';
      if (typeof msgWidth !== 'undefined') {
        // Define the msg style for the KPI Population & Avg Income rows
        (msgWidth === '90%') && (NODE.style.fontSize = '.7em');
        NODE.style.width = msgWidth;
      }
      NODE.appendChild(TEXT_MSG);
      cell.appendChild(NODE);
    }

    /**
     * @name addCommas
     * @description Takes a number or a string representing a number and adds
     * commas to it. Displays numbers with more than 2 decimal places incorrectly,
     * so do not use on floats without using toFixed first, or refactoring the
     * regular expression.
     * @private
     *
     * @param {string|number} x - A number or a string representing a number
     *
     * @return {string} The number displayed with commas
    **/
    function addCommas(x) {
      return (x + '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }
  },

  /**
   * @name animKpi
   * @description Animates the data in a table, cycling through the columns and
   * swapping out values for data visualizations.
   * @public
  **/
  animKpi: function() {
    for (let i = 1; i < 6; i++) {
      if (i === 3) continue;
      animCol(i);
    }
  },

  /**
   * @name fadeOut
   * @description Fades out an HTML element
   * @public
   *
   * @param {HTMLElement} element - HTML element being faded out
   * @param {number} [time=50] - Time in ms each iteration of the fade out takes.
   * 50ms default
  **/
  fadeOut: function(element, time) {
    time = time || 50;
    let opacity = 1; // Set initial opacity of layer at the start of effect
    const timer = setInterval(function() {
      if (opacity <= 0.1) {
        opacity = 0,
        element.style.display = 'none';
        clearInterval(timer);
      }
      element.style.filter = 'alpha(opacity=' + opacity * 100 + ')',
      element.style.opacity = opacity;
      opacity -= opacity * 0.1;
    }, time);
  },

  /**
   * @name fadeIn
   * @description Fades in an HTML element
   * @public
   *
   * @param {HTMLElement} element - HTML element being faded in
   * @param {number} [time=30] - Time in ms each iteration of the fade out takes.
   * 30ms default
   * @param {string} [displayType=block] - The CSS display type.
  **/
  fadeIn: function(element, time, displayType) {
    time = time || 30;
    displayType = displayType || 'block';
    element.style.opacity = 0,
    element.style.display = displayType;

    let opacity = 0.1; // Set initial opacity of layer at start of effect
    const timer = setInterval(function() {
      if (opacity >= 1) {
        opacity = 1;
        clearInterval(timer);
      }
      element.style.filter = 'alpha(opacity=' + opacity * 100 + ')',
      element.style.opacity = opacity;
      opacity += opacity * 0.1;
    }, time);
  },

  /**
   * @name toggleDropdown
   * @description Used for an onclick event on a dropdown. If the dropdown is
   * closed it will apply the open class styling, if it is already open it will
   * remove the open class.
   * @public
   *
   * @param {string} id - The ID of the dropdown that has been opened or closed
  **/
  toggleDropdown: function(id) {
    var dropDown = document.getElementById(id);

    if (dropDown.classList) {
      dropDown.classList.toggle('open');
    }
    else {
      // For IE9
      var classes = element.className.split(' '),
          i = classes.indexOf('open');

      (i >= 0 ? classes.splice(i, 1) : classes.push('open'));

      dropDown.className = classes.join(' ');
    }
  },

  /**
   * @name resetDropdown
   * @description Resets a dropdown to its closed state on blur
   * @public
   *
   * @param {string} id - The ID of the dropdown that is being reset to closed
  **/
  resetDropdown: function(id) {
    document.getElementById(id).className = dropDown.className.
      replace(/\bopen\b/g, '').trim();
  }
};
