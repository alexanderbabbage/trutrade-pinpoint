/**
 * @name animate-charts.js
 *
 * @file This JavaScript file is designed to provide multiple elements of
 * visual functionality to application data. It can take the the values from a
 * table and replace them with several different animated data visualizations
 * that fit within the table cell. It provides sort, fade in, fade out, and
 * element removing and adding to tables and also provides functionality for
 * adding an 'open' state to dropdowns so that their appearance can be altered.
 *
 * @author    Stephen M Irving
 * @version   2.3.1, 05/20/19
 * @requires  progressbar.min.js - https://github.com/kimmobrunfeldt/progressbar.js
**/

/** exported stripCommas */
/**
 * @name stripCommas
 * @description Takes a string of a number with commas in it and strips the
 * commas out of the string, returning the number value.
 * @public
 *
 * @param {string} x - A string representing a number that is displayed with
 * commas
 *
 * @return {number} The number value of the string with the commas
 * stripped
**/
function stripCommas(x) {
return (x + '').replace(/\,/g, '') * 1;
}

/* exported hideCol */
/**
 * @name hideCol
 * @description Hides a table column, removing it from the table
 * @public
 *
 * @param {string} elementClass - The class name for the table cells being hidden
 * @param {string} innerClass - The class name for the content wrapper in those
 * cells
**/
function hideCol(elementClass, innerClass) {
  'use strict';
  const COL_ELEMENTS = document.getElementsByClassName(elementClass),
        H_BAR_WRAP = document.getElementsByClassName('h-bar-container'),
        H_BAR_MC_WRAP = document.getElementsByClassName('h-bar-mc-container');

  Array.prototype.forEach.call(document.getElementsByClassName(innerClass),
    function(el) {
      el.style.display = 'none';
    }
  );

  Array.prototype.forEach.call(COL_ELEMENTS, function(el) {
    el.style.width = '1px';
    el.style.minWidth = '1px';
  });

  setTimeout(function() {
    Array.prototype.forEach.call(COL_ELEMENTS, function(el) {
      el.style.display = 'none';
    });
    if (H_BAR_WRAP.length > 0) {
      Array.prototype.forEach.call(H_BAR_WRAP, function(el) {
        el.style.marginLeft = '32%';
      });
    }
    else if (H_BAR_MC_WRAP.length > 0) {
      Array.prototype.forEach.call(H_BAR_MC_WRAP, function(el) {
        el.style.marginLeft = '10%';
      });
    }
  }, 550);
}

/* exported showCol */
/**
 * @name showCol
 * @description Displays a previously hidden table column
 * @public
 *
 * @param {string} elementClass - The class name for the table cells being
 * unhidden
 * @param {string} innerClass - The class name for the content wrapper in those
 * cells
 *
 * @todo Implement column revealing animation
**/
function showCol(elementClass, innerClass) {
  'use strict';
  const H_BAR_WRAP = document.getElementsByClassName('h-bar-container'),
        H_BAR_MC_WRAP = document.getElementsByClassName('h-bar-mc-container');

  if (H_BAR_WRAP.length > 0) {
    Array.prototype.forEach.call(H_BAR_WRAP, function(el) {
      el.style.marginLeft = '26.5%';
    });
  }
  else if (H_BAR_MC_WRAP.length > 0) {
    Array.prototype.forEach.call(H_BAR_MC_WRAP, function(el) {
      el.style.marginLeft = '0';
    });
  }

  Array.prototype.forEach.call(document.getElementsByClassName(elementClass),
    function(el) {
      el.style.width = '120px';
      el.style.display = 'table-cell';
    }
  );

  Array.prototype.forEach.call(document.getElementsByClassName(innerClass),
    function(el) {
      el.style.display = 'block';
    }
  );
}

/** exported animCol */
/**
 * @name animCol
 * @description Replaces the values of a column with corresponding data
 * animations
 * @public
 *
 * @param {number} [colIndex=0] - The index number of the column whose
 * values are to be replaced with chart elements (index starts at 0)
 * @param {number} [tileType=0] - A number that relates to a given tile. If no
 * value is passed then 0 is the default. Values are:
 * 0 = Key Performance Indicator tile;
 * 1 = Visitor Origin Tile;
 * 2 = Visitor Penetration Tile;
 * 3 = Actual vs Potential Visitors Tile;
**/
function animCol(colIndex, tileType) {
  'use strict';
  // If no argument is passed for colIndex set it to 0
  colIndex = colIndex || 0;

  // If no argument is passed for tileType, or if it falls out of the current
  // range of available tile types for this function, set it to the default of 0
  tileType = (
    (typeof tileType !== 'undefined' && tileType < 4 && tileType >= 0)
      ? tileType : 0
  );

  const NUM_VALS = (tileType === 0 ? 9 : tileType === 3 ? 23 : 2);

  let wrapperName, // The name of all the chart's wrapper's cells
      cellId,      // Placeholder for the IDs of the cells
      cell,        // The cell itself
      cellVal;     // The cell's value to be converted

  // curVal is the current value in the column at that moment in the loop, it
  // starts with 1 at the top of the table
  for (let curVal = 1; curVal < (NUM_VALS + 1); curVal++) {
    wrapperName = (
      tileType === 3 ? 'wrap_val' + curVal
        : 'wrap_col' + colIndex + '_val' + curVal
    );
    cellId = (
      tileType === 3 ? 'av_val' + curVal : 'col' + colIndex + '_val' + curVal
    );

    (tileType === 1
      ? (cellId = 'vo_' + cellId, wrapperName = 'vo_' + wrapperName)
      : (tileType === 2) &&
        (cellId = 'vp_' + cellId, wrapperName = 'vp_' + wrapperName)
    );

    cell = document.getElementById(cellId);

    try {
      cellVal = cell.innerHTML.trim();
    }
    catch (e) {
      if (e instanceof TypeError) {
        console.warn('There is no cell with the ID of "' + cellId + '".');
        break;
      }
      else {
        logMyErrors(e);
        break;
      }
    }

    // Input scrubbing
    if (cellVal.substring(0, 3) === 'n/a') continue;
    if (dataUnavailable(cellVal)) {
      if (NUM_VALS === 2) {
        unavailableMsg(cell, '120px');
      }
      else if (tileType == 0) {
        if (curVal == 1 || curVal == 5) unavailableMsg(cell, '90%');
        else unavailableMsg(cell, '92px');
      }
      else {
        unavailableMsg(cell);
      }
      continue;
    }
    // Fixes possibility of charts sitting on top of one another when swapping
    if (isNaN(cellVal)) break;
    // End of input scrubbing

    if (NUM_VALS === 2) { // This targets tile types 1 and 2
      if (curVal == 1) {
        // Replace the nums in the cells with the corresponding bar wrapper
        cell.innerHTML = '<div class="chart-container"><div id="' +
            wrapperName + '" class="bar-wrap"></div></div>';
        createBar(document.getElementById(wrapperName), cellVal, 3);
        continue;
      }
      else {
        // Replace the nums in the cells with the corresponding radial wrapper
        cell.innerHTML = '<div class="chart-container"><div id="' +
            wrapperName + '" class="rad-wrap-lg"></div></div>';
        createRad(document.getElementById(wrapperName), cellVal, 1);
        continue;
      }
    }
    else if (tileType === 0) {
      // Animate the Population and Avg Income by counting up to their value
      if (curVal == 1 || curVal == 5) {
        countUp(cellId);
        continue;
      }
      else if (colIndex === 1 && curVal == 2) {
        // Skips the MSA col Avg Dwell Time because it will always be 'n/a'
        continue;
      }
      else if (curVal == 2) {
        // Animate the Avg Dwell Time for the other columns
        // Replace the nums in the cells with the corresponding bar wrappers
        cell.innerHTML = '<div class="chart-container"><div id="' +
            wrapperName + '" class="bar-wrap"></div></div>';
        createBar(document.getElementById(wrapperName), cellVal, 2);
        continue;
      } // Animate the percentage radials
      else if ((curVal > 2 && curVal < 5) || curVal == 9) {
        // Replace the cell's content with the corresponding radial wrappers
        cell.innerHTML = '<div class="chart-container"><div id="' +
            wrapperName + '" class="rad-wrap"></div></div>';
        createRad(document.getElementById(wrapperName), cellVal);
        continue;
      }
      else { // The only curVal values left are for the horizontal % bar charts
        cell.innerHTML = '<div class="h-bar-container"><div id="' +
            wrapperName + '" class="h-bar-wrap"></div></div>';
        createBar(document.getElementById(wrapperName), cellVal, 4);
      }
    }
    else { // This targets tile type 3
      // Replace the nums in the cells with the corresponding bar wrappers
      cell.innerHTML = '<div class="h-bar-mc-container"><div id="' +
          wrapperName + '" class="h-bar-mc-wrap"></div></div>';

      createBar(document.getElementById(wrapperName), cellVal, 5);
    }
  } // End the for loop going through all the column's values


  /**
   * @name createRad
   * @description This function will create individual radial charts.
   * @private
   *
   * @param {HTMLElement} wrapperName - Pointer associated with the div
   * wrapper where the radial will go. (#wrap_colX_valX)
   * @param {number} progressNum - Value in the the cell being replaced with a
   * radial
   * @param {number} [radSize=0] - Size of the radial created.
   * Passable values are:
   * 0 = The default radial size of 85px;
   * 1 = The larger size of 120px;
   * The sizes for these radials only effect the font size and text indent in
   * the function, the radial's actual dimensions are set by their css wrapper
  **/
  function createRad(wrapperName, progressNum, radSize) {
    // Set the default radSize to 1 if no value is passed
    radSize = radSize || 0;

    // Set the point the chart will animate to. Must be between 0 and 1
    const PROGRESS    = progressNum / 100.0,
          TEXT_SIZE   = (radSize === 0 ? '20px' : '30px'),
          TEXT_OFFSET = (radSize === 0 ? '.4ch' : '.8ch');

    // eslint-disable-next-line one-var
    const RAD = new ProgressBar.Circle(wrapperName, {
      color: '#2A3C68', // Set the font color for the radial's center value
      strokeWidth: 12,  // To Prevent clipping set to the width it will animate to
      trailWidth: 12,   // The width of the trail the radial will animate along
      trailColor: '#E2E2E2', // The color of the trail
      easing: 'easeInOut',   // Animation easing function
      duration: 1300,   // Sets the total time in ms for the rad to reach 100%
      text: { autoStyleContainer: false },
      // Changes loading bar color & width as it progresses. Keep in mind: The
      // radial bar will only reach the stated 'to' color and width at 100% mark
      from: { // Starting color and width
        color: '#39518C',
        width: 8
      },
      to: { // Ending color and width
        color: '#2A3C68',
        width: 12 // To prevent clipping, make width the same as strokeWidth
      },
      // Animate the path of the radial and the text inside it
      step: function step(state, circle) {
        circle.path.setAttribute('stroke', state.color);
        circle.path.setAttribute('stroke-width', state.width);
        circle.setText(Math.round(circle.value() * 100) + '%');
      }
    });
    // Set the text style of above radial
    RAD.text.style.fontSize = TEXT_SIZE;
    RAD.text.style.fontWeight = '700';
    RAD.text.style.textIndent = TEXT_OFFSET;
    RAD.animate(PROGRESS); // Initializes the animation of above radial
  }

  /**
   * @name createBar
   * @description This function will create vertical bar graphs.
   * @private
   *
   * @param {HTMLElement} wrapperName - Pointer associated with the div
   * wrapper where the bar will go. (wrap_colX_valX)
   * @param {number} progressNum - Value in the the cell being replaced
   * @param {number} barType - Type of bar being created. Values are:
   * 1 = vertical bar based on miles;
   * 2 = vertical bar based on minutes;
   * 3 = vertical bar based on population with the text outside of the bar;
   * 4 = horizontal bar based on a percentage;
   * 5 = horizontal bar based on an index that changes colors based on value;
  **/
  function createBar(wrapperName, progressNum, barType) {
    'use strict';

    const GREEN = '#6DC05D',    // Color of the type 5 bars that exceed 100
          GRAY  = '#524B48',    // Color of the type 5 bars that equal 100
          RED   = '#E61E25',    // Color of the type 5 bars that are less than 100
          GREEN_LT = '#81C973', // 6% lighter color the green bars will begin as
          GRAY_LT  = '#625A56', // 6% lighter color the gray bars will begin as
          RED_LT   = '#E93A40'; // 6% lighter color the red bars will begin as

    let endText    = '',         // The label text after the value
        posType    = 'absolute', // The position type of the bar text
        leftPos    = '-30%',     // Left position value of the bar text
        topPos     = '41%',      // Top position value of the bar text
        textColor  = '#414042',  // Color of the bar's value text
        textSize   = '18px',     // Size of the bar's value text
        textWeight = '400',
        textWidth  = '100%',     // Width of the box containing the value text
        endColor   = '#2A3C68',  // Color the bar ends as
        startColor = '#39518C',  // Color the bar begins as
        barValText,  // Value of the bar as it animates towards progressNum
        progressMax; // The max value for the chart's animation

    if (isBarType(1)) {
      progressMax = 30,
      endText = ' miles',
      textColor = '#FFF';
    }
    else if (isBarType(2)) {
      progressMax = 120,
      endText = ' mins',
      textColor = '#FFF';
    }
    else if (isBarType(3)) {
      progressMax = 5000000;
      posType = 'relative',
      leftPos = '-65px', // Bottom value in barType 1-3 due to rotation
      topPos = '-75px',  // Actually the left value due to chart rotation
      textSize = '22px',
      textWeight = '700';
    }
    else {
      posType = 'relative',
      textSize = '16px',
      textWidth = '60px';

      (isBarType(4)
        ? (progressMax = 100, endText = ' %',
        leftPos = '100px', topPos = '-27px')
        : ((progressMax = 300, leftPos = '120px', topPos = '-25.5px') &&
        (progressNum > 100
          ? (endColor = GREEN, startColor = GREEN_LT)
          : progressNum < 100
            ? (endColor = RED, startColor = RED_LT)
            : (endColor = GRAY, startColor = GRAY_LT)
        ))
      );
    }

    // Set the point the chart will animate to. Must be between 0 and 1
    const PROGRESS = (progressNum >= progressMax ? 1 : progressNum / progressMax);

    // eslint-disable-next-line one-var
    const BAR = new ProgressBar.Line(wrapperName, {
      color: endColor, // Sets the color that the bar will be
      trailColor: '#F1F2F2', // The color of the trail
      easing: 'easeInOut', // Animation easing function
      duration: 1300, // Sets the total time in ms for the bar to reach 100%
      svgStyle: { width: '100%', height: '100%' },
      text: { // Set the attributes of the text inside the bar
        style: {
          color: textColor,
          fontSize: textSize,
          fontWeight: textWeight,
          left: leftPos,
          position: posType,
          top: topPos,
          width: textWidth
        },
        autoStyleContainer: false
      },
      from: { color: startColor }, // Set the starting color of the bar
      to: { color: endColor }, // Sets the color of the bar if it reaches 100%

      // Animate the path of the bar and the text inside it
      step: function step(state, chart) {
        chart.path.setAttribute('stroke', state.color);
        if (isBarType(1) || isBarType(2)) {
          barValText = (chart.value() * progressMax).toFixed(1);
          if (progressNum > progressMax && barValText == progressMax) {
            barValText = (progressNum * 1).toFixed(1);
          }
        }
        else if (isBarType(3)) {
          // Bitwise or (|0) parses string into an int with maximum optimization
          barValText = addCommas((chart.value() * progressMax)|0);
          if (progressNum > progressMax &&
              stripCommas(barValText) == progressMax) {
            barValText = addCommas(progressNum);
          }
        }
        else if (isBarType(4) || isBarType(5)) {
          barValText = Math.round(chart.value() * progressMax);
          if (isBarType(5)) {
            (barValText == progressNum) && chart.path.setAttribute('stroke', endColor);
            (progressNum > progressMax && barValText == progressMax) &&
              (barValText = progressNum|0);
          }
        }
        chart.setText(barValText + endText);
      }
    });
    if (isBarType(1) || isBarType(2) || isBarType(3)) {
      BAR.text.style.transform = 'rotate(90deg)';

      if (isBarType(1) || isBarType(2)) {
        BAR.text.style.textShadow = '1px 1px #A7A9AC';
      }
    }

    BAR.animate(PROGRESS); // Initializes the animation of above bar

    // Apply tooltips on hover to the bars
    if (isBarType(4)) {
      BAR._container.title = (
        wrapperName.id.slice(-1) == 6
          ? 'Millennials - ' + progressNum + '%'
          : wrapperName.id.slice(-1) == 7
            ? 'Generation X - ' + progressNum + '%'
            : 'Baby Boomers - ' + progressNum + '%'
      );
    }
    else if (isBarType(5)) {
      if (progressNum < 100) {
        BAR._container.title = 'Red colored bars indicate that the value is ' +
            'below 100';
      }
      else if (progressNum == 100) {
        BAR._container.title = 'Gray colored bars indicate that the value is ' +
            'equal to 100';
      }
      else {
        BAR._container.title = 'Green colored bars indicate that the value is ' +
            'greater than 100';

        if (progressNum > progressMax) {
          setTimeout(function() {
            BAR.text.style.fontWeight = '700';
            BAR.text.title = 'Bold values are a simple visual indication that ' +
                'the 300 point scale of the chart has been been exceeded.';
          }, 1295);
        }
      }
    }

    /**
     * @name isBarType
     * @description Checks if the passed bar type is the current bar type
     * @private
     *
     * @param {number} type - The number value that represents the bar type. See
     * the comments at the head of the createBar function for the type key.
     *
     * @return {boolean} Returns true if the current bar type is equal to the
     * passed type
    **/
    function isBarType(type) {
      return barType === type;
    }
  }

  /**
   * @name countUp
   * @description Takes a value and visually counts up to the number on the page.
   * @private
   *
   * @param {string} elementId - The ID of the element containing the number
   * being counted
   * @param {number} [startVal=0] - The starting value, defaults to 0
  **/
  function countUp(elementId, startVal) {
    'use strict';

    startVal = startVal || 0;
    const finalVal = document.getElementById(elementId).textContent,
          increment = (finalVal / 77)|0;
    // increment is the number added to the displayed value on every subsequent
    // increase. 77 is an arbitrary number chosen to make the timing close to other
    // animations. Bitwise OR method (|0) converts the float into an int by
    // truncating everything after the decimal place.

    let curVal = startVal;

    document.getElementById(elementId).textContent = curVal;
    const counter = setInterval(function() {
      if (curVal >= finalVal) {
        clearInterval(counter);
        document.getElementById(elementId).textContent = (
          elementId.slice(-1) === '5' ? '$' + addCommas(finalVal)
            : addCommas(finalVal)
        );
      }
      else {
        curVal = stripCommas(document.getElementById(elementId).textContent)|0;
        curVal += increment;
        document.getElementById(elementId).textContent = addCommas(curVal);
      }
    }, 1);
  }

  /**
   * @name dataUnavailable
   * @description Data validation function. Takes the trimmed innerHTML value of
   * a table cell or the HTML element and returns true if the data is
   * unavailable, determined by the value not being a number AND not having HTML
   * tags, OR being equal to either 0 OR an empty string. If those conditions
   * aren't meant, returns false.
   * @private
   *
   * @param {string} cellVal - The inner contents of a table cell or HTML element
   *
   * @return {boolean} True for data not available, false when data is available
  **/
  function dataUnavailable(cellVal) {
    // Regular expression tests true if string can be converted to a float or
    // integer (tests false for negative numbers)
    const IS_NUM = /^[0-9]*\.?[0-9]+$/,
          HAS_TAGS = /[<>]+/; // Tests true if string has < or >

    let notAvailable = false;

    if (
      (!IS_NUM.test(cellVal) && !HAS_TAGS.test(cellVal)) ||
          cellVal === 0 || cellVal === ''
    ) {
      notAvailable = true;
    }

    /** @type {boolean} */
    return notAvailable;
  }


  /**
   * @name unavailableMsg
   * @description Creates a 'data currently unavailable' message and appends it
   * to a given table cell
   * @private
   *
   * @param {HTMLElement} cell - Pointer associated with the cell where
   * the msg goes
   * @param {string} [msgWidth] - Optional argument that sets the width of the
   * unavailable message's div wrapper
  **/
  function unavailableMsg(cell, msgWidth) {
    const TEXT_MSG = document.createTextNode('data currently unavailable'),
          NODE = document.createElement('div');
    cell.innerHTML = '';
    NODE.style.fontSize = '.9em';
    NODE.style.fontVariant = 'small-caps';
    NODE.style.margin = '0 auto';
    if (typeof msgWidth !== 'undefined') {
      if (msgWidth === '90%') {
        // Defines the msg style for the KPI Population & Avg Income rows
        NODE.style.fontSize = '.7em';
      }
      NODE.style.width = msgWidth;
    }
    else {
      // Defines the msg style for the horizontal-mc bars
      NODE.style.fontSize = '.75em';
      NODE.style.textAlign = 'left';
      NODE.style.marginLeft = '-2px';
    }
    NODE.appendChild(TEXT_MSG);
    cell.appendChild(NODE);
  }

  /**
   * @name addCommas
   * @description Takes a number or a string representing a number and adds
   * commas to it. Displays numbers with more than 2 decimal places incorrectly,
   * so do not use on floats without using toFixed() first, or refactoring the
   * regular expression.
   * @private
   *
   * @param {string|number} x - A number or a string representing a number
   *
   * @return {string} The number displayed with commas
  **/
  function addCommas(x) {
    return (x + '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }
}

/* exported animKpi */
/**
 * @name animKpi
 * @description Animates the data in a table, cycling through the columns and
 * swapping out values for data visualizations.
 * @public
**/
function animKpi() {
  for (let i = 1; i < 6; i++) {
    if (i === 3) continue;
    animCol(i);
  }
}

/* exported fadeOut */
/**
 * @name fadeOut
 * @description Fades out an HTML element
 * @public
 *
 * @param {HTMLElement} element - HTML element being faded out
 * @param {number} [time=50] - Time in ms each iteration of the fade out takes.
 * 50ms default
**/
function fadeOut(element, time) {
  time = time || 50;
  let opacity = 1; // Set initial opacity of layer at the start of effect
  const timer = setInterval(function() {
    if (opacity <= 0.1) {
      opacity = 0;
      element.style.display = 'none';
      clearInterval(timer);
    }
    element.style.opacity = opacity;
    element.style.filter = 'alpha(opacity=' + opacity * 100 + ')';
    opacity -= opacity * 0.1;
  }, time);
}

/**
 * @name fadeIn
 * @description Fades in an HTML element
 * @public
 *
 * @param {HTMLElement} element - HTML element being faded in
 * @param {number} [time=30] - Time in ms each iteration of the fade out takes.
 * 30ms default
 * @param {string} [displayType=block] - The CSS display type.
 */
function fadeIn(element, time, displayType) {
  time = time || 30;
  displayType = displayType || 'block';
  element.style.opacity = 0;
  element.style.display = displayType;

  let opacity = 0.1; // Set initial opacity of layer at start of effect
  const timer = setInterval(function() {
    if (opacity >= 1) {
      opacity = 1;
      clearInterval(timer);
    }
    element.style.opacity = opacity;
    element.style.filter = 'alpha(opacity=' + opacity * 100 + ')';
    opacity += opacity * 0.1;
  }, time);
}

/* exported noCompSelected */
/**
 * @name noCompSelected
 * @description Transitions the Visitor Origin tiles when no competitor is
 * selected
 * @public
**/
function noCompSelected() {
  const LEFT_LABEL = document.getElementById('vo_label_left');

  document.getElementById('sub_tile_content').style.display = 'none';
  LEFT_LABEL.style.display = 'none';

  fadeOut(document.getElementById('sub_tile'), 30);
  fadeOut(document.getElementById('vo_map_right'), 30);

  document.getElementById('vo_label_right').style.display = 'none';

  setTimeout(function() {
    document.getElementById('vo_map_left').style.width = '70%';
    LEFT_LABEL.style.width = '70%';
  }, 150);

  setTimeout(function() {
    fadeIn(document.getElementById('no_comp_charts'), 30, 'inline-block');
    fadeIn(LEFT_LABEL, 50, 'inline-block');

    setTimeout(function() {
      animCol(1, 1);
      animCol(2, 1);
    }, 150);
  }, 1300);
}

/* exported compSelected */
/**
 * @name compSelected
 * @description Transitions the Visitor Origin and Visitor Penetration tiles
 * when a competitor is selected again after the user has selected no competitor
 * @public
**/
function compSelected() {
  const LEFT_LABEL = document.getElementById('vo_label_left');

  document.getElementById('sub_tile_content').style.display = 'block';

  fadeIn(document.getElementById('sub_tile'), 20);

  LEFT_LABEL.style.display = 'none';
  document.getElementById('no_comp_charts').style.display = 'none';

  setTimeout(function() {
    document.getElementById('vo_map_left').style.width = '49%';
  }, 50);

  LEFT_LABEL.style.width = '49%';

  setTimeout(function() {
    fadeIn(document.getElementById('vo_map_right'), 30, 'inline-block');
    fadeIn(document.getElementById('vo_label_right'), 30, 'inline-block');
    fadeIn(LEFT_LABEL, 20, 'inline-block');
  }, 1300);
}

/* exported removeCol */
/**
 * @name removeCol
 * @description Removes the competitor column on the Visitor Penetration Tile
 * @public
 *
 * @todo Integrate this function with the hideCol & noCompSelected functions
**/
function removeCol() {
  const COMP_COL = document.getElementsByClassName('vp-comp-col');

  if (
    COMP_COL.length > 0 &&
        window.getComputedStyle(COMP_COL[0]).getPropertyValue('display') ===
        'table-cell'
  ) {
    Array.prototype.forEach.call(COMP_COL, function(cell) {
      fadeOut(cell);
    });
  }
}

/* exported addCol */
/**
 * @name addCol
 * @description Adds the competitor column on the Visitor Penetration Tile back
 * to the table after it has been removed by the user selecting no competitor
 * @public
 *
 * @todo Integrate this function with the showCol & compSelected functions so
 * they are all either one function or one function that calls multiple
 * sub-functions
**/
function addCol() {
  const COMP_COL = document.getElementsByClassName('vp-comp-col');

  if (COMP_COL.length > 0 && COMP_COL[0].style.display === 'none') {
    Array.prototype.forEach.call(COMP_COL, function(cell) {
      fadeIn(cell, 40, 'table-cell');
    });
  }
}

/* exported makeAllSortable */
/**
 * @name makeAllSortable
 * @description Makes the specified tables sortable. If no arguments are passed
 * it will make all tables on the page it is run on sortable.
 * @public
 *
 * @param {string} [tableClass] - Class name for the table or tables you want to
 * be sortable. If undefined function will instead look for all table elements
 * @param {HTMLElement} [parent=document.body] - The parent element of the tables
 * specified in the tableClass argument
**/
function makeAllSortable(tableClass, parent) {
  'use strict';

  parent = document.getElementById(parent) || document.body;

  const table = (
    typeof tableClass !== 'undefined'
      ? parent.getElementsByClassName(tableClass)
      : parent.getElementsByTagName('table')
  );

  let numTables = table.length;

  // Run through every table on the page and make them all sortable
  while (--numTables >= 0) {
    makeSortable(table[numTables]);
  }

  /**
   * @name fixComparison
   * @description Fixes to values for comparison in sortTable
   * @private
   *
   * @param {string} a - First item being compared
   * @param {string} b - Second item being compared
   *
   * @return {string[]} The adjusted a and b fixed for comparison
  **/
  function fixComparison(a, b) {
    let diff;

    if (a.length !== b.length) {
      if (a.length > b.length) {
        diff = a.length - b.length;
        for (let k = 0; k < diff; k++) {
          b = '0' + b;
        }
      }
      else {
        diff = b.length - a.length;
        for (let k = 0; k < diff; k++) {
          a = '0' + a;
        }
      }
    }

    return [a, b];
  }

  /**
   * @name sortTable
   * @description Takes a given table and column, as well as whether or not the
   * column has already been reversed, as input, and then sorts the table column
   * @private
   *
   * @param {HTMLElement} table
   * @param {HTMLElement} col
   * @param {number} reverse - 1 for true 0 for false
   *
   * @todo Refactor logic for optimization & implement a datetime sorting
   * solution for possible future use.
  **/
  function sortTable(table, col, reverse) {
    const T_BODY = table.tBodies[0], // ignore <thead> and <tfoot> rows
          IS_NUM = /^[0-9]*\.?[0-9]+$/;

    let tRows = Array.prototype.slice.call(T_BODY.rows, 0); // Rows into an array

    reverse = -((+reverse) || -1);

    tRows = tRows.sort(function(a, b) { // Sort rows
      let tempA = a.cells[col].textContent.trim(),
          tempB = b.cells[col].textContent.trim();

      if (tempA.slice(-1) === '%' || tempB.slice(-1) === '%') {
        if (tempA.slice(-1) === '%') {
          tempA = tempA.slice(0, tempA.length - 1);
        }
        if (tempB.slice(-1) === '%') {
          tempB = tempB.slice(0, tempB.length - 1);
        }
        [tempA, tempB] = fixComparison(tempA, tempB);
      }
      else if (tempA.substring(0, 1) === '$' || tempB.substring(0, 1) === '$') {
        if (tempA.substring(0, 1) === '$') {
          tempA = tempA.slice(1, tempA.length);
        }
        if (tempB.substring(0, 1) === '$') {
          tempB = tempB.slice(1, tempB.length);
        }
        [tempA, tempB] = fixComparison(tempA, tempB);
      }
      else if (IS_NUM.test(stripCommas(tempA)) ||
          IS_NUM.test(stripCommas(tempB))) {
        if (IS_NUM.test(stripCommas(tempA))) {
          tempA = stripCommas(tempA) + '';
        }
        if (IS_NUM.test(stripCommas(tempB))) {
          tempB = stripCommas(tempB) + '';
        }
        [tempA, tempB] = fixComparison(tempA, tempB);
      }

      return reverse * (tempA.localeCompare(tempB));
    });

    for (let i = 0; i < tRows.length; ++i) {
      T_BODY.appendChild(tRows[i]); // append each row in order
    }
  }

  /**
   * @name makeSortable
   * @description Makes a table sortable
   * @private
   *
   * @param {HTMLElement} table - The table being sorted
  **/
  function makeSortable(table) {
    let th = table.tHead,
        i;

    th && (th = th.rows[0]) && (th = th.cells);

    if (th) {
      i = th.length;
    }
    else {
      return; // if no '<thead>' section then do nothing
    }

    while (--i >= 0) {
      (function(i) {
        var dir = 1;
        th[i].addEventListener('click', function() {
          sortTable(table, i, (dir = 1 - dir));
        });
      }(i));
    }
  }
}

/* exported toggleDropdown */
/**
 * @name toggleDropdown
 * @description Used for an onclick event on a dropdown. If the dropdown is
 * closed it will apply the open class styling, if it is already open it will
 * remove the open class.
 * @public
 *
 * @param {string} id - The ID of the dropdown that has been opened or closed
**/
function toggleDropdown(id) {
  var dropDown = document.getElementById(id);

  if (dropDown.classList) {
    dropDown.classList.toggle('open');
  }
  else {
    // For IE9
    var classes = element.className.split(' '),
        i = classes.indexOf('open');

    (i >= 0 ? classes.splice(i, 1) : classes.push('open'));

    dropDown.className = classes.join(' ');
  }
}

/* exported resetDropdown */
/**
 * @name resetDropdown
 * @description Resets a dropdown to its closed state on blur
 * @public
 *
 * @param {string} id - The ID of the dropdown that is being reset to closed
**/
function resetDropdown(id) {
  var dropDown = document.getElementById(id);
  dropDown.className = dropDown.className.replace(/\bopen\b/g, '').trim();
}

/* exported swapMapUrl */
/**
 * @name swapMapUrl
 * @description Takes a location value from the location dropdown's option
 * value and the tile type and swaps out the iframe map's src URL with the
 * new value. Run this function on the location dropdown's onchange event.
 * @public
 *
 * @param {number} locVal - The location ID pulled from the location
 * dropdown's option element's value
 * @param {boolean} [isComp=false] - True if the swap is for a competitor
 */
function swapMapUrl(locVal, isComp) {
  'use strict';

  isComp = isComp || false;
  const FRAME_IDS = [
          'map_iframe_vo_left',
          'map_iframe_vo_right',
          'map_iframe_vo_full',
          'map_iframe_vp'
        ],

        URL_BASE = 'http://trutradedevelop.alexanderbabbage.com/maptt3/' +
                  'overviewmap.html?centers=' + locValue + '&layerid=';

  let newUrl = URL_BASE;

  isComp ? FRAME_IDS.shift() : FRAME_IDS.splice(1, 1);

  for (let i = 0; i < 3; i++) {
    newUrl += i + 5;
    try {
      document.getElementById(FRAME_IDS[i]).setAttribute('src', newUrl);
    }
    catch (e) {
      if (e instanceof TypeError) {
        console.warn('There is no element with the ID of "' + FRAME_IDS[i] + '".');
      }
      else {
        logMyErrors(e);
      }
    }
  }
}
