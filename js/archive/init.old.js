(function (doc) {
  function loadScript(url, callback) {
    var d = doc,
        script = d.createElement('script');

    if (callback) {
      if (script.readyState) { // IE
        script.onreadystatechange = function () {
          if (script.readyState == 'loaded' || script.readyState == 'complete') {
            script.onreadystatechange = null;
            callback();
          }
        };
      } else { // Others
        script.onload = callback;
      };
    }

    script.src = url;
    d.getElementsByTagName('head')[0].appendChild(script);
  }

  var cb = function () {
    
  };

  if (
    /Edge\//.test(navigator.userAgent) ||
    navigator.userAgent.match(/Trident\/7\./)
  ) {
    loadScript('js/main.ie.min.js', (function () {
      activateTableSorting('ttzc-table', 'ttzc_tile');
    }));
  } else {
    loadScript('js/main.min.js', (function () {
      activateTableSorting('ttzc-table', 'ttzc_tile');
    }));
  }
}(document));
