function makeAllSortable(tableClass, parent) {
  'use strict';

  parent = document.getElementById(parent) || document.body;

  const table = (
    typeof tableClass !== 'undefined'
      ? parent.getElementsByClassName(tableClass)
      : parent.getElementsByTagName('table')
  );

  let numTables = table.length;

  // Run through every table on the page and make them all sortable
  while (--numTables >= 0) {
    makeSortable(table[numTables]);
  }

  function fixComparison(a, b) {
    let diff;

    if (a.length !== b.length) {
      if (a.length > b.length) {
        diff = a.length - b.length;
        for (let k = 0; k < diff; k++) {
          b = '0' + b;
        }
      }
      else {
        diff = b.length - a.length;
        for (let k = 0; k < diff; k++) {
          a = '0' + a;
        }
      }
    }

    return [a, b];
  }

  function sortTable(table, col, reverse) {
    const T_BODY = table.tBodies[0], // ignore <thead> and <tfoot> rows
          IS_NUM = /^[0-9]*\.?[0-9]+$/;

    let tRows = Array.prototype.slice.call(T_BODY.rows, 0); // Rows into an array

    reverse = -((+reverse) || -1);

    tRows = tRows.sort(function(a, b) { // Sort rows
      let tempA = a.cells[col].textContent.trim(),
          tempB = b.cells[col].textContent.trim();

      if (tempA.slice(-1) === '%' || tempB.slice(-1) === '%') {
        if (tempA.slice(-1) === '%') {
          tempA = tempA.slice(0, tempA.length - 1);
        }
        if (tempB.slice(-1) === '%') {
          tempB = tempB.slice(0, tempB.length - 1);
        }
        [tempA, tempB] = fixComparison(tempA, tempB);
      }
      else if (tempA.substring(0, 1) === '$' || tempB.substring(0, 1) === '$') {
        if (tempA.substring(0, 1) === '$') {
          tempA = tempA.slice(1, tempA.length);
        }
        if (tempB.substring(0, 1) === '$') {
          tempB = tempB.slice(1, tempB.length);
        }
        [tempA, tempB] = fixComparison(tempA, tempB);
      }
      else if (IS_NUM.test(stripCommas(tempA)) ||
          IS_NUM.test(stripCommas(tempB))) {
        if (IS_NUM.test(stripCommas(tempA))) {
          tempA = stripCommas(tempA) + '';
        }
        if (IS_NUM.test(stripCommas(tempB))) {
          tempB = stripCommas(tempB) + '';
        }
        [tempA, tempB] = fixComparison(tempA, tempB);
      }

      return reverse * (tempA.localeCompare(tempB));
    });

    for (let i = 0; i < tRows.length; ++i) {
      T_BODY.appendChild(tRows[i]); // append each row in order
    }
  }

  function makeSortable(table) {
    let th = table.tHead,
        i;

    th && (th = th.rows[0]) && (th = th.cells);

    if (th) {
      i = th.length;
    }
    else {
      return; // if no '<thead>' section then do nothing
    }

    while (--i >= 0) {
      (function(i) {
        var dir = 1;
        th[i].addEventListener('click', function() {
          sortTable(table, i, (dir = 1 - dir));
        });
      }(i));
    }
  }
}
