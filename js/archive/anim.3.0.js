function t(t) {
  return 1 * (t + '').replace(/\,/g, '');
}

function hideCol(t, e) {
  'use strict';
  const n = document.getElementsByClassName(t),
        l = document.getElementsByClassName('h-bar-container'),
        o = document.getElementsByClassName('h-bar-mc-container');
  Array.prototype.forEach.call(document.getElementsByClassName(e), function (t) {
    t.style.display = 'none';
  }),
  Array.prototype.forEach.call(n, function (t) {
    (t.style.width = '1px'), (t.style.minWidth = '1px');
  }),
  setTimeout(function () {
    Array.prototype.forEach.call(n, function (t) {
      t.style.display = 'none';
    }),
    l.length > 0
      ? Array.prototype.forEach.call(l, function (t) {
        t.style.marginLeft = '32%';
      })
      : o.length > 0 &&
            Array.prototype.forEach.call(o, function (t) {
              t.style.marginLeft = '10%';
            });
  }, 550);
}

function showCol(t, e) {
  'use strict';
  const n = document.getElementsByClassName('h-bar-container'),
        l = document.getElementsByClassName('h-bar-mc-container');
  n.length > 0
    ? Array.prototype.forEach.call(n, function (t) {
      t.style.marginLeft = '26.5%';
    })
    : l.length > 0 &&
      Array.prototype.forEach.call(l, function (t) {
        t.style.marginLeft = '0';
      }),
  Array.prototype.forEach.call(document.getElementsByClassName(t), function (t) {
    (t.style.width = '120px'), (t.style.display = 'table-cell');
  }),
  Array.prototype.forEach.call(document.getElementsByClassName(e), function (t) {
    t.style.display = 'block';
  });
}

function animCol(e, n) {
  'use strict';
  (e = e || 0), (n = void 0 !== n && n < 4 && n >= 0 ? n : 0);
  const l = 0 === n ? 9 : 3 === n ? 23 : 2;
  let o, i, a, c;
  for (let t = 1; t < l + 1; t++) {
    (o = 3 === n ? 'wrap_val' + t : 'wrap_col' + e + '_val' + t),
    (i = 3 === n ? 'av_val' + t : 'col' + e + '_val' + t),
    1 === n
      ? ((i = 'vo_' + i), (o = 'vo_' + o))
      : 2 === n && ((i = 'vp_' + i), (o = 'vp_' + o)),
    (a = document.getElementById(i));
    try {
      c = a.innerHTML.trim();
    } catch (t) {
      if (t instanceof TypeError) {
        console.warn('There is no cell with the ID of "' + i + '".');
        break;
      }
      logMyErrors(t);
      break;
    }
    if ('n/a' !== c.substr(0, 3)) {
      if (m(c)) {
        2 === l
          ? u(a, '120px')
          : 0 == n
            ? u(a, 1 == t || 5 == t ? '90%' : '92px')
            : u(a);
      } else {
        if (isNaN(c)) break;
        if (2 !== l) {
          if (0 === n) {
            if (1 == t || 5 == t) {
              d(i);
              continue;
            }
            if (1 === e && 2 == t) continue;
            if (2 == t) {
              (a.innerHTML =
                '<div class="chart-container"><div id="' +
                o +
                '" class="bar-wrap"></div></div>'),
              r(document.getElementById(o), c, 2);
              continue;
            }
            if ((t > 2 && t < 5) || 9 == t) {
              (a.innerHTML =
                '<div class="chart-container"><div id="' +
                o +
                '" class="rad-wrap"></div></div>'),
              s(document.getElementById(o), c);
              continue;
            }
            (a.innerHTML =
              '<div class="h-bar-container"><div id="' +
              o +
              '" class="h-bar-wrap"></div></div>'),
            r(document.getElementById(o), c, 4);
          } else {
            (a.innerHTML =
              '<div class="h-bar-mc-container"><div id="' +
              o +
              '" class="h-bar-mc-wrap"></div></div>'),
            r(document.getElementById(o), c, 5);
          }
        } else {
          if (1 == t) {
            (a.innerHTML =
              '<div class="chart-container"><div id="' +
              o +
              '" class="bar-wrap"></div></div>'),
            r(document.getElementById(o), c, 3);
            continue;
          }
          (a.innerHTML =
            '<div class="chart-container"><div id="' +
            o +
            '" class="rad-wrap-lg"></div></div>'),
          s(document.getElementById(o), c, 1);
        }
      }
    }
  }

  function s(t, e, n) {
    n = n || 0;
    const l = e / 100,
          o = 0 === n ? '20px' : '30px',
          i = 0 === n ? '.4ch' : '.8ch',
          a = new ProgressBar.Circle(t, {
            color: '#2A3C68',
            strokeWidth: 12,
            trailWidth: 12,
            trailColor: '#E2E2E2',
            easing: 'easeInOut',
            duration: 1300,
            text: {
              autoStyleContainer: !1
            },
            from: {
              color: '#39518C',
              width: 8
            },
            to: {
              color: '#2A3C68',
              width: 12
            },
            step: function (t, e) {
              e.path.setAttribute('stroke', t.color),
              e.path.setAttribute('stroke-width', t.width),
              e.setText(Math.round(100 * e.value()) + '%');
            }
          });
    (a.text.style.fontSize = o),
    (a.text.style.fontWeight = '700'),
    (a.text.style.textIndent = i),
    a.animate(l);
  }

  function r(e, n, l) {
    let o,
        i,
        a = '',
        c = 'absolute',
        s = '-30%',
        r = '41%',
        d = '#414042',
        m = '18px',
        u = '400',
        p = '100%',
        f = '#2A3C68',
        h = '#39518C';
    E(1)
      ? ((i = 30), (a = ' miles'), (d = '#FFF'))
      : E(2)
        ? ((i = 120), (a = ' mins'), (d = '#FFF'))
        : E(3)
          ? ((i = 5e6),
          (c = 'relative'),
          (s = '-65px'),
          (r = '-75px'),
          (m = '22px'),
          (u = '700'))
          : ((c = 'relative'),
          (m = '16px'),
          (p = '60px'),
          E(4)
            ? ((i = 100), (a = ' %'), (s = '100px'), (r = '-27px'))
            : ((i = 300),
            (s = '120px'),
            (r = '-25.5px') &&
              (n > 100
                ? ((f = '#6DC05D'), (h = '#81C973'))
                : n < 100
                  ? ((f = '#E61E25'), (h = '#E93A40'))
                  : ((f = '#524B48'), (h = '#625A56')))));
    const g = n >= i ? 1 : n / i,
          v = new ProgressBar.Line(e, {
            color: f,
            trailColor: '#F1F2F2',
            easing: 'easeInOut',
            duration: 1300,
            svgStyle: {
              width: '100%',
              height: '100%'
            },
            text: {
              style: {
                color: d,
                fontSize: m,
                fontWeight: u,
                left: s,
                position: c,
                top: r,
                width: p
              },
              autoStyleContainer: !1
            },
            from: {
              color: h
            },
            to: {
              color: f
            },
            step: function (e, c) {
              c.path.setAttribute('stroke', e.color),
              E(1) || E(2)
                ? ((o = (c.value() * i).toFixed(1)),
                n > i && o == i && (o = (1 * n).toFixed(1)))
                : E(3)
                  ? ((o = y((c.value() * i) | 0)), n > i && t(o) == i && (o = y(n)))
                  : (E(4) || E(5)) &&
                ((o = Math.round(c.value() * i)),
                5 === l &&
                  (o == n && c.path.setAttribute('stroke', f),
                  n > i && o == i && (o = 0 | n))),
              c.setText(o + a);
            }
          });

    function E(t) {
      return l === t;
    }
    (E(1) || E(2) || E(3)) &&
      ((v.text.style.transform = 'rotate(90deg)'),
      (E(1) || E(2)) && (v.text.style.textShadow = '1px 1px #A7A9AC')),
    v.animate(g),
    E(4)
      ? (v._container.title =
            6 == e.id.slice(-1)
              ? 'Millennials - ' + n + '%'
              : 7 == e.id.slice(-1)
                ? 'Generation X - ' + n + '%'
                : 'Baby Boomers - ' + n + '%')
      : E(5) &&
          (n < 100
            ? (v._container.title =
                'Red colored bars indicate that the value is below 100')
            : 100 == n
              ? (v._container.title =
                'Gray colored bars indicate that the value is equal to 100')
              : ((v._container.title =
                'Green colored bars indicate that the value is greater than 100'),
              n > i &&
                setTimeout(function () {
                  (v.text.style.fontWeight = '700'),
                  (v.text.title =
                      'Bold values are a simple visual indication that the 300 point scale of the chart has been been exceeded.');
                }, 1295)));
  }

  function d(e, n) {
    n = n || 0;
    const l = document.getElementById(e).textContent,
          o = (l / 77) | 0;
    let i = n;
    document.getElementById(e).textContent = i;
    const a = setInterval(function () {
      i >= l
        ? (clearInterval(a),
        (document.getElementById(e).textContent =
            '5' === e.slice(-1) ? '$' + y(l) : y(l)))
        : ((i = 0 | t(document.getElementById(e).textContent)),
        (i += o),
        (document.getElementById(e).textContent = y(i)));
    }, 1);
  }

  function m(t) {
    let e = !1;
    return (
      ((/^[0-9]*\.?[0-9]+$/.test(t) || /[<>]+/.test(t)) && 0 !== t && '' !== t) ||
        (e = !0),
      e
    );
  }

  function u(t, e) {
    const n = document.createTextNode('data currently unavailable'),
          l = document.createElement('div');
    (t.innerHTML = ''),
    (l.style.fontSize = '.9em'),
    (l.style.fontVariant = 'small-caps'),
    (l.style.margin = '0 auto'),
    void 0 !== e
      ? ('90%' === e && (l.style.fontSize = '.7em'), (l.style.width = e))
      : ((l.style.fontSize = '.75em'),
      (l.style.textAlign = 'left'),
      (l.style.marginLeft = '-2px')),
    l.appendChild(n),
    t.appendChild(l);
  }

  function y(t) {
    return (t + '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }
}

function animKpi() {
  for (let t = 1; t < 6; t++) 3 !== t && animCol(t);
}

function fadeOut(t, e) {
  e = e || 50;
  let n = 1;
  const l = setInterval(function () {
    n <= 0.1 && ((n = 0), (t.style.display = 'none'), clearInterval(l)),
    (t.style.opacity = n),
    (t.style.filter = 'alpha(opacity=' + 100 * n + ')'),
    (n -= 0.1 * n);
  }, e);
}

function e(t, e, n) {
  (e = e || 30), (n = n || 'block'), (t.style.opacity = 0), (t.style.display = n);
  let l = 0.1;
  const o = setInterval(function () {
    l >= 1 && ((l = 1), clearInterval(o)),
    (t.style.opacity = l),
    (t.style.filter = 'alpha(opacity=' + 100 * l + ')'),
    (l += 0.1 * l);
  }, e);
}

function noCompSelected() {
  const t = document.getElementById('vo_label_left');
  (document.getElementById('sub_tile_content').style.display = 'none'),
  (t.style.display = 'none'),
  fadeOut(document.getElementById('sub_tile'), 30),
  fadeOut(document.getElementById('vo_map_right'), 30),
  (document.getElementById('vo_label_right').style.display = 'none'),
  setTimeout(function () {
    (document.getElementById('vo_map_left').style.width = '70%'),
    (t.style.width = '70%');
  }, 150),
  setTimeout(function () {
    e(document.getElementById('no_comp_charts'), 30, 'inline-block'),
    e(t, 50, 'inline-block'),
    setTimeout(function () {
      animCol(1, 1), animCol(2, 1);
    }, 150);
  }, 1300);
}

function compSelected() {
  const t = document.getElementById('vo_label_left');
  (document.getElementById('sub_tile_content').style.display = 'block'),
  e(document.getElementById('sub_tile'), 20),
  (t.style.display = 'none'),
  (document.getElementById('no_comp_charts').style.display = 'none'),
  setTimeout(function () {
    document.getElementById('vo_map_left').style.width = '49%';
  }, 50),
  (t.style.width = '49%'),
  setTimeout(function () {
    e(document.getElementById('vo_map_right'), 30, 'inline-block'),
    e(document.getElementById('vo_label_right'), 30, 'inline-block'),
    e(t, 20, 'inline-block');
  }, 1300);
}

function removeCol() {
  const t = document.getElementsByClassName('vp-comp-col');
  t.length > 0 &&
    'table-cell' === window.getComputedStyle(t[0]).getPropertyValue('display') &&
    Array.prototype.forEach.call(t, function (t) {
      fadeOut(t);
    });
}

function addCol() {
  const t = document.getElementsByClassName('vp-comp-col');
  t.length > 0 &&
    'none' === t[0].style.display &&
    Array.prototype.forEach.call(t, function (t) {
      e(t, 40, 'table-cell');
    });
}

function makeAllSortable(e, n) {
  'use strict';
  n = document.getElementById(n) || document.body;
  const l =
    void 0 !== e ? n.getElementsByClassName(e) : n.getElementsByTagName('table');
  let o = l.length;
  for (; --o >= 0; ) c(l[o]);

  function i(t, e) {
    let n;
    if (t.length !== e.length) {
      if (t.length > e.length) {
        n = t.length - e.length;
        for (let t = 0; t < n; t++) e = '0' + e;
      } else {
        n = e.length - t.length;
        for (let e = 0; e < n; e++) t = '0' + t;
      }
    }
    return [t, e];
  }

  function a(e, n, l) {
    const o = e.tBodies[0],
          a = /^[0-9]*\.?[0-9]+$/;
    let c = Array.prototype.slice.call(o.rows, 0);
    (l = -(+l || -1)),
    (c = c.sort(function (e, o) {
      let c = e.cells[n].textContent.trim(),
          s = o.cells[n].textContent.trim();
      return (
        '%' === c.slice(-1) || '%' === s.slice(-1)
          ? ('%' === c.slice(-1) && (c = c.slice(0, c.length - 1)),
          '%' === s.slice(-1) && (s = s.slice(0, s.length - 1)),
          ([c, s] = i(c, s)))
          : '$' === c.substr(0, 1) || '$' === s.substr(0, 1)
            ? ('$' === c.substr(0, 1) && (c = c.slice(1, c.length)),
            '$' === s.substr(0, 1) && (s = s.slice(1, s.length)),
            ([c, s] = i(c, s)))
            : (a.test(t(c)) || a.test(t(s))) &&
              (a.test(t(c)) && (c = t(c) + ''),
              a.test(t(s)) && (s = t(s) + ''),
              ([c, s] = i(c, s))),
        l * c.localeCompare(s)
      );
    }));
    for (let t = 0; t < c.length; ++t) o.appendChild(c[t]);
  }

  function c(t) {
    let e,
        n = t.tHead;
    if ((n && (n = n.rows[0]) && (n = n.cells), n)) {
      for (e = n.length; --e >= 0; ) {
        !(function (e) {
          var l = 1;
          n[e].addEventListener('click', function () {
            a(t, e, (l = 1 - l));
          });
        })(e);
      }
    }
  }
}

function toggleDropdown(t) {
  var e = document.getElementById(t);
  if (e.classList) e.classList.toggle('open');
  else {
    var n = element.className.split(' '),
        l = n.indexOf('open');
    l >= 0 ? n.splice(l, 1) : n.push('open'), (e.className = n.join(' '));
  }
}

function resetDropdown(t) {
  var e = document.getElementById(t);
  e.className = e.className.replace(/\bopen\b/g, '').trim();
}

function swapMapUrl(t, e) {
  'use strict';
  e = e || !1;
  const n = [
          'map_iframe_vo_left',
          'map_iframe_vo_right',
          'map_iframe_vo_full',
          'map_iframe_vp'
        ],
        l =
      'http://trutradedevelop.alexanderbabbage.com/maptt3/overviewmap.html?centers=' +
      locValue +
      '&layerid=';
  let o = l;
  e ? n.shift() : n.splice(1, 1);
  for (let t = 0; t < 3; t++) {
    o += t + 5;
    try {
      document.getElementById(n[t]).setAttribute('src', o);
    } catch (e) {
      e instanceof TypeError
        ? console.warn('There is no element with the ID of "' + n[t] + '".')
        : logMyErrors(e);
    }
  }
}
var windowObjectHandle = null;

function requestNewView(e, t, l) {
  null == windowObjectHandle || windowObjectHandle.closed
    ? (windowObjectHandle = window.open(e, t, l))
    : windowObjectHandle.focus();
}

function sendPageElements(e) {
  'use strict';
  return (function (e) {
    for (
      var t,
          l = document.querySelectorAll(e),
          n = l.length,
          s = document.getElementById('OVERVIEW_BUILD.V.R1.TBLOCATION').value,
          o = [],
          i = 0;
      i < n;
      ++i
    ) {
      (c = l[i]),
      'none' !== getComputedStyle(c).getPropertyValue('display') &&
          'def-tile' !== l[i].classList[1] &&
          'vo-tile-top' !== l[i].classList[1] &&
          'vo-tile-sub' !== l[i].classList[1] &&
          'bav-tile' !== l[i].classList[1] &&
          'vp-tile' !== l[i].classList[1] &&
          ((t = l[i].cloneNode(!0)), o.push(t));
    }
    var c;
    return o.push(s.slice(0, s.lastIndexOf('-'))), o;
  })(e);
}
