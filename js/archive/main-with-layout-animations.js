/**
 * @file main.js
 *
 * @fileoverview This JavaScript file is designed to provide multiple elements
 * of visual functionality to application data. It can take the the values from
 * a table and replace them with several different animated data visualizations
 * that fit within the table cell. It provides sort, fade in, fade out, and
 * element removing and adding to tables and also provides functionality for
 * adding an 'open' state to dropdowns so that their appearance can be altered.
 * This file also can take a request for a new window to be opened. If the
 * window already exists, it is focused instead. It provides a function
 * that allows the newly opened child window to request all elements of a given
 * selector that appear in the parent window's DOM, and those elements are
 * returned as an array of HTMLElements.
 *
 * @author   Stephen M Irving
 * @version  3.0.9, 09/12/19
 * @requires progressbar.min.js - https://github.com/kimmobrunfeldt/progressbar.js
 */

/** @global {Object} */
winHandle = null;

/**
 * An immediately invoked function expression that uses a closure to track the
 * state of competitor visibility.
 * @public @global
 *
 * @example compVisibility.hide()  > Set competitor state to false for hidden
 * @example compVisibility.show()  > Set competitor state to true for shown
 * @example compVisibility.state() > returns true for a visibile competitor
 */
const compVisibility = function () {
  let compVisible = !0;

  return {
    hide: function () {
      compVisible = !1;
    },
    show: function () {
      compVisible = !0;
    },
    toggle: function () {
      return compVisible = (compVisible ? !1 : !0);
    },
    state: function () {
      return compVisible;
    }
  };
}();

/**
 * @function addCommas
 * @description Takes a number or a string representing a number and adds
 * commas to it. Displays numbers with more than 2 decimal places incorrectly,
 * so do not use on floats without using toFixed() first, or refactoring the
 * regular expression.
 * @public @global
 *
 * @param {string|number} x - A number or a string representing a number.
 * @return {string} The number displayed with commas.
 */
function addCommas(x) {
  return (x + '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

/**
 * @function modifyValues
 * @description Modifies the values for the Population and Avg Household Income
 * row on the KPI table so that they have commas separating the groupings of
 * digits.
 * @public @global
 */
function modifyValues() {
  for (let i = 1, curVal; i < (compVisibility.state() ? 5 : 4); i++) {
    curVal = document.getElementById('pop_val' + i);
    if (curVal) curVal.innerHTML = addCommas(curVal.innerHTML);
    curVal = document.getElementById('inc_val' + i);
    if (curVal) curVal.innerHTML = addCommas(curVal.innerHTML);
  }
}

/**
 * @function animCol
 * @description Replaces the values of a column with corresponding data
 * animations.
 * @public @global
 *
 * @param {number} [colIndex=0] - The index number of the column whose
 * values are to be replaced with chart elements. The index starts at 0 for the
 * Actual vs Potential Visitor tile's table, but all other table's columns start
 * at 1.
 * @param {number} [tileType=0] - A number that relates to a given tile. If no
 * value is passed then 0 is the default. Values are:
 * 0 = Key Performance Indicator tile;
 * 1 = Visitor Origin Tile;
 * 2 = Visitor Penetration Tile;
 * 3 = Actual vs Potential Visitors Tile.
 */
function animCol(colIndex, tileType) {
  // If no argument is passed for colIndex set it to the default of 0
  colIndex = colIndex || 0;

  // If no argument is passed for tileType set it to the default of 0
  tileType = tileType || 0;

  const NUM_VALS = (tileType === 0 ? 7 : tileType === 3 ? 23 : 2),
        D = document,
        CHART_CONTAINER = D.createElement('div'),
        jQ = jQuery;

  let wrapperName, // The name of all the chart's wrapper's cells
      cellId,      // Placeholder for the IDs of the cells
      cell,        // The cell itself
      cellVal,     // The cell's value to be converted
      tempNode1,   // Temporary node holder
      tempNode2;   // Temporary node holder

  CHART_CONTAINER.className = 'chart-container';

  // curVal is the current value in the column at that moment in the loop, it
  // starts with 1 at the top of the table
  for (let curVal = 1; curVal < (NUM_VALS + 1); curVal++) {
    wrapperName = (
      3 === tileType
        ? 'wrap_val' + curVal
        : 'wrap_col' + colIndex + '_val' + curVal
    );
    cellId = (
      tileType === 3
        ? 'av_val' + curVal
        : 'col' + colIndex + '_val' + curVal
    );

    (1 === tileType
      ? (cellId = 'vo_' + cellId, wrapperName = 'vo_' + wrapperName)
      : (tileType === 2) &&
        (cellId = 'vp_' + cellId, wrapperName = 'vp_' + wrapperName)
    );

    cell = D.getElementById(cellId);

    if (!cell) break;
    cellVal = cell.innerHTML.trim();

    // Input scrubbing
    if (cellVal.slice(0, 3) === 'n/a') continue;
    if (dataUnavailable(cellVal)) {
      if (2 === NUM_VALS) {
        unavailableMsg(cell, '120px');
      } else if (0 === tileType) {
        unavailableMsg(
          cell,
          ((4 === curVal || 5 === curVal || curVal === 6) ? '90%' : '50%')
        );
      } else {
        unavailableMsg(cell);
      }
      continue;
    }
    // End of input scrubbing

    cell.innerHTML = '';

    if (2 === NUM_VALS) { // This targets tile types 1 and 2
      if (1 === curVal) {
        // Replace the nums in the cells with the corresponding bar wrapper
        // <div class="chart-container"><div id="wrapperName" class="bar-wrap"></div></div>
        tempNode1 = CHART_CONTAINER.cloneNode(false);
        tempNode2 = D.createElement('div');
        tempNode2.id = wrapperName;
        tempNode2.className = 'bar-wrap';
        tempNode1.appendChild(tempNode2);
        cell.appendChild(tempNode1);
        // Create the chart in the new wrapper
        createBar(D.getElementById(wrapperName), cellVal, 3);
        continue;
      }
      // Replace the nums in the cells with the corresponding radial wrapper
      // <div class="chart-container"><div id="wrapperName" class="rad-wrap-lg"></div></div>
      tempNode1 = CHART_CONTAINER.cloneNode(false);
      tempNode2 = D.createElement('div');
      tempNode2.id = wrapperName;
      tempNode2.className = 'rad-wrap-lg';
      tempNode1.appendChild(tempNode2);
      cell.appendChild(tempNode1);
      createRad(D.getElementById(wrapperName), cellVal, 1);
      continue;
    } else if (0 === tileType) { // Targets KPI tile
      // Skip MSA colummn's Avg Dwell Time (is always n/a)
      if (1 === colIndex && 1 === curVal) continue;
      if (1 === curVal) {
        // Animate the Avg Dwell Time vertical bar charts for the other columns
        // <div class="chart-container"><div id="wrapperName" class="bar-wrap"></div></div>
        tempNode1 = CHART_CONTAINER.cloneNode(false);
        tempNode2 = D.createElement('div');
        tempNode2.id = wrapperName;
        tempNode2.className = 'bar-wrap';
        tempNode1.appendChild(tempNode2);
        cell.appendChild(tempNode1);
        createBar(D.getElementById(wrapperName), cellVal, 2);
        continue;
      }
      if ((curVal > 1 && curVal < 4) || 7 === curVal) { // Animate the % radials
        // Replace the cell's content with the corresponding radial wrappers
        // <div class="chart-container"><div id="wrapperName" class="rad-wrap"></div></div>
        tempNode1 = CHART_CONTAINER.cloneNode(false);
        tempNode2 = D.createElement('div');
        tempNode2.id = wrapperName;
        tempNode2.className = 'rad-wrap';
        tempNode1.appendChild(tempNode2);
        cell.appendChild(tempNode1);
        createRad(D.getElementById(wrapperName), cellVal);
        continue;
      }
      // The only curVal values left for KPI are for the horizontal % bar charts
      // <div class="h-bar-container"><div id="wrapperName" class="h-bar-wrap"></div></div>
      tempNode1 = D.createElement('div');
      tempNode1.className = 'h-bar-container';
      tempNode2 = D.createElement('div');
      tempNode2.id = wrapperName;
      tempNode2.className = 'h-bar-wrap';
      tempNode1.appendChild(tempNode2);
      cell.appendChild(tempNode1);
      createBar(D.getElementById(wrapperName), cellVal, 4);
    } else { // Targets Actual vs Potential Visitors Tile
      // Replace the nums in the cells with the corresponding bar wrappers
      // <div class="h-bar-mc-container"><div id="wrapperName" class="h-bar-mc-wrap"></div></div>
      tempNode1 = D.createElement('div');
      tempNode1.className = 'h-bar-mc-container';
      tempNode2 = D.createElement('div');
      tempNode2.id = wrapperName;
      tempNode2.className = 'h-bar-mc-wrap';
      tempNode1.appendChild(tempNode2);
      cell.appendChild(tempNode1);
      createBar(D.getElementById(wrapperName), cellVal, 5);
    }
  } // End the for loop going through all the column's values


  /**
   * @function createRad
   * @description This function will create individual radial charts.
   * @private
   *
   * @param {Element} wrapPointer - Pointer associated with the div
   * wrapper where the radial will go. (#wrap_colX_valX)
   * @param {string|number} progressNum - Value in the the cell being replaced with a
   * radial
   * @param {number} [radSize=0] - Size of the radial created.
   * Passable values are:
   * 0 = The default radial size of 85px;
   * 1 = The larger size of 120px;
   * The sizes for these radials only effect the font size and text indent in
   * the function, the radial's actual dimensions are set by their css wrapper.
   */
  function createRad(wrapPointer, progressNum, radSize) {
    radSize = radSize || 0;

    // Set the point the chart will animate to. Must be between 0 and 1
    const PROGRESS = progressNum / 100,
          TEXT_SIZE   = (0 === radSize ? '20px' : '30px'),
          TEXT_OFFSET = (0 === radSize ? '.4ch' : '.8ch');

    // eslint-disable-next-line one-var
    const RAD = new ProgressBar.Circle(wrapPointer, {
      color: '#2a3c68', // Set the font color for the radial's center value
      strokeWidth: 12,  // To Prevent clipping set to the width it will animate to
      trailWidth: 12,   // The width of the trail the radial will animate along
      trailColor: 'rgba(79,162,217,.13)',
      easing: 'easeInOut',
      duration: 1300, // Sets the total time in ms for the rad to reach 100%
      text: { autoStyleContainer: false },
      // Changes loading bar color & width as it progresses. Keep in mind: The
      // radial bar will only reach the stated 'to' color and width at 100% mark
      from: { // Starting color and width
        color: '#39518c',
        width: 8
      },
      to: { // Ending color and width
        color: '#2a3c68',
        width: 12 // To prevent clipping, make width the same as strokeWidth
      },
      // How to animate the path of the radial and the text inside it
      step: function step(state, circle) {
        circle.path.setAttribute('stroke', state.color);
        circle.path.setAttribute('stroke-width', state.width);
        circle.setText(Math.round(circle.value() * 100) + '%');
      }
    });
    // Set the text style of above radial
    RAD.text.style.fontSize = TEXT_SIZE;
    RAD.text.style.fontWeight = '700';
    RAD.text.style.textIndent = TEXT_OFFSET;
    // Initializes the animation of above radial if pointer is on screen
    // TODO: Figure out the delay here and then add the animation back in
    // if (isOnScreen('#' + wrapPointer.id)) RAD.animate(PROGRESS);
    RAD.set(PROGRESS); // Otherwise merely set the radial w/o animating
  }

  /**
   * @function createBar
   * @description This function will create vertical bar graphs.
   * @private
   *
   * @param {Element} wrapPointer - Pointer associated with the div
   * wrapper where the bar will go. (wrap_colX_valX)
   * @param {string|number} progressNum - Value in the the cell being replaced
   * @param {number} barType - Type of bar being created. Values are:
   * 1 = vertical bar based on miles;
   * 2 = vertical bar based on minutes;
   * 3 = vertical bar based on population with the text outside of the bar;
   * 4 = horizontal bar based on a percentage;
   * 5 = horizontal bar based on an index that changes colors based on the value.
   */
  function createBar(wrapPointer, progressNum, barType) {
    const GREEN = '#6dc05d', // Color of the type 5 bars that exceed 100
          GRAY  = '#524b48', // Color of the type 5 bars that equal 100
          RED   = '#e61e25', // Color of the type 5 bars that are less than 100
          GREEN_LT = '#81c973', // 6% lighter color the green bars will begin as
          GRAY_LT  = '#625a56', // 6% lighter color the gray bars will begin as
          RED_LT   = '#e93a40'; // 6% lighter color the red bars will begin as

    let endText    = '',         // The label text for the value
        posType    = 'absolute', // The position type of the bar text
        leftPos    = '-30%',     // Left position value of the bar text
        topPos     = '41%',      // Top position value of the bar text
        textSize   = '18px',     // Size of the bar's value text
        textWeight = '400',
        textWidth  = '100%',     // Width of the box containing the value text
        textColor  = '#414042',  // Color of the bar's value text
        endColor   = '#2a3c68',  // Color the bar ends as
        startColor = '#39518c',  // Color the bar begins as
        barValText,  // Value of the bar as it animates towards progressNum
        progressMax; // The max value for the chart's

    progressNum *= 1;

    if (1 === barType || 2 === barType) {
      if (progressNum < 15) {
        textColor = '#2a3c68',
        textWeight = '700';
      } else {
        textColor = '#fff';
      }
      if (1 === barType) {
        endText = ' miles',
        progressMax = 30;
      } else {
        endText = ' mins',
        progressMax = 120;
      }
    } else if (3 === barType) {
      progressMax = 5000000;
      posType = 'relative',
      leftPos = '-65px', // Bottom value in barType 1-3 due to rotation
      topPos = '-75px',  // Actually the left value due to chart rotation
      textSize = '22px',
      textWeight = '700';
    } else {
      posType = 'relative',
      textSize = '1em',
      textWidth = '60px';

      (4 === barType
        ? (progressMax = 100, endText = ' %', leftPos = '5em',
        topPos = '-25px', textWeight = '700')
        : ((progressMax = 300, leftPos = '125px', topPos = '-20px') &&
          (progressNum > 100
            ? (endColor = GREEN, startColor = GREEN_LT)
            : progressNum < 100
              ? (endColor = RED, startColor = RED_LT)
              : (endColor = GRAY, startColor = GRAY_LT)
          )
        )
      );
    }

    // Set the point the chart will animate to. Must be between 0 and 1
    const PROGRESS = (progressNum >= progressMax ? 1 : progressNum / progressMax);

    // eslint-disable-next-line one-var
    const BAR = new ProgressBar.Line(wrapPointer, {
      color: endColor, // Sets the color that the bar will be
      trailColor: 'rgba(79,162,217,.13)',
      easing: 'easeInOut',
      duration: 1300, // Sets the total time in ms for the bar to reach 100%
      svgStyle: { width: '100%', height: '100%' },
      text: {
        style: {
          color: textColor,
          fontSize: textSize,
          fontWeight: textWeight,
          left: leftPos,
          position: posType,
          top: topPos,
          width: textWidth
        },
        autoStyleContainer: false
      },
      from: { color: startColor }, // Set the starting color of the bar
      to: { color: endColor }, // Sets the color of the bar if it reaches 100%

      // How to animate the path of the bar and the text inside it
      step: function step(state, chart) {
        chart.path.setAttribute('stroke', state.color);
        if (1 === barType || 2 === barType) {
          barValText = (chart.value() * progressMax).toFixed(1);
          if (progressNum > progressMax && barValText == progressMax) {
            barValText = progressNum.toFixed(1);
          }
        } else if (3 === barType) {
          barValText = addCommas((chart.value() * progressMax)|0);
          if (progressNum > progressMax &&
              stripCommas(barValText) == progressMax) {
            barValText = addCommas(progressNum);
          }
        } else if (4 === barType || 5 === barType) {
          barValText = Math.round(chart.value() * progressMax);
          if (5 === barType) {
            (barValText == progressNum) &&
              (chart.path.setAttribute('stroke', endColor));
            (progressNum > progressMax && barValText == progressMax) &&
              (barValText = progressNum|0);
          }
        }
        chart.setText(barValText + endText);
      }
    });
    if (1 === barType || 2 === barType || 3 === barType) {
      BAR.text.style.transform = 'rotate(90deg)';

      if (
        (1 === barType || 2 === barType) &&
        progressNum > 14 && progressNum < 38
      ) {
        BAR.text.style.textShadow = '#000 1px 1px';
      }
    }
    // Initialize the animation of above bar if it is visible on screen
    // TODO: Figure out the delay here and then add the animation back in
    // if (isOnScreen('#' + wrapPointer.id)) BAR.animate(PROGRESS);
    BAR.set(PROGRESS); // Otherwise merely set it w/o animating

    // Apply tooltips on hover to the bars
    if (4 === barType) {
      BAR._container.title = (
        wrapPointer.id.slice(-1) === '6'
          ? 'Millennials - ' + progressNum + '%'
          : wrapPointer.id.slice(-1) === '7'
            ? 'Generation X - ' + progressNum + '%'
            : 'Baby Boomers - ' + progressNum + '%'
      );
    } else if (5 === barType) {
      if (progressNum < 100) {
        BAR._container.title = 'Red colored bars indicate that the value is' +
            ' below 100';
      } else if (progressNum == 100) {
        BAR._container.title = 'Gray colored bars indicate that the value is' +
            ' equal to 100';
      } else {
        BAR._container.title = 'Green colored bars indicate that the value is' +
            ' greater than 100';

        if (progressNum > progressMax) {
          setTimeout(function () {
            BAR.text.style.fontWeight = '700';
            BAR.text.title = 'Bold values are a visual indication that the ' +
                '300 point scale of the chart has been been exceeded.';
          }, 1295);
        }
      }
    }

    /**
     * @function stripCommas
     * @description Takes a string of a number with commas in it and strips the
     * commas out of the string, returning the number value.
     * @private
     *
     * @param {string} x - A string representing a number that is displayed with
     * commas
     * @return {number} The number value of the string with the commas
     * stripped
     */
    function stripCommas(x) {
      return (x + '').replace(/\,/g, '') * 1;
    }
  }

  /**
   * @function dataUnavailable
   * @description Data validation function. Takes the trimmed innerHTML value of
   * a table cell or the HTML element and returns true if the data is
   * unavailable, determined by the value not being a number AND not having HTML
   * tags, OR being equal to either 0 OR an empty string. If those conditions
   * aren't meant, returns false.
   * @private
   *
   * @param {string} cellVal - The inner contents of a table cell or HTML element.
   * @return {boolean} True for data not available, false when data is available.
   */
  function dataUnavailable(cellVal) {
    // Regular expression tests true if string can be converted to a float or
    // integer (tests false for negative numbers)
    const IS_NUM = /^[0-9]*\.?[0-9]+$/,
          HAS_TAGS = /[<>]+/, // Tests true if string has < or >
          notAvailable = (
            ((!IS_NUM.test(cellVal) && !HAS_TAGS.test(cellVal)) || !cellVal ||
            cellVal.substring(0, 2) === '$<')
              ? !0
              : !1
          );

    return notAvailable;
  }

  /**
   * @function unavailableMsg
   * @description Creates a 'data currently unavailable' message and appends it
   * to a given table cell.
   * @private
   *
   * @param {Element} cell - Pointer associated with the cell where
   * the msg goes.
   * @param {string} [msgWidth] - Optional argument that sets the width of the
   * unavailable message's div wrapper.
   */
  function unavailableMsg(cell, msgWidth) {
    const TEXT_MSG = D.createTextNode('data currently unavailable'),
          NODE = D.createElement('div');

    cell.innerHTML = '';
    NODE.className = 'unavailable-msg';
    if (typeof msgWidth !== 'undefined') {
      if (msgWidth === '90%') {
        // Defines the msg style for the KPI Population & Avg Income rows
        NODE.classList.add('unavailable-msg--sm');
      }
      NODE.style.width = msgWidth;
    } else {
      // Defines the msg style for the horizontal-mc bars
      NODE.classList.add('unavailable-msg--mc-bars');
    }
    NODE.appendChild(TEXT_MSG);
    cell.appendChild(NODE);
  }

  /**
   * @function isOnScreen
   * @description Returns true if the given element is visible on screen.
   * @param {string} element - The jQuery selector for the element, or
   * the element itself. (ex: '#container').
   * @return {boolean} True if the element is visible on screen, false if not.
   */
  function isOnScreen(element) {
    const vpH = jQ(window).height(), // Viewport Height
          scrollTop = jQ(window).scrollTop(),
          elTop = jQ(element).offset().top,
          elHeight = jQ(element).height();

    return ( (elTop < (vpH + scrollTop)) && (elTop > (scrollTop - elHeight)) );
  }
}

/**
 * @function animKpi
 * @description Animates the data in the Key Performance Indicator table,
 * cycling through the columns and swapping out values for data visualizations.
 * @public @global
 *
 * @param {boolean} [isCompShown=true] - True if the Competitor column is
 * visible, false if it is hidden.
 */
function animKpi(isCompShown) {
  for (
    let i = 1;
    i < (typeof isCompShown === 'undefined' || isCompShown ? 6 : 5);
    i++
  ) {
    if (i === 3) continue;
    animCol(i);
  }
}

/**
 * @function animVoVp
 * @description Animates the data in the Visitor Origin or Visitor Penetration
 * table, cycling through the columns and swapping out values for data
 * visualizations.
 * @public @global
 *
  * @param {number} tileNum - Pass 1 to animate the Visitor Origin charts, pass
  * 2 to animate the Visitor Penetration charts.
 * @param {boolean} [isCompShown=true] - True if the Competitor column is
 * visible, false if it is hidden.
 */
function animVoVp(tileNum, isCompShown) {
  isCompShown = typeof isCompShown === 'undefined' ? !0 : isCompShown;

  // If there is a competitor shown, there are no VO charts visible
  if ((tileNum === 1 && isCompShown) || tileNum === 2) {
    for (let i = 1; i < (tileNum === 2 && isCompShown ? 4 : 3); i++) {
      animCol(i, tileNum);
    }
  }
}

/**
 * @function hideComp
 * @description This function will initiate all the effects that result from
 * "No Competitor" being selected or the "Focus one location" checkbox being
 * selected.
 * @public @global
 */
function hideComp() {
  const D = document,
        jQ = jQuery;

  // Transition the Visitor Origin tile to the "No Competitor" state
  noCompVo();

  // Hide the columns for the rest of the applicable tables on the page
  hideCol();

  /**
   * @function hideCol
   * @description Hides the various tables' competitor columns, removing them
   * from the table.
   * @private
   */
  function hideCol() {
    const H_BAR_WRAP = D.getElementsByClassName('h-bar-container'),
          H_BAR_MC_WRAP = D.getElementsByClassName('h-bar-mc-container');

    // Fade out the contents of the cells
    jQ('.comp-val').fadeOut(200, function () {
      // Then hide the cells, reducing their width and opacity over 700ms
      jQ('.comp-col').hide({
        duration: 700,
        easing: 'linear',
        complete: function () {
          // When complete, adjust the placement of the tables' charts
          if (H_BAR_WRAP.length !== 0) {
            [].forEach.call(H_BAR_WRAP, function (el) {
              el.style.marginLeft = '32%';
            });
          }
          if (H_BAR_MC_WRAP.length !== 0) {
            [].forEach.call(H_BAR_MC_WRAP, function (el) {
              el.style.marginLeft = '10%';
            });
          }
        }
      });
    });
    // Hide the Visitor Penetration competitor column
    jQ('.vp-comp-col').hide(50);
  }

  /**
   * @function noCompVO
   * @description Transitions the Visitor Origin tiles when "No Competitor" is
   * selected.
   * @private
   */
  function noCompVo() {
    const SUB_TILE_CONTENT = D.getElementById('sub_tile_content'),
          LEFT_MAP = D.getElementById('vo_map_left'),
          LEFT_LABEL = D.getElementById('vo_label_left'),
          RIGHT_LABEL = D.getElementById('vo_label_right');

    // Hide the content of the sub-tile as well as the left and right label
    SUB_TILE_CONTENT.classList.add('is-hidden');
    LEFT_LABEL.classList.add('is-hidden');
    RIGHT_LABEL.classList.add('is-hidden');

    // Animate the hiding of the sub-tile
    jQ('#sub_tile').hide({
      duration: 60,
      easing: 'linear',
      complete: function () {
        // Clear the competitor map's source from the main tile
        jQ('#map_iframe_vo_right').setAttribute('src', '');
        // Fade out the container for the competitor map
        jQ('#vo_map_right').hide({
          duration: 40,
          easing: 'linear',
          complete: function () {
            // Add class to stretch size of the left map. The animation that is
            // taking place upon applying this class is a CSS transition with a
            // duration of 800ms
            LEFT_MAP.classList.add('no-comp-width');
            LEFT_LABEL.classList.add('no-comp-width');

            // Wait for the CSS transition to finish
            setTimeout(function () {
              // Unhide the left map label
              LEFT_LABEL.classList.remove('is-hidden');

              // fade in the chart elements
              jQ('#no_comp_charts').fadeIn(50, function () {
                // Once the chart elements are in, animate them
                animVoVp(1, false);
              });
            }, 817); // 1sec/60frames = 16.666ms per frame = ~817ms (next frame after 800ms)
          }
        });
      }
    });
  }
}

/**
 * @function activateTableSorting
 * @description Implements sorting on any tables found in the markup or any
 * tables that match the given class name, q.
 * @public @global
 *
* @param {string} q - The class name for the table(s) having sorting applied.
 */
function activateTableSorting(q) {
  const n = document;

  q = n.getElementsByClassName(q);

  const l = parseFloat,
        m = u(/^(?:\s*)([+-]?(?:\d+)(?:,\d{3})*)(\.\d*)?$/g, /,/g),
        g = u(/^(?:\s*)([+-]?(?:\d+)(?:\.\d{3})*)(,\d*)?$/g, /\./g);

  n.addEventListener('DOMContentLoaded', function () {
    for (let e = 0; e < r(q); ++e) {
      try {
        p(q[e]);
      } catch (a) {}
    }
  });

  function r(n) {
    return n.length;
  }

  function t(n, t) {
    if (n) {
      for (let e = 0, a = r(n); a > e; ++e) {
        t(n[e], e);
      }
    }
  }

  function e(n) {
    return n.split('').reverse().join('');
  }

  function a(n) {
    let e = n[0];

    return t(n, function (n) {
      for (; !n.startsWith(e); ) {
        e = e.substring(0, r(e) - 1);
      }
    }),
    r(e);
  }

  function o(n, r) {
    return -1 != n.map(r).indexOf(!0);
  }

  function u(n, r) {
    return function (t) {
      let e = '';

      return (
        t.replace(n, function (n, t, a) {
          return (e = t.replace(r, '') + '.' + (a || '').substring(1));
        }),
        l(e)
      );
    };
  }

  function i(n) {
    const t = l(n);

    return !isNaN(t) && r('' + t) + 1 >= r(n) ? t : NaN;
  }

  function s(n) {
    let e = [];

    return t([i, m, g], function (t) {
      let a;

      r(e) || o((a = n.map(t)), isNaN) || (e = a);
    }),
    e;
  }

  function c(n) {
    let t = s(n);

    if (!r(t)) {
      const o = a(n),
            u = a(n.map(e)),
            i = n.map(function (n) {
              return n.substring(o, r(n) - u);
            });
      t = s(i);
    }
    return t;
  }

  function f(n) {
    const r = n.map(Date.parse);

    return o(r, isNaN) ? [] : r;
  }

  function v(n, r) {
    r(n),
    t(n.childNodes, function (n) {
      v(n, r);
    });
  }

  function d(n) {
    const t = [],
          e = [];
    let r;

    return v(n, function (n) {
      const a = n.nodeName;

      ( ('TR' === a)
        ? ( ('TFOOT' !== n.parentNode.nodeName) &&
            ((r = []), t.push(r), e.push(n)) )
        : ( ('TFOOT' !== n.parentNode.nodeName) &&
            (('TD' === a || 'TH' === a) && r.push(n)) )
      );
    }),
    [t, e];
  }

  function p(n) {
    if ('TABLE' === n.nodeName) {
      for (
        var e = d(n),
            a = e[0],
            o = e[1],
            u = r(a),
            i = u > 1 && (r(a[0]) < r(a[1]) ? 1 : 0),
            s = i + 1,
            v = a[i],
            p = r(v),
            l = [],
            m = [],
            g = [],
            h = s;
        u > h;
        ++h
      ) {
        for (let N = 0; p > N; ++N) {
          const T = a[h][N],
                C = T.textContent || T.innerText || '';

          r(m) < p && m.push([]);
          m[N].push(C.trim());
        }
        g.push(h - s);
      }

      const L = 'table-sort-asc',
            E = 'table-sort-desc',
            b = function () {
              for (let n = 0; p > n; ++n) {
                const r = v[n].classList;

                r.remove(L), r.remove(E), (l[n] = 0);
              }
            };

      t(v, function (n, t) {
        l[t] = 0;
        const e = n.classList;

        e.add('table-sort-header'),
        n.addEventListener('click', function () {
          function n(n, r) {
            const t = d[n],
                  e = d[r];

            return (t > e ? a : e > t ? -a : a * (n - r));
          }

          var a = l[t];

          b(),
          (a = 1 == a ? -1 : +!a),
          a && e.add(a > 0 ? L : E),
          (l[t] = a);

          const i = m[t];
          let v = function (n, r) {
            return a * i[n].localeCompare(i[r]) || a * (n - r);
          };
          var d = c(i);

          (r(d) || r((d = f(i)))) && (v = n);

          const p = g.slice();
          p.sort(v);

          for (var h = null, N = s; u > N; ++N) {
            (h = o[N].parentNode), h.removeChild(o[N]);
          }
          for (N = s; u > N; ++N) {
            h.appendChild(o[s + p[N - s]]);
          }
        });
      });
    }
  }
}

/**
 * @name requestNewView
 * @description Initiates the Window interface's open() method if the window
 * is does not already exist or is closed. If it does exist then the window is
 * focused.
 * @public @global
 *
 * @param {string} url A DOMString indicating the URL of rthe resource to be
 * loaded. This can be a path or URL to an HTML page, image file, or any other
 * browser supported resource. If an empty string ('') is specified as the url,
 * a blank page is opened into the targeted browsing context.
 * @param {string} windowName A DOMString specifying the name of the browsing
 * context (window, iframe, or tab) into which to load the specified resource.
 * If the name doesn't indicate an existing context, a new window is created and
 * is given the name specified by windowName. The name should not contain any
 * whitespace. Keep in mind that this will NOT be used as the window's title.
 * @param {string} [windowFeatures] - A DOMString containing a comma-separated
 * list of window features given with their corresponding values in the form of
 * "name=value". The features list string must not contain whitespace.
 */
function requestNewView(url, windowName, windowFeatures) {
  if (winHandle == null || winHandle.closed) {
    winHandle = window.open(url, windowName, windowFeatures);
  } else {
    winHandle.focus();
  }
}

/**
 * @name sendPageElements
 * @public
 * @description Provides a function for a child window to call that will return
 * an array of HTMLElements containing the elements matching selector which have
 * a display value that is not 'none'.
 *
 * @param {string} selector - The selector used in a querySelectorAll method.
 * @return {Element[]|undefined[]} The array of Elements found by selector
 * and then filtered to not include any elements with a display value of 'none'.
 */
function sendPageElements(selector) {
  return grabVisibleElements(selector);

  /**
   * @name grabVisibleElements
   * @private
   * @description Grabs the reports on the 3.1 Overview page and compiles them
   * into an array of elements for unpacking in the overview-report.html file.
   * @param {string} selector - A CSS selector used by querySelectorAll().
   * @return {Element[]|undefined[]} An array of the selected Elements that have
   * been determined to have a display value that is not 'none'.
   */
  function grabVisibleElements(selector) {
    const allElements = document.querySelectorAll(selector),
          numElements = allElements.length,
          locName = document.getElementById('OVERVIEW_BUILD.V.R1.TBLOCATION').value,
          visibleElArray = [];

    let cloneEl,
        curEl,
        curClass,
        i;

    for (i = 0; i < numElements; i++) {
      curEl = allElements[i],
      curClass = curEl.classList[1];

      if (
        getComputedStyle(curEl).getPropertyValue('display') !==
        'none' && curClass !== 'def-tile' && curClass !== 'vo-tile-top' &&
        curClass !== 'vo-tile-sub' && curClass !== 'bav-tile' &&
        curClass !== 'vp-tile'
      ) {
        // Clone the node to prevent taking the actual element from the DOM
        cloneEl = allElements[i].cloneNode(true);
        // Package the element in an array to be sent to the child window
        visibleElArray.push(cloneEl);
      }
    }

    // Attach the name of the selected location to the end of the array
    visibleElArray.push(locName.slice(0, locName.lastIndexOf('-')));

    return visibleElArray;
  }
}

function accessCheckRefresh() {
  swal({
    title: 'Access Restriction',
    text: 'You do not have access to this module. Please contact us to receive access.',
    icon: 'error',
    button: 'OK'
  });
}
