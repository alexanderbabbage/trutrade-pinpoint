function createRad(wrapperName, progressNum, startColor, endColor, animTime) {
    'use strict';
    // Set value the chart will animate to. Must be between 0 and 1 (2 decimals)
    const PROGRESS = progressNum / 100.0;
    // Create a radial
    const RAD = new ProgressBar.Circle(wrapperName, {
        color: endColor, // Set the font color of the num in the center of the radial
        strokeWidth: 12, // To prevent clipping, make the same as the max width it will animate to
        trailWidth: 12, // The width of the trail the radial will animate along
        trailColor: '#e2e2e2', // The color of the trail
        easing: 'easeInOut', // Animation easing function
        duration: animTime, // Sets the total time in ms for the rad to reach 100%
        text: { autoStyleContainer: false },
        // Changes loading bar color & width as it progresses. Keep in mind: The
        // radial bar will only reach the stated 'to' color and width at 100% mark
        from: { // Starting color and width
            color: startColor,
            width: 5
        },
        to: { // Ending color and width
            color: endColor,
            width: 12 // To prevent clipping, make width here the same as strokeWidth
        },
        // Animate the path of the radial and the text inside it
        step: function step(state, circle) {
            circle.path.setAttribute('stroke', state.color);
            circle.path.setAttribute('stroke-width', state.width);
            const value = Math.round(circle.value() * 100);
            circle.setText(value + '%');
        }
    });
    // Set the text style of above radial
    RAD.text.style.fontFamily = '"Roboto","Helvetica Neue", HelveticaNeue, Helvetica-Neue, Helvetica, -apple-system, BlinkMacSystemFont, "BBAlpha Sans", "Segoe UI", Arial, "Noto Sans", sans-serif';
    RAD.text.style.fontSize = '20px';
    RAD.text.style.fontWeight = '900';
    RAD.text.style.textIndent = '.4ch';
    RAD.animate(PROGRESS); // Initializes the animation of above radial
}

function createBar(wrapperName, progressNum, startColor, endColor, animTime, endText) {
    'use strict';
    // Set the max value for the charts, max of 30 for miles charts, 120 for mins
    const PROGRESS_MAX = (endText === 'miles') ? 30 : 120;
    // Set value the chart will animate to. Must be between 0 and 1
    const PROGRESS = (progressNum >= PROGRESS_MAX) ? 1 : progressNum / PROGRESS_MAX; // Divide by the maximum num of the chart
    const BAR = new ProgressBar.Line(wrapperName, {
        color: endColor, // Sets the color that the bar will be
        trailColor: '#e2e2e2', // The color of the trail
        easing: 'easeInOut', // Animation easing function
        duration: animTime, // Sets the total time in ms for the bar to reach 100%
        svgStyle: { width: '100%', height: '100%' },
        text: { // Set the attributes of the text inside the bar
            style: {
                width: '100%',
                color: '#fff', // Set text color
                fontfamily: '"Roboto","Helvetica Neue", HelveticaNeue, Helvetica-Neue, Helvetica, -apple-system, BlinkMacSystemFont, "BBAlpha Sans", "Segoe UI", Arial, "Noto Sans", sans-serif',
                fontSize: '18px', // Text font size
                transform: 'rotate(90deg)', // Rotate the text back so that it is not sideways.
                position: 'absolute',
                textShadow: '1px 1px #a7a9ac',
                left: '-30%', // Due to rotation, 'left' attribute is really 'bottom' attribute
                top: '41%' // Due to rotation, 'top' attribute is really 'left' attribute
            },
            autoStyleContainer: false
        },
        from: { color: startColor }, // Set the starting color of the bar
        to: { color: endColor }, // Sets the color of the bar if it reaches 100%

        // Animate the path of the bar and the text inside it
        step: function step(state, bar) {
            bar.path.setAttribute('stroke', state.color);
            bar.setText(progressNum + ' ' + endText);
        }
    });
    BAR.animate(PROGRESS); // Initializes the animation of above bar
}

function replaceColWithCharts(colIndex) {
    'use strict';
    const ANIM_TIME = 1400; // Time it takes radial to reach 100% completion, in ms
    const START_COLOR_BAR = '#93a597'; // Set color the bar charts begin as at 0%
    const START_COLOR_RAD = '#93a597'; // Set color the rad charts begin as at 0%
    const END_COLOR = '#58b86b'; // Set color of radial text & charts at 100%
    const TOTAL_CHARTS = (colIndex === 1) ? 2 : 4;
    let cellVal; // The cell value to be converted
    let wrapperName; // The name of all the radial wrapper's cell
    let cellId; // Placeholder for the IDs of the cells
    let barText; // Either 'miles' or 'mins'

    for (let i = 1; i < (TOTAL_CHARTS+1); i++) {
        wrapperName = 'chart_wrapper_col' + colIndex + '_val' + i;
        cellId = 'col' + colIndex + '_val' + i;
        if (isNaN(document.getElementById(cellId).innerHTML)) break; // Validation
        cellVal = document.getElementById(cellId).innerHTML;
        // Check that there are 4 charts (not 2 like 1st col - still on the bars)
        if (TOTAL_CHARTS === 4 && i < 3) {
            // Replace the nums in the cells with the corresponding bar wrappers
            document.getElementById(cellId).innerHTML =
            '<div class="chart-container"><div id="' + wrapperName
                + '" class="bar-wrapper"></div></div>';
            barText = (i === 1) ? 'miles' : 'mins';
            createBar(
                document.getElementById(wrapperName),
                cellVal,
                START_COLOR_BAR,
                END_COLOR,
                ANIM_TIME,
                barText
            );
        } else { // bars finished, now do the bar chart wrapppers
            // Replace the cell's content with the corresponding radial wrappers
            document.getElementById(cellId).innerHTML =
            '<div class="chart-container"><div id="' + wrapperName
                + '" class="rad-wrapper"></div></div>';
            createRad(
                document.getElementById(wrapperName),
                cellVal,
                START_COLOR_RAD,
                END_COLOR,
                ANIM_TIME
            );
        }
    }
}
