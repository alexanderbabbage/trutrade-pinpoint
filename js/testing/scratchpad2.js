// var classAPointer = document.querySelectorAll('.class a'),
//     i = 0;

// for (; i < 48; ++i) {
//   classAPointer[i].click();
// }

// const swapArrayLocs = (arr, index1, index2) => {
//   [arr[index1], arr[index2]] = [arr[index2], arr[index1]];
//   return arr;
// };

// swapArrayLocs(myArray, 0, 4);


const genNums = (totalNums) => {
  const arr = [],
        getRandomInt = (min, max) => {
          min = Math.ceil(min);
          return Math.floor(Math.random() * (Math.floor(max) - min + 1)) + min;
        };

  for (let i = totalNums; i--;) {
    arr.push(getRandomInt(0, 99));
  }

  return arr;
};

const revArr = (numArray) => {
  for (
    let i = numArray.length,
        breakPoint = ((i / 2)|0) - 1,
        k = 0,
        temp;
    --i !== breakPoint;
    ++k
  ) {
    temp = numArray[i];
    numArray[i] = numArray[k];
    numArray[k] = temp;
  }

  return numArray;
};

let arr = [1, 2, {a: 2, b: 'z'}, 'a'];

revArr(arr);//?

const isEven = number => number % 2 === 0;

module.exports = {
  isEven,
  isTenEven: () => isEven(10)
};
