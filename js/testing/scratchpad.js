// const x = {dialog.object}.getValue('SlideCharts') * 1;

// (x < 5
//   ? (
//     1 === x
//       ? {dialog.object}.runAction('chartHHIncome')
//       : 2 === x
//         ? {dialog.object}.runAction('chartWealthy')
//         : 3 === x
//           ? {dialog.object}.runAction('chartDuration')
//           : 4 === x && {dialog.object}.runAction('chartDistance')
//   )
//   : (
//     5 === x
//       ? {dialog.object}.runAction('charFrrequency')
//       : 6 === x
//         ? {dialog.object}.runAction('chartAverageAge')
//         : 7 === x
//           ? {dialog.object}.runAction('chartFamily')
//           : 8 === x
//             ? {dialog.object}.runAction('chartEducated')
//             : {dialog.object}.panelSetActive('CHARTS', false)
//   )
// );
// function swapArrPlaces(arr, fromIndex, toIndex) {
//   var temp = arr[fromIndex];

//   arr[fromIndex] = arr[toIndex];
//   arr[toIndex] = temp;
// }

const replacer = (key, value) => (
  typeof value === 'undefined'
    ? 'undefined'
    : (
      value instanceof Function || Number.isNaN(value) || value === Infinity
        ? value.toString()
        : value
    )
);
