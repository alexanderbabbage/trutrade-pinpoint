(function () {
  // The maximum is exclusive and the minimum is inclusive
  function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }
  function isNamber(n){return !isNaN(parseFloat(n))&&!isNaN(n-0);}
  const isNam=/^[0-9]*\.?[0-9]+$/;
        notNums=['a', 'b', 'fwqf', {}, '123ad', !1, '1243.2', [], 'avwe234', '<img>', true, [], {}, false, '000.3a'];

  let notNumsIndex = 0;

  console.time('RegExp - saving pattern in var | ⌛: ');
  const isNum=/^[0-9]*\.?[0-9]+$/;
  for(let i=99999,result;i--;)result=isNum.test((i%2===0?notNums[getRandomInt(0, 15)]:i));
  console.timeEnd('RegExp - saving pattern in var | ⌛: ');

  console.time('RegExp - just .test() | ⌛: ');
  for(let i=99999,result;i--;)result=isNam.test((i%2===0?notNums[getRandomInt(0, 15)]:i));
  console.timeEnd('RegExp - just .test() | ⌛: ');

  console.time('isNumber - with function declaration | ⌛: ');
  function isNumber(n){return !isNaN(parseFloat(n))&&!isNaN(n-0);}
  for(let i=99999,result;i--;)result=isNumber((i%2===0?notNums[getRandomInt(0, 15)]:i));
  console.timeEnd('isNumber - with function declaration | ⌛: ');

  console.time('isNumber - just isNumber() | ⌛: ');
  function isNumber(n){return !isNaN(parseFloat(n))&&!isNaN(n-0);}
  for(let i=99999,result;i--;)result=isNumber((i%2===0?notNums[getRandomInt(0, 15)]:i));
  console.timeEnd('isNumber - just isNumber() | ⌛: ');
}());
