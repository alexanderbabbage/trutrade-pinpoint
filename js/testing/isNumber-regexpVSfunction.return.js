(function () {
  function isNumber(n){return !isNaN(parseFloat(n))&&!isNaN(n-0);}
  const isNum=/^[0-9]*\.?[0-9]+$/,
        testValues = [
          'a', 'b', 'fwqf', {}, '123ad', !1, '1243.2', [], 'avwe234', '<img>',
          true, [], {}, false, '000.3a', !0, '', ' ', null, undefined, '300', 650,
          '99999999', '00000008', '000000.000001', '0000000000000z', '$', '_',
          ['300'], ['aa', '200'], [3, 4, 5], {1: 1, 2: 2, 3: 3}, 000005, '   ',
          '300  ', '   300', 'a  20', '  a  20', NaN, [''], -9999, '-500', '  -500',
          '-1', -0, 0, '-0', '0'
        ],
        nonMatchingTestValues = [];

  let allTestsMatch = true,
      regExpTestReturn,
      funcTestReturn,
      curVal,
      curTestsMatch;

  console.log('TESTING BEGINS ...');
  console.log('===================================================');
  for (let i = testValues.length; i--;) {
    curVal = testValues[i];
    console.log('Current Value : ' + curVal);
    regExpTestReturn = isNum.test(curVal);
    funcTestReturn = isNumber(curVal);
    curTestsMatch = regExpTestReturn === funcTestReturn;
    console.log('RegExp isNum Test Result   : ' + regExpTestReturn);
    console.log('Function isNum Test Result : ' + funcTestReturn);
    console.log('TESTS MATCH? ‍ : ' + (curTestsMatch ? 'TRUE' : 'FALSE'));
    if (!curTestsMatch) {
      allTestsMatch = false;
      nonMatchingTestValues.push(curVal);
    }
    console.log('----------------------');
  }
  console.log('===================================================');
  console.log('TESTING COMPLETE ...');
  console.log('ALL TESTS MATCH? : ' + (allTestsMatch ? 'TRUE ' : 'FALSE '));
  console.log(nonMatchingTestValues);
}());
