// @ts-check

/**
 * @fileoverview This file handles some JavaScript based functionality for the
 * Reports page of the TruTrade and PinPoint web applications,
 * copyright © 2020, Alexander Babbage, Inc.
 *
 * @author Stephen M Irving
 * @version 4.2.3 10/15/20
**/

(function (global) {
  'use strict';

  const doc = global.document;

  /**
   * Activates sorting functionality on all table elements of a given class
   * name. The user can click on a table's header cell once to sort the table
   * according to the ascending order of that cell's column. Clicking the same
   * header cell again will reverse the sort into descending order. Clicking a
   * third time returns the table to its original order. The function sorts
   * a variety of data formats, including integers, floats, strings, currency,
   * percentages, and dates in the standardized formats accepted by the
   * Date.parse() API. The implementations works in in conjunction with the
   * following CSS classes which are applied by the function:
   * '.table-sort-header', 'table-sort-asc', and '.table-sort-desc'. They are
   * applied to the `th` elements that are children of the `thead` element and
   * provide a visual indication for the sorting feature itself and the
   * current sorting order (ascending, descending, or unsorted).
   *
   * @method activateTableSorting
   * @public
   *
   * @param {string} tables - The class name for the table(s) having the
   *    sorting feature applied.
   * @param {string} [docRoot=document] - The ID of a root element that
   *    contains the tables being sorted.
   */
  const activateTableSorting = (tables, docRoot) => {
    const l = parseFloat,
          m = u(/^(?:\s*)([\-+]?(?:\d+)(?:,\d{3})*)(\.\d*)?$/g, /,/g),
          g = u(/^(?:\s*)([\-+]?(?:\d+)(?:\.\d{3})*)(,\d*)?$/g, /\./g),
          getLen = x => x.length,
          containsTrue = (arr, cBack) => arr.map(cBack).indexOf(true) !== -1,
          forEach = (arr, cBack) => {
            for (let index = 0, len = getLen(arr); index < len; ++index) {
              cBack(arr[index], index);
            }
          },
          v = (n, r) => {
            r(n), forEach(n.childNodes, n => void v(n, r));
          },
          s = (n) => {
            const i = (n) => {
              const t = l(n);

              return !global.isNaN(t) && getLen(t + '') + 1 >= getLen(n)
                ? t
                : global.NaN;
            };

            let e = [];

            return (
              forEach([i, m, g], (t) => {
                getLen(e) ||
                containsTrue((t = n.map(t)), global.isNaN) ||
                (e = t);
              }),
              e
            );
          },
          activateSorting = (table) => {
            if (table.nodeName === 'TABLE') {
              const e = d(table),
                    a = e[0],
                    o = e[1],
                    u = getLen(a),
                    i = u > 1 && (getLen(a[0]) < getLen(a[1]) ? 1 : 0),
                    s = i + 1,
                    v = a[i],
                    p = getLen(v),
                    l = [],
                    m = [],
                    g = [];

              for (let h = s; h < u; ++h) {
                for (let index = 0; index < p; ++index) {
                  getLen(m) < p && m.push([]);
                  m[index].push((a[h][index].textContent || '').trim());
                }
                g.push(h - s);
              }

              const classNameSortAsc = 'table-sort-asc',
                    classNameSortDesc = 'table-sort-desc',
                    b = () => {
                      for (let n = p; n--; ) {
                        const headerClasses = v[n].classList;

                        headerClasses.remove(classNameSortAsc),
                        headerClasses.remove(classNameSortDesc),
                        (l[n] = 0);
                      }
                    };

              forEach(v, (headerCell, t) => {
                const headerClasses = headerCell.classList;

                l[t] = 0;

                headerClasses.add('table-sort-header'),
                (headerCell.title = 'Click to sort'),
                headerCell.addEventListener('click', () => {
                  const i = m[t],
                        p = g.slice(),
                        n = (k, r) => {
                          // Comparison function for sorting
                          const t = d[k],
                                e = d[r];

                          return t > e ? a : e > t ? -a : a * (k - r);
                        },
                        f = (n) => {
                          const r = n.map(Date.parse);

                          return containsTrue(r, global.isNaN) ? [] : r;
                        };

                  let parentEl = null,
                      a = l[t],
                      d = c(i),
                      // Comparison function for sorting
                      v = (n, r) => a * i[n].localeCompare(i[r]) || a * (n - r);

                  b(),
                  (a = a === 1 ? -1 : +!a) &&
                    headerClasses.add(
                      a > 0 ? classNameSortAsc : classNameSortDesc
                    ),
                  (l[t] = a);

                  (getLen(d) || getLen((d = f(i)))) && (v = n);

                  p.sort(v);

                  for (let nodeIndex = s; nodeIndex < u; ++nodeIndex) {
                    (parentEl = o[nodeIndex].parentNode)
                      .removeChild(o[nodeIndex]);
                  }

                  for (let nodeIndex = s; nodeIndex < u; ++nodeIndex) {
                    parentEl.appendChild(o[s + p[nodeIndex - s]]);
                  }
                });
              });
            }
          },
          c = (n) => {
            let z = s(n);

            if (!getLen(z)) {
              const a = (n) => {
                      let e = n[0];

                      return (
                        forEach(n, (n) => {
                          while (!n.startsWith(e)) {
                            e = e.substring(0, getLen(e) - 1);
                          }
                        }),
                        getLen(e)
                      );
                    },
                    o = a(n),
                    u = a(n.map(str => str.split('').reverse().join('')));
              z = s(n.map(n => n.substring(o, getLen(n) - u)));
            }

            return z;
          },
          d = (n) => {
            const t = [],
                  e = [];

            let r;

            return (
              v(n, (node) => {
                const curNodeName = node.nodeName;

                if (node.parentNode.nodeName !== 'TFOOT') {
                  curNodeName === 'TR'
                    ? ((r = []), t.push(r), e.push(node))
                    : (curNodeName === 'TD' || curNodeName === 'TH') &&
                      r.push(node);
                }
              }),
              [t, e]
            );
          };

    function u(n, r) {
      return (t) => {
        let e = '';

        return (
          t.replace(
            n,
            (n, t, a) => (e = t.replace(r, '') + '.' + (a || '').substring(1))
          ),
          l(e)
        );
      };
    }

    docRoot = (docRoot !== undefined ? doc.getElementById(docRoot) : doc) || doc;

    tables = docRoot.getElementsByClassName(tables);

    for (let tableNum = getLen(tables); tableNum--; ) {
      try {
        activateSorting(tables[tableNum]);
      } catch (e) {}
    }
  };

  global.addEventListener('DOMContentLoaded', () => {
    const zcomRows = doc.querySelectorAll('.js-zcom-row'),
          elRecordCount = doc.getElementById('zcom_record_count'),
          toggleCbx = doc.getElementById('toggleCbx'),
          stateField = doc.querySelector('.js-toggle-state-hidden-field'),
          numRows = zcomRows.length;

    elRecordCount.textContent = numRows;

    toggleCbx.addEventListener('change', (evt) => {
      const dropdownWrappers = doc.querySelectorAll('.nav-dropdown-wrapper'),
            toggleIsAll = evt.target.checked;

      for (let i = 0; i < 4; ++i) {
        dropdownWrappers[i].classList.toggle('is-hidden');
      }

      stateField && (stateField.value = (toggleIsAll ? 'all' : 'my'));

      evt.stopPropagation();
    }, { passive: true });

    stateField && stateField.value === 'all' && toggleCbx.click();

    activateTableSorting('ttzc-table--zcom', 'zcom_tile');
  });
})(window);
