# Convert main.min.js and main.ie.min.js from TruTrade to the PinPoint Version

Copy the newly minified version into the version prefixed with pp- and then
Find and Replace the Following Values in both files

* "#2a3c68" >>> "#a11d27" (4 instances)

* "#39518c" >>> "#d51f27" (2 instances)

* "#d4d2e4" >>> "#dab9b9" (1 instance)

* "rgba(79,162,217,.14)" >>> "rgba(216,79,79,.1)" (2 instances)
