// @ts-check

(function (global) {
  'use strict';

  var doc = global.document,
      uAgent = navigator.userAgent;

  // Opera 8.0+
  var isOpera =
    (!!global.opr && !!opr.addons) ||
    !!global.opera ||
    uAgent.indexOf(' OPR/') >= 0;

  // Firefox 1.0+
  var isFirefox = typeof InstallTrigger !== 'undefined';

  // Safari 3.0+ '[object HTMLElementConstructor]'
  var isSafari =
    /constructor/i.test(global.HTMLElement) ||
    (function (p) {
      return p.toString() === '[object SafariRemoteNotification]';
    })(
      !global['safari'] ||
      (typeof safari !== 'undefined' && safari.pushNotification)
    );

  // Internet Explorer 6-11
  var isIe = /*@cc_on!@*/false || !!doc.documentMode;

  // Edge 20+
  var isEdge = !isIe && !!global.StyleMedia;

  // Chrome 1 - 79
  var isChrome =
    !!global.chrome &&
    (!!global.chrome.webstore || !!global.chrome.runtime);

  // Edge (based on chromium) detection
  var isEdgeChromium = isChrome && (uAgent.indexOf('Edg') !== -1);

  if (isIe) {
    // TODO: Put sweet alert for unsupported browser (IE) here
  } else if (isChrome && !isEdgeChromium) {
    var chromeVersion =
      +uAgent
        .substring(uAgent.indexOf('Chrome/') + 7)
        .substring(0, 2);

    if (chromeVersion < 60) {
      // TODO: Put sweet alert for unsupported Chrome version here
    }
  } else if (isFirefox) {
    var firefoxVersion =
      +uAgent
        .substring(uAgent.indexOf('Firefox/') + 8)
        .substring(0, 2);

    if (firefoxVersion < 52) {
      // TODO: Put sweet alert for unsupported Firefox version here
    }
  } else if (isEdge && !isEdgeChromium) {
    // TODO: Put sweet alert for legacy edge here
  } else if (isSafari) {
    var safariVersion =
      +uAgent
        .substring(uAgent.indexOf('Version/') + 8)
        .substring(0, 2);

    if (safariVersion < 11) {
      // TODO: Put sweeet alert here for unsupported Safari version
    }
  } else if (isOpera) {
    var operaVersion =
      +uAgent
        .substring(uAgent.indexOf('OPR/') + 4)
        .substring(0, 2);

    if (operaVersion < 47) {
      // TODO: Put sweet alert for unsupported Opera version here
    }
  }
}(this));
