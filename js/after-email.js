function messageAttempted(e) {
  ((e.hasError)
    ? swal({
      title: 'Error Encountered',
      text: 'Message was not sent because of an error. Please ensure all fields were filled out correctly and then try again. If the problem persists, go to the Help section, or contact your Alexander Babbage representative.',
      icon: 'error',
      button: 'OK'
    })
    : swal({
      title: 'Success!',
      text: 'Your message was sent successfully.',
      icon: 'success',
      button: 'OK'
    })
  );
}
