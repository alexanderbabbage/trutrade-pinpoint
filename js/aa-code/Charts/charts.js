/**
 * @fileoverview This file handles the majority of the front-end JavaScript
 * functionality for the Charts page of the TruTrade and PinPoint web
 * applications, copyright © 2020, Alexander Babbage, Inc.
 *
 * @author Stephen M Irving
 * @version 4.2.3 10/15/20
 * @requires a5.js
 * @requires sweetalert.js
 * @link https://sweetalert.js.org/
**/

/**
 * @namespace CHARTS
 */
var CHARTS = CHARTS || {};

(function (global) {
  'use strict';

  const doc = global.document;

  CHARTS = (() => {
    /**
     * An error handler that tracks any errors caused by this script, privately
     * storing them using a closure.
     *
     * @member {Object} CHARTS.errHandler
     * @memberof CHARTS
     * @public
     */
    const errHandler = (() => {
      /** @const {Error[]} */
      const _errors = [];

      return {
        /**
         * Get the array of Error objects.
         *
         * @type {Error[]}
         * @memberof CHARTS.errHandler
         * @public
         */
        get errors() {
          return (
            _errors.length
              ? _errors
              : console.log('No errors have been logged.')
          );
        },

        /**
         * Log an error with the handler.
         *
         * @method log
         * @memberof CHARTS.errHandler
         * @public
         *
         * @param {Error} error - The Error object.
         */
        log(error) {
          _errors.push(error);
        },

        /**
         * Clear the log of all error messages.
         *
         * @method clear
         * @memberof CHARTS.errHandler
         * @public
         */
        clear() {
          _errors.length = 0;
        },

        /**
         * Print all logged error messages to the console, starting with the
         * most recent.
         *
         * @method print
         * @memberof CHARTS.errHandler
         * @public
         *
         * @param {string} [errType] - You may optionally pass an error type to
         *    the print method so that only errors of that type are displayed.
         */
        print(errType) {
          if (_errors.length) {
            if (errType === undefined) {
              for (let i = _errors.length; i--;) {
                console.error(_errors[i]);
              }
            } else {
              let foundErrType = false;

              errType = errType.toLowerCase();

              console.group('Error Log');
              for (let i = _errors.length; i--;) {
                if (_errors[i].name.toLowerCase().indexOf(errType) !== -1) {
                  console.error(_errors[i]);
                  foundErrType = true;
                }
              }
              !foundErrType &&
                console.log(`No errors of type [ ${errType} ] have been logged`);
              console.groupEnd('Error Log');
            }
          } else {
            console.log('No errors have been logged.');
          }
        }
      };
    })();

    /**
     * Prints a default error message which can be replaced with a custom
     * message by passing it as an argument.
     *
     * @method errMsg
     * @memberof CHARTS
     * @public
     *
     * @param {string} [msg] - If a message other than the default error message
     *    is needed, the custom message can be passed as an argument.
     */
    const errMsg = (msg) => {
      msg = msg ||
        'Your request for data has timed out. Please try again. ' +
        'If this problem persists, go to the Help section, or ' +
        'contact your Alexander Babbage representative.';

      swal({
        title: 'Error Encountered!',
        text: msg,
        icon: 'error',
        button: 'OK'
      });
    };

    /**
     * Allows the user to download a selected chart.
     *
     * @method saveVis
     * @memberof CHARTS
     * @public
     *
     * @param {string|number} chartVal - The numbers or letters assigned to
     *    to the chart to differentiate it from the others.
     */
    const saveVis = (chartVal) => {
      const chart = (chartVal !== 1
              ? '{dialog.object}'['_chart' + chartVal]
              : '{dialog.object}'._chart
            ),
            swalErrMsg = (msg) => {
              swal({
                title: 'Error Encountered!',
                text: msg,
                icon: 'error',
                button: 'OK'
              });
            };

      if (chart !== undefined) {
        (typeof chart.getImageURI === 'function'
          ? '{dialog.object}'.ajaxCallback(
            '', '', 'printChart', '',
            A5.ajax.buildURLParam('_base64data', chart.getImageURI())
          )
          : swalErrMsg('This type of chart does not currently support downloading.')
        );
      } else {
        swalErrMsg('There was an error downloading this chart. Please try ' +
            'again. If the problem persists, contact your Alexander Babbage ' +
            'representative.');
      }
    };

    return {
      errHandler,
      errMsg,
      saveVis
    };
  })();

  global.addEventListener('DOMContentLoaded', () => {
    const toggleCbx = doc.getElementById('toggleCbx'),
          stateField = doc.querySelector('.js-toggle-state-hidden-field');

    toggleCbx.addEventListener('change', (evt) => {
      const dropdownWrappers = doc.querySelectorAll('.nav-dropdown-wrapper'),
            toggleIsAll = evt.target.checked;

      for (let i = 0; i < 4; ++i) {
        dropdownWrappers[i].classList.toggle('is-hidden');
      }

      stateField && (stateField.value = (toggleIsAll ? 'all' : 'my'));

      evt.stopPropagation();
    }, { passive: true });

    stateField && stateField.value === 'all' && toggleCbx.click();
  }, { passive: true, once: true });
})(this);
