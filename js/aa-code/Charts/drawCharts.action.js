(function () {
  const drawVis = CHARTS.drawVis,
        percentConfig1 = {
          pattern: '#%',
          width: '85%',
          format: '###%'
        },
        percentConfig2 = {
          pattern: '#%',
          width: '100%',
          format: '###%'
        },
        decimalConfig = {
          pattern: '#.#',
          width: '100%',
          format: '###.#'
        },
        stackedConfig1 = {
          pattern: '#%',
          width: '85%',
          format: '###.#%',
          isBar: true,
          stacked: 'percent'
        },
        stackedConfig2 = {
          pattern: '#%',
          width: '100%',
          format: '###.#%',
          isBar: true,
          stacked: 'percent'
        };

  drawVis('visualization', 1, {
    pattern: '$',
    width: '85%',
    format: '$#,###'
  });

  // Full-size Household Income chart - Default chart on initial paint
  drawVis('v', 1, {
    pattern: '$#,###',
    width: '100%',
    format: '$#,###'
  });

  drawVis('visualization2', 2, percentConfig1);

  drawVis('v2', 2, percentConfig2);

  drawVis('visualization3', 3, percentConfig1);

  drawVis('v3', 3, percentConfig2);

  drawVis('visualization4', 4, percentConfig1);

  drawVis('v4', 4, percentConfig2);

  drawVis('visualization5', 5);

  drawVis('v5', 5, decimalConfig);

  drawVis('visualization6', 6);

  drawVis('v6', 6, decimalConfig);

  drawVis('visualization7', 7);

  drawVis('v7', 7, decimalConfig);

  drawVis('visualization8', 8);

  drawVis('v8', 8, decimalConfig);

  // Traffic chart
  // TODO: Fix the pattern here and in charts.js, pattern makes no sense for
  //       chart because it does not use '$' but needed to but the '$' there
  //       in order for the CHARTS.drawVis method to give the chart the correct
  //       configuration
  drawVis('v9', 9, {
    pattern: '#,###',
    width: '100%',
    format: 'decimal'
  });

  drawVis('visualization10', 10, {
    pattern: '#.#',
    width: '85%',
    format: '###.#%'
  });

  drawVis('v10', 10, {
    pattern: '#%',
    width: '100%',
    format: '###.#%'
  });

  drawVis('visualization11', 11, {
    pattern: '#%',
    width: '85%',
    format: '###.#%',
    isBar: true,
    stacked: 'true'
  });

  drawVis('v11', 11, {
    pattern: '#%',
    width: '100%',
    format: '###.#%',
    isBar: true,
    stacked: 'true'
  });

  drawVis('visualization12', 12, stackedConfig1);

  drawVis('v12', 12, stackedConfig2);

  drawVis('visualizationFV', 'FV', stackedConfig1);

  drawVis('vFV', 'FV', stackedConfig2);
})();
