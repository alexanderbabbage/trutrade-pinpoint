function dateCheckRefresh() {
  var s = new Date('{dialog.object}'.getValue('FromDate')),
      e = new Date('{dialog.object}'.getValue('ToDate'));
  s.getTime() <= e.getTime()
    ? '{dialog.object}'.runAction('getCharts')
    : swal({
      title: 'Error Encountered',
      text:
          'The date range you have chosen for the charts is incorrect. You must choose an end date that is after the chosen start date. Please check the dropdowns labeled Start Date and End Date and try again.',
      icon: 'error',
      button: 'OK'
    });
}
function genericErrMsg() {
  swal({
    title: 'Error Encountered!',
    text:
      'Your request for data has timed out. Please try again. If this problem persists, go to the Help section, or contact your Alexander Babbage representative.',
    icon: 'error',
    button: 'OK'
  });
}
function drawVisualization() {
  try {
    var chartdata = '{dialog.object}'._data,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 2,
          prefix: '$'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '85%',
        height: '80%',
        top: 25,
        left: 70
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      vAxis: {
        textPosition: 'out',
        format: '$#,###',
        gridlines: { color: 'none' }
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      legend: { position: 'bottom', maxLines: 4 },
      curveType: 'function'
    };
    var chart = new google.visualization.LineChart(
      document.getElementById('visualization')
    );
    chart.draw(data, options);
    '{dialog.object}'._chart = chart;
  } catch (err) {}
}
function drawV() {
  try {
    var chartdata = '{dialog.object}'._data,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 2,
          pattern: '$#,###'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '100%',
        height: '80%',
        top: 25,
        left: 100
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      vAxis: {
        textPosition: 'out',
        format: 'currency',
        gridlines: { color: 'none' }
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      legend: { position: 'bottom', maxLines: 4 },
      curveType: 'function'
    };
    var chart = new google.visualization.LineChart(document.getElementById('v'));
    chart.draw(data, options);
    '{dialog.object}'._chart = chart;
  } catch (err) {}
}
function drawVisualization2() {
  try {
    var chartdata = '{dialog.object}'._data2,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 2,
          pattern: '#%'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '85%',
        height: '80%',
        top: 25,
        left: 50
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      vAxis: {
        textPosition: 'out',
        format: '###%',
        gridlines: { color: 'none' }
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      legend: { position: 'bottom', maxLines: 4 },
      curveType: 'function'
    };
    var chart = new google.visualization.LineChart(
      document.getElementById('visualization2')
    );
    chart.draw(data, options);
    '{dialog.object}'._chart2 = chart;
  } catch (err) {}
}
function drawV2() {
  try {
    var chartdata = '{dialog.object}'._data2,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 2,
          pattern: '#%'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '100%',
        height: '80%',
        top: 25,
        left: 50
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      vAxis: {
        textPosition: 'out',
        format: '###%',
        gridlines: { color: 'none' }
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      legend: { position: 'bottom', maxLines: 4 },
      curveType: 'function'
    };
    var chart = new google.visualization.LineChart(document.getElementById('v2'));
    chart.draw(data, options);
    '{dialog.object}'._chart2 = chart;
  } catch (err) {}
}
function drawVisualization3() {
  try {
    var chartdata = '{dialog.object}'._data3,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 2,
          pattern: '#%'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '85%',
        height: '80%',
        top: 25,
        left: 50
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      vAxis: {
        textPosition: 'out',
        format: '###%',
        gridlines: { color: 'none' }
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      legend: { position: 'bottom', maxLines: 4 },
      curveType: 'function'
    };
    var chart = new google.visualization.LineChart(
      document.getElementById('visualization3')
    );
    chart.draw(data, options);
    '{dialog.object}'._chart3 = chart;
  } catch (err) {}
}
function drawV3() {
  try {
    var chartdata = '{dialog.object}'._data3,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 2,
          pattern: '#%'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '100%',
        height: '80%',
        top: 25,
        left: 50
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      vAxis: {
        textPosition: 'out',
        format: '###%',
        gridlines: { color: 'none' }
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      legend: { position: 'bottom', maxLines: 4 },
      curveType: 'function'
    };
    var chart = new google.visualization.LineChart(document.getElementById('v3'));
    chart.draw(data, options);
    '{dialog.object}'._chart3 = chart;
  } catch (err) {}
}
function drawVisualization4() {
  try {
    var chartdata = '{dialog.object}'._data4,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 2,
          pattern: '#%'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '85%',
        height: '80%',
        top: 25,
        left: 50
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      vAxis: {
        textPosition: 'out',
        format: '###%',
        gridlines: { color: 'none' }
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      legend: { position: 'bottom', maxLines: 4 },
      curveType: 'function'
    };
    var chart = new google.visualization.LineChart(
      document.getElementById('visualization4')
    );
    chart.draw(data, options);
    '{dialog.object}'._chart4 = chart;
  } catch (err) {}
}
function drawV4() {
  try {
    var chartdata = '{dialog.object}'._data4,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 2,
          pattern: '#%'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '100%',
        height: '80%',
        top: 25,
        left: 50
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      vAxis: {
        textPosition: 'out',
        format: '###%',
        gridlines: { color: 'none' }
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      legend: { position: 'bottom', maxLines: 4 },
      curveType: 'function'
    };
    var chart = new google.visualization.LineChart(document.getElementById('v4'));
    chart.draw(data, options);
    '{dialog.object}'._chart4 = chart;
  } catch (err) {}
}
function drawVisualization5() {
  try {
    var chartdata = '{dialog.object}'._data5,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 1,
          pattern: '#.#'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '85%',
        height: '80%',
        top: 25,
        left: 50
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      vAxis: {
        textPosition: 'out',
        gridlines: { color: 'none' },
        format: '###.#'
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      legend: { position: 'bottom', maxLines: 4 },
      curveType: 'function'
    };
    var chart = new google.visualization.LineChart(
      document.getElementById('visualization5')
    );
    chart.draw(data, options);
    '{dialog.object}'._chart5 = chart;
  } catch (err) {}
}
function drawV5() {
  try {
    var chartdata = '{dialog.object}'._data5,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 1,
          pattern: '#.#'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '100%',
        height: '80%',
        top: 25,
        left: 50
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      vAxis: {
        textPosition: 'out',
        gridlines: { color: 'none' },
        format: '###.#'
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      legend: { position: 'bottom', maxLines: 4 },
      curveType: 'function'
    };
    var chart = new google.visualization.LineChart(document.getElementById('v5'));
    chart.draw(data, options);
    '{dialog.object}'._chart5 = chart;
  } catch (err) {}
}
function drawVisualization6() {
  try {
    var chartdata = '{dialog.object}'._data6,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 1,
          pattern: '#.#'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '85%',
        height: '80%',
        top: 25,
        left: 50
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      vAxis: {
        textPosition: 'out',
        gridlines: { color: 'none' },
        format: '###.#'
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      legend: { position: 'bottom', maxLines: 4 },
      curveType: 'function'
    };
    var chart = new google.visualization.LineChart(
      document.getElementById('visualization6')
    );
    chart.draw(data, options);
    '{dialog.object}'._chart6 = chart;
  } catch (err) {}
}
function drawV6() {
  try {
    var chartdata = '{dialog.object}'._data6,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 1,
          pattern: '#.#'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '100%',
        height: '80%',
        top: 25,
        left: 50
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      vAxis: {
        textPosition: 'out',
        gridlines: { color: 'none' },
        format: '###.#'
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      legend: { position: 'bottom', maxLines: 4 },
      curveType: 'function'
    };
    var chart = new google.visualization.LineChart(document.getElementById('v6'));
    chart.draw(data, options);
    '{dialog.object}'._chart6 = chart;
  } catch (err) {}
}
function drawVisualization7() {
  try {
    var chartdata = '{dialog.object}'._data7,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 1,
          pattern: '#.#'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '85%',
        height: '80%',
        top: 25,
        left: 50
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      vAxis: {
        textPosition: 'out',
        gridlines: { color: 'none' },
        format: '###.#'
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      legend: { position: 'bottom', maxLines: 4 },
      curveType: 'function'
    };
    var chart = new google.visualization.LineChart(
      document.getElementById('visualization7')
    );
    chart.draw(data, options);
    '{dialog.object}'._chart7 = chart;
  } catch (err) {}
}
function drawV7() {
  try {
    var chartdata = '{dialog.object}'._data7,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 1,
          pattern: '#.#'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '100%',
        height: '80%',
        top: 25,
        left: 50
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      vAxis: {
        textPosition: 'out',
        gridlines: { color: 'none' },
        format: '###.#'
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      legend: { position: 'bottom', maxLines: 4 },
      curveType: 'function'
    };
    var chart = new google.visualization.LineChart(document.getElementById('v7'));
    chart.draw(data, options);
    '{dialog.object}'._chart7 = chart;
  } catch (err) {}
}
function drawVisualization8() {
  try {
    var chartdata = '{dialog.object}'._data8,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 1,
          pattern: '#.#'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '85%',
        height: '80%',
        top: 25,
        left: 50
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      vAxis: {
        textPosition: 'out',
        gridlines: { color: 'none' },
        format: '###.#'
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      legend: { position: 'bottom', maxLines: 4 },
      curveType: 'function'
    };
    var chart = new google.visualization.LineChart(
      document.getElementById('visualization8')
    );
    chart.draw(data, options);
    '{dialog.object}'._chart8 = chart;
  } catch (err) {}
}
function drawV8() {
  try {
    var chartdata = '{dialog.object}'._data8,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 1,
          pattern: '#.#'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '100%',
        height: '80%',
        top: 25,
        left: 50
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      vAxis: {
        textPosition: 'out',
        gridlines: { color: 'none' },
        format: '###.#'
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      legend: { position: 'bottom', maxLines: 4 },
      curveType: 'function'
    };
    var chart = new google.visualization.LineChart(document.getElementById('v8'));
    chart.draw(data, options);
    '{dialog.object}'._chart8 = chart;
  } catch (err) {}
}
function drawVisualization10() {
  try {
    var chartdata = '{dialog.object}'._data10,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 1,
          pattern: '#.#'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '85%',
        height: '80%',
        top: 25,
        left: 50
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      vAxis: {
        textPosition: 'out',
        gridlines: { color: 'none' },
        format: '###.#%'
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      legend: { position: 'bottom', maxLines: 4 },
      curveType: 'function'
    };
    var chart = new google.visualization.LineChart(
      document.getElementById('visualization10')
    );
    chart.draw(data, options);
    '{dialog.object}'._chart10 = chart;
  } catch (err) {}
}
function drawV10() {
  try {
    var chartdata = '{dialog.object}'._data10,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 2,
          pattern: '#%'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '85%',
        height: '80%',
        top: 25,
        left: 50
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      vAxis: {
        textPosition: 'out',
        gridlines: { color: 'none' },
        format: '###.#%'
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      legend: { position: 'bottom', maxLines: 4 },
      curveType: 'function'
    };
    var chart = new google.visualization.LineChart(
      document.getElementById('v10')
    );
    chart.draw(data, options);
    '{dialog.object}'._chart10 = chart;
  } catch (err) {}
}
function drawVisualization11() {
  try {
    var chartdata = '{dialog.object}'._data11,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 2,
          pattern: '#%'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '85%',
        height: '80%',
        top: 25,
        left: 50
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      legend: { position: 'bottom', maxLines: 4 },
      vAxis: {
        textPosition: 'out',
        gridlines: { color: 'none' },
        format: '###.#%'
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      bar: { groupWidth: '75%' },
      isStacked: 'true'
    };
    var chart = new google.visualization.ColumnChart(
      document.getElementById('visualization11')
    );
    chart.draw(data, options);
    '{dialog.object}'._chart11 = chart;
  } catch (err) {}
}
function drawV11() {
  try {
    var chartdata = '{dialog.object}'._data11,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 2,
          pattern: '#%'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '85%',
        height: '80%',
        top: 25,
        left: 50
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      legend: { position: 'bottom', maxLines: 4 },
      vAxis: {
        textPosition: 'out',
        gridlines: { color: 'none' },
        format: '###.#%'
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      bar: { groupWidth: '75%' },
      isStacked: 'true'
    };
    var chart = new google.visualization.ColumnChart(
      document.getElementById('v11')
    );
    chart.draw(data, options);
    '{dialog.object}'._chart11 = chart;
  } catch (err) {}
}
function drawVisualization12() {
  try {
    var chartdata = '{dialog.object}'._data12,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 2,
          pattern: '#%'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '85%',
        height: '80%',
        top: 25,
        left: 50
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      legend: { position: 'bottom', maxLines: 4 },
      vAxis: {
        textPosition: 'out',
        gridlines: { color: 'none' },
        format: '###.#%'
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      bar: { groupWidth: '75%' },
      isStacked: 'percent'
    };
    var chart = new google.visualization.ColumnChart(
      document.getElementById('visualization12')
    );
    chart.draw(data, options);
    '{dialog.object}'._chart12 = chart;
  } catch (err) {}
}
function drawV12() {
  try {
    var chartdata = '{dialog.object}'._data12,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 2,
          pattern: '#%'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '85%',
        height: '80%',
        top: 25,
        left: 50
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      legend: { position: 'bottom', maxLines: 4 },
      vAxis: {
        textPosition: 'out',
        gridlines: { color: 'none' },
        format: '###.#%'
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      bar: { groupWidth: '75%' },
      isStacked: 'percent'
    };
    var chart = new google.visualization.ColumnChart(
      document.getElementById('v12')
    );
    chart.draw(data, options);
    '{dialog.object}'._chart12 = chart;
  } catch (err) {}
}
function drawVisualizationFV() {
  try {
    var chartdata = '{dialog.object}'._dataFV,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 2,
          pattern: '#%'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '85%',
        height: '80%',
        top: 25,
        left: 50
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      legend: { position: 'bottom', maxLines: 4 },
      vAxis: {
        textPosition: 'out',
        gridlines: { color: 'none' },
        format: '###.#%'
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      bar: { groupWidth: '75%' },
      isStacked: 'percent'
    };
    var chart = new google.visualization.ColumnChart(
      document.getElementById('visualizationFV')
    );
    chart.draw(data, options);
    '{dialog.object}'._chartFV = chart;
  } catch (err) {}
}
function drawVFV() {
  try {
    var chartdata = '{dialog.object}'._dataFV,
        data = google.visualization.arrayToDataTable(chartdata),
        numCols = chartdata[0].length,
        formatter = new google.visualization.NumberFormat({
          fractionDigits: 2,
          pattern: '#%'
        });
    for (let i = 1; i < numCols; i++) formatter.format(data, i);
    var options = {
      chartArea: {
        backgroundColor: { stroke: '#BCBEC0' },
        width: '85%',
        height: '80%',
        top: 25,
        left: 50
      },
      titlePosition: 'none',
      colors: [
        '#2A3C68',
        '#F7C01C',
        '#D43045',
        '#4FA2D9',
        '#5D3B97',
        '#366952',
        '#FCE6A3',
        '#F3C1C8',
        '#8ED8F8',
        '#96CB69',
        '#B0CCBF'
      ],
      legend: { position: 'bottom', maxLines: 4 },
      vAxis: {
        textPosition: 'out',
        gridlines: { color: 'none' },
        format: '###.#%'
      },
      hAxis: {
        format: 'MMM yyyy',
        showTextEvery: 2,
        gridlines: { color: 'none' }
      },
      bar: { groupWidth: '75%' },
      isStacked: 'percent'
    };
    var chart = new google.visualization.ColumnChart(
      document.getElementById('vFV')
    );
    chart.draw(data, options);
    '{dialog.object}'._chartFV = chart;
  } catch (err) {}
}
function downloadChart() {
  var chart = '{dialog.object}'._chart;
  if ('undefined' === typeof chart) {
    return (
      swal(
        'You must click a button to select a chart type before you can download a chart.',
        'error'
      ),
      !1
    );
  }
  if ('function' === typeof chart.getImageURI) {
    var a = chart.getImageURI(),
        t = A5.ajax.buildURLParam('_base64data', a);
    '{dialog.object}'.ajaxCallback('', '', 'printChart', '', t);
  } else swal('This type of chart does not support downloading.', 'error');
}
function downloadChart2() {
  var chart = '{dialog.object}'._chart2;
  if ('undefined' === typeof chart) {
    return (
      swal(
        'You must click a button to select a chart type before you can download a chart.',
        'error'
      ),
      !1
    );
  }
  if ('function' === typeof chart.getImageURI) {
    var a = chart.getImageURI(),
        t = A5.ajax.buildURLParam('_base64data', a);
    '{dialog.object}'.ajaxCallback('', '', 'printChart', '', t);
  } else swal('This type of chart does not support downloading.', 'error');
}
function downloadChart3() {
  var chart = '{dialog.object}'._chart3;
  if ('undefined' === typeof chart) {
    return (
      swal(
        'You must click a button to select a chart type before you can download a chart.',
        'error'
      ),
      !1
    );
  }
  if ('function' === typeof chart.getImageURI) {
    var a = chart.getImageURI(),
        t = A5.ajax.buildURLParam('_base64data', a);
    '{dialog.object}'.ajaxCallback('', '', 'printChart', '', t);
  } else swal('This type of chart does not support downloading.', 'error');
}
function downloadChart4() {
  var chart = '{dialog.object}'._chart4;
  if ('undefined' === typeof chart) {
    return (
      swal(
        'You must click a button to select a chart type before you can download a chart.',
        'error'
      ),
      !1
    );
  }
  if ('function' === typeof chart.getImageURI) {
    var a = chart.getImageURI(),
        t = A5.ajax.buildURLParam('_base64data', a);
    '{dialog.object}'.ajaxCallback('', '', 'printChart', '', t);
  } else swal('This type of chart does not support downloading.', 'error');
}
function downloadChart5() {
  var chart = '{dialog.object}'._chart5;
  if ('undefined' === typeof chart) {
    return (
      swal(
        'You must click a button to select a chart type before you can download a chart.',
        'error'
      ),
      !1
    );
  }
  if ('function' === typeof chart.getImageURI) {
    var a = chart.getImageURI(),
        t = A5.ajax.buildURLParam('_base64data', a);
    '{dialog.object}'.ajaxCallback('', '', 'printChart', '', t);
  } else swal('This type of chart does not support downloading.', 'error');
}
function downloadChart6() {
  var chart = '{dialog.object}'._chart6;
  if ('undefined' === typeof chart) {
    return (
      swal(
        'You must click a button to select a chart type before you can download a chart.',
        'error'
      ),
      !1
    );
  }
  if ('function' === typeof chart.getImageURI) {
    var a = chart.getImageURI(),
        t = A5.ajax.buildURLParam('_base64data', a);
    '{dialog.object}'.ajaxCallback('', '', 'printChart', '', t);
  } else swal('This type of chart does not support downloading.', 'error');
}
function downloadChart7() {
  var chart = '{dialog.object}'._chart7;
  if ('undefined' === typeof chart) {
    return (
      swal(
        'You must click a button to select a chart type before you can download a chart.',
        'error'
      ),
      !1
    );
  }
  if ('function' === typeof chart.getImageURI) {
    var a = chart.getImageURI(),
        t = A5.ajax.buildURLParam('_base64data', a);
    '{dialog.object}'.ajaxCallback('', '', 'printChart', '', t);
  } else swal('This type of chart does not support downloading.', 'error');
}
function downloadChart8() {
  var chart = '{dialog.object}'._chart8;
  if ('undefined' === typeof chart) {
    return (
      swal(
        'You must click a button to select a chart type before you can download a chart.',
        'error'
      ),
      !1
    );
  }
  if ('function' === typeof chart.getImageURI) {
    var a = chart.getImageURI(),
        t = A5.ajax.buildURLParam('_base64data', a);
    '{dialog.object}'.ajaxCallback('', '', 'printChart', '', t);
  } else swal('This type of chart does not support downloading.', 'error');
}
function downloadChart9() {
  var chart = '{dialog.object}'._chart9;
  if ('undefined' === typeof chart) {
    return (
      swal(
        'You must click a button to select a chart type before you can download a chart.',
        'error'
      ),
      !1
    );
  }
  if ('function' === typeof chart.getImageURI) {
    var a = chart.getImageURI(),
        t = A5.ajax.buildURLParam('_base64data', a);
    '{dialog.object}'.ajaxCallback('', '', 'printChart', '', t);
  } else swal('This type of chart does not support downloading.', 'error');
}
function downloadChart10() {
  var chart = '{dialog.object}'._chart10;
  if ('undefined' === typeof chart) {
    return (
      swal(
        'You must click a button to select a chart type before you can download a chart.',
        'error'
      ),
      !1
    );
  }
  if ('function' === typeof chart.getImageURI) {
    var a = chart.getImageURI(),
        t = A5.ajax.buildURLParam('_base64data', a);
    '{dialog.object}'.ajaxCallback('', '', 'printChart', '', t);
  } else swal('This type of chart does not support downloading.', 'error');
}
function downloadChart11() {
  var chart = '{dialog.object}'._chart11;
  if ('undefined' === typeof chart) {
    return (
      swal(
        'You must click a button to select a chart type before you can download a chart.',
        'error'
      ),
      !1
    );
  }
  if ('function' === typeof chart.getImageURI) {
    var a = chart.getImageURI(),
        t = A5.ajax.buildURLParam('_base64data', a);
    '{dialog.object}'.ajaxCallback('', '', 'printChart', '', t);
  } else swal('This type of chart does not support downloading.', 'error');
}
function downloadChart12() {
  var chart = '{dialog.object}'._chart12;
  if ('undefined' === typeof chart) {
    return (
      swal(
        'You must click a button to select a chart type before you can download a chart.',
        'error'
      ),
      !1
    );
  }
  if ('function' === typeof chart.getImageURI) {
    var a = chart.getImageURI(),
        t = A5.ajax.buildURLParam('_base64data', a);
    '{dialog.object}'.ajaxCallback('', '', 'printChart', '', t);
  } else swal('This type of chart does not support downloading.', 'error');
}
function downloadChartMS() {
  var chart = '{dialog.object}'._chartMS;
  if ('undefined' === typeof chart) {
    return (
      swal(
        'You must click a button to select a chart type before you can download a chart.',
        'error'
      ),
      !1
    );
  }
  if ('function' === typeof chart.getImageURI) {
    var a = chart.getImageURI(),
        t = A5.ajax.buildURLParam('_base64data', a);
    '{dialog.object}'.ajaxCallback('', '', 'printChart', '', t);
  } else swal('This type of chart does not support downloading.', 'error');
}
function downloadChartFV() {
  var chart = '{dialog.object}'._chartFV;
  if ('undefined' === typeof chart) {
    return (
      swal(
        'You must click a button to select a chart type before you can download a chart.',
        'error'
      ),
      !1
    );
  }
  if ('function' === typeof chart.getImageURI) {
    var a = chart.getImageURI(),
        t = A5.ajax.buildURLParam('_base64data', a);
    '{dialog.object}'.ajaxCallback('', '', 'printChart', '', t);
  } else swal('This type of chart does not support downloading.', 'error');
}
