/**
 * @fileoverview This file handles the majority of the front-end JavaScript
 * functionality for the Charts page of the TruTrade and PinPoint web
 * applications, copyright © 2020, Alexander Babbage, Inc.
 *
 * @author Stephen M Irving
 * @version 4.0.0 07/10/20
 * @requires a5.js
 * @requires sweetalert.js
 * @link https://sweetalert.js.org/
**/

/**
 * @namespace CHARTS
 */
var CHARTS = CHARTS || {};

(function (doc) {
  'use strict';

  CHARTS = (() => {
    /**
     * An error handler that tracks any errors caused by this script, privately
     * storing them using a closure.
     *
     * @member {Object} CHARTS.errHandler
     * @memberof CHARTS
     * @public
     */
    const errHandler = (() => {
      /** @const {Error[]} */
      const _errors = [];

      return {
        /**
         * Get the array of Error objects.
         *
         * @type {Error[]}
         * @memberof CHARTS.errHandler
         * @public
         */
        get errors() {
          return (
            _errors.length
              ? _errors
              : console.log('No errors have been logged.')
          );
        },

        /**
         * Log an error with the handler.
         *
         * @method log
         * @memberof CHARTS.errHandler
         * @public
         *
         * @param {Error} error - The Error object.
         */
        log(error) {
          _errors.push(error);
        },

        /**
         * Clear the log of all error messages.
         *
         * @method clear
         * @memberof CHARTS.errHandler
         * @public
         */
        clear() {
          _errors.length = 0;
        },

        /**
         * Print all logged error messages to the console, starting with the
         * most recent.
         *
         * @method print
         * @memberof CHARTS.errHandler
         * @public
         *
         * @param {string} [errType] - You may optionally pass an error type to
         *    the print method so that only errors of that type are displayed.
         */
        print(errType) {
          if (_errors.length) {
            if (errType === undefined) {
              for (let i = _errors.length; i--;) {
                console.error(_errors[i]);
              }
            } else {
              let foundErrType = false;

              errType = errType.toLowerCase();

              console.group('Error Log');
              for (let i = _errors.length; i--;) {
                if (_errors[i].name.toLowerCase().indexOf(errType) !== -1) {
                  console.error(_errors[i]);
                  foundErrType = true;
                }
              }
              !foundErrType &&
                console.log(`No errors of type [ ${errType} ] have been logged`);
              console.groupEnd('Error Log');
            }
          } else {
            console.log('No errors have been logged.');
          }
        }
      };
    })();

    /**
     * When the date dropdowns are changed checkDate tests to ensure that the
     * 'from' date is before the 'to' date. If the dates make logical sense
     * the getCharts action is run, otherwise it will print an error message.
     *
     * @method checkDate
     * @memberof CHARTS
     * @public
     */
    const checkDate = () => {
      const fromDate = new Date('{dialog.object}'.getValue('FromDate')),
            toDate = new Date('{dialog.object}'.getValue('ToDate'));

      (fromDate.getTime() < toDate.getTime()
        ? '{dialog.object}'.runAction('getCharts')
        : swal({
          title: 'Error Encountered',
          text:
            'The date range you have chosen for the charts is incorrect.\n' +
            'You must choose an end date that is after the chosen start' +
            ' date. Please select a valid time range from the dropdown ' +
            'controls labeled Start Date and End Date.',
          icon: 'error',
          button: 'OK'
        })
      );
    };

    /**
     * Prints a default error message which can be replaced with a custom
     * message by passing it as an argument.
     *
     * @method errMsg
     * @memberof CHARTS
     * @public
     *
     * @param {string} [msg] - If a message other than the default error message
     *    is needed, the custom message can be passed as an argument.
     */
    const errMsg = (msg) => {
      msg = msg ||
        'Your request for data has timed out. Please try again. ' +
        'If this problem persists, go to the Help section, or ' +
        'contact your Alexander Babbage representative.';

      swal({
        title: 'Error Encountered!',
        text: msg,
        icon: 'error',
        button: 'OK'
      });
    };

    /**
     * Draws a chart visualization.
     *
     * @method drawVis
     * @memberof CHARTS
     * @public
     *
     * @param {string} wrapId - The ID of the element where the chart will go.
     * @param {string|number} chartVal - The numbers or letters assigned to the
     *    chart to differentiate it from the others.
     * @param {object} [config] - A configuration object for the chart.
     */
    const drawVis = (wrapId, chartVal, config) => {
      // Creates a default config object
      config = config || {
        pattern: '#.#',
        width: '85%',
        format: '###.#'
      };

      try {
        const { pattern, width, format, isBar, stacked } = config,
              googleVis = google.visualization,
              data = (chartVal !== 1
                ? '{dialog.object}'['_data' + chartVal]
                : '{dialog.object}'._data
              ),
              dataTable = googleVis.arrayToDataTable(data),
              formatter = new googleVis.NumberFormat(
                (pattern !== '$'
                  ? {
                    fractionDigits: 2,
                    pattern: pattern
                  }
                  : {
                    fractionDigits: 2,
                    prefix: '$'
                  }
                )
              );

        for (let col = data[0].length; --col;) {
          formatter.format(dataTable, col);
        }

        const options = {
          chartArea: {
            backgroundColor: { stroke: '#bcbec0' },
            width: width,
            height: '80%',
            top: 25,
            left: (pattern === '$'
              ? 70
              : (pattern.endsWith('#,###') ? 100 : 50)
            )
          },
          fontName: 'Roboto',
          titlePosition: 'none',
          colors: [
            '#a11d27',
            '#4fa2d9',
            '#5d3b97',
            '#58b86b',
            '#2a3c68',
            '#f7c01c',
            '#b0ccbf',
            '#e75a60',
            '#ceb7a3',
            '#96cb69',
            '#f3c1c8'
          ],
          vAxis: {
            format: format,
            textPosition: 'out',
            gridlines: { color: 'none' }
          },
          hAxis: {
            format: 'MMM yyyy',
            showTextEvery: 2,
            gridlines: { color: 'none' }
          },
          legend: {
            position: 'bottom',
            maxLines: 4
          }
        };

        if (isBar) {
          options.bar = { groupWidth: '75%' };
          options.isStacked = stacked;
        } else {
          options.curveType = 'function';
        }

        const chart = new googleVis[isBar ? 'ColumnChart' : 'LineChart'](
          doc.getElementById(wrapId)
        );

        chart.draw(dataTable, options);

        (chartVal !== 1
          ? '{dialog.object}'['_chart' + chartVal] = chart
          : '{dialog.object}'._chart = chart
        );
      } catch (e) {
        errHandler.log(e);
      }
    };

    /**
     * Allows the user to download a selected chart.
     *
     * @method saveVis
     * @memberof CHARTS
     * @public
     *
     * @param {string|number} chartVal - The numbers or letters assigned to
     *    to the chart to differentiate it from the others.
     */
    const saveVis = (chartVal) => {
      const chart = (chartVal !== 1
              ? '{dialog.object}'['_chart' + chartVal]
              : '{dialog.object}'._chart
            ),
            swalErrMsg = (msg) => {
              swal({
                title: 'Error Encountered!',
                text: msg,
                icon: 'error',
                button: 'OK'
              });
            };

      if (chart !== undefined) {
        (typeof chart.getImageURI === 'function'
          ? '{dialog.object}'.ajaxCallback(
            '', '', 'printChart', '',
            A5.ajax.buildURLParam('_base64data', chart.getImageURI())
          )
          : swalErrMsg('This type of chart does not currently support downloading.')
        );
      } else {
        swalErrMsg('There was an error downloading this chart. Please try ' +
            'again. If the problem persists, contact your Alexander Babbage ' +
            'representative.');
      }
    };

    return {
      errHandler,
      errMsg,
      checkDate,
      drawVis,
      saveVis
    };
  })();
})(document);
