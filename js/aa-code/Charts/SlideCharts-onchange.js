const x = '{dialog.object}'.getValue('SlideCharts');
'1' === x
  ? '{dialog.object}'.runAction('chartHHIncome')
  : '2' === x
    ? '{dialog.object}'.runAction('chartWealthy')
    : '3' === x
      ? '{dialog.object}'.runAction('chartDuration')
      : '4' === x
        ? '{dialog.object}'.runAction('chartDistance')
        : '5' === x
          ? '{dialog.object}'.runAction('charFrrequency')
          : '6' === x
            ? '{dialog.object}'.runAction('chartAverageAge')
            : '7' === x
              ? '{dialog.object}'.runAction('chartFamily')
              : '8' === x
                ? '{dialog.object}'.runAction('chartEducated')
                : '9' === x
                  ? '{dialog.object}'.runAction('chartTraffic')
                  : '10' === x && '{dialog.object}'.panelSetActive('CHARTS', false);
