A5.executeThisThenThat(
  function () {
    A5.u.element.hide($('{dialog.componentname}.V.R1.CONTAINER_120'), {
      type: 'blind',
      duration: 450
    });
    A5.u.element.hide($('{dialog.componentname}.V.R1.CONTAINER_154'), {
      type: 'blind',
      duration: 450
    });
  },
  function () {
    setTimeout(function () {
      document
        .getElementById('CHARTS_BUILD.V.R1.BUTTON_109')
        .classList.remove('is-open');
      document
        .getElementById('CHARTS_BUILD.V.R1.BUTTON_86')
        .classList.remove('is-open');
      document
        .getElementById('CHARTS_BUILD.V.R1.BUTTON_58')
        .classList.toggle('is-open');
      A5.u.element.toggle($('{dialog.componentname}.V.R1.CONTAINER_82'), {
        show: { type: 'drop', duration: 450 },
        hide: { type: 'drop', duration: 450 }
      });
    }, 450);
  }
);
