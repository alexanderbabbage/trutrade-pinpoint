(function (global) {
  global.addEventListener('DOMContentLoaded', () => {
    const doc = global.document,
          stateField = doc.querySelector('.js-toggle-state-hidden-field'),
          transSliderInternalMsg = doc.getElementById('trans_internal_msg'),
          topTripsSliderInternalMsg = doc.getElementById('top_trips_internal_msg');

    transSliderInternalMsg.parentNode.parentNode.classList.add(
      'nav-label',
      'nav-label--slider',
      'nav-label--new-slider',
      'js-slider-msg',
      'is-hidden'
    );

    topTripsSliderInternalMsg.parentNode.parentNode.classList.add(
      'nav-label',
      'nav-label--slider',
      'nav-label--new-slider',
      'js-slider-msg',
      'is-hidden'
    );

    toggleCbx.addEventListener('change', (evt) => {
      const dropdownWrappers = doc.querySelectorAll('.nav-dropdown-wrapper'),
            toggleIsAll = evt.target.checked;

      for (let i = 0; i < 4; ++i) {
        dropdownWrappers[i].classList.toggle('is-hidden');
      }

      stateField && (stateField.value = toggleIsAll ? 'all' : 'my');

      evt.stopPropagation();
    }, { passive: true });

    stateField && stateField.value === 'all' && toggleCbx.click();

    evt.stopPropagation();
  }, { once: true, passive: true });
})(this);
