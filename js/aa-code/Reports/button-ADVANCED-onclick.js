(function (self) {
  self.classList.toggle('is-open');

  const toggleAdvancedRadios = () => {
          A5.u.element.toggle($('{dialog.componentname}.V.R1.CONTAINER_113'), {
            show: { type: 'drop', duration: 350 },
            hide: { type: 'drop', duration: 350 }
          });
        },
        closeZcomForm = () => {
          A5.u.element.hide($('{dialog.componentname}.V.R1.CONTAINER_162'), {
            type: 'drop',
            duration: 350
          });
        };

  A5.executeThisThenThat(closeZcomForm, toggleAdvancedRadios);
})(this);
