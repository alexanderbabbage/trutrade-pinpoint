(function () {
  const value = '{dialog.object}'.getValue('SelectReports'),
        action = ('1' === value
          ? 'slideCompReport'
          : ('2' === value
            ? 'slideDistance'
            : ('3' === value
              ? 'slideDuration'
              : ('4' === value
                ? 'slideFrequency'
                : ('5' === value
                  ? 'slideTopZipCodes'
                  : '6' === value && 'slideTopStates'
                )
              )
            )
          )
        );

  '{dialog.object}'.runAction(action);
})();
