(function (global) {
  const value = '{dialog.object}'.getValue('AdvancedOptionsReports'),
        formTogglingConfig = {
          type: 'drop',
          duration: 350
        },
        wait = delay => (
          new Promise((resolve) => {
            global.setTimeout(resolve, delay);
          })
        ),
        closeZcomForm = () => {
          A5.u.element.hide(
            $('{dialog.componentname}.V.R1.CONTAINER_162'),
            formTogglingConfig
          );
        },
        openZcomForm = () => {
          wait(50).then(() => {
            A5.u.element.show(
              $('{dialog.componentname}.V.R1.CONTAINER_162'),
              formTogglingConfig
            );
          });
        },
        action = ('1' === value
          ? 'slideCompMarketShare'
          : ('2' === value
            ? 'slideCompOverlay'
            : ('3' === value
              ? 'slideNationalMarket'
              : ('4' === value
                ? 'slideDemographics'
                : ('5' === value
                  ? 'slideInternational'
                  : '6' === value && 'slideZipOpportunity'
                )
              )
            )
          )
        ),
        runAction = () => {
          wait(50).then(() => {
            '{dialog.object}'.runAction(action);
          });
        };

  if (value !== '6') {
    A5.executeThisThenThat(
      closeZcomForm,
      runAction
    );
  } else {
    A5.executeThisThenThat(
      runAction,
      openZcomForm
    );
  }
})(window);
