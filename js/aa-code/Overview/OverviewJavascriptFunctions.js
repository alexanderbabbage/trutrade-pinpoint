function scrollToY(y){
  A5.u.element.setScroll('{dialog.object}'.panelGetId('{dialog.object}'.panelGetActive(),"body"),0,y)
}
function swapNonMapLayouts(layoutName) {
  'use strict';
  '{dialog.object}'.getControl('viewbox_kpi').setLayout(layoutName, true);
  '{dialog.object}'.getControl('viewbox_avpv').setLayout(layoutName, true);
}
function swapTileLayout(tile, layoutName) {
  '{dialog.object}'.getControl(tile).setLayout(
    (tile === 'viewbox_visorig' && layoutName === 'BenchAndComp'
      ? 'NoBench'
      : layoutName
    ),
    true
  );
}
function refreshNonMapTiles(modifyValues) {
  'use strict';
  A5.executeThisThenThat(
    (function () {
      '{dialog.object}'.refreshViewBoxData('viewbox_kpi');
    }),
    modifyValues
  );
  '{dialog.object}'.refreshViewBoxData('viewbox_avpv');
  '{dialog.object}'.refreshViewBoxData('viewbox_ttzc');
}
function refreshVoTile() {
  '{dialog.object}'.refreshViewBoxData('viewbox_visorig')
}
function refreshVpTile() {
  '{dialog.object}'.refreshViewBoxData('viewbox_vispen')
}
function refreshBavTile() {
  '{dialog.object}'.refreshViewBoxData('viewbox_bav');
}
