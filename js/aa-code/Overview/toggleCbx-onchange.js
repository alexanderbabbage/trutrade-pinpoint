(function () {
  window.addEventListener('load', () => {
    document.getElementById('toggleCbx').addEventListener('change', (evt) => {
      const dropdownWrappers = doc.querySelectorAll('.nav-dropdown-wrapper'),
            toggleIsAll = evt.target.checked;

      for (let i = 0; i < 4; ++i) {
        dropdownWrappers[i].classList.toggle('is-hidden');
      }

      '{dialog.object}'.getControl('view').setValue(toggleIsAll ? 'all' : 'my');

      evt.stopPropagation();
    }, { passive: true });
  }, { passive: true, once: true });
})();
