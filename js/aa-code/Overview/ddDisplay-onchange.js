/**
 * The onchange event for the Display dropdown in Alpha Anywhere.
 *
 * @param {string} displayValue - The value returned by this.value
 *    being pulled from the Display dropdown.
 */
(function (displayValue) {
  'use strict';

  const panelBody = document.getElementById(
          'OVERVIEW_BUILD.R1.PANEL.0.PANEL.0.PANEL.0.BODY'
        ),
        tileSet = panelBody.querySelectorAll('.tile'),
        downArrowButtons = panelBody.querySelectorAll('.icon-dbl-down-wrapper'),
        breaks = panelBody.querySelectorAll('br');

  if (displayValue === 'ac') {
    for (const tile of tileSet) {
      tile.parentElement.classList.remove('is-hidden');
    }

    for (const downButton of downArrowButtons) {
      downButton.classList.remove('is-hidden');
    }

    for (const brk of breaks) {
      brk.id.endsWith('BREAK') && brk.classList.remove('is-hidden');
    }
  } else {
    for (const tile of tileSet) {
      if (!tile.id.startsWith(displayValue)) {
        tile.parentElement.classList.add('is-hidden');
      } else {
        tile.parentElement.classList.remove('is-hidden');
      }
    }

    for (const downButton of downArrowButtons) {
      downButton.classList.add('is-hidden');
    }

    for (const brk of breaks) {
      brk.id.endsWith('BREAK') && brk.classList.add('is-hidden');
    }
  }
}(this.value));
