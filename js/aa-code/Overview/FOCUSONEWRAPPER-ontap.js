setTimeout(cbxOnClick, 610);

function cbxOnClick() {
  'use strict';
  const cbx = document.getElementById('no_comp_cbx');
  if (cbx.checked) {
    if (cbx.getAttribute('disabled') === null) {
      cbx.setAttribute('disabled', '');
      {dialog.object}.setValue('tbCompetitor', 'No Competitor');
      {dialog.object}.setValue('hCompetitor', '0');
      window.blur();
    } else {
      {dialog.object}.setValue('tbCompetitor', '');
      {dialog.object}.setFocus('tbCompetitor');
    }
  } else {
    window.blur();
  }
  // ORIGINAL LOGIC:
  // if (cbx.checked && cbx.getAttribute('disabled') === null) {
  //   cbx.setAttribute('disabled', '');
  //   {dialog.object}.setValue('tbCompetitor', 'No Competitor');
  //   {dialog.object}.setValue('hCompetitor', '0');
  //   window.blur();
  // } else if (cbx.checked) {
  //   {dialog.object}.setValue('tbCompetitor', '');
  //   {dialog.object}.setFocus('tbCompetitor');
  // } else {
  //   window.blur();
  // }
}

function fireNoCompetitor(){const c=document.getElementById('no_comp_cbx')c.click();cbxOnClick();}
