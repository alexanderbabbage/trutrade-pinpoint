function refreshNonMapTiles(modifyValues) {
  'use strict';
  A5.executeThisThenThat(
    (function () {
      {dialog.object}.refreshViewBoxData('viewbox_kpi');
    }),
    modifyValues
  );
  {dialog.object}.refreshViewBoxData('viewbox_avpv');
  {dialog.object}.refreshViewBoxData('viewbox_ttzc');
}
function refreshVoTile() {
  {dialog.object}.refreshViewBoxData('viewbox_visorig')
}
function refreshVpTile() {
  {dialog.object}.refreshViewBoxData('viewbox_vispen')
}
function refreshBavTile() {
  {dialog.object}.refreshViewBoxData('viewbox_bav');
}
