/**
 * Sets the layout of the viewboxes on the 3.1 Overview page.
 *
 * @param {string} layoutName - The name of the viewbox layout.
 */
function swapNonMapLayouts(layoutName) {
  'use strict';
  "{dialog.object}".getControl('viewbox_kpi').setLayout(layoutName, true, false);
  "{dialog.object}".getControl('viewbox_avpv').setLayout(layoutName, true, false);
}

/**
 * Swaps the layout of a single tile (viewbox).
 *
 * @param {string} tile - The name of the viewbox containing the desired tile.
 * (eg. 'viewbox_visorig').
 * @param {string} layoutName - The name of the viewbox layout.
 */
function swapTileLayout(tile, layoutName) {
  // The logic here in the first setLayout argument is due to there not being
  // a 'BenchAndComp' layout for the visitor origin tile, that layout is
  // equal to the 'NoBench' layout for that tile.
  {dialog.object}.getControl(tile).setLayout(
    (tile === 'viewbox_visorig' && layoutName === 'BenchAndComp'
      ? 'NoBench'
      : layoutName
    ),
    true,
    false
  );
}
