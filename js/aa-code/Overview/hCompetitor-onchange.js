/**
* New Desired onChange order for all competitor location changes
*
*  1. Set layout state
*  2. Swap layout of all non-map tiles that need swapping
*       * KPI
*       * AVPV
*  3. Refresh all tiles without maps
*      * KPI
*      * AVPV
*      * TTZC
*  4. Run onChange actions for those tiles
*    * modifyValues()
*    * animate non-map tiles
*        - animCol(0, 3); // Anim AvPV table
*        - animKpi(compVisibility.state()); // Anim KPI table
*    * activateTableSorting('ttzc-table');
* 5. Swap layout for VO tile
* 6. Refresh VO tile
* 7. animate VO tile: animVoVp(1, compVisibility.state());
* 8. Swap layout for VP tile
* 9. Refresh VP tile
* 10. Animate VP tile: animVoVp(2, compVisibility.state());
* 11. Refresh BAV (heatmap) tile
*/
(function (field) {
  'use strict';

  const mods = AB.modules,
        dom = mods.dom;

  dom.setLayoutState(field);

  const animVoVp = dom.animVoVp,
        state = mods.state,
        visibilityState = state.getVisibility(),
        layoutName = state.getLayout();

  A5.executeThisThenThat(
    (function () {
      swapNonMapLayouts(layoutName);
      refreshNonMapTiles(dom.modifyValues);
    }),
    (function () {
      dom.locOnChangeNonMap();
      swapTileLayout('viewbox_visorig', layoutName);
      refreshVoTile();
    }),
    (function () {
      animVoVp(1, visibilityState);
      swapTileLayout('viewbox_vispen', layoutName);
      refreshVpTile();
    }),
    (function () {
      animVoVp(2, visibilityState);
    })
  );
}(this));
