(function (global) {
  'use strict';

  const modules = global.AB.modules,
        domMod = modules.dom,
        stateMod = modules.state,
        animVoVp = domMod.animVoVp,
        compFieldValue = '{dialog.object}'.getValue('hCompetitor'),
        benchFieldValue = '{dialog.object}'.getValue('ddBenchmark'),
        displayFieldValue = '{dialog.object}'.getValue('ddDisplay'),
        panelBody = global.document.getElementById(
          'OVERVIEW_BUILD.R1.PANEL.0.PANEL.0.PANEL.0.BODY'
        ),
        tileSet = panelBody.querySelectorAll('.tile'),
        arrowsAndBreaks = panelBody.querySelectorAll(
          '.icon-dbl-down-wrapper, br[id$="BREAK"]'
        );

  // Set what tiles are getting displayed and hide what needs to be hidden
  if (displayFieldValue === 'ac') {
    for (const tile of tileSet) {
      tile.parentElement.classList.remove('is-hidden');
    }

    for (const el of arrowsAndBreaks) {
      el.classList.remove('is-hidden');
    }
  } else {
    for (const tile of tileSet) {
      if (tile.id.startsWith(displayFieldValue)) {
        tile.parentElement.classList.remove('is-hidden');
      } else {
        tile.parentElement.classList.add('is-hidden');
      }
    }

    for (const el of arrowsAndBreaks) {
      el.classList.add('is-hidden');
    }
  } // End of Display scripting

  // Store the correct layout state privately within a closure
  domMod.storeLayoutState(compFieldValue, benchFieldValue);

  // Get the determined layout name and visibility state and store them locally
  const layoutName = stateMod.getLayout(),
        visibilityState = stateMod.getVisibility();

  // Asynchronously execute the changes to the layout state
  A5.executeThisThenThat(
    (function () {
      swapNonMapLayouts(layoutName);
      refreshNonMapTiles(domMod.modifyValues);
    }),
    (function () {
      domMod.locOnChangeNonMap();
      swapTileLayout('viewbox_visorig', layoutName);
      refreshVoTile();
    }),
    (function () {
      animVoVp(1, visibilityState);
      swapTileLayout('viewbox_vispen', layoutName);
      refreshVpTile();
    }),
    (function () {
      animVoVp(2, visibilityState);
      refreshBavTile();
    })
  );
}(window));
