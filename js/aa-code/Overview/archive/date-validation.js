var startingDate=new Date({dialog.object}.getValue('FromDate'));
var endingDate=new Date({dialog.object}.getValue('ToDate'));
if (startingDate.getTime() < endingDate.getTime()) {
  {dialog.object}.runAction('getCharts')
} else {
  alert('The date range you have chosen for the charts is incorrect. You must choose an end date that is after the chosen start date. Please check the Start Date and End Date dropdowns and try again')
}
