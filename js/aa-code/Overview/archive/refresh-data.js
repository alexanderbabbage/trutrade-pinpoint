function refreshNonMapTiles(noZip) {
  'use strict';
  noZip = typeof noZip !== 'undefined' ? noZip : !0;
  A5.executeThisThenThat(
    (function () {
      '{dialog.object}'.refreshViewBoxData('VIEWBOX_KPI');
    }),
    modifyValues
  );
  '{dialog.object}'.refreshViewBoxData('VIEWBOX_AVPV');
  if (!noZip) '{dialog.object}'.refreshViewBoxData('VIEWBOX_TTZC');
}

// function refreshVoTile(){
//   {dialog.object}.refreshViewBoxData('VIEWBOX_VISORIG')
// }
// function refreshVpTile(){
//   {dialog.object}.refreshViewBoxData('VIEWBOX_VISPEN')
// }
// function refreshBavTile(){
//   {dialog.object}.refreshViewBoxData('VIEWBOX_BAV');
// }
