/**
 * Sets the layout of the viewboxes on the 3.1 Overview page.
 *
 * @param {number|boolean|string} [layout] - Pass 0, false, or 'Default' for
 * the Default layout, pass 1, true, or 'NoCompetitor' for the No Competitor
 * layout. If no value is passed then the competitor visibility is checked using
 * compVisibility.state() and layout is based on the returning value. If a
 * string is passed, that string is used as the name of the layout.
 */
function swapNonMapLayouts(layout) {
  layout = determineLayout(layout);

  {dialog.object}.getControl('viewbox_kpi').setLayout(layout,!0);
  {dialog.object}.getControl('viewbox_avpv').setLayout(layout,!0);
}

/**
 * Swaps the layout of a single tile (viewbox).
 *
 * @param {string} tile - The name of the viewbox containing the desired tile.
 * (eg. 'viewbox_visorig').
 * @param {number|boolean|string} [layout] - Pass 0, false, or 'Default' for
 * the Default layout, pass 1, true, or 'NoCompetitor' for the No Competitor
 * layout. If no value is passed then the competitor visibility is checked using
 * compVisibility.state() and layout is based on the returning value. If a
 * string is passed, that string is used as the name of the layout.
 */
function swapTileLayout(tile, layout) {
  {dialog.object}.getControl(tile).setLayout(determineLayout(layout),!0);
}
