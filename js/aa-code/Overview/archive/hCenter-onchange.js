/**
 * New Desired onChange order for all location and benchmark changes
 *
 *  1. Swap layout of all non-map tiles that need swapping
 *       * KPI
 *       * AVPV
 *  2. Refresh all tiles without maps
 *      * KPI + modifyValues()
 *      * AVPV
 *      * TTZC
 * 3. Run onChange actions for those tiles
 *    * animate non-map tiles
 *        - animCol(0, 3); // Anim AvPV table
 *        - animKpi(compVisibility.state()); // Anim KPI table
 *    * activateTableSorting('ttzc-table');
 * 4. Swap layout for VO tile
 * 5. Refresh VO tile
 * 6. animate VO tile: animVoVp(1, compVisibility.state());
 * 7. Swap layout for VP tile
 * 8. Refresh VP tile
 * 9. Animate VP tile: animVoVp(2, compVisibility.state());
 * 10. Refresh BAV (heatmap) tile
 */

(function () {
  'use strict';

  const compVisible = compVisibility.state();

  A5.executeThisThenThat(
    resetNonMapLayoutState,
    (function () {
      refreshNonMapTiles(!1);
    }),
    locOnChangeNonMap,
    resetVoLayoutState,
    refreshVoTile,
    (function () {
      animVoVp(1, compVisible);
    }),
    resetVpLayoutState,
    refreshVpTile,
    (function () {
      animVoVp(2, compVisible);
    }),
    refreshBavTile
  );
}());
