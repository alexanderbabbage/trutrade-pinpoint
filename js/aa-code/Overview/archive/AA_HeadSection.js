document.addEventListener('keydown', function(e) {
  const evtobj = (window.event ? event : e);
  if (evtobj.keyCode == 66 && evtobj.ctrlKey) {
    document.getElementById('loading_screen').style.display = 'none';
  }
}, false);

window.onload = function() {
  for (let i = 1; i < 5; i++) {
    replaceColWithCharts(i);
  }
  var curActiveNav = document.getElementsByClassName('active-nav');
  if (!curActiveNav.length) {
    document.getElementById('PORTAL_BUILD.R1.HEADER.Overview').classList.add('active-nav');
    sessionStorage.activePage = 1;
  }
  fade(document.getElementById('loading_screen'));
};
