/**
* New Desired onChange order for all competitor location changes
*
*  1. Set layout state
*  2. Swap layout of all non-map tiles that need swapping
*       * KPI
*       * AVPV
*  3. Refresh all tiles without maps
*      * KPI
*      * AVPV
*      * TTZC
*  4. Run onChange actions for those tiles
*    * modifyValues()
*    * animate non-map tiles
*        - animCol(0, 3); // Anim AvPV table
*        - animKpi(compVisibility.state()); // Anim KPI table
*    * activateTableSorting('ttzc-table');
* 5. Swap layout for VO tile
* 6. Refresh VO tile
* 7. animate VO tile: animVoVp(1, compVisibility.state());
* 8. Swap layout for VP tile
* 9. Refresh VP tile
* 10. Animate VP tile: animVoVp(2, compVisibility.state());
* 11. Refresh BAV (heatmap) tile
*/
(function (field) {
  'use strict';

  setLayoutState(field.value);

  const compVisible = compVisibility.state();

  A5.executeThisThenThat(
    (function () {
      swapNonMapLayouts(!compVisible);
    }),
    refreshNonMapTiles,
    locOnChangeNonMap,
    resetVoLayoutState,
    refreshVoTile,
    (function () {
      animVoVp(1, compVisible);
    }),
    resetVpLayoutState,
    refreshVpTile,
    (function () {
      animVoVp(2, compVisible);
    }),
  );
}(this));
