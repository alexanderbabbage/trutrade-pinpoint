(function() {
  const testCellKpiAvp = document.getElementById('comp_col_head'),
        compHidden = document.getElementById('OVERVIEW_BUILD.V.R1.HCOMPETITOR'),
        index = (
          (compHidden)
            ? compHidden.value
            : document.getElementById('DLG1.V.R1.HCOMPETITOR').value
        );

  animCol(0, 3); // Animate AvPV table
  if ('0' === index) {
    // "No Competitor" transformations
    noCompSelected();
    removeCol();
    hideCol('comp-col', 'comp-val');

    for (let i = 1; i < 5; i++) {
      // Animate KPI table with no competitor
      if (3 === i) continue;
      animCol(i);

      // Anim VO and VP tables w/o a competitor selected
      if (3 > i) {
        animCol(i, 1);
        animCol(i, 2);
      }
    }
  }
  else {
    addCol();
    (testCellKpiAvp) &&
        (window.getComputedStyle(testCellKpiAvp).getPropertyValue('display') ===
        'none') && (showCol('comp-col', 'comp-val'));
    for (let i = 1; i < 4; i++) {
      ( (3 === i)
        ? animCol(i, 2)
        : (animCol(i, 1), animCol(i, 2))
      );
    }
    animKpi();
  }
  // Remove waiting screen in 2.5sec
  setTimeout(function() {
    document.getElementById('waiting_cover').style.display = 'none';
  }, 2500);
})();
