document.addEventListener('keydown', function (e) {
  var evtobj = (window.event ? event : e);
  if (evtobj.keyCode === 66 && evtobj.ctrlKey) {
    document.getElementById('loading_screen').style.display = 'none';
  }
}, false);

window.onload = function () {
  if (!sessionStorage.alreadyRan) {
    document.getElementById('PORTAL_BUILD.R1.HEADER.Overview').classList.add('active-nav');
    sessionStorage.activePage = 1;
    sessionStorage.alreadyRan = true;
  }
  fadeOut(document.getElementById('loading_screen'), 88);
  makeAllSortable('ttzc-table', 'ttzc_tile');
};
