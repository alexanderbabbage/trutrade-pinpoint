(function() {
  const compHiddenField = document.getElementById('OVERVIEW_BUILD.V.R1.HCOMPETITOR'),
        compIndex = (
          (compHiddenField)
            ? compHiddenField.value
            : document.getElementById('DLG1.V.R1.HCOMPETITOR').value
        );

  animCol(0, 3); // Anim AvPV table
  if ('0' === compIndex) {
    // No competitor has been selected

    for (let i = 1; i < 5; i++) {
      // Animate KPI table with no competitor
      if (3 === i) continue;
      animCol(i);

      // Anim VO and VP tables w/o a competitor selected
      if (3 > i) {
        animCol(i, 1);
        animCol(i, 2);
      }
    }
  }
  else {
    animKpi();
    // Anim VO and VP table w/ a competitor selected
    for (let i = 1; i < 4; i++) {
      animCol(i, 1);
      animCol(i, 2);
    }
  }
})();
