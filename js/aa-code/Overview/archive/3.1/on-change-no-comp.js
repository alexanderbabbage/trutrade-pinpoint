const boxActual = document.getElementById('OVERVIEW_BUILD.V.R1.FOCUSONELOCATION'),
      noCompCheckBox = (
        (boxActual)
          ? boxActual
          : document.getElementById('DLG1.V.R1.FOCUSONELOCATION')
      );

if (document.getElementById('OVERVIEW_BUILD.V.R1.FOCUSONELOCATION').getAttribute('a5lastpersistvalue') == 'true') {
  {dialog.object}.setValue('tbCompetitor', 'No Competitor');
  {dialog.object}.setValue('hCompetitor', '0');
} else {
  {dialog.object}.setValue('tbCompetitor', '');
}
