(function() {
  animCol(0, 3); // Anim AvPV table
  if ('0' === index) {
    // "No Competitor" is selected

    for (let i = 1; i < 5; i++) {
      // Animate KPI table with no competitor
      if (3 === i) continue;
      animCol(i);

      // Anim VO and VP tables w/o a competitor selected
      if (3 > i) {
        animCol(i, 1);
        animCol(i, 2);
      }
    }
  } else {
    animKpi();

    // Anim VO and VP table w/ a competitor selected
    for (let i = 1; i < 4; i++) {
      animCol(i, 1);
      animCol(i, 2);
    }
  }
})();
