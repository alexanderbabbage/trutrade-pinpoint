
/**
 * Draws the mobile chart for the COVID-19 page in TruTrade.
 */
function drawCovidVisMobile() {
  const data = '{dialog.object}'._data3,
        googleVis = google.visualization,
        dataTable = googleVis.arrayToDataTable(data),
        numCols = data[0].length,
        formatter = new googleVis.NumberFormat({
          fractionDigits: 2,
          pattern: '###.#'
        }),
        searchedLocValue = '{dialog.Object}'.getValue('poi2'),
        geoAreaValue = '{dialog.Object}'.getValue('geog'),
        geoAreaValExists = (geoAreaValue && geoAreaValue !== 'null'),
        colorArray = [],
        hexColorGeographic = '#fcba03',
        hexColorBordersGridlines = '#bcbec0';

  for (let i = 1; i < numCols; ++i) {
    formatter.format(dataTable, i);
    colorArray.push('#ccc');
  }

  colorArray[numCols - 2] = '#1b0c7d';

  if (searchedLocValue && searchedLocValue !== 'null') {
    colorArray[numCols - 3] = '#eb4034';
    if (geoAreaValExists) {
      colorArray[numCols - 4] = hexColorGeographic;
    }
  } else if (geoAreaValExists) {
    colorArray[numCols - 3] = hexColorGeographic;
  }

  const options = {
    chartArea: {
      backgroundColor: { stroke: hexColorBordersGridlines },
      width: '100%',
      height: '80%',
      top: 25,
      left: 70
    },
    titlePosition: 'none',
    colors: colorArray,
    vAxis: {
      textPosition: 'out',
      showTextEvery: 2,
      gridlines: {
        color: hexColorBordersGridlines
      }
    },
    hAxis: {
      format: 'MMM dd',
      showTextEvery: 7,
      gridlines: {
        color: 'none'
      }
    }
  };

  const chart = new googleVis.LineChart(
    document.getElementById('covidchartmobile')
  );

  chart.draw(dataTable, options);

  '{dialog.object}'._chart = chart;
}
