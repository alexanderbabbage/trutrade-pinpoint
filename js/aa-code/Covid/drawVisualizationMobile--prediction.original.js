function drawVisualizationMobile() {
  const t = '{dialog.object}'._data3,
        o = google.visualization,
        e = o.arrayToDataTable(t),
        i = t[0].length,
        a = new o.NumberFormat({
          fractionDigits: 2,
          pattern: '###.#'
        }),
        n = '{dialog.Object}'.getValue('poi2'),
        r = '{dialog.Object}'.getValue('geog'),
        c = [];
  for (let t = 1; t < i; ++t) {
    a.format(e, t),
    c.push('#ccc');
  }

  (c[i - 2] = '#1b0c7d'),
  n && 'null' !== n && +n > 0
    ? ((c[i - 3] = '#eb4034'), r && 'null' !== r && (c[i - 4] = '#fcba03'))
    : r && 'null' !== r && (c[i - 3] = '#fcba03');
  const l = {
          chartArea: {
            backgroundColor: { stroke: '#bcbec0' },
            width: '100%',
            height: '80%',
            top: 25,
            left: 70
          },
          titlePosition: 'none',
          colors: c, // ["#fce6a3","#96cb69","#a29490"],
          vAxis: {
            textPosition: 'out',
            showTextEvery: 2,
            gridlines: { color: '#bcbec0' }
          },
          hAxis: {
            format: 'MMM dd',
            showTextEvery: 7,
            gridlines: {
              color: 'none'
            }
          }
        },
        s = new o.LineChart(document.getElementById('covidchartmobile'));
  s.draw(e, l);

  '{dialog.object}'._chart=s
}
