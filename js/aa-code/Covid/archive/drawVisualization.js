
/**
 * Draws the primary chart for the COVID-19 Page in TruTrade.
 */
function drawVisualization() {
  const data = {dialog.object}._data1,
        googleVis = google.visualization,
        dataTable = googleVis.arrayToDataTable(data),
        numCols = data[0].length,
        formatter = new googleVis.NumberFormat({
          fractionDigits: 2,
          pattern: '###.#%'
        }),
        searchedLocValue = {dialog.Object}.getValue('poi2'),
        geoAreaValue = {dialog.Object}.getValue('geog'),
        colorArray = [],
        hexColorGeographic = '#fcba03',
        hexColorBordersGridlines = '#bcbec0';

  for (let i = 1; i < numCols; ++i) {
    formatter.format(dataTable, i);
    colorArray.push('#ccc');
  }

  colorArray[numCols - 2] = '#1b0c7d';

  if (searchedLocValue && searchedLocValue !== 'null' && +searchedLocValue > 0) {
    colorArray[numCols - 3] = '#eb4034';
    if (geoAreaValue && geoAreaValue !== 'null') {
      colorArray[numCols - 4] = hexColorGeographic;
    }
  } else if (geoAreaValue && geoAreaValue !== 'null') {
    colorArray[numCols - 3] = hexColorGeographic;
  }

  const options = {
    chartArea: {
      backgroundColor: { stroke: hexColorBordersGridlines },
      width: '100%',
      height: '80%',
      top: 25,
      left: 70
    },
    titlePosition: 'none',
    colors: colorArray,
    vAxis: {
      textPosition: 'out',
      format: 'percent',
      ticks: [0.2, 0.4, 0.6, 0.8, 1],
      showTextEvery: 2,
      gridlines: {
        color: hexColorBordersGridlines
      }
    },
    minorGridlines: {
      ticks: [0.1, 0.3, 0.5, 0.7, 0.9]
    },
    hAxis: {
      format: 'MMM dd',
      showTextEvery: 7,
      gridlines: {
        color: 'none'
      }
    }
  };

  const chart = new googleVis.LineChart(
    document.getElementById('covidchart1')
  );

  chart.draw(dataTable, options);

  {dialog.object}._chart = chart;
}
