function drawVisualization() {
  const data = {dialog.object}._data1,
      dataTable = google.visualization.arrayToDataTable(data),
      numCols = data[0].length,
      formatter = new google.visualization.NumberFormat({
        fractionDigits: 2,
        pattern: '###.#%'
      }),
      searchedLocValue = {dialog.Object}.getValue('poi2'),
      colorArray = [];

  for (let i = 1; i < numCols; ++i) {
    formatter.format(dataTable, i);
    colorArray.push('#ccc');
  }

  colorArray[numCols - 2] = '#1b0c7d';

  if (searchedLocValue && +searchedLocValue > 0) {
    colorArray[numCols - 3] = '#eb4034';
  }

  const options = {
    chartArea: {
      backgroundColor: { stroke: '#bcbec0' },
      width: '95%',
      height: '80%',
      top: 25,
      left: 70
    },
    titlePosition: 'none',
    colors: colorArray,
    vAxis: {
      textPosition: 'out',
      format: 'percent',
      ticks: [0.2, 0.4, 0.6, 0.8, 1],
      showTextEvery: 2,
      gridlines: {
        color: '#bcbec0'
      }
    },
    minorGridlines: {
      ticks: [0.1, 0.3, 0.5, 0.7, 0.9]
    },
    hAxis: {
      format: 'MMM dd',
      showTextEvery: 2,
      gridlines: {
        color: 'none'
      }
    }
  };

  const chart = new google.visualization.LineChart(
    document.getElementById('covidchart1')
  );
  chart.draw(dataTable, options);

  {dialog.object}._chart = chart;
}
