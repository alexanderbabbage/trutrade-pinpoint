/**
 * Draws the full-time chart for the COVID-19 page in TruTrade.
 */
function drawVisualizationFT() {
  const data = '{dialog.object}'._dataFT,
        googleVis = google.visualization,
        dataTable = googleVis.arrayToDataTable(data),
        numCols = data[0].length,
        formatter = new googleVis.NumberFormat({
          fractionDigits: 2,
          pattern: '###.#%'
        }),
        searchedLocValue = '{dialog.Object}'.getValue('poi2'),
        geoAreaValue = '{dialog.Object}'.getValue('geog'),
        colorArray = [],
        hexColorGeographic = '#fcba03',
        hexColorBordersGridlines = '#bcbec0';

  for (let i = 1; i < numCols; ++i) {
    formatter.format(dataTable, i);
    colorArray.push('#ccc');
  }

  colorArray[numCols - 2] = '#1b0c7d';

  if (searchedLocValue && searchedLocValue !== 'null' && +searchedLocValue > 0) {
    colorArray[numCols - 3] = '#eb4034';
    if (geoAreaValue && geoAreaValue !== 'null') {
      colorArray[numCols - 4] = hexColorGeographic;
    }
  } else if (geoAreaValue && geoAreaValue !== 'null') {
    colorArray[numCols - 3] = hexColorGeographic;
  }

  const options = {
    chartArea: {
      backgroundColor: { stroke: hexColorBordersGridlines },
      width: '100%',
      height: '80%',
      top: 25,
      left: 70
    },
    titlePosition: 'none',
    colors: colorArray,
    vAxis: {
      textPosition: 'out',
      format: 'percent',
      ticks: [0.1, 0.2, 0.3, 0.4, 0.5],
      showTextEvery: 2,
      gridlines: { color: hexColorBordersGridlines }
    },
    minorGridlines: {
      ticks: [0.05, 0.15, 0.25, 0.35, 0.45]
    },
    hAxis: {
      format: 'MMM dd',
      showTextEvery: 7,
      gridlines: {
        color: 'none'
      }
    }
  };

  const chart = new googleVis.LineChart(
    document.getElementById('covidchartFT')
  );

  chart.draw(dataTable, options);

  '{dialog.object}'._chart = chart;
}
