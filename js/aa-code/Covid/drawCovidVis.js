
/**
 * Draws the all the chart visualizations for the COVID-19 page except for
 * the out-of-home mobile chart.
 *
 * @param {string} chartId - The ID of the element where the chart will be
 *    rendered.
 * @param {number} dataRef - The reference for data being pulled from the Xbasic
 *    code. For example, for '_dataFT' you would pass 'FT', for '_data3' you
 *    would simply pass 3.
 * @param {boolean} isWorkChart - True if the intendended visualization is for
 *    the part-time or full-time charts.
 */
function drawCovidVis(chartId, dataRef, isWorkChart) {
  const data = '{dialog.object}'['_data' + dataRef],
        googleVis = google.visualization,
        dataTable = googleVis.arrayToDataTable(data),
        numCols = data[0].length,
        formatter = new googleVis.NumberFormat({
          fractionDigits: 2,
          pattern: '###.#%'
        }),
        searchedLocValue = '{dialog.Object}'.getValue('poi2'),
        geoAreaValue = '{dialog.Object}'.getValue('geog'),
        geoAreaValExists = (geoAreaValue && geoAreaValue !== 'null'),
        colorArray = [],
        hexColorGeographic = '#fcba03',
        hexColorBordersGridlines = '#bcbec0';

  for (let i = 1; i < numCols; ++i) {
    formatter.format(dataTable, i);
    colorArray.push('#ccc');
  }

  colorArray[numCols - 2] = '#1b0c7d';

  if (searchedLocValue && searchedLocValue !== 'null') {
    colorArray[numCols - 3] = '#eb4034';
    if (geoAreaValExists) {
      colorArray[numCols - 4] = hexColorGeographic;
    }
  } else if (geoAreaValExists) {
    colorArray[numCols - 3] = hexColorGeographic;
  }

  const options = {
    chartArea: {
      backgroundColor: { stroke: hexColorBordersGridlines },
      width: '100%',
      height: '80%',
      top: 25,
      left: 70
    },
    titlePosition: 'none',
    colors: colorArray,
    vAxis: {
      textPosition: 'out',
      format: 'percent',
      ticks:
        (isWorkChart
          ? [0.1, 0.2, 0.3, 0.4, 0.5]
          : [0.2, 0.4, 0.6, 0.8, 1]
        ),
      showTextEvery: 2,
      gridlines: { color: hexColorBordersGridlines }
    },
    minorGridlines: {
      ticks:
        (isWorkChart
          ? [0.05, 0.15, 0.25, 0.35, 0.45]
          : [0.1, 0.3, 0.5, 0.7, 0.9]
        )
    },
    hAxis: {
      format: 'MMM dd',
      showTextEvery: 2,
      gridlines: {
        color: 'none'
      }
    }
  };

  const chart = new googleVis.LineChart(
    document.getElementById(chartId)
  );

  chart.draw(dataTable, options);

  '{dialog.object}'._chart = chart;
}
