(function (global) {
  global.addEventListener('DOMContentLoaded', () => {
    const doc = global.document,
          toggleCbx = doc.getElementById('toggleCbx'),
          stateField = doc.querySelector('.js-toggle-state-hidden-field');

    toggleCbx.addEventListener('change', (evt) => {
      const d = doc.querySelectorAll('.nav-dropdown-wrapper'),
            toggleIsAll=evt.target.checked;

      for (let i = 0; i < 4; ++i) d[i].classList.toggle('is-hidden');

      stateField && (stateField.value = toggleIsAll ? 'all' : 'my');

      evt.stopPropagation();
    }, { passive: true });

    stateField && stateField.value === 'all' && toggleCbx.click();

    global.addEventListener('resize', () => {
      global.setTimeout(() => {
        e.history.go();
      }, 2000);

      evt.stopPropagation();
    }, { passive: true });
  }, { passive: true, once: true });
})(window);
