(function () {
  'use strict';

  const displayFieldValue = '{dialog.object}'.getValue('ddDisplay'),
        tileSet = document.querySelectorAll('.chart-tile, .map-tile');

  if (displayFieldValue === 'AC') {
    for (const tile of tileSet) {
      tile.classList.remove('is-hidden');
    }
  } else {
    for (const tile of tileSet) {
      if (tile.id.endsWith(displayFieldValue)) {
        tile.classList.remove('is-hidden');
      } else {
        tile.classList.add('is-hidden');
      }
    }
  }
}());
