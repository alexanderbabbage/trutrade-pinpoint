// Main page to view the rendered reports. 
const ReportViewer = () => {
    return(
        <div>
            <TopTenZCTable />
            <TopCountriesTableContainer />
            <ZipCodeTable />
        </div>
    );
}

const Table = (props) => {
    return(
        <div>
            <h1 className="title">{props.title}</h1>
            <table className="locations">                        
                <tbody>
                    <tr>
                        {props.renderTableHeader()}
                    </tr>
                    {props.renderTableData()}
                </tbody>
            </table>
        </div>
    );
}

const ZipCodeSearchBar = (props) => {
    return(
        <div>
            <form onSubmit={props.onFormSubmit}>
                <input type="text" placeholder="Zip Code" name="zipCodeNumbers" />
                <button>Submit</button>
            </form>
        </div>
    );
}

class ZipCodeTable extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            zipCode: ''
        }
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }
    
    onFormSubmit(e) {
        e.preventDefault();
        this.setState({ zipCode: e.target.zipCodeNumbers.value });
    }
    render() {
        return(
            <div>
                <ZipCodeSearchBar onFormSubmit={this.onFormSubmit} />
                <ZipCodeTableContainer zipCode={this.state.zipCode} />
            </div>
        );
    }
}

// Fetches the top countries data and sends data to table. 
class TopCountriesTableContainer extends React.Component {
    constructor(props) {
        super(props) 
        this.state = {
            locations: [
                { location: 'Top International', ponceCityMarket: 42.9, atlanticStation: 38.5, avalon: 4.7 },
                { location: `Top 5 In'tl Countries (among all visits)`, ponceCityMarket: 26.8, atlanticStation: 24.7, avalon: 2.7 },
                { location: `Top 5 In'tl Countries (among all int'l vists)`, ponceCityMarket: 62.3, atlanticStation: 64.1, avalon: 57.7 }
            ],
            headers: ['Location', 'Ponce City Market', 'Atlantic Station', 'Avalon' ]
        }
        this.renderTableData = this.renderTableData.bind(this);
        this.renderTableHeader = this.renderTableHeader.bind(this);
    }

    renderTableData(){
        return this.state.locations.map((lcn, index) => {
            const { location, ponceCityMarket, atlanticStation, avalon } = lcn;
            return(
                <tr key={index}>
                    <td>{location}</td>
                    <td>{ponceCityMarket}</td>
                    <td>{atlanticStation}</td>
                    <td>{avalon}</td>
                </tr>
            );
        });
    } 
     

    renderTableHeader(){
        return this.state.headers.map((header, index) => {
            return(
                <th key={index}>{header}</th>
            );
        });
    }
    
    render() {
        return <Table renderTableHeader={this.renderTableHeader} renderTableData={this.renderTableData} title={'Top Countries Table'}/>
    }
}

// Fetches the top zipcodes data and sends data to table. 
class TopTenZCTable extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            JSONdata: [
                {"Row" : "1", "ClusterID" : "822073", "Zip" : "08401", "Centername" : "Tanger Outlets at The Walk", "CITY" : "Atlantic City", "STATE" : "NJ", "TOTPOP_CY" : " 39,880", "TOTHH_CY" : " 15,417", "PopGrowth" : "0.2", "AverageIncome" : "$47,382", "PercentVisits" : "13.3", "CumulativeTotal" : "13.3"},
                {"Row" : "2", "ClusterID" : "822073", "Zip" : "08234", "Centername" : "Tanger Outlets at The Walk", "CITY" : "Egg Harbor Township", "STATE" : "NJ", "TOTPOP_CY" : " 43,472", "TOTHH_CY" : " 14,976", "PopGrowth" : "-0.1", "AverageIncome" : "$96,574", "PercentVisits" : "3.0", "CumulativeTotal" : "16.3"},
                {"Row" : "3", "ClusterID" : "822073", "Zip" : "08232", "Centername" : "Tanger Outlets at The Walk", "CITY" : "Pleasantville", "STATE" : "NJ", "TOTPOP_CY" : " 19,390", "TOTHH_CY" : " 6,153", "PopGrowth" : "-1.0", "AverageIncome" : "$59,595", "PercentVisits" : "2.4", "CumulativeTotal" : "18.7"},
                {"Row" : "4", "ClusterID" : "822073", "Zip" : "08205", "Centername" : "Tanger Outlets at The Walk", "CITY" : "Absecon", "STATE" : "NJ", "TOTPOP_CY" : " 28,904", "TOTHH_CY" : " 10,445", "PopGrowth" : "-1.0", "AverageIncome" : "$88,527", "PercentVisits" : "2.0", "CumulativeTotal" : "20.6"},
                {"Row" : "5", "ClusterID" : "822073", "Zip" : "08330", "Centername" : "Tanger Outlets at The Walk", "CITY" : "Mays Landing", "STATE" : "NJ", "TOTPOP_CY" : " 27,973", "TOTHH_CY" : " 10,099", "PopGrowth" : "-1.0", "AverageIncome" : "$77,371", "PercentVisits" : "1.9", "CumulativeTotal" : "22.6"},
                {"Row" : "6", "ClusterID" : "822073", "Zip" : "08406", "Centername" : "Tanger Outlets at The Walk", "CITY" : "Ventnor City", "STATE" : "NJ", "TOTPOP_CY" : " 10,792", "TOTHH_CY" : " 4,592", "PopGrowth" : "-0.6", "AverageIncome" : "$90,352", "PercentVisits" : "1.2", "CumulativeTotal" : "23.8"},
                {"Row" : "7", "ClusterID" : "822073", "Zip" : "08203", "Centername" : "Tanger Outlets at The Walk", "CITY" : "Brigantine", "STATE" : "NJ", "TOTPOP_CY" : " 9,384", "TOTHH_CY" : " 4,227", "PopGrowth" : "-1.3", "AverageIncome" : "$94,204", "PercentVisits" : "1.1", "CumulativeTotal" : "24.8"},
                {"Row" : "8", "ClusterID" : "822073", "Zip" : "08201", "Centername" : "Tanger Outlets at The Walk", "CITY" : "Absecon", "STATE" : "NJ", "TOTPOP_CY" : " 9,881", "TOTHH_CY" : " 3,757", "PopGrowth" : "-0.7", "AverageIncome" : "$80,057", "PercentVisits" : "1.0", "CumulativeTotal" : "25.8"},
                {"Row" : "9", "ClusterID" : "822073", "Zip" : "08260", "Centername" : "Tanger Outlets at The Walk", "CITY" : "Wildwood", "STATE" : "NJ", "TOTPOP_CY" : " 12,608", "TOTHH_CY" : " 5,832", "PopGrowth" : "-4.5", "AverageIncome" : "$68,037", "PercentVisits" : "0.7", "CumulativeTotal" : "26.4"},
                {"Row" : "10", "ClusterID" : "822073", "Zip" : "08360", "Centername" : "Tanger Outlets at The Walk", "CITY" : "Vineland", "STATE" : "NJ", "TOTPOP_CY" : " 43,118", "TOTHH_CY" : " 14,863", "PopGrowth" : "-0.9", "AverageIncome" : "$62,285", "PercentVisits" : "0.6", "CumulativeTotal" : "27.0"}
                
            ],
            headers: ['Rank', 'Zip Code', 'City', 'State', 'Percent Visits', 'Cumulative Percent Vists', 'Total Households', 'Total Population', 'Average Household Income', 'Population Growth']
        }
        this.renderTableData = this.renderTableData.bind(this);
        this.renderTableHeader = this.renderTableHeader.bind(this);
    }

    renderTableData() {
        return this.state.JSONdata.map((location, index) => {
            const { Row, Zip, CITY, STATE, PercentVisits, CumulativeTotal, TOTHH_CY, TOTPOP_CY, AverageIncome, PopGrowth } = location;
                return (
                    <tr key={Row}>
                        <td>{Row}</td>
                        <td>{Zip}</td>
                        <td>{CITY}</td>
                        <td>{STATE}</td>
                        <td>{PercentVisits}</td>
                        <td>{CumulativeTotal}</td>
                        <td>{TOTHH_CY}</td>
                        <td>{TOTPOP_CY}</td>
                        <td>{AverageIncome}</td>
                        <td>{PopGrowth}</td>
                    </tr>
                );
        });
    }

    renderTableHeader() {
        return this.state.headers.map((header, index) => {
            return(
                <th key={index}>{header}</th>
            );
        })
    }


    render() {
        return <Table renderTableData={this.renderTableData} renderTableHeader={this.renderTableHeader} title={'Top Ten Zip Codes'}/>
    }
}

class ZipCodeTableContainer extends React.Component {
    constructor(props) {
        super(props) 
        this.state = {
            
        }
    }

    componentDidMount() {

    }

    renderTableData() {
        
    }

    renderTableHeader() {

    }


    render() {
        console.log(this.props.zipCode)
        return <Table renderTableData={this.renderTableData} renderTableHeader={this.renderTableHeader} title={'Zip Code Opportunity Table'}/>
    }
}

ReactDOM.render(<ReportViewer />, document.getElementById('reports-container'));