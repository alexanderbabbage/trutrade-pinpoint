"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// Main page to view the rendered reports. 
var ReportViewer = function ReportViewer() {
    return React.createElement(
        "div",
        null,
        React.createElement(TopTenZCTable, null),
        React.createElement(TopCountriesTableContainer, null),
        React.createElement(ZipCodeTable, null)
    );
};

var Table = function Table(props) {
    return React.createElement(
        "div",
        null,
        React.createElement(
            "h1",
            { className: "title" },
            props.title
        ),
        React.createElement(
            "table",
            { className: "locations" },
            React.createElement(
                "tbody",
                null,
                React.createElement(
                    "tr",
                    null,
                    props.renderTableHeader()
                ),
                props.renderTableData()
            )
        )
    );
};

var ZipCodeSearchBar = function ZipCodeSearchBar(props) {
    return React.createElement(
        "div",
        null,
        React.createElement(
            "form",
            { onSubmit: props.onFormSubmit },
            React.createElement("input", { type: "text", placeholder: "Zip Code", name: "zipCodeNumbers" }),
            React.createElement(
                "button",
                null,
                "Submit"
            )
        )
    );
};

var ZipCodeTable = function (_React$Component) {
    _inherits(ZipCodeTable, _React$Component);

    function ZipCodeTable(props) {
        _classCallCheck(this, ZipCodeTable);

        var _this = _possibleConstructorReturn(this, (ZipCodeTable.__proto__ || Object.getPrototypeOf(ZipCodeTable)).call(this, props));

        _this.state = {
            zipCode: ''
        };
        _this.onFormSubmit = _this.onFormSubmit.bind(_this);
        return _this;
    }

    _createClass(ZipCodeTable, [{
        key: "onFormSubmit",
        value: function onFormSubmit(e) {
            e.preventDefault();
            this.setState({ zipCode: e.target.zipCodeNumbers.value });
        }
    }, {
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                null,
                React.createElement(ZipCodeSearchBar, { onFormSubmit: this.onFormSubmit }),
                React.createElement(ZipCodeTableContainer, { zipCode: this.state.zipCode })
            );
        }
    }]);

    return ZipCodeTable;
}(React.Component);

// Fetches the top countries data and sends data to table. 


var TopCountriesTableContainer = function (_React$Component2) {
    _inherits(TopCountriesTableContainer, _React$Component2);

    function TopCountriesTableContainer(props) {
        _classCallCheck(this, TopCountriesTableContainer);

        var _this2 = _possibleConstructorReturn(this, (TopCountriesTableContainer.__proto__ || Object.getPrototypeOf(TopCountriesTableContainer)).call(this, props));

        _this2.state = {
            locations: [{ location: 'Top International', ponceCityMarket: 42.9, atlanticStation: 38.5, avalon: 4.7 }, { location: "Top 5 In'tl Countries (among all visits)", ponceCityMarket: 26.8, atlanticStation: 24.7, avalon: 2.7 }, { location: "Top 5 In'tl Countries (among all int'l vists)", ponceCityMarket: 62.3, atlanticStation: 64.1, avalon: 57.7 }],
            headers: ['Location', 'Ponce City Market', 'Atlantic Station', 'Avalon']
        };
        _this2.renderTableData = _this2.renderTableData.bind(_this2);
        _this2.renderTableHeader = _this2.renderTableHeader.bind(_this2);
        return _this2;
    }

    _createClass(TopCountriesTableContainer, [{
        key: "renderTableData",
        value: function renderTableData() {
            return this.state.locations.map(function (lcn, index) {
                var location = lcn.location,
                    ponceCityMarket = lcn.ponceCityMarket,
                    atlanticStation = lcn.atlanticStation,
                    avalon = lcn.avalon;

                return React.createElement(
                    "tr",
                    { key: index },
                    React.createElement(
                        "td",
                        null,
                        location
                    ),
                    React.createElement(
                        "td",
                        null,
                        ponceCityMarket
                    ),
                    React.createElement(
                        "td",
                        null,
                        atlanticStation
                    ),
                    React.createElement(
                        "td",
                        null,
                        avalon
                    )
                );
            });
        }
    }, {
        key: "renderTableHeader",
        value: function renderTableHeader() {
            return this.state.headers.map(function (header, index) {
                return React.createElement(
                    "th",
                    { key: index },
                    header
                );
            });
        }
    }, {
        key: "render",
        value: function render() {
            return React.createElement(Table, { renderTableHeader: this.renderTableHeader, renderTableData: this.renderTableData, title: 'Top Countries Table' });
        }
    }]);

    return TopCountriesTableContainer;
}(React.Component);

// Fetches the top zipcodes data and sends data to table. 


var TopTenZCTable = function (_React$Component3) {
    _inherits(TopTenZCTable, _React$Component3);

    function TopTenZCTable(props) {
        _classCallCheck(this, TopTenZCTable);

        var _this3 = _possibleConstructorReturn(this, (TopTenZCTable.__proto__ || Object.getPrototypeOf(TopTenZCTable)).call(this, props));

        _this3.state = {
            JSONdata: [{ "Row": "1", "ClusterID": "822073", "Zip": "08401", "Centername": "Tanger Outlets at The Walk", "CITY": "Atlantic City", "STATE": "NJ", "TOTPOP_CY": " 39,880", "TOTHH_CY": " 15,417", "PopGrowth": "0.2", "AverageIncome": "$47,382", "PercentVisits": "13.3", "CumulativeTotal": "13.3" }, { "Row": "2", "ClusterID": "822073", "Zip": "08234", "Centername": "Tanger Outlets at The Walk", "CITY": "Egg Harbor Township", "STATE": "NJ", "TOTPOP_CY": " 43,472", "TOTHH_CY": " 14,976", "PopGrowth": "-0.1", "AverageIncome": "$96,574", "PercentVisits": "3.0", "CumulativeTotal": "16.3" }, { "Row": "3", "ClusterID": "822073", "Zip": "08232", "Centername": "Tanger Outlets at The Walk", "CITY": "Pleasantville", "STATE": "NJ", "TOTPOP_CY": " 19,390", "TOTHH_CY": " 6,153", "PopGrowth": "-1.0", "AverageIncome": "$59,595", "PercentVisits": "2.4", "CumulativeTotal": "18.7" }, { "Row": "4", "ClusterID": "822073", "Zip": "08205", "Centername": "Tanger Outlets at The Walk", "CITY": "Absecon", "STATE": "NJ", "TOTPOP_CY": " 28,904", "TOTHH_CY": " 10,445", "PopGrowth": "-1.0", "AverageIncome": "$88,527", "PercentVisits": "2.0", "CumulativeTotal": "20.6" }, { "Row": "5", "ClusterID": "822073", "Zip": "08330", "Centername": "Tanger Outlets at The Walk", "CITY": "Mays Landing", "STATE": "NJ", "TOTPOP_CY": " 27,973", "TOTHH_CY": " 10,099", "PopGrowth": "-1.0", "AverageIncome": "$77,371", "PercentVisits": "1.9", "CumulativeTotal": "22.6" }, { "Row": "6", "ClusterID": "822073", "Zip": "08406", "Centername": "Tanger Outlets at The Walk", "CITY": "Ventnor City", "STATE": "NJ", "TOTPOP_CY": " 10,792", "TOTHH_CY": " 4,592", "PopGrowth": "-0.6", "AverageIncome": "$90,352", "PercentVisits": "1.2", "CumulativeTotal": "23.8" }, { "Row": "7", "ClusterID": "822073", "Zip": "08203", "Centername": "Tanger Outlets at The Walk", "CITY": "Brigantine", "STATE": "NJ", "TOTPOP_CY": " 9,384", "TOTHH_CY": " 4,227", "PopGrowth": "-1.3", "AverageIncome": "$94,204", "PercentVisits": "1.1", "CumulativeTotal": "24.8" }, { "Row": "8", "ClusterID": "822073", "Zip": "08201", "Centername": "Tanger Outlets at The Walk", "CITY": "Absecon", "STATE": "NJ", "TOTPOP_CY": " 9,881", "TOTHH_CY": " 3,757", "PopGrowth": "-0.7", "AverageIncome": "$80,057", "PercentVisits": "1.0", "CumulativeTotal": "25.8" }, { "Row": "9", "ClusterID": "822073", "Zip": "08260", "Centername": "Tanger Outlets at The Walk", "CITY": "Wildwood", "STATE": "NJ", "TOTPOP_CY": " 12,608", "TOTHH_CY": " 5,832", "PopGrowth": "-4.5", "AverageIncome": "$68,037", "PercentVisits": "0.7", "CumulativeTotal": "26.4" }, { "Row": "10", "ClusterID": "822073", "Zip": "08360", "Centername": "Tanger Outlets at The Walk", "CITY": "Vineland", "STATE": "NJ", "TOTPOP_CY": " 43,118", "TOTHH_CY": " 14,863", "PopGrowth": "-0.9", "AverageIncome": "$62,285", "PercentVisits": "0.6", "CumulativeTotal": "27.0" }],
            headers: ['Rank', 'Zip Code', 'City', 'State', 'Percent Visits', 'Cumulative Percent Vists', 'Total Households', 'Total Population', 'Average Household Income', 'Population Growth']
        };
        _this3.renderTableData = _this3.renderTableData.bind(_this3);
        _this3.renderTableHeader = _this3.renderTableHeader.bind(_this3);
        return _this3;
    }

    _createClass(TopTenZCTable, [{
        key: "renderTableData",
        value: function renderTableData() {
            return this.state.JSONdata.map(function (location, index) {
                var Row = location.Row,
                    Zip = location.Zip,
                    CITY = location.CITY,
                    STATE = location.STATE,
                    PercentVisits = location.PercentVisits,
                    CumulativeTotal = location.CumulativeTotal,
                    TOTHH_CY = location.TOTHH_CY,
                    TOTPOP_CY = location.TOTPOP_CY,
                    AverageIncome = location.AverageIncome,
                    PopGrowth = location.PopGrowth;

                return React.createElement(
                    "tr",
                    { key: Row },
                    React.createElement(
                        "td",
                        null,
                        Row
                    ),
                    React.createElement(
                        "td",
                        null,
                        Zip
                    ),
                    React.createElement(
                        "td",
                        null,
                        CITY
                    ),
                    React.createElement(
                        "td",
                        null,
                        STATE
                    ),
                    React.createElement(
                        "td",
                        null,
                        PercentVisits
                    ),
                    React.createElement(
                        "td",
                        null,
                        CumulativeTotal
                    ),
                    React.createElement(
                        "td",
                        null,
                        TOTHH_CY
                    ),
                    React.createElement(
                        "td",
                        null,
                        TOTPOP_CY
                    ),
                    React.createElement(
                        "td",
                        null,
                        AverageIncome
                    ),
                    React.createElement(
                        "td",
                        null,
                        PopGrowth
                    )
                );
            });
        }
    }, {
        key: "renderTableHeader",
        value: function renderTableHeader() {
            return this.state.headers.map(function (header, index) {
                return React.createElement(
                    "th",
                    { key: index },
                    header
                );
            });
        }
    }, {
        key: "render",
        value: function render() {
            return React.createElement(Table, { renderTableData: this.renderTableData, renderTableHeader: this.renderTableHeader, title: 'Top Ten Zip Codes' });
        }
    }]);

    return TopTenZCTable;
}(React.Component);

var ZipCodeTableContainer = function (_React$Component4) {
    _inherits(ZipCodeTableContainer, _React$Component4);

    function ZipCodeTableContainer(props) {
        _classCallCheck(this, ZipCodeTableContainer);

        var _this4 = _possibleConstructorReturn(this, (ZipCodeTableContainer.__proto__ || Object.getPrototypeOf(ZipCodeTableContainer)).call(this, props));

        _this4.state = {};
        return _this4;
    }

    _createClass(ZipCodeTableContainer, [{
        key: "componentDidMount",
        value: function componentDidMount() {}
    }, {
        key: "renderTableData",
        value: function renderTableData() {}
    }, {
        key: "renderTableHeader",
        value: function renderTableHeader() {}
    }, {
        key: "render",
        value: function render() {
            console.log(this.props.zipCode);
            return React.createElement(Table, { renderTableData: this.renderTableData, renderTableHeader: this.renderTableHeader, title: 'Zip Code Opportunity Table' });
        }
    }]);

    return ZipCodeTableContainer;
}(React.Component);

ReactDOM.render(React.createElement(ReportViewer, null), document.getElementById('reports-container'));
