(function (win, doc) {

  const init = (event) => {
    const navSectionList = doc.getElementById('nav_section_list'),
          dropContentMenus = doc.querySelectorAll('.main-nav__drop-content'),
          btnHamburger = doc.getElementById('btn_hamburger'),
          btnFlyoutEsc = doc.getElementById('btn_hamburger_esc'),
          menuFlyout = doc.getElementById('flyout_menu'),
          flyoutLinks = doc.querySelectorAll(
            '.main-nav__flyout-link, #btn_hamburger_esc'
          ),
          siteFooter = doc.getElementById('site_footer'),
          isFocused = el => doc.activeElement === el,
          parseBool = str =>
            str === 'true' ? true : str === 'false' ? false : undefined,
          startAnimation = (el) => {
            el.style.animationPlayState = 'running';
          },
          pauseAnimation = (el) => {
            el.style.animationPlayState = 'paused';
          },
          hideDropdownAria = (el) => {
            el.hasAttribute('aria-hidden') &&
              !parseBool(el.getAttribute('aria-hidden')) &&
                el.setAttribute('aria-hidden', true);

            parseBool(el.getAttribute('aria-expanded')) &&
              el.setAttribute('aria-expanded', false);
          },
          showDropdownAria = (el) => {
            parseBool(el.getAttribute('aria-hidden')) &&
              el.setAttribute('aria-hidden', false);

            el.hasAttribute('aria-expanded') &&
              !parseBool(el.getAttribute('aria-expanded')) &&
                el.setAttribute('aria-expanded', true);
          },
          toggleDropdownAria = (el) => {
            el.hasAttribute('aria-hidden') &&
              (parseBool(el.getAttribute('aria-hidden'))
                ? el.setAttribute('aria-hidden', false)
                : el.setAttribute('aria-hidden', true)
              );

            el.hasAttribute('aria-expanded') &&
              (parseBool(el.getAttribute('aria-expanded'))
                ? el.setAttribute('aria-expanded', false)
                : el.setAttribute('aria-expanded', true)
              );
          },
          showElement = (el) => {
            el.dataset.toggle = 'visible';
            el.removeAttribute('hidden');
            showDropdownAria(el);
          },
          openElement = (el) => {
            showElement(el);
            setTimeout((el) => {
              el.classList.add('is-open');
            }, 50, el);
          },
          hideElement = (el) => {
            el.dataset.toggle && (el.dataset.toggle = 'hidden');
            el.setAttribute('hidden', '');
            hideDropdownAria(el);
          },
          closeElement = (el) => {
            el.classList.remove('is-open');
            hideElement(el);
          };

    const windowSizeHandler = (win) => {
      const viewWidth = win.innerWidth,
            viewHeight = win.innerHeight;

      // If the viewport is smaller than 699px the hamburger will be visible
      if (viewWidth < 800) {
        btnHamburger.tabIndex = '0';
        btnHamburger.removeAttribute('hidden');
      } else {
        btnHamburger.tabIndex = '-1';
        btnHamburger.setAttribute('hidden', '');
        if (menuFlyout.dataset.toggle === 'visible') {
          btnFlyoutEsc.style.display = 'block!important';
        }
      }

      if (viewHeight < 400) {
        hideElement(siteFooter);
      } else {
        if (menuFlyout.dataset.toggle === 'hidden') {
          showElement(siteFooter);
        }
      }
    };

    windowSizeHandler(win);

    win.addEventListener('resize', () => {
      windowSizeHandler(this);
    });

    // Keyboard navigation
    const keypressHandler = (evt) => {
      evt = evt || win.event;

      if (evt.defaultPrevented) return;

      const linkOverview = doc.getElementById('nav_link_overview'),
            linkMap = doc.getElementById('nav_link_maps'),
            linkReports = doc.getElementById('nav_link_reports'),
            linkCharts = doc.getElementById('nav_link_charts'),
            btnHelp = doc.getElementById('nav_btn_help'),
            linkHelpGuide = doc.getElementById('nav_link_help_guide'),
            linkHelpForm = doc.getElementById('nav_link_help_form'),
            btnAcct = doc.getElementById('nav_btn_acct'),
            linkAcctProfile = doc.getElementById('nav_link_profile'),
            linkAcctSignOut = doc.getElementById('nav_link_so'),
            navElementList = doc.querySelectorAll(
              '.main-nav__section-link, .main-nav__drop-btn, .main-nav__drop-link'
            ),
            navElementListLength = navElementList.length;

      let currentFocusIndex = 0;

      switch (evt.key) {
        case '1':
          linkOverview.focus();
          break;
        case '2':
          linkMap.focus();
          break;
        case '3':
          linkReports.focus();
          break;
        case '4':
          linkCharts.focus();
          break;
        case '5':
          btnHelp.focus();
          break;
        case '6':
          btnAcct.focus();
          break;
        case 'ArrowRight':
          if (isFocused(linkAcctSignOut)) {
            currentFocusIndex = -1;
          } else {
            for (let index = navElementListLength; index--;) {
              if (isFocused(navElementList[index])) {
                currentFocusIndex = index;
              }
            }
          }
          navElementList[++currentFocusIndex].focus();
          break;
        case 'ArrowLeft':
          // FIXME: Broken logic here for left arrow cycling
          if (isFocused(linkOverview)) {
            currentFocusIndex = navElementListLength;
            btnAcct.focus();
          } else {
            for (let index = navElementListLength; index--;) {
              if (isFocused(navElementList[index])) {
                currentFocusIndex = index;
              }
            }
          }
          navElementList[--currentFocusIndex].focus();
          break;
        case 'Space':
        default:
          return;
      }

      evt.preventDefault();
    };

    win.addEventListener('keydown', keypressHandler);

    const mainNavMouseOverHandler = (evt) => {
      evt = evt || win.event;

      const evtSrc = evt.target;

      if (evtSrc.classList[0] === 'main-nav__drop-btn') {
        const dropdownMenu = target.firstElementChild.nextElementSibling;

        if (dropdownMenu.nodeName === 'UL') {
          showDropdownAria(dropdownMenu);
        }
      } else {
        if (!evtSrc.closest('.main-nav__drop-btn')) {
          for (const menu of dropContentMenus) {
            hideDropdownAria(menu);
          }
        }
      }

      event.stopPropagation();
    };

    const mainNavMouseOutHandler = (evt) => {
      evt = evt || win.event;

      for (const menu of dropContentMenus) {
        hideDropdownAria(menu);
      }

      evt.stopPropagation();
    };

    const mainNavFocusOutHandler = (evt) => {
      evt = evt || win.event;

      const evtSrc = evt.target;

      if (evtSrc.classList[0] === 'main-nav__drop-btn') {
        const dropdownMenu = target.firstElementChild.nextElementSibling;

        if (dropdownMenu.nodeName === 'UL') {
          hideDropdownAria(dropdownMenu);
        }
      }
    };

    // FIXME: This area broken
    // navSectionList.addEventListener('mouseover', mainNavMouseOverHandler);
    // navSectionList.addEventListener('focusin', mainNavMouseOverHandler);
    // navSectionList.addEventListener('mouseout', mainNavMouseOutHandler);
    // navSectionList.addEventListener('focusout', mainNavFocusOutHandler);

    // Hamburger flyout menu click event
    btnHamburger.addEventListener('click', (evt) => {
      evt = evt || win.event;

      hideElement(siteFooter);
      openElement(menuFlyout);

      // Add all flyout links to the tab index
      for (const link of flyoutLinks) {
        link.tabIndex = '0';
      }

      evt.stopPropagation();
    });

    // Flyout menu close click event
    btnFlyoutEsc.addEventListener('click', (evt) => {
      evt = evt || win.event;

      showElement(siteFooter);
      closeElement(menuFlyout);

      // Remove all flyout links from the tab index
      for (const link of flyoutLinks) {
        link.tabIndex = '-1';
      }

      evt.stopPropagation();
    });
  };

  win.addEventListener('DOMContentLoaded', init);
})(this, document);
