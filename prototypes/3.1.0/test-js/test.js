function timingTestRegEx() {
  const IS_NUM = /^[0-9]*\.?[0-9]+$/;
  var holder;

  console.time('timerRegEx');

  for (let i = 0; i < 99999; i++) {
    holder = IS_NUM.test('145744.3698');
  }
  console.log(holder);
  console.log('Time for RegEx:');
  console.timeEnd('timerRegEx');
}

function timingTestIsNaN() {
  var holder;

  console.time('timerIsNan');

  for (let i = 0; i < 99999; i++) {
    holder = !isNaN('145744.3698');
  }
  console.log(holder);
  console.log('Time for isNaN:');
  console.timeEnd('timerIsNan');
}

timingTestRegEx();
timingTestIsNaN();
