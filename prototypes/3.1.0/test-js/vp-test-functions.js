/**
* @name changeContent
* @description Changes the content of a table with new values. In the real thing
* this would be done by pulling values from the database instead of from arrays.
* This function is only necessary for this proof of concept.
*
* @param {string} ddIndex - The index associated with a dropdown selection
*/
function changeContent(ddIndex) {
  'use strict';
  // Create all the values that will be replaced (normally this would come from a database)
  const LOC0_DATA = [3456396, 45],
        LOC1_DATA = [2154766, 33],
        COMP0_DATA = [1535642, 44],
        COMP1_DATA = [555444, 19],
        LOC_ITERATIONS = LOC0_DATA.length + 1;

  var i = 0, id;

  if (ddIndex === 'loc0' || ddIndex === 'loc1') {
    i = 0;
    if (ddIndex === 'loc0') {
      for (let k = 1; k < LOC_ITERATIONS; k++) {
        id = 'vp_col1_val' + k;
        document.getElementById(id).innerHTML = LOC0_DATA[i++];
      }
    }
    else {
      for (let k = 1; k < LOC_ITERATIONS; k++) {
        id = 'vp_col1_val' + k;
        document.getElementById(id).innerHTML = LOC1_DATA[i++];
      }
    }
  }
  else if (ddIndex === 'comp0' || ddIndex === 'comp1') {
    i = 0;
    if (ddIndex === 'comp1') {
      for (let k = 1; k < BENCH_ITERATIONS; k++) {
        id = 'vp_col3_val' + k;
        document.getElementById(id).innerHTML = COMP0_DATA[i++];
      }
    }
    else {
      for (let k = 1; k < BENCH_ITERATIONS; k++) {
        id = 'vp_col3_val' + k;
        document.getElementById(id).innerHTML = COMP1_DATA[i++];
      }
    }
  }
  else {
    alert('ERROR. Passed in DDIndex value incorrect');
  }
}

window.onload = function() {
  'use strict';

  let visibleColumns = 3;

  for (let i = 1; i < visibleColumns + 1; i++) {
    animCol(i, 2);
  }

  document.getElementById('dd1').onchange = function() {
    const INDEX = this.value;
    let label;

    if (INDEX == 908) {
      label = this.options[0].text;
      swapLabel('loc-name', label);
      swapMapUrl(INDEX);
      changeContent('loc0');
      animCol(1, 2);
    }
    else if (INDEX == 909) {
      label = this.options[1].text;
      swapLabel('loc-name', label);
      swapMapUrl(INDEX);
      changeContent('loc1');
      animCol(1, 2);
    }
  };

  document.getElementById('dd1').onclick = function() {
    toggleDropdown('dd1');
  };

  document.getElementById('dd1').onblur = function() {
    resetDropdown('dd1');
  };

  document.getElementById('dd2').onchange = function() {
    const INDEX = this.value;
    let label;

    if (INDEX == 0) {
      removeCol();
      visibleColumns = 2;
    }
    else if (INDEX == 1) {
      label = this.options[(INDEX * 1)].text;
      addCol();
      visibleColumns = 3;
      changeContent('comp1');
      swapLabel('comp-name', label);
      animCol(3, 2);
    }
    else if (INDEX == 2) {
      label = this.options[(INDEX * 1)].text;
      addCol();
      visibleColumns = 3;
      changeContent('comp1');
      swapLabel('comp-name', label);
      animCol(3, 2);
    }
  };

  document.getElementById('dd2').onclick = function() {
    toggleDropdown('dd2');
  };

  document.getElementById('dd2').onblur = function() {
    resetDropdown('dd2');
  };
};
