/**
* @name changeContent
* @description Changes the content of a table with new values. In the real thing
* this would be done by pulling values from the database instead of from arrays.
* This function is only necessary for this proof of concept.
*
* @param {string} ddIndex - The index associated with a dropdown selection
*/
function changeContent(ddIndex) {
  'use strict';
  // Create all the values that will be replaced (normally this would come from a database)
  // Value types:         Heading      ,     Pop  , Time, % , % ,   $  ,  % , % , % , %
  const loc0Data =   ['Arcadia Market',    3257168, 77.3, 58, 32, 69998, 38, 32, 29, 59],
        loc1Data =   ['Lunchs Counter',    2153659, 59.9, 44, 74, 89458, 31, 27, 25, 68],
        comp1Data =  ['Bernie\'s Beanery',  256543, 69.9, 39, 55, 66421, 29, 28, 26, 49],
        comp2Data =  ['Pizza Shackz',       111111, 66.5, 38, 51, 38198, 27, 26, 38, 50];

  var i = 0, id, heading;

  if (ddIndex === 'loc0' || ddIndex === 'loc1') {
    i = 0;
    heading = 'heading2';
    if (ddIndex === 'loc0') {
      document.getElementById(heading).innerHTML = loc0Data[i++];
      for (let k = 1; k < 10; k++) {
        id = 'col2_val' + k;
        document.getElementById(id).innerHTML = loc0Data[i++];
      }
    }
    else {
      document.getElementById(heading).innerHTML = loc1Data[i++];
      for (let k = 1; k < 10; k++) {
        id = 'col2_val' + k;
        document.getElementById(id).innerHTML = loc1Data[i++];
      }
    }
  }
  else if (ddIndex === 'comp1' || ddIndex === 'comp2') {
    i = 0;
    heading = 'heading5';
    if (ddIndex === 'comp1') {
      document.getElementById(heading).innerHTML = comp1Data[i++];
      for (let k = 1; k < 10; k++) {
        id = 'col5_val' + k;
        document.getElementById(id).innerHTML = comp1Data[i++];
      }
    }
    else if (ddIndex === 'comp2') {
      document.getElementById(heading).innerHTML = comp2Data[i++];
      for (let k = 1; k < 10; k++) {
        id = 'col5_val' + k;
        document.getElementById(id).innerHTML = comp2Data[i++];
      }
    }
  }
  else {
    alert('ERROR. Passed in DDIndex value incorrect');
  }
}

window.onload = function() {
  'use strict';
  let hidden = 0;

  animKpi();

  document.getElementById('dd1').onchange = function() {
    const INDEX = this.value;
    if (INDEX == 0) {
      changeContent('loc0');
      animCol(2);
    }
    else {
      changeContent('loc1');
      animCol(2);
    }
  };


  document.getElementById('dd1').onclick = function() {
    toggleDropdown('dd1');
  };

  document.getElementById('dd1').onblur = function() {
    resetDropdown('dd1');
  };

  document.getElementById('dd2').onchange = function() {
    const INDEX = this.value;
    if (INDEX == 0) {
      hideCol('comp-col', 'comp-val');
      hidden = 1;
    }
    else if (INDEX == 1) {
      if (hidden) {
        showCol('comp-col', 'comp-val');
        hidden = 0;
      }
      changeContent('comp1');
      animCol(5);
    }
    else {
      if (hidden) {
        showCol('comp-col', 'comp-val');
        hidden = 0;
      }
      changeContent('comp2');
      animCol(5);
    }
  };


  document.getElementById('dd2').onclick = function() {
    toggleDropdown('dd2');
  };

  document.getElementById('dd2').onblur = function() {
    resetDropdown('dd2');
  };
};
