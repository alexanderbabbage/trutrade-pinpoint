/**
* @name changeContent
* @description Changes the content of a table with new values. In the real thing
* this would be done by pulling values from the database instead of from arrays.
* This function is only necessary for this proof of concept.
*
* @param {string} ddIndex - The index associated with a dropdown selection
*/
function changeContent(ddIndex) {
  'use strict';
  // Create all the values that will be replaced (normally this would come from a database)
  const LOC0_DATA =[
          129, 465, 51, 100, 99, 99.5, 100.4, 299, 300, 89, 152, 33,
          22, 132, 55, 99, 203, 311, 101, 100, 235, 382, 300
        ],
        LOC1_DATA = [
          19, 100, 66, 77, 55, 110, 153, 19, 1, 55, 66, 100,
          99, 103, 157, 0, 41, 99, 204, 332, 129, 99, 57
        ],
        COMP1_DATA = [
          '99,854', 96, 71, 55, 6, 85, 19, 100, 42, 22, 51, 28,
          25, 17, 21, 44, 12, 47, 13, 70, 17, 23, 84
        ],
        COMP2_DATA = [
          '154,875', 27, 81, 44, 81, 67, 45, 19, 90, 60, 30, 45, 74,
          75, 87, 69, 47, 40, 30, 17, 31, 43, 59
        ],
        ITERATION_NUM = LOC0_DATA.length + 1,
        COMP_VALS = document.getElementsByClassName('.comp-val');

  let i = 0, id;
  if (ddIndex == 'loc0' || ddIndex == 'loc1') {
    i = 0;
    if (ddIndex === 'loc0') {
      for (let k = 1; k < ITERATION_NUM; k++) {
        id = 'av_val' + k;
        document.getElementById(id).innerHTML = LOC0_DATA[i++];
      }
    }
    else {
      for (let k = 1; k < ITERATION_NUM; k++) {
        id = 'av_val' + k;
        document.getElementById(id).innerHTML = LOC1_DATA[i++];
      }
    }
  }
  else {
    i = 0;
    if (ddIndex == 'comp1') {
      Array.prototype.forEach.call(COMP_VALS, function(element) {
        element.innerHTML = COMP1_DATA[i++];
      });
    }
    else {
      Array.prototype.forEach.call(COMP_VALS, function(element) {
        element.innerHTML = COMP2_DATA[i++];
      });
    }
  }
}


window.onload = function() {
  'use strict';

  animCol(0, 3);

  document.getElementById('dd1').onchange = function() {
    const INDEX = this.value;
    if (INDEX == 0) {
      changeContent('loc0');
      animCol(0, 3);
    }
    else if (INDEX == 1) {
      changeContent('loc1');
      animCol(0, 3);
    }
  };


  document.getElementById('dd1').onclick = function() {
    toggleDropdown('dd1');
  };

  document.getElementById('dd1').onblur = function() {
    resetDropdown('dd1');
  };

  document.getElementById('dd2').onchange = function() {
    const INDEX = this.value;

    if (INDEX == 0) {
      hideCol('comp-col', 'comp-val');
    }
    else if (INDEX == 1 || INDEX == 2) {
      if (document.getElementById('comp_col_head').style.display === 'none') {
        showCol('comp-col', 'comp-val');
      }
      if (INDEX == 1) {
        swapLabel('comp-name', this.options[(INDEX * 1)].text);
        changeContent('comp1');
      }
      else {
        swapLabel('comp-name', this.options[(INDEX * 1)].text);
        changeContent('comp2');
      }
    }
  };


  document.getElementById('dd2').onclick = function() {
    toggleDropdown('dd2');
  };

  document.getElementById('dd2').onblur = function() {
    resetDropdown('dd2');
  };
};

/**
 *
 * @param {number} min The inclusive mininum integer value
 * @param {number} max The exclusive maximum integer value
 * @return {number} The random integer being returned
 */
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

/**
 *
 * @param {number} length The length of the array
 * @param {number} maxVal The max value of the number in the array
 * @return {number[]} The generated array
 */
function generateRandomArray(length, maxVal) {
  let randomArray = [];

  for (let i = 0; i < length; i++) {
    randomArray.push(getRandomInt(1, maxVal + 1));
  }

  return randomArray;
}
