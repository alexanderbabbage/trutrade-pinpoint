/**
* @name changeContent
* @description Changes the content of a table with new values. In the real thing
* this would be done by pulling values from the database instead of from arrays.
* This function is only necessary for this proof of concept.
*
* @param {string} ddIndex - The index associated with a dropdown selection
*/
function changeContent(ddIndex) {
  'use strict';
  // Create all the values that will be replaced (normally this would come from a database)
  const LOC0_DATA = [3456396, 45],
        LOC1_DATA = [2154766, 0],
        BENCH0_DATA = [420667, 31],
        BENCH1_DATA = [665419, 88],
        LOC_ITERATIONS = LOC0_DATA.length + 1,
        BENCH_ITERATIONS = BENCH0_DATA.length + 1;

  var i = 0, id;

  if (ddIndex === 'loc0' || ddIndex === 'loc1') {
    i = 0;
    if (ddIndex === 'loc0') {
      for (let k = 1; k < LOC_ITERATIONS; k++) {
        id = 'vo_col1_val' + k;
        document.getElementById(id).innerHTML = LOC0_DATA[i++];
      }
    }
    else {
      for (let k = 1; k < LOC_ITERATIONS; k++) {
        id = 'vo_col1_val' + k;
        document.getElementById(id).innerHTML = LOC1_DATA[i++];
      }
    }
  }
  // else if (ddIndex === 'bench0' || ddIndex === 'bench1') {
  //   i = 0;
  //   if (ddIndex === 'bench0') {
  //     for (let k = 1; k < BENCH_ITERATIONS; k++) {
  //       id = 'vo_col2_val' + k;
  //       document.getElementById(id).innerHTML = BENCH0_DATA[i++];
  //     }
  //   }
  //   else {
  //     for (let k = 1; k < BENCH_ITERATIONS; k++) {
  //       id = 'vo_col2_val' + k;
  //       document.getElementById(id).innerHTML = BENCH1_DATA[i++];
  //     }
  //   }
  // }
  else {
    alert('ERROR. Passed in DDIndex value incorrect');
  }
}

window.onload = function() {
  'use strict';
  document.getElementById('dd1').onchange = function() {
    const INDEX = this.value;
    let label;
    if (INDEX == 0) {
      label = this.options[(INDEX*1)+1].text;
      swapLabel('loc-name', label);
      changeContent('loc0');
      animCol(1, 1);
    }
    else if (INDEX == 1) {
      label = this.options[(INDEX*1)+1].text;
      swapLabel('loc-name', label);
      changeContent('loc1');
      animCol(1, 1);
    }
  };


  document.getElementById('dd1').onclick = function() {
    toggleDropdown('dd1');
  };

  document.getElementById('dd1').onblur = function() {
    resetDropdown('dd1');
  };

  // document.getElementById('dd2').onchange = function() {
  //   if (document.getElementById('sub_tile').style.display === 'none') {
  //     const INDEX = this.value;
  //     let label;
  //     if (INDEX == 0) {
  //       label = this.options[(INDEX*1)+1].text;
  //       changeContent('bench0');
  //       swapLabel('bench-name', label);
  //       animCol(2, 2, 1);
  //     }
  //     else {
  //       label = this.options[(INDEX*1)+1].text;
  //       changeContent('bench1');
  //       swapLabel('bench-name', label);
  //       animCol(2, 2, 1);
  //     }
  //   }
  // };

  document.getElementById('dd2').onchange = function() {
    const INDEX = this.value;
    let label;
    if (INDEX == 0) {
      noCompSelected();
    }
    else if (INDEX == 1 || INDEX == 2) {
      if (document.getElementById('sub_tile').style.display === 'none') {
        compSelected();
      }
      if (INDEX == 1) {
        label = this.options[(INDEX*1)+1].text;
        swapLabel('comp-name', label);
      }
      else {
        label = this.options[(INDEX*1)+1].text;
        swapLabel('comp-name', label);
      }
    }
  };


  document.getElementById('dd2').onclick = function() {
    toggleDropdown('dd2');
  };

  document.getElementById('dd2').onblur = function() {
    resetDropdown('dd2');
  };
};
