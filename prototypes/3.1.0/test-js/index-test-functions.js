/**
* @name changeContent
* @description Changes the content of a table with new values. In the real thing
* this would be done by pulling values from the database instead of from arrays.
* This function is only necessary for this proof of concept.
*
* @param {string} ddIndex - The index associated with a dropdown selection
*/
function changeContent(ddIndex) {
  'use strict';
  // Create all the values that will be replaced (normally this would come from a database)
  const VO_LOC0_DATA = [3456396, 45],
        VO_LOC1_DATA = [2154766, 0],
        VO_LOC_ITERATIONS = VO_LOC0_DATA.length + 1,
        KPI_LOC0_DATA =   ['Arcadia Market',    3257168, 77.3, 58, 32, 69998, 38, 32, 29, 59],
        KPI_LOC1_DATA =   ['Lunchs Counter',    2153659, 59.9, 44, 74, 89458, 31, 27, 25, 68],
        KPI_COM1_DATA =  ['Bernie\'s Beanery',  256543, 69.9, 39, 55, 66421, 29, 28, 26, 49],
        KPI_COM2_DATA =  ['Pizza Shackz',       111111, 66.5, 38, 51, 38198, 27, 26, 38, 50],
        AVPV_LOC0_DATA =[
          129, 465, 51, 100, 99, 99.5, 100.4, 299, 300, 89, 152, 33,
          22, 132, 55, 99, 203, 311, 101, 100, 235, 382, 300
        ],
        AVPV_LOC1_DATA = [
          19, 100, 66, 77, 55, 110, 153, 19, 1, 55, 66, 100,
          99, 103, 157, 0, 41, 99, 204, 332, 129, 99, 57
        ],
        AVPV_ITERATION_NUM = 23,
        LOC0_DATA = [3456396, 45],
        LOC1_DATA = [2154766, 33],
        COMP0_DATA = [1535642, 44],
        COMP1_DATA = [555444, 19];


  var i = 0,
      j = 0,
      f = 0,
      q = 0,
      voId,
      kpiId,
      avpvId,
      vpId,
      kpiHeading;

  if (ddIndex === 'loc0' || ddIndex === 'loc1') {
    i = 0,
    j = 0,
    f = 0,
    q = 0,
    kpiHeading = 'heading2';
    if (ddIndex === 'loc0') {
      document.getElementById(kpiHeading).innerHTML = KPI_LOC0_DATA[j++];
      for (let k = 1; k < VO_LOC_ITERATIONS; k++) {
        voId = 'vo_col1_val' + k;
        document.getElementById(voId).innerHTML = VO_LOC0_DATA[i++];
        vpId = 'vp_col1_val' + k;
        document.getElementById(vpId).innerHTML = LOC0_DATA[q++];
      }
      for (let k = 1; k < 10; k++) {
        kpiId = 'col2_val' + k;
        document.getElementById(kpiId).innerHTML = KPI_LOC0_DATA[j++];
      }
      for (let k = 1; k < AVPV_ITERATION_NUM; k++) {
        avpvId = 'av_val' + k;
        document.getElementById(avpvId).innerHTML = AVPV_LOC0_DATA[f++];
      }
    }
    else {
      document.getElementById(kpiHeading).innerHTML = KPI_LOC1_DATA[j++];
      for (let k = 1; k < VO_LOC_ITERATIONS; k++) {
        voId = 'vo_col1_val' + k;
        document.getElementById(voId).innerHTML = VO_LOC1_DATA[i++];
        vpId = 'vp_col1_val' + k;
        document.getElementById(vpId).innerHTML = LOC1_DATA[q++];
      }
      for (let k = 1; k < 10; k++) {
        kpiId = 'col2_val' + k;
        document.getElementById(kpiId).innerHTML = KPI_LOC1_DATA[j++];
      }
      for (let k = 1; k < AVPV_ITERATION_NUM; k++) {
        avpvId = 'av_val' + k;
        document.getElementById(avpvId).innerHTML = AVPV_LOC1_DATA[f++];
      }
    }
  }
  else if (ddIndex === 'comp1' || ddIndex === 'comp2') {
    j = 0,
    f = 0,
    q = 0,
    kpiHeading = 'heading5';
    if (ddIndex === 'comp1') {
      document.getElementById(kpiHeading).innerHTML = KPI_COM1_DATA[j++];
      for (let k = 1; k < 10; k++) {
        kpiId = 'col5_val' + k;
        document.getElementById(kpiId).innerHTML = KPI_COM1_DATA[j++];
      }
      for (let k = 1; k < VO_LOC_ITERATIONS; k++) {
        vpId = 'vp_col3_val' + k;
        document.getElementById(vpId).innerHTML = COMP0_DATA[q++];
      }
    }
    else if (ddIndex === 'comp2') {
      document.getElementById(kpiHeading).innerHTML = KPI_COM2_DATA[j++];
      for (let k = 1; k < 10; k++) {
        kpiId = 'col5_val' + k;
        document.getElementById(kpiId).innerHTML = KPI_COM2_DATA[j++];
      }
      for (let k = 1; k < VO_LOC_ITERATIONS; k++) {
        vpId = 'vp_col3_val' + k;
        document.getElementById(vpId).innerHTML = COMP1_DATA[q++];
      }
    }
  }
  else {
    alert('ERROR. Passed in DDIndex value incorrect');
  }
}

window.onload = function() {
  'use strict';

  const DD1 = document.getElementById('dd1'),
        DD2 = document.getElementById('dd2');

  let hidden = 0,
      visibleColumns = 3;

  animKpi();
  animCol(0, 3);

  for (let i = 1; i < visibleColumns + 1; i++) {
    animCol(i, 2);
  }

  DD1.onclick = function() {
    toggleDropdown('dd1');
  };

  DD1.onblur = function() {
    resetDropdown('dd1');
  };

  DD1.onchange = function() {
    const INDEX = this.value;
    if (INDEX == 0) {
      changeContent('loc0');
      animCol(1, 1);
      animCol(2);
      animCol(0, 3);
      animCol(1, 2);
    }
    else if (INDEX == 1) {
      changeContent('loc1');
      animCol(1, 1);
      animCol(2);
      animCol(0, 3);
      animCol(1, 2);
    }
  };

  DD2.onclick = function() {
    toggleDropdown('dd2');
  };

  DD2.onblur = function() {
    resetDropdown('dd2');
  };

  DD2.onchange = function() {
    const INDEX = this.value;
    if (INDEX == 0) {
      hideComp();
      hidden = 1;
      visibleColumns = 2;
    }
    else if (INDEX == 1 || INDEX == 2) {
      if (hidden) {
        showComp();
        hidden = 0;
      }
      if (INDEX == 1) {
        addCol();
        visibleColumns = 3;
        changeContent('comp1');
        animCol(5);
        animCol(3, 2);
      }
      else {
        if (hidden) {
          showCol('comp-col', 'comp-val');
          hidden = 0;
        }
        addCol();
        visibleColumns = 3;
        changeContent('comp2');
        animCol(5);
        animCol(3, 2);
      }
    }
  };
};
