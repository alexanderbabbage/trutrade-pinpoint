/**
* @file insert-reports.js
* @fileoverview Grabs an array of HTMLElements sent by the sendPageElements
* function and appends them into the child window that calls this file.
*
* @version 1.1.2 02/25/2020 - TruTrade Version
* @requires main.js
* @author Stephen M. Irving
**/

(function (win) {
  'use strict';
  const doc = win.document,
        elArray = win.opener.AB.modules.dom.sendPageElements('.tile'),
        locName = elArray.pop(),
        numElements = elArray.length,
        elRootInsertionPoint = doc.getElementById('report_box'),
        elPageBreakTxt = doc.createElement('span'),
        elPageDivider = doc.createElement('hr'),
        nodePageBreakTxtContent = doc.createTextNode('PAGE BREAK'),
        elYear = doc.getElementById('report_year'),
        elYearCr = doc.getElementById('report_year2'),
        elMonthYear = doc.getElementById('report_month_year'),
        curDate = new Date(),
        curYear = curDate.getFullYear(), // Four digit year
        curMonth = curDate.getMonth() + 1, // 1 or 2 digit month (no leading zero)
        dateTimeMonth = (curMonth < 10 ? '0' + curMonth : curMonth), // 2 digit month
        curDay = curDate.getDate(),
        curMonthYear = new Intl // Format example: 'September 2019' or '09/2019' on legacy
          .DateTimeFormat('en-US', { month: 'long', year: 'numeric' })
          .format(curDate) || dateTimeMonth + '/' + curYear;

  let elPage = doc.createElement('div'),
      elPageInsertionPoint = doc.createElement('div'),
      elPageBreak = doc.createElement('div');

  // Insert the location name into the report
  doc.getElementById('report_loc_name').textContent = locName;

  // Insert the current date information into the report
  elYear.textContent = curYear;
  elYear.setAttribute('datetime', curYear);
  elYearCr.textContent = curYear;
  elYearCr.setAttribute('datetime', curYear);
  elMonthYear.textContent = curMonthYear;
  elMonthYear.setAttribute('datetime', curYear + '/' + dateTimeMonth);

  // Apply static page element class attributes
  elPage.className = 'page page--report';
  elPageInsertionPoint.className = 'page__inlay';
  elPageBreak.className = 'pg-break no-print';
  elPageBreakTxt.className = 'pg-break__txt';
  elPageDivider.className = 'pg-divider';

  // Create a page divider
  elPageBreak
    .appendChild(elPageBreakTxt)
    .appendChild(nodePageBreakTxtContent);
  elPageBreak
    .appendChild(elPageDivider);
  elPageBreak
    .appendChild(elPageBreakTxt.cloneNode(true));

  for (let index = 0; index < numElements; ++index) {
    // Regenerate static page elements via cloning after the first iteration
    if (index) {
      elPage = elPage.cloneNode(false);
      elPageInsertionPoint = elPageInsertionPoint.cloneNode(false);
      elPageBreak = elPageBreak.cloneNode(true);
    }

    // Create a new content page container with inlayed insertion point
    elPage.appendChild(elPageInsertionPoint);

    // Insert the next iterated element into the page container just created
    elPageInsertionPoint.appendChild(elArray[index]);

    // Append the page container to the root insertion point element
    elRootInsertionPoint.appendChild(elPageInsertionPoint);

    // Append the page break element to the root insertion point
    elRootInsertionPoint.appendChild(elPageBreak);
  }

  // Add current date to title so it is there by default when saving to PDF
  document.title =
    'TruTrade Report - ' + (locName || 'Overview') + ' - ' + dateTimeMonth +
    '.' + (curDay < 10 ? '0' : '') + curDay + '.' + (curYear + '').slice(-2);
}(this));
