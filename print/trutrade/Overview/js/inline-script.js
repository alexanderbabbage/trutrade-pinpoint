(function () {
  'use strict';

  const waitMsg =
    'Please wait while the print version of your report is being processed.';

  function reportReadyModal() {
    swal({
      closeOnClickOutside: false,
      title: 'Report Complete',
      text:
        'Your TruTrade® report is finished and ready for printing!\n' +
        'Scroll down and ensure your report is correct, then print or save ' +
        'to PDF via the printing window in your browser.\n\n' +
        'Keyboard Shortcut: CTRL + P',
      icon: 'success',
      buttons: 'OK'
    });
  }

  if (
    typeof Promise !== 'undefined' &&
    Promise.toString().indexOf('[native code]') !== -1
  ) {
    swal({
      closeOnClickOutside: false,
      closeOnEsc: false,
      title: 'Processing Report',
      text: waitMsg,
      icon: 'info',
      buttons: false,
      timer: 1e4
    }).then(reportReadyModal);
  } else {
    swal({
      closeOnClickOutside: false,
      closeOnEsc: false,
      title: 'Processing Report',
      text: waitMsg,
      icon: 'info',
      buttons: false,
      timer: 9985
    });
    setTimeout(reportReadyModal, 1e4);
  }
}());
