/**
 * Provides functionality to the print button on the International Visitation
 * Report.
 *
 * @param {object} wrapper - Pointer to HTML object being passed.
 * @param {string} winTitle - The desired title of the new printing window.
 * @param {number} width - The desired width of the printing window.
 * @param {number} height - The desired height of the printing window.
**/
function prntIntlVis(wrapper, winTitle, width, height) {
  const STYLE = '@font-face{font-family:Roboto;font-style:normal;font-weight:400;src:local("Roboto"),local("Roboto-Regular"),url("css/iOS7Local/fonts/Roboto Regular V2.woff2") format("woff2"),url("css/iOS7Local/fonts/Roboto Regular V2.woff") format("woff"),url("css/iOS7Local/fonts/Roboto Regular V2.ttf") format("truetype");font-display:swap;unicode-range:U+0020-007F,U+00A9,U+00AB,U+00AE,U+00BB}@font-face{font-family:Roboto;font-style:italic;font-weight:400;src:local("Roboto Italic"),local("Roboto-Italic"),url("css/iOS7Local/fonts/Roboto Italic V2.woff2") format("woff2"),url("css/iOS7Local/fonts/Roboto Italic V2.woff") format("woff"),url("css/iOS7Local/fonts/Roboto Italic V2.ttf") format("truetype");font-display:swap;unicode-range:U+0020-007F}@font-face{font-family:Roboto;font-style:normal;font-weight:700;src:local("Roboto Bold"),local("Roboto-Bold"),url("css/iOS7Local/fonts/Roboto Bold V2.woff2") format("woff2"),url("css/iOS7Local/fonts/Roboto Bold V2.woff") format("woff"),url("css/iOS7Local/fonts/Roboto Bold V2.ttf") format("truetype");font-display:swap;unicode-range:U+0020-007F,U+00A9,U+00AB,U+00AE,U+00BB}html{box-sizing:border-box;font-family:sans-serif;font-size:12pt}*,:after,:before{box-sizing:inherit;margin:0;min-width:0;padding:0;text-shadow:none}body{color:#000;font:9.75pt/1.2 Roboto,Arial,-apple-system,Ubuntu,"Segoe UI",BlinkMacSystemFont,sans-serif;height:100%;text-rendering:optimizeLegibility}.report-loading-text,button,img,svg{display:none!important}table,td,th{text-align:right}body,table{overflow-x:hidden;width:100%}table{border-collapse:collapse;border:0;max-height:100%;max-width:100%}td,th{border-color:#aaa;border-style:solid}html,th{word-wrap:break-word}th{background:#2a3c68!important;border-width:1.5pt 0 2.25pt!important;color:#fff;font-size:10.5pt;font-weight:700;max-width:90pt;overflow:hidden;padding:18pt 7.5pt;text-overflow:ellipsis}td{border-width:.75pt 0!important;padding:7.5pt}div{display:inline-block!important;margin:0;padding:0}table.divided-table{flex:0 90 0;margin:0}div.divided-wrapper{align-content:flex-start;align-items:center;display:flex;flex-wrap:nowrap;justify-content:flex-start;max-height:100%!important;max-width:100%!important;margin:0;padding:0}table tbody tr:nth-child(odd){background:#ededf6}.indent{text-indent:7.5pt}.first-col{border-left:.75pt solid #aaa;text-align:left}.first-col td,.first-col th{padding-left:10.5pt;text-align:left}.last-col{border-right:.75pt solid #aaa}.last-col td,.last-col th{padding-right:10.5pt}div.table-container{display:inline-block!important;margin:0 -5.25pt 0 0!important;max-height:100%!important;padding:0!important;width:auto!important}.report-header{color:#2a3c68;font:700 18pt Lato,Oxygen,"Open Sans","Lucida Grande",Tahoma,Verdana,sans-serif;margin:0 0 9pt}.A5CWLayout{float:left;width:auto!important}.A5CWLayout:after{display:table;clear:left;content:""}@media only screen{body{padding:20px}}@media print{body,html{-webkit-print-color-adjust:exact;color-adjust:exact;height:98.5%;margin:0!important;overflow:hidden;padding:0!important}table{-webkit-box-decoration-break:clone;box-decoration-break:clone;max-height:100%;max-width:100%;overflow:hidden;-webkit-column-break-after:avoid;page-break-after:avoid;-webkit-break-after:avoid;break-after:avoid}td,tr{page-break-inside:avoid}}',
        printWin = window.open(
          '', winTitle,
          'toolbar=no,scrollbars=no,titlebar=no,status=no,location=no,menubar=no,resizable=yes,width=' +
          width + ',height=' + height + '', 'true'
        );
  printWin.document.open('text/html');
  printWin.document.writeln(
    '<!DOCTYPE html><html lang="en-US"><head><meta charset="utf-8"><title>' + winTitle + '</title>' +
    '<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">' +
    '<style>' + STYLE + '</style></head><body><div class="divided-wrapper">' +
    wrapper.innerHTML + '</div></body></html>'
  );
  printWin.document.close();
  printWin.print();
}
