/**
 * Provides functionality to the print button on the Top States Report
 *
 * @param {object} wrapper - Pointer to HTML object being passed in.
 * @param {string} winTitle - The desired title of the new printing window.
 * @param {number} width - The desired width of the printing window.
**/
function prntStates(wrapper, winTitle, width) {
  const STYLE = '@font-face{font-family:Roboto;font-style:normal;font-weight:400;font-display:swap;src:local("Roboto"),local("Roboto-Regular"),url(css/iOS7Local/fonts/Roboto%20Regular%20V2.woff2) format("woff2"),url(css/iOS7Local/fonts/Roboto%20Regular%20V2.woff) format("woff"),url(css/iOS7Local/fonts/Roboto%20Regular%20V2.ttf) format("truetype");unicode-range:U+000-5FF}@font-face{font-family:Roboto;font-style:normal;font-weight:700;font-display:swap;src:local("Roboto Bold"),local("Roboto-Bold"),url(css/iOS7Local/fonts/Roboto%20Bold%20V2.woff2) format("woff2"),url(css/iOS7Local/fonts/Roboto%20Bold%20V2.woff) format("woff"),url(css/iOS7Local/fonts/Roboto%20Bold%20V2.ttf) format("truetype");unicode-range:U+000-5FF}@font-face{font-family:Lato;font-style:normal;font-weight:700;font-display:swap;src:local("Lato Bold"),local("Lato-Bold"),url(css/iOS7Local/fonts/Lato%20Bold.woff2) format("woff2"),url(css/iOS7Local/fonts/Lato%20Bold.woff) format("woff"),url(css/iOS7Local/fonts/Lato%20Bold.ttf) format("truetype");unicode-range:U+000-5FF}html{box-sizing:border-box;font-family:sans-serif;font-size:12pt;word-wrap:break-word}*{box-sizing:inherit;margin:0;min-width:0;padding:0;text-shadow:none}body{color:#000;font:9pt/1.3 Roboto,"Source Sans Pro",-apple-system,Ubuntu,"Segoe UI",BlinkMacSystemFont,"Helvetica Neue",Arial,sans-serif;height:100%;overflow-x:hidden;text-rendering:optimizeLegibility;width:100%}.iOS7LocalGridFooter,.iOS7LocalGridSeparator,.img-wrapper,.record-count,.report-loading-text,.search-heading,button,img,svg{display:none!important}a,a:active,a:hover,a:visited{color:inherit;cursor:default;text-decoration:none!important}.iOS7LocalGrid{border-collapse:collapse;border:1.5pt solid #aaa!important;max-height:100%;max-width:100%;overflow-y:visible;text-align:right;width:100%}.iOS7LocalGrid tbody tr:nth-child(odd){background:#f8f9f9}td,th{border-color:#aaa;border-style:solid}.iOS7LocalGridHeader{background:#a11d27!important;border-width:1.5pt 0 2.25pt!important;color:#fff;font-size:9.75pt;max-width:90pt;overflow:hidden;padding:18pt 3pt!important;text-overflow:ellipsis;word-wrap:break-word}td{border-width:.75pt 0!important;padding:4.5pt 0!important}td:first-child,th:first-child{border-left-width:1.5pt!important;padding-left:4.5pt!important;padding-right:0!important;text-align:left!important}td:last-child,th:last-child{border-right-width:1.5pt!important;padding-left:0!important;padding-right:4.5pt!important}.table-wrapper{border:0;max-height:100%;max-width:100%;overflow:visible}.report-header{color:#222;font:700 18pt Lato,Oxygen,"Open Sans",Montserrat,"PT Sans",Verdana,-apple-system,"Segoe UI",Ubuntu,BlinkMacSystemFont,sans-serif;margin:0 0 9pt}@media only screen{body{padding:20px}}@media print{body,html{-webkit-print-color-adjust:exact;color-adjust:exact;height:auto!important;margin:0!important;overflow-x:hidden;overflow-y:visible;padding:0!important}table{-webkit-box-decoration-break:clone;box-decoration-break:clone;max-height:100%;max-width:100%;-webkit-column-break-after:avoid;page-break-after:avoid;-webkit-break-after:avoid;break-after:avoid;page-break-inside:auto}tbody,td,thead,tr{page-break-inside:avoid}thead{display:table-header-group}tbody:last-of-type{-webkit-column-break-after:avoid;page-break-after:avoid;-webkit-break-after:avoid;break-after:avoid}@page{margin:1.7cm .85cm}@page :first{margin:1cm .85cm}}',
        printWin = window.open(
          '', winTitle,
          'toolbar=no,scrollbars=no,titlebar=no,status=no,location=no,menubar=no,resizable=yes,width=' +
          width + ',height=' + (screen.height - 100) + '', 'true'
        );
  printWin.document.open('text/html');
  printWin.document.writeln(
    '<!DOCTYPE html><html lang="en-US"><head><meta charset="utf-8"><title>' + winTitle + '</title>' +
    '<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">' +
    '<style>' + STYLE + '</style></head><body>' + wrapper.innerHTML + '</body></html>'
  );
  printWin.document.close();
  printWin.print();
}
