require([
  'dojo/number',
  'esri/InfoTemplate',
  'esri/layers/FeatureLayer',
  'esri/map',
  'esri/tasks/query',
  'esri/renderers/HeatmapRenderer',
  'dojo/domReady!'
], function (number, InfoTemplate, FeatureLayer, Map, Query, HeatmapRenderer) {
  const doc = document,
        get = doc.getElementById,
        map = new Map('map', {
          basemap: 'hybrid',
          zoom: 18,
          center: [-84.36607414, 33.77219444],
          minZoom: 1,
          maxZoom: 23
        });
  // --------------------------------------------------------------------
  // Format the magnitude value in the pop up to show one decimal place.
  // Uses the dojo/number module to perform formatting.
  // --------------------------------------------------------------------
  formatMagnitude = function (value, key, data) {
    return number.format(value, { places: 1, locale: 'en-US' });
  };

  const serviceURL = 'https://arcgis4.roktech.net/arcgis/rest/services/alexanderbabbage/TruTrade_Overview31/MapServer/5',
        heatmapFeatureLayerOptions = {
          mode: FeatureLayer.MODE_SNAPSHOT,
          outFields: ['*']
        },
        urlCenters = getUrlVars()['centers'],
        heatmapFeatureLayer = new FeatureLayer(serviceURL, heatmapFeatureLayerOptions),
        query = 'CenterID = ' + urlCenters;

  heatmapFeatureLayer.setDefinitionExpression(query);

  heatmapFeatureLayer.queryFeatures(query, function (result) {
    // var extent = esri.graphicsExtent(result.features);
    // map.setExtent(extent)
    map.centerAndZoom(result.features[0].geometry, 16);

    setTimeout(function () {
      extentInitial = map.extent.expand(1.5);
    }, 3e3);
  });

  function getUrlVars() {
    const vars = {};
    window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
      vars[key] = value;
    });
    return vars;
  }

  const blurCtrl = get('blurControl'),
        maxCtrl = get('maxControl'),
        minCtrl = get('minControl'),
        valCtrl = get('valueControl'),
        heatmapRenderer = new HeatmapRenderer({
          field: 'NNeighbors',
          colors: ['rgba(0,0,255,0)', 'rgb(0,0,255)', 'rgb(255,237,125)', 'rgb(255,0,0)'],
          blurRadius: blurCtrl.value,
          maxPixelIntensity: maxCtrl.value,
          minPixelIntensity: minCtrl.value
        });

  heatmapFeatureLayer.setRenderer(heatmapRenderer);
  map.addLayer(heatmapFeatureLayer);

  // Add event handlers for interactivity

  const sliders = doc.querySelectorAll('.blurInfo p~input[type=range]'),
        slidersLen = sliders.length,
        addLiveValue = function (ctrl) {
          const val = ctrl.previousElementSibling.querySelector('span');
          ctrl.addEventListener('input', function (evt) {
            val.innerHTML = evt.target.value;
          });
        };

  for (let i = 0; i < slidersLen; ++i) addLiveValue(sliders.item(i));

  blurCtrl.addEventListener('change', function (evt) {
    const r = +evt.target.value;
    if (r !== heatmapRenderer.blurRadius) {
      heatmapRenderer.blurRadius = r;
      heatmapFeatureLayer.redraw();
    }
  });
  maxCtrl.addEventListener('change', function (evt) {
    const r = +evt.target.value;
    if (r !== heatmapRenderer.maxPixelIntensity) {
      heatmapRenderer.maxPixelIntensity = r;
      heatmapFeatureLayer.redraw();
    }
  });
  minCtrl.addEventListener('change', function (evt) {
    const r = +evt.target.value;
    if (r !== heatmapRenderer.minPixelIntensity) {
      heatmapRenderer.minPixelIntensity = r;
      heatmapFeatureLayer.redraw();
    }
  });

  // --------------------------------------------------------------------
  // When check / uncheck the control for the HeatmapRenderer field,
  // we will leave the blurRadius and the minPixelIntensity values the
  // same. However we will adjust the maxPixelIntensity value so it
  // spreads the colors across the range of magnitude values. For your
  // own dataset, you will need to experiment to find what looks good
  // based upon the level of geography when you display the heatmap
  // and the values in your dataset.
  // --------------------------------------------------------------------

  valCtrl.addEventListener('change', function (evt) {
    const chk = evt.target.checked;
    if (!chk) {
      get('maxValue').innerHTML = 21;
      maxCtrl.value = 21;
      heatmapRenderer.maxPixelIntensity = 21;
    } else {
      get('maxValue').innerHTML = 250;
      maxCtrl.value = 250;
      heatmapRenderer.maxPixelIntensity = 250;
    }
    heatmapRenderer.field = (chk) ? 'NNeighbors' : null;
    heatmapFeatureLayer.redraw();
  });
});
