function dateCheckRefresh(){
  var s=new Date({dialog.object}.getValue('FromDate')),
      e=new Date({dialog.object}.getValue('ToDate'));

((s.getTime()<=e.getTime())
? {dialog.object}.runAction('getCharts')
: swal({
  title: 'Error Encountered',
  text: 'The date range you have chosen for the charts is incorrect. You must choose an end date that is after the chosen start date. Please check the dropdowns labeled Start Date and End Date and try again.',
  icon: 'error',
  button: 'OK'
  })
);
}
