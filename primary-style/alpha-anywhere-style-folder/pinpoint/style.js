A5.themes.add('iOS7Local',{
	general: {
		pageClassName: 'iOS7LocalPage',
		headingClassName: 'iOS7LocalHeading',
		text: {
			className: 'iOS7LocalText',
			highlightClassName: 'iOS7LocalTextHighlight'
		},
		linkClassName: 'iOS7LocalLink',
		group: {
			className: 'iOS7LocalGroup',
			labelClassName: 'iOS7LocalGroupLabel'
		},
		icons: {
			expand: 'cssIcon=iOS7LocalIcon iOS7LocalIconRight',
			collapse: 'cssIcon=iOS7LocalIcon iOS7LocalIconDown',
			help: 'cssIcon=iOS7LocalIcon iOS7LocalIconHelp',
			info: 'cssIcon=iOS7LocalIcon iOS7LocalIconInfo',
			warning: 'cssIcon=iOS7LocalIcon iOS7LocalIconWarning',
			error: 'cssIcon=iOS7LocalIcon iOS7LocalIconError',
			menu: 'cssIcon=iOS7LocalIcon iOS7LocalIconMenu',
			refresh: 'cssIcon=iOS7LocalIcon iOS7LocalIconRefresh',
			refreshDisabled: 'cssIcon=iOS7LocalIcon iOS7LocalIconDisabled iOS7LocalIconRefresh',
			panel: {
				collapseRight: 'cssIcon=iOS7LocalIcon iOS7LocalIconLast',
				expandRight: 'cssIcon=iOS7LocalIcon iOS7LocalIconFirst',
				collapseLeft: 'cssIcon=iOS7LocalIcon iOS7LocalIconFirst',
				expandLeft: 'cssIcon=iOS7LocalIcon iOS7LocalIconLast'
			},
			list: {
				navigation: 'cssIcon=iOS7LocalIcon iOS7LocalIconRight',
				navigationSubtle: 'cssIcon=iOS7LocalIcon iOS7LocalIconRight'
			},
			edit: {
				dropdown: 'cssIcon=iOS7LocalIcon iOS7LocalIconDown',
				date: 'cssIcon=iOS7LocalIcon iOS7LocalIconDown',
				dateTime: 'cssIcon=iOS7LocalIcon iOS7LocalIconDown',
				time: 'cssIcon=iOS7LocalIcon iOS7LocalIconDown',
				lookup: 'cssIcon=iOS7LocalIcon iOS7LocalIconDown',
				calculator: 'cssIcon=iOS7LocalIcon iOS7LocalIconDown'
			}
		},
		iconSet: {
			type: ''
		}
	},
	grid: {
		base: {
			grid: {
				className: 'iOS7LocalGrid',
				headerClassName: 'iOS7LocalGridHeader',
				footerClassName: 'iOS7LocalGridFooter',
				summaryLabelClassName: 'iOS7LocalGridSummaryLabel',
				summaryDataClassName: 'iOS7LocalGridSummaryData',
				qbeClassName: 'iOS7LocalGridQBE',
				separatorClassName: 'iOS7LocalGridSeparator',
				rowHeaderClassName: 'iOS7LocalGridRowHeader',
				rowHoverClassName: 'iOS7LocalGridRowHover',
				rowSelectedClassName: 'iOS7LocalGridRowSelected',
				dataHeaderClassName: 'iOS7LocalGridDataHeader',
				dataClassName: 'iOS7LocalGridData',
				dataAltClassName: 'iOS7LocalGridDataAlt',
				dataErrorClassName: 'iOS7LocalGridDataError',
				navClassName: 'iOS7LocalGridNav'
			},
			detailView: {
				className: 'iOS7LocalGridForm',
				labelClassName: 'iOS7LocalGridFormLabel',
				dataClassName: 'iOS7LocalGridFormData',
				dataErrorClassName: 'iOS7LocalGridFormDataError'
			},
			icons: {
				sort: {
					asc: 'cssIcon=iOS7LocalIconSm iOS7LocalIconUp',
					ascDisabled: 'cssIcon=iOS7LocalIconSm iOS7LocalIconDisabled iOS7LocalIconUp',
					desc: 'cssIcon=iOS7LocalIconSm iOS7LocalIconDown',
					descDisabled: 'cssIcon=iOS7LocalIconSm iOS7LocalIconDisabled iOS7LocalIconDown'
				},
				page: {
					first: 'cssIcon=iOS7LocalIcon iOS7LocalIconFirst',
					firstDisabled: 'cssIcon=iOS7LocalIcon iOS7LocalIconDisabled iOS7LocalIconFirst',
					prev: 'cssIcon=iOS7LocalIcon iOS7LocalIconLeft',
					prevDisabled: 'cssIcon=iOS7LocalIcon iOS7LocalIconDisabled iOS7LocalIconLeft',
					next: 'cssIcon=iOS7LocalIcon iOS7LocalIconRight',
					nextDisabled: 'cssIcon=iOS7LocalIcon iOS7LocalIconDisabled iOS7LocalIconRight',
					last: 'cssIcon=iOS7LocalIcon iOS7LocalIconLast',
					lastDisabled: 'cssIcon=iOS7LocalIcon iOS7LocalIconDisabled iOS7LocalIconLast'
				},
				row: {
					selected: 'cssIcon=iOS7LocalIcon iOS7LocalIconRight',
					newRow: 'cssIcon=iOS7LocalIcon iOS7LocalIconDisabled iOS7LocalIconNew',
					newRowSelected: 'cssIcon=iOS7LocalIcon iOS7LocalIconNew',
					expand: 'cssIcon=iOS7LocalIcon iOS7LocalIconRight',
					collapse: 'cssIcon=iOS7LocalIcon iOS7LocalIconDown',
					error: 'cssIcon=iOS7LocalIcon iOS7LocalIconError',
					edit: 'cssIcon=iOS7LocalIcon iOS7LocalIconEdit',
					editDisabled: 'cssIcon=iOS7LocalIcon iOS7LocalIconDisabled iOS7LocalIconEdit',
					save: 'cssIcon=iOS7LocalIcon iOS7LocalIconSave',
					saveDisabled: 'cssIcon=iOS7LocalIcon iOS7LocalIconDisabled iOS7LocalIconSave',
					undoEdits: 'cssIcon=iOS7LocalIcon iOS7LocalIconUndo',
					undoEditsDisabled: 'cssIcon=iOS7LocalIcon iOS7LocalIconDisabled iOS7LocalIconUndo',
					cancelEdits: 'cssIcon=iOS7LocalIcon iOS7LocalIconCancelEdit',
					cancelEditsDisabled: 'cssIcon=iOS7LocalIcon iOS7LocalIconDisabled iOS7LocalIconCancelEdit',
					deleteRow: 'cssIcon=iOS7LocalIcon iOS7LocalIconDelete',
					deleteRowDisabled: 'cssIcon=iOS7LocalIcon iOS7LocalIconDisabled iOS7LocalIconDelete'
				},
				qbe: {
					menu: 'cssIcon=iOS7LocalIcon iOS7LocalIconFilter',
					toggle: 'cssIcon=iOS7LocalIcon iOS7LocalIconFilter',
					selected: 'cssIcon=iOS7LocalIconSm iOS7LocalIconCheck'
				},
				search: {
					search: 'cssIcon=iOS7LocalIcon iOS7LocalIconSearch',
					clear: 'cssIcon=iOS7LocalIcon iOS7LocalIconDisabled iOS7LocalIconFilter',
					toggle: 'cssIcon=iOS7LocalIcon iOS7LocalIconFilter',
					save: 'cssIcon=iOS7LocalIcon iOS7LocalIconSave',
					load: 'cssIcon=iOS7LocalIcon iOS7LocalIconLoad'
				},
				record: {
					close: 'cssIcon=iOS7LocalIcon iOS7LocalIconClose',
					first: 'cssIcon=iOS7LocalIcon iOS7LocalIconFirst',
					firstDisabled: 'cssIcon=iOS7LocalIcon iOS7LocalIconDisabled iOS7LocalIconFirst',
					prev: 'cssIcon=iOS7LocalIcon iOS7LocalIconLeft',
					prevDisabled: 'cssIcon=iOS7LocalIcon iOS7LocalIconDisabled iOS7LocalIconLeft',
					next: 'cssIcon=iOS7LocalIcon iOS7LocalIconRight',
					nextDisabled: 'cssIcon=iOS7LocalIcon iOS7LocalIconDisabled iOS7LocalIconRight',
					last: 'cssIcon=iOS7LocalIcon iOS7LocalIconLast',
					lastDisabled: 'cssIcon=iOS7LocalIcon iOS7LocalIconDisabled iOS7LocalIconLast',
					newRecord: 'cssIcon=iOS7LocalIcon iOS7LocalIconNew',
					save: 'cssIcon=iOS7LocalIcon iOS7LocalIconSave',
					saveDisabled: 'cssIcon=iOS7LocalIcon iOS7LocalIconDisabled iOS7LocalIconSave',
					saveAndEnter: 'cssIcon=iOS7LocalIcon iOS7LocalIconSaveAndEnter',
					saveAndEnterDisabled: 'cssIcon=iOS7LocalIcon iOS7LocalIconDisabled iOS7LocalIconSaveAndEnter',
					deleteRecord: 'cssIcon=iOS7LocalIcon iOS7LocalIconDelete',
					deleteRecordDisabled: 'cssIcon=iOS7LocalIcon iOS7LocalIconDisabled iOS7LocalIconDelete',
					switchToView: 'cssIcon=iOS7LocalIcon iOS7LocalIconCancel',
					switchToEdit: 'cssIcon=iOS7LocalIcon iOS7LocalIconEdit',
					cancelEdits: 'cssIcon=iOS7LocalIcon iOS7LocalIconCancelEdit',
					cancelEditsDisabled: 'cssIcon=iOS7LocalIcon iOS7LocalIconDisabled iOS7LocalIconCancelEdit',
					newCancelEdits: 'cssIcon=iOS7LocalIcon iOS7LocalIconCancelEdit'
				}
			}
		}
	},
	ux: {
		base: {
			labelClassName: 'iOS7LocalDialogLabel',
			repeatingSection: {
				headerClassName: 'iOS7LocalDialogRSHeader',
				footerClassName: 'iOS7LocalDialogRSFooter',
				rowClassName: 'iOS7LocalDialogRSRow',
				rowSelectedClassName: 'iOS7LocalDialogRSRowSelected',
				rowHoverClassName: 'iOS7LocalDialogRSRowHover',
				rowErrorClassName: 'iOS7LocalDialogRSRowError',
				rowSeparatorClassName: 'iOS7LocalDialogRSRowSeparator'
			},
			icons: {
				repeatingSection: {
					rowSelected: 'cssIcon=iOS7LocalIcon iOS7LocalIconRight',
					rowError: 'cssIcon=iOS7LocalIconBtn iOS7LocalIconError',
					rowAdd: 'cssIcon=iOS7LocalIconBtn iOS7LocalIconAdd',
					rowDelete: 'cssIcon=iOS7LocalIconBtn iOS7LocalIconDelete'
				}
			}
		}
	},
	panelCard: {
		base: {
			className: 'iOS7LocalPanelCard',
			header: {
				className: 'iOS7LocalPanelHeader'
			},
			body: {
				className: 'iOS7LocalPanelBody'
			},
			footer: {
				className: 'iOS7LocalPanelFooter'
			}
		},
		top: {
			className: 'iOS7LocalPanelCard',
			header: {
				className: 'iOS7LocalPanelHeader'
			},
			body: {
				className: 'iOS7LocalPanelBody'
			},
			footer: {
				className: 'iOS7LocalPanelFooter'
			}
		}
	},
	panelNavigator: {
		base: {
			className: '',
			header: {
				className: 'iOS7LocalPanelHeader'
			},
			body: {
				className: 'iOS7LocalPanelBody'
			},
			footer: {
				className: 'iOS7LocalPanelFooter'
			},
			indicator: {
				className: 'iOS7LocalPanelNavInd',
				panelClassName: 'iOS7LocalPanelNavIndButton',
				panelSelectedClassName: 'iOS7LocalPanelNavIndButtonSelected'
			}
		},
		top: {
			className: '',
			header: {
				className: 'iOS7LocalPanelHeader'
			},
			body: {
				className: 'iOS7LocalPanelBody'
			},
			footer: {
				className: 'iOS7LocalPanelFooter'
			},
			indicator: {
				className: 'iOS7LocalPanelNavInd',
				panelClassName: 'iOS7LocalPanelNavIndButton',
				panelSelectedClassName: 'iOS7LocalPanelNavIndButtonSelected'
			}
		}
	},
	panelLayout: {
		base: {
			className: '',
			header: {
				className: 'iOS7LocalPanelHeader'
			},
			body: {
				className: 'iOS7LocalPanelBody',
				ltrClassName: '',
				rtlClassName: '',
				ttbClassName: '',
				bttClassName: ''
			},
			footer: {
				className: 'iOS7LocalPanelFooter'
			},
			dock: {
				panel: {
					beforeClassName: '',
					afterClassName: ''
				},
				flowLock: {
					className: ''
				}
			}
		},
		top: {
			className: '',
			header: {
				className: 'iOS7LocalPanelHeader'
			},
			body: {
				className: 'iOS7LocalPanelBody',
				ltrClassName: '',
				rtlClassName: '',
				ttbClassName: '',
				bttClassName: ''
			},
			footer: {
				className: 'iOS7LocalPanelFooter'
			},
			dock: {
				panel: {
					beforeClassName: '',
					afterClassName: ''
				},
				flowLock: {
					className: ''
				}
			}
		}
	},
	window: {
		base: {
			className: 'iOS7LocalWin',
			activeClassName: '',
			adjustmentClassName: '',
			outerWrapClassName: 'iOS7LocalWinInner',
			innerWrapClassName: 'iOS7LocalWinBodyContainer',
			lockUIClassName: 'iOS7LocalUILock',
			title: {
				className: 'iOS7LocalWinTitle',
				tools: {
					inset: '3px',
					verticalInset: '3px',
					className: ''
				},
				location: '',
				direction: ''
			},
			header: {
				className: 'iOS7LocalWinHeader'
			},
			tbar: {
				className: 'iOS7LocalWinTBar'
			},
			body: {
				className: 'iOS7LocalWinBody'
			},
			buttons: {
				className: 'iOS7LocalWinButtons'
			},
			bbar: {
				className: 'iOS7LocalWinBBar'
			},
			footer: {
				className: 'iOS7LocalWinFooter'
			},
			resizer: {
				className: 'iOS7LocalWinResizer',
				thumbImage: 'cssIcon=iOS7LocalIcon iOS7LocalIconResizeThumb'
			},
			pointer: {
				size: 15,
				upClassName: 'iOS7LocalWinPointerUp',
				leftClassName: 'iOS7LocalWinPointerLeft',
				downClassName: 'iOS7LocalWinPointerDown',
				rightClassName: 'iOS7LocalWinPointerRight'
			},
			_buttonDefaults: {
				className: 'iOS7LocalButton',
				hoverClassName: 'iOS7LocalButtonHover',
				pressedClassName: 'iOS7LocalButtonPressed',
				disabledClassName: 'iOS7LocalButtonDisabled'
			},
			_defaultTools: {
				close: {
					action: 'close',
					name: 'close',
					image: 'cssIcon=iOS7LocalIcon iOS7LocalIconClose',
					imageHover: '',
					imagePressed: ''
				}
			}
		},
		dropdown: {
			className: 'iOS7LocalDDWin',
			activeClassName: '',
			adjustmentClassName: '',
			outerWrapClassName: 'iOS7LocalDDWinInner',
			innerWrapClassName: 'iOS7LocalDDWinBodyContainer',
			lockUIClassName: 'iOS7LocalUILock',
			title: {
				className: 'iOS7LocalDDWinTitle',
				tools: {
					inset: '3px',
					verticalInset: '3px',
					className: ''
				},
				location: '',
				direction: ''
			},
			header: {
				className: 'iOS7LocalDDWinHeader'
			},
			tbar: {
				className: 'iOS7LocalDDWinTBar'
			},
			body: {
				className: 'iOS7LocalDDWinBody'
			},
			buttons: {
				className: 'iOS7LocalDDWinButtons'
			},
			bbar: {
				className: 'iOS7LocalDDWinBBar'
			},
			footer: {
				className: 'iOS7LocalDDWinFooter'
			},
			resizer: {
				className: 'iOS7LocalDDWinResizer',
				thumbImage: 'cssIcon=iOS7LocalIcon iOS7LocalIconResizeThumb'
			},
			pointer: {
				size: 15,
				upClassName: 'iOS7LocalDDWinPointerUp',
				leftClassName: 'iOS7LocalDDWinPointerLeft',
				downClassName: 'iOS7LocalDDWinPointerDown',
				rightClassName: 'iOS7LocalDDWinPointerRight'
			},
			_buttonDefaults: {
				className: 'iOS7LocalButton',
				hoverClassName: 'iOS7LocalButtonHover',
				pressedClassName: 'iOS7LocalButtonPressed',
				disabledClassName: 'iOS7LocalButtonDisabled'
			},
			_defaultTools: {
				close: {
					action: 'close',
					name: 'close',
					image: 'cssIcon=iOS7LocalIcon iOS7LocalIconClose',
					imageHover: '',
					imagePressed: ''
				}
			}
		},
		panel: {
			className: 'iOS7LocalPanelCard',
			activeClassName: '',
			adjustmentClassName: '',
			outerWrapClassName: '',
			innerWrapClassName: '',
			lockUIClassName: 'iOS7LocalUILock',
			title: {
				className: 'iOS7LocalPanelHeader',
				location: '',
				direction: '',
				tools: {
					inset: '2px',
					verticalInset: '',
					className: ''
				}
			},
			header: {
				className: 'iOS7LocalPanelHeader'
			},
			tbar: {
				className: 'iOS7LocalPanelHeader'
			},
			body: {
				className: 'iOS7LocalPanelBody'
			},
			buttons: {
				className: 'iOS7LocalPanelFooter'
			},
			bbar: {
				className: 'iOS7LocalPanelFooter'
			},
			footer: {
				className: 'iOS7LocalPanelFooter'
			},
			resizer: {
				className: 'iOS7LocalDDWinResizer',
				thumbImage: 'cssIcon=iOS7LocalIcon iOS7LocalIconResizeThumb'
			},
			_buttonDefaults: {
				className: 'iOS7LocalButton',
				hoverClassName: 'iOS7LocalButtonHover',
				pressedClassName: 'iOS7LocalButtonPressed',
				disabledClassName: 'iOS7LocalButtonDisabled'
			},
			_defaultTools: {
				close: {
					action: 'close',
					name: 'close',
					image: 'cssIcon=iOS7LocalIcon iOS7LocalIconClose',
					imageHover: '',
					imagePressed: ''
				}
			},
			pointer: {
				size: 10,
				upClassName: '',
				leftClassName: '',
				downClassName: '',
				rightClassName: ''
			}
		}
	},
	menu: {
		base: {
			className: 'iOS7LocalMenu',
			innerClassName: 'iOS7LocalMenuInner',
			iconColumn: {
				className: '',
				preventIndentClassName: '',
				width: '30px'
			},
			cascadeOffsetX: 0,
			cascadeOffsetY: -2,
			item: {
				className: 'iOS7LocalMenuItem',
				labelClassName: '',
				hoverClassName: 'iOS7LocalMenuItemHover',
				selectedClassName: 'iOS7LocalMenuItemSelected',
				disabledClassName: 'iOS7LocalMenuItemDisabled',
				disabledHoverClassName: '',
				cascadeClassName: 'iOS7LocalMenuItemCascade',
				separatorClassName: 'iOS7LocalMenuSeparator',
				titleClassName: 'iOS7LocalMenuTitle',
				radioImage: 'cssIcon=iOS7LocalIconSm iOS7LocalIconCheck',
				checkImage: 'cssIcon=iOS7LocalIconSm iOS7LocalIconCheck',
				iconClassName: ''
			},
			pointer: {
				show: true,
				location: 'auto',
				size: 15,
				upClassName: 'iOS7LocalMenuPointerUp',
				leftClassName: 'iOS7LocalMenuPointerLeft',
				downClassName: 'iOS7LocalMenuPointerDown',
				rightClassName: 'iOS7LocalMenuPointerRight'
			}
		}
	},
	menubar: {
		base: {
			item: {
				className: 'iOS7LocalMenubarHItem',
				hoverClassName: 'iOS7LocalMenubarHItemHover',
				pressedClassName: 'iOS7LocalMenubarHItemPressed',
				disabledClassName: 'iOS7LocalMenubarHItemDisabled',
				cascadeClassName: 'iOS7LocalMenubarHItemCascade',
				separatorClassName: 'iOS7LocalMenubarHSeparator'
			},
			className: '',
			layout: 'horizontal'
		},
		vertical: {
			item: {
				className: 'iOS7LocalMenubarVItem',
				hoverClassName: 'iOS7LocalMenubarVItemHover',
				pressedClassName: 'iOS7LocalMenubarVItemPressed',
				disabledClassName: 'iOS7LocalMenubarVItemDisabled',
				cascadeClassName: 'iOS7LocalMenubarVItemCascade',
				separatorClassName: 'iOS7LocalMenubarVSeparator'
			},
			className: '',
			layout: 'horizontal'
		}
	},
	listbox: {
		base: {
			className: 'iOS7LocalList',
			focusClassName: '',
			item: {
				className: 'iOS7LocalListItem',
				hoverClassName: '',
				selectedClassName: 'iOS7LocalListItemSelected',
				titleClassName: 'iOS7LocalListTitle',
				separatorClassName: 'iOS7LocalListSeparator',
				parts: {
					mainClassName: 'iOS7LocalListItemLabelMain',
					subClassName: 'iOS7LocalListItemLabelSub',
					contextClassName: 'iOS7LocalListItemLabelContext',
					detailClassName: 'iOS7LocalListItemLabelDetail',
					contentClassName: 'iOS7LocalListItemContent',
					icons: {
						navigate: 'cssIcon=iOS7LocalIcon iOS7LocalIconRight',
						navigateSubtle: 'cssIcon=iOS7LocalIcon iOS7LocalIconRight'
					}
				}
			},
			view: {
				navigation: {
					prev: {
						className: 'iOS7LocalListNavPrev',
						pressedClassName: 'iOS7LocalListNavPressed'
					},
					next: {
						className: 'iOS7LocalListNavNext',
						pressedClassName: 'iOS7LocalListNavPressed'
					}
				}
			},
			group: {
				navigator: {
					className: 'iOS7LocalListGroupNav',
					focusClassName: 'iOS7LocalListGroupNavFocus',
					location: 'right',
					offset: 4,
					size: 28
				}
			},
			columnLayout: {
				header: {
					className: 'iOS7LocalListHeader',
					item: {
						className: 'iOS7LocalListHeaderItem',
						hoverClassName: '',
						order: {
							ascendingImage: 'cssIcon=iOS7LocalIconSm iOS7LocalIconUp',
							descendingImage: 'cssIcon=iOS7LocalIconSm iOS7LocalIconDown',
							style: 'position: absolute; right: 12px; top: 50%; margin-top: -9px; display: none;',
							className: ''
						},
						resize: {
							className: 'iOS7LocalListHeaderItemResizeOverlay',
							handle: {
								size: '40px',
								className: ''
							},
							location: 'after'
						}
					}
				},
				data: {
					item: {
						className: ''
					}
				}
			},
			disabledClassName: '',
			header: {
				className: ''
			},
			footer: {
				className: ''
			},
			lock: {
				className: ''
			},
			content: {
				header: {
					className: ''
				},
				footer: {
					className: ''
				}
			}
		}
	},
	spinList: {
		base: {
			className: 'iOS7LocalSpinList',
			item: {
				className: 'iOS7LocalSpinListItem'
			},
			disabledClassName: '',
			groupClassName: 'iOS7LocalSpinListGroup'
		}
	},
	tree: {
		base: {
			className: 'iOS7LocalTree',
			focusClassName: '',
			node: {
				className: 'iOS7LocalTreeNode',
				hoverClassName: '',
				selectedClassName: 'iOS7LocalTreeNodeSelected',
				labelClassName: 'iOS7LocalTreeNodeLabel',
				leaf: {
					firstImage: '',
					image: 'cssIcon=iOS7LocalIconBtn',
					lastImage: ''
				},
				branch: {
					firstExpandedImage: '',
					firstCollapsedImage: '',
					firstExpandedHoverImage: '',
					firstCollapsedHoverImage: '',
					expandedImage: 'cssIcon=iOS7LocalIconBtn iOS7LocalIconDown',
					collapsedImage: 'cssIcon=iOS7LocalIconBtn iOS7LocalIconRight',
					expandedHoverImage: '',
					collapsedHoverImage: '',
					lastExpandedImage: '',
					lastCollapsedImage: '',
					lastExpandedHoverImage: '',
					lastCollapsedHoverImage: ''
				}
			},
			branch: {
				className: '',
				lineImage: '',
				nullImage: 'cssIcon=iOS7LocalIconBtn'
			},
			disabledClassName: ''
		}
	},
	datePicker: {
		base: {
			className: 'iOS7LocalDP',
			calendar: {
				daysOfWeek: {
					className: 'iOS7LocalDPDaysOfWeek'
				},
				weeksOfYear: {
					className: 'iOS7LocalDPWeeksOfYear',
					headerClassName: 'iOS7LocalDPWeeksOfYearHeader'
				},
				date: {
					className: 'iOS7LocalDPDate',
					weekendClassName: 'iOS7LocalDPDate',
					todayClassName: 'iOS7LocalDPDateToday',
					item: {
						className: 'iOS7LocalDPItem',
						hoverClassName: 'iOS7LocalDPItemHover',
						selectedClassName: 'iOS7LocalDPItemSelected',
						disabledClassName: 'iOS7LocalDPItemDisabled',
						outOfRangeClassName: 'iOS7LocalDPItemOutOfRange'
					}
				}
			},
			navigator: {
				header: {
					className: 'iOS7LocalDPHeader',
					prevIcon: 'cssIcon=iOS7LocalIconBtn iOS7LocalIconLeft',
					nextIcon: 'cssIcon=iOS7LocalIconBtn iOS7LocalIconRight',
					prevIconHover: '',
					nextIconHover: '',
					item: {
						className: 'iOS7LocalDPHeaderButton',
						hoverClassName: 'iOS7LocalDPHeaderButtonHover',
						selectedClassName: 'iOS7LocalDPHeaderButtonSelected'
					},
					todayButton: {
						location: 'right',
						html: 'Today',
						tip: 'Select today\'s date'
					}
				},
				panel: {
					className: 'iOS7LocalDPPanel',
					edit: {
						className: 'iOS7LocalEdit'
					},
					item: {
						className: 'iOS7LocalDPPanelItem',
						hoverClassName: 'iOS7LocalDPPanelItemHover',
						selectedClassName: 'iOS7LocalDPPanelItemSelected',
						disabledClassName: 'iOS7LocalDPPanelItemDisabled'
					}
				}
			},
			disabledClassName: ''
		}
	},
	timePicker: {
		base: {
			className: 'iOS7LocalTP',
			edit: {
				className: 'iOS7LocalEdit',
				buttonClassName: 'iOS7LocalTPEditButton',
				buttonImage: 'cssIcon=iOS7LocalIcon iOS7LocalIconDown'
			},
			meridianClassName: 'iOS7LocalTPMeridianButton',
			disabledClassName: ''
		}
	},
	edit: {
		base: {
			className: 'iOS7LocalEdit',
			errorClassName: 'iOS7LocalEditError',
			watermark: {
				className: ''
			}
		}
	},
	editButtonGroup: {
		base: {
			watermark: {
				className: 'iOS7LocalEditBGWatermark',
				style: ''
			},
			container: {
				className: 'iOS7LocalEditBG',
				hoverClassName: 'iOS7LocalEditBGHover',
				focusClassName: 'iOS7LocalEditBGFocus',
				separatorClassName: 'iOS7LocalEditBGSeparator',
				editClassName: 'iOS7LocalEditBGEdit',
				button: {
					html: '__replace_1',
					className: 'iOS7LocalEditBGButton',
					hoverClassName: 'iOS7LocalEditBGButtonHover',
					pressedClassName: 'iOS7LocalEditBGButtonPressed'
				},
				errorClassName: ''
			},
			window: {
				pointer: {
					show: true,
					location: 'auto'
				}
			}
		}
	},
	button: {
		base: {
			className: 'iOS7LocalButton',
			pressedClassName: 'iOS7LocalButtonPressed',
			disabledClassName: 'iOS7LocalButtonDisabled',
			hoverClassName: ''
		},
		confirm: {
			className: 'iOS7LocalButtonConfirm',
			pressedClassName: 'iOS7LocalButtonConfirmPressed',
			disabledClassName: 'iOS7LocalButtonConfirmDisabled',
			hoverClassName: ''
		},
		deny: {
			className: 'iOS7LocalButtonDeny',
			pressedClassName: 'iOS7LocalButtonDenyPressed',
			disabledClassName: 'iOS7LocalButtonDenyDisabled',
			hoverClassName: ''
		},
		left: {
			className: 'iOS7LocalButtonLeft',
			pressedClassName: 'iOS7LocalButtonLeftPressed',
			disabledClassName: 'iOS7LocalButtonDisabled',
			hoverClassName: ''
		},
		right: {
			className: 'iOS7LocalButtonRight',
			pressedClassName: 'iOS7LocalButtonRightPressed',
			disabledClassName: 'iOS7LocalButtonDisabled',
			hoverClassName: ''
		},
		buttonTT: {
			className: 'buttonTT',
			pressedClassName: 'buttonTTPressed',
			disabledClassName: 'buttonTTDisabled',
			hoverClassName: 'buttonTTHover'
		},
		buttonTT2: {
			className: 'buttonTT2',
			pressedClassName: 'buttonTTPressed2',
			disabledClassName: 'buttonTTDisabled',
			hoverClassName: 'buttonTTHover2'
		}
	},
	buttonDropdown: {
		base: {
			className: 'iOS7LocalButtonDD',
			hoverClassName: '',
			pressedClassName: 'iOS7LocalButtonDDPressed',
			disabledClassName: 'iOS7LocalButtonDDDisabled',
			contentClassName: 'iOS7LocalButtonDDContent',
			dropdown: {
				className: 'iOS7LocalButtonDDDropdown',
				icon: '',
				location: 'right',
				html: ''
			}
		}
	},
	buttonSplit: {
		base: {
			className: 'iOS7LocalButtonDD',
			hoverClassName: '',
			pressedClassName: 'iOS7LocalButtonDDPressed',
			disabledClassName: 'iOS7LocalButtonDDDisabled',
			contentClassName: 'iOS7LocalButtonDDSplitContent',
			dropdown: {
				className: 'iOS7LocalButtonDDSplitDropdown',
				hoverClassName: '',
				icon: '',
				location: 'right',
				html: ''
			}
		}
	},
	buttonList: {
		base: {
			button: {
				className: 'iOS7LocalButton',
				firstClassName: 'iOS7LocalButtonGroupHF',
				lastClassName: 'iOS7LocalButtonGroupHL',
				middleClassName: 'iOS7LocalButtonGroupHM',
				hoverClassName: '',
				pressedClassName: 'iOS7LocalButtonPressed',
				disabledClassName: 'iOS7LocalButtonDisabled'
			},
			className: ''
		},
		vertical: {
			button: {
				className: 'iOS7LocalButton',
				firstClassName: 'iOS7LocalButtonGroupVF',
				lastClassName: 'iOS7LocalButtonGroupVL',
				middleClassName: 'iOS7LocalButtonGroupVM',
				hoverClassName: '',
				pressedClassName: 'iOS7LocalButtonPressed',
				disabledClassName: 'iOS7LocalButtonDisabled'
			},
			className: ''
		},
		chartButtonList: {
			button: {
				className: 'iOS7LocalButton',
				firstClassName: 'iOS7LocalButtonGroupHF',
				lastClassName: 'iOS7LocalButtonGroupHL',
				middleClassName: 'iOS7LocalButtonGroupHM',
				hoverClassName: '',
				pressedClassName: 'iOS7LocalButtonPressed',
				disabledClassName: 'iOS7LocalButtonDisabled'
			},
			className: ''
		},
		localButtonListCopy: {
			button: {
				className: 'iOS7LocalButton',
				firstClassName: 'iOS7LocalButtonGroupHF',
				lastClassName: 'iOS7LocalButtonGroupHL',
				middleClassName: 'iOS7LocalButtonGroupHM',
				hoverClassName: '',
				pressedClassName: 'iOS7LocalButtonPressed',
				disabledClassName: 'iOS7LocalButtonDisabled'
			},
			className: ''
		}
	},
	accordion: {
		base: {
			className: '',
			titleClassName: 'iOS7LocalAccordionButton',
			titleSelectedClassName: 'iOS7LocalAccordionButtonSelected',
			titleDisabledClassName: 'iOS7LocalAccordionButtonDisabled',
			paneClassName: 'iOS7LocalAccordionPane'
		}
	},
	tab: {
		base: {
			className: '',
			tabClassName: 'iOS7LocalTabTButton',
			tabSelectedClassName: 'iOS7LocalTabTButtonSelected',
			tabDisabledClassName: 'iOS7LocalTabTButtonDisabled',
			paneClassName: 'iOS7LocalTabTPane',
			location: 'top',
			tabsClassName: '',
			panesClassName: ''
		},
		bottom: {
			className: '',
			tabClassName: 'iOS7LocalTabBButton',
			tabSelectedClassName: 'iOS7LocalTabBButtonSelected',
			tabDisabledClassName: 'iOS7LocalTabBButtonDisabled',
			paneClassName: 'iOS7LocalTabBPane',
			location: 'top',
			tabsClassName: '',
			panesClassName: ''
		},
		right: {
			className: '',
			tabClassName: 'iOS7LocalTabRButton',
			tabSelectedClassName: 'iOS7LocalTabRButtonSelected',
			tabDisabledClassName: 'iOS7LocalTabRButtonDisabled',
			paneClassName: 'iOS7LocalTabRPane',
			location: 'top',
			tabsClassName: '',
			panesClassName: ''
		},
		left: {
			className: '',
			tabClassName: 'iOS7LocalTabLButton',
			tabSelectedClassName: 'iOS7LocalTabLButtonSelected',
			tabDisabledClassName: 'iOS7LocalTabLButtonDisabled',
			paneClassName: 'iOS7LocalTabLPane',
			location: 'top',
			tabsClassName: '',
			panesClassName: ''
		}
	},
	tabBand: {
		base: {
			location: 'top',
			className: 'iOS7LocalTabband',
			separator: {
				className: 'iOS7LocalTabbandSeparator',
				show: false
			},
			tab: {
				className: 'iOS7LocalTabbandButton',
				selectedClassName: 'iOS7LocalTabbandButtonSelected',
				disabledClassName: 'iOS7LocalTabbandButtonDisabled',
				closeClassName: 'iOS7LocalTabbandButtonClose',
				closeIcon: 'cssIcon=iOS7LocalIcon iOS7LocalIconClose iOS7LocalTabbandClose',
				closeIconHover: '',
				closeIconStyle: 'position: absolute; top: 0px; right: -6px;',
				closeHoverIcon: ''
			},
			scrollButtons: {
				overlay: true,
				fullHeight: true,
				prevClassName: 'iOS7LocalTabbandTLScroll',
				prevDisabledClassName: 'iOS7LocalTabbandTLScrollDisabled',
				prevImage: 'cssIcon=',
				prevDisabledImage: 'cssIcon=',
				nextClassName: 'iOS7LocalTabbandTRScroll',
				nextDisabledClassName: 'iOS7LocalTabbandTRScrollDisabled',
				nextImage: 'cssIcon=',
				nextDisabledImage: 'cssIcon='
			}
		},
		icon: {
			location: 'bottom',
			className: 'iOS7LocalTabbandIcon',
			separator: {
				className: 'iOS7LocalTabbandSeparator',
				show: false
			},
			tab: {
				className: 'iOS7LocalTabbandButton',
				selectedClassName: 'iOS7LocalTabbandButtonSelected',
				disabledClassName: 'iOS7LocalTabbandButtonDisabled',
				closeClassName: 'iOS7LocalTabbandButtonClose',
				closeIcon: 'cssIcon=iOS7LocalIcon iOS7LocalIconClose iOS7LocalTabbandClose',
				closeIconHover: '',
				closeIconStyle: 'position: absolute; top: 0px; right: -6px;',
				closeHoverIcon: ''
			},
			scrollButtons: {
				overlay: true,
				fullHeight: true,
				prevClassName: 'iOS7LocalTabbandTLScroll',
				prevDisabledClassName: 'iOS7LocalTabbandTLScrollDisabled',
				prevImage: 'cssIcon=',
				prevDisabledImage: 'cssIcon=',
				nextClassName: 'iOS7LocalTabbandTRScroll',
				nextDisabledClassName: 'iOS7LocalTabbandTRScrollDisabled',
				nextImage: 'cssIcon=',
				nextDisabledImage: 'cssIcon='
			}
		}
	},
	slider: {
		base: {
			className: 'iOS7LocalSlider',
			innerClassName: 'iOS7LocalSliderInner',
			handle: {
				className: 'iOS7LocalSliderHandle',
				selectedClassName: 'iOS7LocalSliderHandleSelected',
				hoverClassName: '',
				minClassName: '',
				maxClassName: ''
			},
			range: {
				className: 'iOS7LocalSliderRange',
				selectedClassName: 'iOS7LocalSliderRangeSelected',
				hoverClassName: ''
			},
			disabledClassName: ''
		}
	},
	switch: {
		base: {
			className: 'iOS7LocalSwitch',
			onClassName: 'iOS7LocalSwitchOn',
			offClassName: 'iOS7LocalSwitchOff',
			innerClassName: 'iOS7LocalSwitchInner',
			rightClassName: 'iOS7LocalSwitchRight',
			leftClassName: 'iOS7LocalSwitchLeft',
			buttonClassName: 'iOS7LocalSwitchButton',
			flow: 'ltr',
			disabledClassName: ''
		}
	},
	scroller: {
		base: {
			className: 'iOS7LocalScroller',
			focusClassName: 'iOS7LocalScrollerFocus',
			dialClassName: 'iOS7LocalScrollerDialExpanded',
			dial: {
				className: 'iOS7LocalScrollerDial',
				invertDrag: false,
				message: {
					location: 'center',
					className: '',
					mainClassName: ''
				}
			},
			slider: {
				className: 'iOS7LocalScrollerSlider',
				handle: {
					className: 'iOS7LocalScrollerSliderHandle'
				},
				shadow: {
					className: 'iOS7LocalScrollerSliderShadow'
				},
				message: {
					className: 'iOS7LocalScrollerSliderMsg',
					bottomClassName: 'iOS7LocalScrollerSliderMsgB',
					topClassName: 'iOS7LocalScrollerSliderMsgT',
					rightClassName: 'iOS7LocalScrollerSliderMsgR',
					leftClassName: 'iOS7LocalScrollerSliderMsgL'
				},
				innerClassName: ''
			},
			flow: 'ttb',
			location: 'after',
			offset: {
				top: '0px',
				bottom: '0px',
				left: '0px',
				right: '0px'
			},
			leftClassName: '',
			rightClassName: '',
			topClassName: '',
			bottomClassName: ''
		}
	},
	scroll: {
		base: {
			indicator: {
				h: {
					className: 'iOS7LocalScrollIndH',
					barClassName: 'iOS7LocalScrollIndHBar',
					location: 'bottom',
					offset: '0px',
					left: '0px',
					right: '0px'
				},
				v: {
					className: 'iOS7LocalScrollIndV',
					barClassName: 'iOS7LocalScrollIndVBar',
					location: 'right',
					offset: '0px',
					top: '0px',
					bottom: '0px'
				}
			}
		}
	},
	ink: {
		base: {
			view: {
				className: 'iOS7LocalInkArea',
				zoomBox: {
					className: 'iOS7LocalInkZoomBox',
					navigateClassName: 'iOS7LocalInkZoomBox',
					scale: {
						className: 'iOS7LocalInkZoomBoxScale'
					}
				},
				page: {
					className: 'iOS7LocalInkPage',
					offset: 1
				}
			},
			editor: {
				className: 'iOS7LocalInkArea',
				page: {
					className: 'iOS7LocalInkPage',
					offset: 1
				}
			},
			split: {
				bar: {
					size: '1px',
					className: 'iOS7LocalInkSplitBar',
					verticalClassName: '',
					horizontalClassName: '',
					show: true
				}
			},
			tools: {
				pen: {
					activeClassName: '',
					preview: {
						size: 40,
						width: {
							show: 'never'
						},
						clip: {
							shape: 'circle'
						}
					}
				},
				eraser: {
					activeClassName: '',
					areaClassName: 'iOS7LocalInkEraser'
				},
				pan: {
					activeClassName: ''
				}
			},
			ui: {
				popup: {
					lockClassName: 'iOS7LocalInkPopupLock',
					className: 'iOS7LocalInkPopup',
					group: {
						className: 'iOS7LocalInkPopupGroup'
					},
					swatch: {
						className: 'iOS7LocalInkPopupSwatch',
						selectedClassName: 'iOS7LocalInkPopupSwatchSelected'
					},
					button: {
						className: 'iOS7LocalInkPopupButton'
					}
				},
				button: {
					className: 'iOS7LocalInkButton',
					selectedClassName: 'iOS7LocalInkButtonSelected'
				},
				bar: {
					topClassName: 'iOS7LocalInkBarTop',
					bottomClassName: 'iOS7LocalInkBarBottom',
					leftClassName: 'iOS7LocalInkBarLeft',
					rightClassName: 'iOS7LocalInkBarRight'
				},
				group: {
					topClassName: 'iOS7LocalInkGroupTop',
					bottomClassName: 'iOS7LocalInkGroupBottom',
					leftClassName: 'iOS7LocalInkGroupLeft',
					rightClassName: 'iOS7LocalInkGroupRight'
				},
				statusClassName: 'iOS7LocalInkStatus',
				icons: {
					left: 'cssIcon=iOS7LocalIcon iOS7LocalIconLeft',
					right: 'cssIcon=iOS7LocalIcon iOS7LocalIconRight',
					newLineLTR: '<svg width="40" height="40" style="fill: none; stroke-width: 2px;"><path d="m 16.582034,25.646454 -3.75,-3.75 3.75,-3.75 m 7,-4.5 9e-5,6.25 c 0,0 0,2.25 -2.25,2.25 l -8,0" /></svg>',
					newLineRTL: '<svg width="40" height="40" style="fill: none; stroke-width: 2px;"><path d="m 23.417966,25.646454 3.75,-3.75 -3.75,-3.75 m -7,-4.5 -9e-5,6.25 c 0,0 0,2.25 2.25,2.25 l 8,0" /></svg>',
					eraser: '<svg width="40" height="40" style="stroke-width: 2px;"><path d="m 12,16 8,8 -3,2.5 -6.5,0 -4.5,-4.5 z" style="fill: none;" /><path d="m 20,8 8,8 -8,8 -8,-8 z" /><rect x="12" y="26" height="2" width="20" style="stroke: none;"/></svg>',
					undo: 'cssIcon=iOS7LocalIcon iOS7LocalIconUndo',
					redo: 'cssIcon=iOS7LocalIcon iOS7LocalIconRedo',
					expand: '<svg width="40" height="40" style="fill: none; stroke-width: 2px;"><path d="m 10.99995,29.00005 7.5,-7.5 m -1.014243,7.9999 -6.985657,0 0,-6.985658 m 18.5,-11.514342 -7.5,7.5 m 1.014243,-7.9999 6.985657,0 0,6.985658" /></svg>',
					collapse: '<svg width="40" height="40" style="fill: none; stroke-width: 2px;"><path d="m 28.485707,18.49995 -6.985657,0 0,-6.985658 m 0.4999,6.485758 7.5,-7.5 m -17.985657,11 6.985657,0 0,6.985658 m -0.4999,-6.485758 -7.5,7.5" /></svg>',
					in: '',
					out: '',
					fit: '',
					pan: ''
				}
			},
			className: 'iOS7LocalInk'
		}
	},
	controlBar: {
		base: {
			topClassName: '',
			bottomClassName: '',
			leftClassName: '',
			rightClassName: '',
			separator: {
				horizontalClassName: 'iOS7LocalCtrlBarSepH',
				verticalClassName: 'iOS7LocalCtrlBarSepV'
			},
			disclosure: {
				animation: {
					duration: 300
				},
				coverClassName: '',
				cover: {
					topClassName: '',
					bottomClassName: '',
					leftClassName: '',
					rightClassName: ''
				},
				extend: {
					topClassName: '',
					bottomClassName: '',
					leftClassName: '',
					rightClassName: ''
				},
				expand: {
					topClassName: '',
					bottomClassName: '',
					leftClassName: '',
					rightClassName: ''
				},
				defaults: {
					className: 'iOS7LocalCtrlBarDis'
				}
			}
		}
	},
	tabbedUI: {
	}
});
