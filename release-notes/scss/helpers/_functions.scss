/// Asset URL builder.
/// @access private
///
/// @param {String} $type - Asset type, matching folder name.
/// @param {String} $file - Asset file name, including extension
///
/// @return {String} A file location leading to the asset.
/// @require {variable} $asset-base-path
@function asset($type, $file) {
  $asset-path: if(
    global-variable-exists(asset-base-path),
    $asset-base-path,
    '../..'
  );

  @return unquote($asset-path + '/' + $type + '/' + $file);
}

/// Image asset helper.
/// @access public
///
/// @param {String} $file - Asset file name, including extension.
/// @return {String} A file location leading to the asset.
///
/// @require {function} asset
@function image($file) {
  @return asset('img', $file);
}

/// Checks whether `$functions` exist in global scope.
/// @access public
///
/// @param {ArgList} $functions - A list of functions to check for.
/// @return {Boolean} Whether or not there are missing dependencies.
@function missing-dependencies($functions...) {
  $missing-dependencies: ();

  @each $function in $functions {
    @if not function-exists($function) {
      $missing-dependencies: append($missing-dependencies, $function, comma);
    }
  }

  @if length($missing-dependencies) > 0 {
    @error 'You have unmet dependencies! The following functions are ' +
        'required and have not been imported: #{inspect($missing-dependencies)}.';
  }

  @return length($missing-dependencies) > 0;
}

/// Strips a numeric value of its unit and returns a unitless number
@function strip-unit($num) {
  @return $num / ($num * 0 + 1);
}

/// Returns the truthiness of `$value`.
/// @access public
///
/// @param {*} $value - A value to check.
/// @return {Boolean} Returns true if `value` has truthiness.
@function is-true($value) {
  @return if(
    $value == null,
    false,
    $value and $value != '' and $value != () and value != (())
  );
}

/// Checks if a value is an absolute length.
/// @access public
///
/// @param {*} $value - A value to be checked.
/// @return {Boolean} True if $value is an absolute length, false if it is not.
@function is-absolute-length($value) {
  @return (
    type-of($value) == 'number' and
    index('cm' 'mm' 'in' 'px' 'pt' 'pc', unit($value)) != null
  );
}

/// Checks if a value is an angle.
/// @access public
///
/// @param {*} $value - A value to be checked.
/// @return {Boolean} True if $value is an angle, false if it is not.
@function is-angle($value) {
  @return (
    type-of($value) == 'number' and
    index('deg' 'rad' 'grad' 'turn', unit($value)) != null
  );
}

/// Checks if a value is a duration.
/// @access public
///
/// @param {*} $value - A value to be checked.
/// @return {Boolean} True if $value is a duration, false if it is not.
///
/// @alias is-time
@function is-duration($value) {
  @return type-of($value) == 'number' and index('ms' 's', unit($value)) != null;
}

/// Checks if a value is a floating point (decimal) number.
/// @access public
///
/// @param {*} $value - A value to be checked.
/// @return {Boolean} True if $value is a float, false if it is not.
@function is-float($value) {
  @return type-of($value) == 'number' and round($value) != $value;
}

/// Checks if a value is a frequency.
/// @access public
///
/// @param {*} $value - A value to be checked.
/// @return {Boolean} True if $value is a frequency, false if it is not.
@function is-frequency($value) {
  @return (
    type-of($value) == 'number' and index('Hz' 'kHz', unit($value)) != null
  );
}

/// Checks if a value is an integer.
/// @access public
///
/// @param {*} $value - A value to be checked.
/// @return {Boolean} True if $value is an integer, false if it is not.
@function is-integer($value) {
  @return type-of($value) == 'number' and round($value) == $value;
}

/// Checks if a value is a length.
/// @access public
///
/// @param {*} $value - A value to be checked.
/// @return {Boolean} True if $value is a length, false if it is not.
///
/// @require {function} is-relative-length
/// @require {function} is-absolute-length
@function is-length($value) {
  @return is-relative-length($value) or is-absolute-length($value);
}

/// Checks if a value is a number.
/// @access public
///
/// @param {*} $value - A value to be checked.
/// @return {Boolean} True if $value is a number, false if it is not.
@function is-number($value) {
  @return type-of($value) == 'number';
}

/// Checks if a value is a percentage.
/// @access public
///
/// @param {*} $value - A value to be checked.
/// @return {Boolean} True if $value is a percentage, false if it is not.
@function is-percentage($value) {
  @return type-of($value) == 'number' and unit($value) == '%';
}

/// Checks if a value is a position.
/// @access public
///
/// @param {*} $value - A value to be checked.
/// @return {Boolean} True if $value is a position, false if it is not.
///
/// @require {function} is-length
/// @require {function} is-percentage
@function is-position($value) {
  @return (
    is-length($value) or is-percentage($value) or
    index('top' 'right' 'bottom' 'left' 'center', $value) != null
  );
}

/// Checks if a value is a relative length.
/// @access public
///
/// @param {*} $value - A value to be checked.
/// @return {Boolean} True if $value is a relative length, false if it is not.
@function is-relative-length($value) {
  @return (
    type-of($value) == 'number' and
    index(
      'em' 'ex' 'ch' 'rem' '%' 'vw' 'vh' 'vmin' 'vmax',
      unit($value)
    ) != null
  );
}

/// Checks if a value is a resolution.
/// @access public
///
/// @param {*} $value - A value to be checked.
/// @return {Boolean} True if $value is a resolution, false if it is not.
@function is-resolution($value) {
  @return (
    type-of($value) == 'number' and
    index('dpi' 'dpcm' 'dppx', unit($value)) != null
  );
}

/// Checks if a value is a time.
/// @access public
///
/// @param {*} $value - A value to be checked.
/// @return {Boolean} True if $value is a time, false if it is not.
///
/// @alias is-duration
@function is-time($value) {
  @return type-of($value) == 'number' and index('ms' 's', unit($value)) != null;
}

/// Converts values into REM units.
/// @access public
///
/// @param {Length-Percentage} $values - Values to convert to REM units.
/// @param {Number} $base [null] - The base font-size.
/// @return {Length} The values in REMs.
///
/// @require {function} strip-unit
@function conv-to-rem($values, $base: null) {
  $rem-values: ();
  $count: length($values);

  // If no base is defined, defer to the $base-font-size variable. If that does
  // not exist, default to 16
  @if not $base {
    $base: if(global-variable-exists(base-font-size), $base-font-size, 16);
  }

  // If the base font size is a %, then multiply it by 16px
  // This is because 100% font size = 16px in most all browsers
  @if unit($base) == '%' {
    $base: ($base / 100%) * 16px;
  }

  // Using rem as base allows correct scaling
  @if unit($base) == 'rem' {
    $base: strip-unit($base) * 16px;
  }

  @if $count == 1 {
    @return px-to-rem($values, $base);
  }

  @for $i from 1 through $count {
    $rem-values: append($rem-values, px-to-rem(nth($values, $i), $base));
  }

  @return $rem-values;
}

/// Converts a pixel value to matching rem value. *Any* value passed, regardless
/// of unit, is assumed to be a pixel value. By default, the base pixel value
/// used to calculate the rem value is taken from the `$base-font-size` variable.
/// @access private
///
/// @param {Number} $value - Pixel value to convert.
/// @param {Number} $base [null] - Base for pixel conversion.
/// @return {Number} A number in rems, calculated based on the given value and
/// the base pixel value. rem values are passed through as is.
///
/// @require {function} strip-unit
@function px-to-rem($value, $base: null) {
  @if type-of($value) != 'number' {
    @error inspect($value) + ', which is not a number, was passed ' +
        'to conv-to-rem().';
  }

  // Transform em into rem if someone hands over 'em's
  @if unit($value) == 'em' {
    $value: strip-unit($value) * 1rem;
  }

  // Calculate rem if units for $value is not rem or em
  @if unit($value) != 'rem' {
    $value: strip-unit($value) / strip-unit($base) * 1rem;
  }

  // Eliminate units for 0rem
  @if $value == 0rem {
    $value: 0;
  }

  @return $value;
}

/// Converts a pixel value into em units.
/// @access public
///
/// @param {Length-Percentage} $pixels - A value to convert to em units.
/// @param {Number} $base-context [null] - The contextual base font-size.
/// @return {Length} The values in em.
///
/// @require {function} strip-unit
/// @throw Invalid data type or units for $pixels
@function conv-to-em($pixels, $base-context: null) {
  @if (
    type-of($pixels) != 'number' or
    (type-of($pixels) == 'number' and
    not unitless($pixels) and unit($pixels) != 'px')
  ) {
    @error 'Invalid value of (#{inspect($pixels)}) passed to conv-to-em(). ' +
        'Value of $pixels must be number in px units.';
  }

  // If no base is defined, defer to the $base-font-size variable. If that does
  // not exist, default to 16
  @if not $base-context {
    $base-context: if(
      global-variable-exists(base-font-size),
      $base-font-size,
      16
    );
  }

  @if unitless($pixels) {
    $pixels: $pixels * 1px;
  }

  @if unitless($base-context) {
    $base-context: $base-context * 1px;
  }

  @return $pixels / $base-context * 1em;
}

/// Converts a rem value into pixel units.
/// @access public
///
/// @param {Length} $rems - Values in REM's.
/// @param {Number} $base - The base font-size.
/// @return {Length} The values in pixels (px).
@function conv-to-px($rems, $base: null) {
  $px-vals: ();

  // If no base is defined, defer to the $base-font-size variable. If that does
  // not exist, default to 16
  @if not $base {
    $base: if($base-font-size, $base-font-size, 16);
  }

  // If the base font size is a %, then multiply it by 16px
  // This is because 100% font size = 16px in most all browsers
  @if unit($base) == '%' {
    $base: ($base / 100%) * 16px;
  }

  @each $val in $rems {
    $val-in-pixels: strip-unit($val) * $base-font-size * 1px;
    $px-vals: append($px-vals, $val-in-pixels);
  }

  @if length($px-vals) == 1 {
    @return nth($px-vals, 1);
  } @else {
    @return $px-vals;
  }
}

/// Takes a given number value and returns that value multipled by pi.
/// @access public
///
/// @param {Number} $number [1] - The given number. Can take any number,
/// unitless or with any unit.
/// @return {Number} - The new value.
///
/// @throw Invalid $number unit type.
@function pi($number: 1) {
  $pi: if(global-variable-exists('pi'), $pi, 3.1415926536);

  @if type-of($number) != 'number' {
    @error 'Value of $number for pi() must be a valid number.';
  }

  @return $number * $pi;
}

/// Takes a given $angle in either turns (turn), degrees (deg), gradians (grad),
/// or radian (rad) units, and converts that angle into an alternate unit type
/// defined by $conversion-unit,
///
/// @param {Angle} $angle - An angle value. Can be in turn, deg, grad, or rad
/// units.
/// @param {String} $conversion-unit
/// @return {Angle} The converted angle.
///
/// @require {function} is-angle
/// @require {function} strip-unit
/// @require {function} pi
/// @require {function} missing-dependencies
@function conv-angle($angle, $conv-unit) {
  $_: missing-dependencies('is-angle', 'strip-unit', 'pi');
  $start-unit: unit($angle);
  $divide-supported: if(function-exists('divide'), true, false);

  @if not is-angle($angle) {
    @error 'Value of $angle in conv-angle() must be a valid angle.';
  }

  @if $conv-unit == 'turn' or $conv-unit == 'turns' or $conv-unit == 't' {
    $conv-unit: turn;
  } @else if (
    $conv-unit == 'degrees' or $conv-unit == 'degree' or
    $conv-unit == 'deg' or $conv-unit == 'degs' or $conv-unit == 'd'
  ) {
    $conv-unit: deg;
  } @else if (
    $conv-unit == 'radians' or $conv-unit == 'radian' or $conv-unit == 'rads' or
    $conv-unit == 'rad' or $conv-unit == 'r'
  ) {
    $conv-unit: rad;
  } @else if (
    $conv-unit == 'gradians' or $conv-unit == 'gradian' or
    $conv-unit == 'grads' or $conv-unit == 'grad' or $conv-unit == 'g'
  ) {
    $conv-unit: grad;
  }

  @if unit($angle) == $conv-unit {
    @return $angle;
  }

  $angle: strip-unit($angle);

  @if $start-unit == 'turn' {
    @if $conv-unit == 'deg' {
      $angle: $angle * 360deg;
    } @else if $conv-unit == 'rad' {
      $angle: $angle * pi(2rad);
    } @else if $conv-unit == 'grad' {
      $angle: $angle * 400grad;
    }
  } @else if $start-unit == 'deg' {
    @if $conv-unit == 'turn' {
      $angle: if(
        $divide-supported,
        divide($angle, 360) * 1turn,
        ($angle / 360) * 1turn
      );
    } @else if $conv-unit == 'rad' {
      $angle: if(
        $divide-supported,
        $angle * divide(pi(), 180) * 1rad,
        $angle * (pi() / 180) * 1rad
      );
    } @else if $conv-unit == 'grad' {
      $angle: if(
        $divide-supported,
        $angle * divide(200, 180) * 1grad,
        $angle * (200 / 180) * 1grad
      );
    }
  } @else if $start-unit == 'rad' {
    @if $conv-unit == 'turn' {
      $angle: if(
        $divide-supported,
        divide($angle, pi(2)) * 1turn,
        ($angle / pi(2)) * 1turn
      );
    } @else if $conv-unit == 'deg' {
      $angle: if(
        $divide-supported,
        $angle * divide(180, pi()) * 1deg,
        $angle * (180 / pi()) * 1deg
      );
    } @else if $conv-unit == 'grad' {
      $angle: if(
        $divide-supported,
        $angle * divide(200, pi()) * 1grad,
        $angle * (200 / pi()) * 1grad
      );
    }
  } @else if $start-unit == 'grad' {
    @if $conv-unit == 'turn' {
      $angle: if(
        $divide-supported,
        divide($angle, 400) * 1turn,
        $angle / 400 * 1turn
      );
    } @else if $conv-unit == 'deg' {
      $angle: if(
        $divide-supported,
        $angle * divide(180, 200) * 1deg,
        $angle * (180 / 200) * 1deg
      );
    } @else if $conv-unit == 'rad' {
      $angle: if(
        $divide-supported,
        $angle * divide(pi(), 200) * 1rad,
        $angle * (pi() / 200) * 1rad
      );
    }
  }

  @return $angle;
}

/// Clamp `$number` between `$min` and `$max`
///
/// @param {Number} $number - The number to clamp.
/// @param {Number} $min - The minimum value.
/// @param {Number} $max - The maximum value.
/// @return {Number} If number is within the limits, returns the number, if it
/// is below the limit, it will return the min, if it is above the max, it will
/// return the max.
@function clamp($number, $min, $max) {
  @return min(max($number, $min), $max);
}

/// Takes the summation of a range of numbers on an itaratee. Used in the `pow`
/// function that takes a number to a given power.
///
/// @param {Number} $itaratee
/// @param {Number} $input
/// @param {Number} $initial [0]
/// @param {Number} $limit [100]
///
/// @return {Number} The result of the operation
@function summation($iteratee, $input, $initial: 0, $limit: 100) {
  $sum: 0;

  @for $index from $initial to $limit {
    $sum: $sum + call($iteratee, $input, $index);
  }

  @return $sum;
}

/// Complete an exponentiation. Used for the `pow` function that takes a number
/// to a given power.
///
/// @param {Number} $value
/// @return {Number} The result of the operation.
///
/// @require {function} summation
@function exp($value) {
  @return summation('exp-maclaurin', $value, 0, 100);
}

/// Takes a number to the given power.
///
/// @param {Number} $number - A number, the base of the operation
/// @param {Number} $exponent - The exponent taking the $number to its power
/// @return {Number} The result of the operation
///
/// @require {function} exp
/// @require {function} ln
@function pow($number, $exponent) {
  @if (round($exponent) != $exponent) {
    @return exp($exponent * ln($number));
  }

  $value: 1;

  @if ($exponent > 0) {
    @for $i from 1 through $exponent {
      $value: $value * $number;
    }
  } @else if ($exponent < 0) {
    @for $i from 1 through $exponent {
      $value: $value / $number;
    }
  }

  @return $value;
}

/// Fixes a number to a fixed number of decimal places.
///
/// @param {Number} $num - The number to format.
/// @param {Integer} $digits [2] - Number of digits to leave after the decimal.
/// @return {Number} The fixed digit number.
///
/// @require {function} pow
/// @throw $digits precision too high (if greater than 10).
@function to-fixed($num, $digits: 2) {
  $sass-precision: 10;

  @if ($digits > $sass-precision) {
    @warn 'Sass numbers only support up to #{inspect($sass-precision)} digits' +
        ' of precision after the decimal point. You have attempted to set the' +
        ' precision to `#{inspect($digits)}` in the `to-fixed` function. The ' +
        'number of digits after the decimal point was reduced from your ' +
        'requested precision to the maximum of 10.';
    $digits: 10;
  }

  $pow: pow(10, $digits);
  @return round($num * $pow) / $pow;
}

/// Given a key, safely return the corresponding value from a map.
///
/// @param {Map} $map - Map to retrieve a value from.
/// @param {String} $key - Name of the map key.
///
/// @return {List} - Found value.
/// @throw $key not available in $map
/// @throw $map is not a valid map
@function get-val($map, $key) {
  @if type-of($map) == 'map' or (type-of($map) == 'list' and length($map) == 0) {
    @if map-has-key($map, $key) {
      @return map-get($map, $key);
    } @else {
      @error 'Key: `#{inspect($key)}` is not available in `#{inspect($map)}`';
    }
  } @else {
    @error '`#{inspect($map)}` is not a valid map';
  }
}

/// Incrementally lighten a color in a more effective way than with darken()
///
/// @param {Color} $color - Color to tint.
/// @param {Number} $percentage - Percentage of white in the returned color
/// @return {Color} - The lightened color
@function tint($color, $percentage) {
  @return mix(#fff, $color, $percentage);
}

/// Incrementally darken a color in a more effective way than with darken()
/// @access public
/// @param {Color} $color - Color to shade
/// @param {Number} $percentage - Percentage of black in the returned color
/// @return {Color} - The darkened color
@function shade($color, $percentage) {
  @return mix(#000, $color, $percentage);
}

/// Helper function, mix black with a given opacity level.000
///
/// @param {Number} $opacity [1] - An opacity level between either 0 and 1
/// or 0% and 100%, inclusive.
/// @return {Color} The black color at the given opacity level.
///
/// @throw Invalid data type for $opacity
/// @throw Invalid unit type for $opacity
/// @throw $opacity value out of range
@function black($opacity: 1) {
  @if type-of($opacity) != 'number' {
    @error 'Invalid data type passed to black(). Opacity must be a number.';
  }

  @if not unitless($opacity) and unit($opacity) != '%' {
    @error 'Invalid opacity unit type of [#{inspect(unit($opacity))}] passed' +
        ' to black(). Opacity must be a unitless decimal or a percentage.';
  }

  @if (
    (unit($opacity) == '%' and ($opacity < 0% or $opacity > 100%)) or
    (unitless($opacity) and ($opacity < 0 or $opacity > 1))
  ) {
    @error 'Invalid opacity level of (#{inspect($opacity)}) passed to black().';
  }

  @if unit($opacity) == '%' {
    $opacity: $opacity / 100%;
  }

  @if unit($opacity) == '%' {
    $opacity: $opacity / 100%;
  }

  @if $opacity == 1 {
    @return #000;
  } @else if $opacity == 0 {
    @return transparent;
  } @else {
    @return rgba(#000, $opacity);
  }
}

/// Helper function, mix white with a given opacity level.
///
/// @param {Number} $opacity [1] - An opacity level between either 0 and 1
/// or 0% and 100%, inclusive.
/// @return {Color} The white color at the given opacity level.
///
/// @throw Invalid data type for $opacity.
/// @throw Invalid unit type for $opacity.
/// @throw $opacity value out of range.
@function white($opacity: 1) {
  @if type-of($opacity) != 'number' {
    @error 'Invalid data type passed to white(). Opacity must be a number.';
  }

  @if not unitless($opacity) and unit($opacity) != '%' {
    @error 'Invalid opacity unit type of [#{inspect(unit($opacity))}] passed' +
        ' to white(). Opacity must be a unitless decimal or a percentage.';
  }

  @if (
    (unit($opacity) == '%' and ($opacity < 0% or $opacity > 100%)) or
    (unitless($opacity) and ($opacity < 0 or $opacity > 1))
  ) {
    @error 'Invalid opacity level of (#{inspect($opacity)}) passed to white().';
  }

  @if unit($opacity) == '%' {
    $opacity: $opacity / 100%;
  }

  @if $opacity == 1 {
    @return #fff;
  } @else {
    @return rgba(#fff, $opacity);
  }
}

/// Computes a top-shadow for a card effect.
///
/// @param {Number} $depth - The depth level.
/// @return {List} Computed Values for the top-shadow
@function top-shadow($depth) {
  $primary-offset: nth(1.5 3 10 14 19, $depth) * 1px;
  $blur: nth(1.5 3 10 14 19, $depth) * 4px;
  $color: rgba(#000, nth(0.12 0.16 0.19 0.25 0.3, $depth));

  @return 0 $primary-offset $blur $color;
}

/// Computes a bottom-shadow for a card effect.
///
/// @param {Number} $depth - The depth level.
/// @return {List} Computed values for the bottom-shadow
@function bottom-shadow($depth) {
  $primary-offset: nth(1.5 3 6 10 15, $depth) * 1px;
  $blur: nth(1 3 3 5 6, $depth) * 4px;
  $color: rgba(#000, nth(0.24 0.23 0.23 0.22 0.22, $depth));

  @return 0 $primary-offset $blur $color;
}

/// Takes a character or a string representation for the name of a character
/// and returns the CSS entity code for that character.
///
/// @example char('&') and char('amp') both return '\0026'
/// @example char('trademark') and char('tm') both return '\2122'
///
/// @param {String} $char - The character (if it is available on a standard
/// keyboard), name of the character, or an abbreviated name for the character,
/// being converted into a CSS entity code.
/// @return {String} - The code for the desired character.
///
/// @throw Value of $char not a valid string.
/// @throw Value of $char does not correspond to a known CSS entity code.
@function entity($char) {
  @if type-of($char) != 'string' {
    @error 'Value of $char must be of type "string" in entity().';
  }

  $entity: '\0020'; // Default entity, the code for a blank space
  $char: to-lower-case($char);

  @if $char == '&' or str-slice($char, 1, 3) == 'amp' {
    $entity: '\0026'; // Ampersand: &
  } @else if str-slice($char, 1, 4) == 'copy' or $char == 'cr' {
    $entity: '\00A9'; // Copyright: ©
  } @else if str-slice($char, 1, 3) == 'reg' or $char == 'rtm' {
    $entity: '\00AE'; // Registered: ®
  } @else if str-slice($char, 1, 5) == 'trade' or $char == 'tm' {
    $entity: '\2122'; // Trademark: ™
  } @else if $char == '$' or $char == 'dollar' or $char == 'dlr' {
    $entity: '\0024'; // Dollar: $
  } @else if $char == 'cents' or $char == 'cent' {
    $entity: '\00A2'; // Cents: ¢
  } @else if str-slice($char, 1, 4) == 'euro' or $char == 'eu' {
    $entity: '\20AC'; // Euro: €
  } @else if str-slice($char, 1, 5) == 'pound' or $char == 'gbp' {
    $entity: '\00A3'; // British Pound: £
  } @else if $char == 'yen' {
    $entity: '\00A5'; // Japanese Yen ¥
  } @else if $char == 'rupee' {
    $entity: '\20B9'; // Indian Rupee ₹
  } @else if $char == 'peso' {
    $entity: '\20B1'; // Mexican Peso ₱
  } @else if $char == 'currency' or $char == 'curr' {
    $entity: '\00A4'; // Universal currency symbol ¤
  } @else if (
    $char == 'open-quote' or $char == 'openquote' or $char == 'oquote' or
    $char == 'o-quote' or $char == 'oq' or $char == 'o"' or $char == 'o\''
  ) {
    $entity: '\201C'; // Open double quote: “
  } @else if (
    $char == 'close-quote' or $char == 'closed-quote' or
    $char == 'closequote' or $char == 'closedquote' or $char == 'cquote' or
    $char == 'c-quote' or $char == 'cq' or $char == 'c"' or $char == 'c\''
  ) {
    $entity: '\201D'; // Close double quote: ”
  } @else if (
    $char == 'ldaq' or $char == 'left-double-angle-quote' or
    $char == 'left-double-angle' or $char == 'ldang'
  ) {
    $entity: '\00AB'; // Left-pointing double-angle quation mark: «
  } @else if (
    $char == 'rdaq' or $char == 'right-double-angle-quote' or
    $char == 'rdang' or $char == 'right-double-angle'
  ) {
    $entity: '\00BB'; // Right-pointing double-angle quation mark: »
  } @else if (
    str-slice($char, 1, 4) == 'apos' or $char == '\'' or $char == 'aps'
  ) {
    $entity: '\2019'; // Apostrophe: ’
  } @else if (
    $char == '-' or $char == 'ndash' or
    $char == 'endash' or $char == 'nd'
  ) {
    $entity: '\2013'; // En dash: –
  } @else if (
    $char == '--' or $char == 'mdash' or
    $char == 'emdash' or $char == 'md'
  ) {
    $entity: '\2014'; // Em dash: —
  } @else if (
    $char == '...' or str-slice($char, 1, 5) == 'ellip' or
    str-slice($char, 1, 4) == 'elip'
  ) {
    $entity: '\2026'; // Ellipsis: …
  } @else if (
    $char == '>' or $char == 'greater-than' or $char == 'greaterthan' or
    $char == 'greater-then' or $char == 'greaterthen'
  ) {
    $entity: '\003E'; // Greater than symbol: >
  } @else if (
    $char == '<' or $char == 'less-than' or $char == 'lessthan' or
    $char == 'less-then' or $char == 'less-then'
  ) {
    $entity: '\003C'; // Less than symbol: <
  } @else if (
    $char == '^!' or $char == 'inverted-exclamation' or
    $char == 'inverted!' or $char == 'invert!' or $char == 'inv!'
  ) {
    $entity: '\00A1'; // Inverted exclamation mark ¡
  } @else if (
    $char == 'account-of' or $char == 'accountof' or $char == 'accof' or
    $char == 'account' or $char == 'acc'
  ) {
    $entity: '\2100'; // Account Of: ℀
  } @else if str-slice($char, 1, 7) == 'address' {
    $entity: '\2101'; // Addressed to the subject of: ℁
  } @else if $char == 'degree' or $char == 'degrees' or $char == 'deg' {
    $entity: '\00B0'; // Degrees: °
  } @else if (
    $char == 'degrees-celcius' or $char == 'degree-celcius' or
    $char == 'degreescelcius' or $char == 'degreecelcius' or
    $char == 'celcius' or $char == 'degcel' or $char == 'deg-cel' or
    $char == 'degree-c' or $char == 'degrees-c' or $char == 'deg-c' or
    $char == 'degc'
  ) {
    $entity: '\2103'; // Degrees Celcius: ℃
  } @else if (
    $char == 'degrees-farenheit' or $char == 'degree-farenheit' or
    $char == 'degreesfarenheit' or $char == 'degreefarenheit' or
    $char == 'farenheit' or $char == 'faren' or $char == 'degfar' or
    $char == 'deg-far' or $char == 'deg-faren' or $char == 'deg-f' or
    $char == 'degree-f' or $char == 'degrees-f' or $char == 'degf'
  ) {
    $entity: '\2109'; // Degrees Celcius: ℉
  } @else if $char == 'infinity' or $char == 'inf' {
    $entity: '\221E'; // Infinity: ∞
  } @else if (
    $char == 'small-triangle--up' or $char == 'sm-tri--up' or
    $char == 'smalltriangleup' or  $char == 'small-triangle-up' or
    $char == 'smtriup' or $char == 'triangleupsmall' or
    $char == 'triangle-up-small' or $char == 'triupsm' or
    $char == 'uptrianglesmall' or $char == 'up-triangle-small' or
    $char == 'uptrism'
  ) {
    $entity: '\25B2'; // Small Triangle - Up: ▴
  } @else if (
    $char == 'small-triangle--down' or $char == 'sm-tri--down' or
    $char == 'smalltriangledown' or $char == 'small-triangle-down' or
    $char == 'smtridown' or $char == 'triangledownsmall' or
    $char == 'triangle-down-small' or $char == 'trismdown' or
    $char == 'downtrianglesmall' or $char == 'down-triangle-small' or
    $char == 'downtrism'
  ) {
    $entity: '\25BE'; // Small Triangle - Down: ▾
  } @else if (
    $char == 'small-triangle--left' or $char == 'sm-tri--left' or
    $char == 'smalltriangleleft' or $char == 'small-triangle-left' or
    $char == 'smtrileft' or $char == 'triangleleftsmall' or
    $char == 'triangle-left-small' or $char == 'trileftsm' or
    $char == 'lefttrianglesmall' or $char == 'left-triangle-small' or
    $char == 'lefttrism'
  ) {
    $entity: '\25C2'; // Small Triangle - Left: ◂
  } @else if (
    $char == 'small-triangle--right' or $char == 'sm-tri--right' or
    $char == 'smalltriangleup' or $char == 'small-triangle-up' or
    $char == 'smtriup' or $char == 'triangleupsmall' or
    $char == 'triangle-up-small' or $char == 'triupsm' or
    $char == 'uptrianglesmall' or $char == 'up-triangle-small' or
    $char == 'uptrism'
  ) {
    $entity: '\25B8'; // Small Triangle - Right: ▸
  } @else if $char == 'nbsp' or $char == 'nobreakspace' {
    $entity: '\00A0'; // Non-breaking space
  } @else if $char != ' ' {
    @error 'You must enter a valid character or string for $char in entity(),' +
        ' one that corresponds with a CSS entity character.';
  }

  @return $entity;
}
